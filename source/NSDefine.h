#ifndef  NSDEFINE_INC
#define  NSDEFINE_INC

//#define USE_ZLIB_CRC32					//使用zlib的crc32替代内部实现(要链接libz)
//#define ENABLE_THREAD_LOCAL_DATA		//允许使用线程本地数据
//#define ENABLE_INTERNAL_CHECK_PARAM	//允许开启内部函数调用参数检查(内部函数间的调用一般可以保证安全，不需要开启此特性)
//#define USE_SAFE_HANDLE				//使用NS内核安全句柄

#define ENABLE_PRINTK					//允许printk记录系统状态

#if defined(EX_TEST) && !defined(ENABLE_PRINTK)	//在打开测试的时候强制启用printk
	#define ENABLE_PRINTK
#endif

/* 定义默认Debug事件级别 */
#define	CORE_DBG_LEVEL			0
#define	SYS_PLUGIN_DBG_LEVEL	1
#define	EXT_PLUGIN_DBG_LEVEL	2
#define TEST_DBG_LEVEL			3
#define	LAST_DEFINE_DBG_LEVEL	3	//如果要定义自己的level，level要大于本值

#ifdef ENABLE_PRINTK
	//#define	USE_FILE_LOG				//记录状态到文件中(开启ENABLE_PRINTK时才有效)，否则记录到终端
	//#define	ERR_LOG_TO_FILE				//是否将错误记录到文件中
	#define PRINT_DATE_TIME				//记录事件时间
	#ifdef EX_TEST
		#define DBG_LEVEL_SOL	TEST_DBG_LEVEL+1
		#ifndef USE_FILE_LOG
			#define USE_FILE_LOG
		#endif
	#else
		#define DBG_LEVEL_SOL	0			//记录级别小于DBG_LEVEL的DEBUG事件(0表示不记录DEBUG事件)
	#endif
	#ifdef USE_FILE_LOG
		#define LOG_FILE_APPEND			//使用附加模式操作记录文件（开启USE_FILE_LOG时才有效）
		#ifndef ERR_LOG_TO_FILE
			#define ERR_LOG_TO_FILE		//强制错误记录到文件
		#endif
	#endif
#endif

//#define ENABLE_WRRM_LOCK				//允许使用WRRM(WriteRarelyReadMany)锁进行线程同步和用于替代引用计数

#endif   /* ----- #ifndef NSDEFINE_INC  ----- */
