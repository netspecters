#include "UIHost.h"
#include "../../NSErrorCode.h"

using namespace Netspecters::KernelPlus::UIHost;


CUIHost::CUIHost()
	: m_gui_instance_ptr( NULL ), m_is_looping( false )
{
}

CUIHost::~CUIHost()
{
}

int CUIHost::onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param )
{
	printk( KINFORMATION"UIHost loaded.\n" );
	return NSEC_SUCCESS;
}

void CUIHost::onUnload( void )
{
	m_is_looping = false;

	printk( KINFORMATION"UIHost unloaded.\n" );
}

IGUIInstance *CUIHost::registerGUI( IGUIInstance *gui_instance_ptr )
{
	IGUIInstance *tmp_store_instance_ptr = m_gui_instance_ptr;
	m_gui_instance_ptr = gui_instance_ptr;

	if( gui_instance_ptr != NULL )
	{
		std::lock_guard<std::mutex> locker( m_mutex );
		m_cond_wait.notify_one();
	}

	return tmp_store_instance_ptr;
}

int CUIHost::onLoop()
{
	m_is_looping = true;
	while( !m_gui_instance_ptr )
	{
		std::unique_lock<std::mutex> locker( m_mutex );
		m_cond_wait.wait( locker );
		if( !m_is_looping )
			return 0;
	}

	return m_gui_instance_ptr->onRun();
}

int CUIHost::getParam( const char *param_name, void *value_buffer_ptr )
{
	return 0;
}

void *CUIHost::setParam( const char *param_name, void *value_ptr )
{
	return NULL;
}

void CUIHost::kill( int exit_code )
{
	m_is_looping = false;
	if( m_gui_instance_ptr )
		m_gui_instance_ptr->kill( exit_code );
	else
		m_cond_wait.notify_one();
}
