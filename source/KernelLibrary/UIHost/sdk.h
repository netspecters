#ifndef UIHOST_SDK_H_INCLUDED
#define UIHOST_SDK_H_INCLUDED

#include "../../Core/PlusSDK.h"

namespace Netspecters{ namespace KernelPlus{ namespace UIHost{

template<typename TSubClass>
class AGUIInstance
{
	public:
		int a_onRun(void){
			return getUpperClassPtr()->onRun();
		}
		void a_kill( int exit_code )
		{
			getUpperClassPtr()->kill(exit_code);
		}
	private:
		TSubClass *getUpperClassPtr(void)
		{
			return static_cast<TSubClass*>(this);
		}
};

template<typename TSubClass>
class AUIHost : public ANSMain<AUIHost<TSubClass>>
{
	public:
		template<class TGUIInstanceClass>
		AGUIInstance<TGUIInstanceClass> *a_registerGUI( AGUIInstance<TGUIInstanceClass> *gui_instance_ptr ){
			return getUpperClassPtr()->registerGUI(gui_instance_ptr);
		}
		bool a_isLooping(void){
			return getUpperClassPtr()->isLooping();
		}
	private:
		TSubClass *getUpperClassPtr(void)
		{
			return static_cast<TSubClass*>(this);
		}
};

}/* UIHost **/}/* KernelPlus **/}/* Netspecters **/

#endif // UIHOST_SDK_H_INCLUDED
