/*
 * =====================================================================================
 *
 *       Filename:  UIHost.h
 *
 *    Description:  netspecters' UI Host
 *
 *        Version:  1.0
 *        Created:  2009-7-18 23:30:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (WG), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */

#ifndef  UIHOST_INC
#define  UIHOST_INC

#include <mutex>
#include <condition_variable>
#include "Interfaces.h"
#include "sdk.h"


namespace Netspecters { namespace KernelPlus { namespace UIHost {

///实现 IUIHost 接口
class CUIHost : public IUIHost, public AUIHost<CUIHost>
{
	public:
		virtual NSAPI int onLoad(IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param);
		virtual NSAPI void onUnload();
		virtual NSAPI int onLoop();
		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr);
		virtual NSAPI void *setParam(const char *param_name, void *value_ptr);

		virtual NSAPI IGUIInstance *registerGUI(IGUIInstance *gui_instance_ptr);
		virtual NSAPI bool isLooping() {
			return m_is_looping;
		}
		virtual NSAPI void kill(int exit_code);

		CUIHost();
		~CUIHost();
	private:
		IGUIInstance *m_gui_instance_ptr;
		bool m_is_looping;
		std::mutex m_mutex;
		std::condition_variable m_cond_wait;
};

}/* UIHost **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef UIHOST_INC  ----- */
