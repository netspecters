#ifndef  uihost_interfaces_INC
#define  uihost_interfaces_INC

#include "../../Core/PlusInterface.h"

namespace Netspecters{ namespace KernelPlus{ namespace UIHost{

/**
 * @brief GUI接口
 * @note 如果需要设计GUI，GUI类要实现本接口才能挂到GUI管理器上
*/
class IGUIInstance
{
	public:
		virtual NSAPI int onRun() = 0;
		virtual NSAPI void kill( int exit_code ) = 0;
};

///GUI管理器接口
class IUIHost : public INSMain
{
	public:
		virtual ~IUIHost(){}

		/**
		 * @brief 注册一个gui实例
		 * @param gui_instance_ptr gui对象指针
		 * @return 前一个注册的gui对象的指针
		*/
		virtual NSAPI IGUIInstance *registerGUI( IGUIInstance *gui_instance_ptr ) = 0;
		virtual NSAPI bool isLooping() = 0;
};

}/* UIHost **/}/* KernelPlus **/}/* Netspecters **/

#endif   /* ----- #ifndef uihost_interfaces_INC  ----- */
