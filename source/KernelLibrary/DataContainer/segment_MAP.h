#ifndef SEGMENT_MAP_H_INCLUDED
#define SEGMENT_MAP_H_INCLUDED

#include "segment.h"

namespace Netspecters { namespace KernelPlus { namespace DataContainer {

class CSegment_MAP : public CSegment
{
	public:
		CSegment_MAP( CDataContainer &parent, const ISegment::ST_SegmentInfor &segment_create_infor );
		virtual ~CSegment_MAP();

		virtual NSAPI int read( void *data_ptr, size_t data_size, off_t offset, long io_tag );
		virtual NSAPI int write( const void *data_ptr, size_t data_size, off_t offset, long io_tag ) {
			return NSEC_NSERROR;
		}
		virtual NSAPI void flushData( void ){}

		virtual NSAPI off_t getIOPosition() {
			return 0;
		}
		virtual NSAPI void getSegmentInformation( ST_SegmentInfor &segment_infor );
		virtual NSAPI bool setDataWindowSize( size_t data_window_size ) {
			return true;
		}

	private:
		void *m_segment_mapped_ptr;	//区段映射内存的首址
};

}/* DataContainer **/ }/* KernelPlus**/ }/* Netspecters **/

#endif // SEGMENT_MAP_H_INCLUDED
