#ifndef  DATACONTAINER_INC
#define  DATACONTAINER_INC

#include <set>
#include <mutex>
#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#	define VIRTUAL_ON_TEST virtual
#else
#	define VIRTUAL_ON_TEST
#endif
#include "Interfaces.h"

#include "nslib.h"
#include "file.h"
#include "segment.h"

namespace Netspecters { namespace KernelPlus { namespace DataContainer {

/**
 * @addtogroup DataContainer Data Container(数据容器)
 * @{
*/

using namespace Netspecters::NSStdLib;

/**
 * @brief 实现 IDataContainer 接口
*/
class CDataContainer : public IDataContainer, public CReferenceInterface
{
	public:
		/**
		 * @name 实现 IDataContainer 接口
		 * @{
		*/
		virtual NSAPI const char *getDataContainerName();
		virtual NSAPI unsigned dumpSegmentRanges( void *buffer );

		virtual NSAPI ISegment *addSegment( ENM_OffsetType offset_type, ISegment::ST_SegmentInfor &segment_create_infor );		virtual NSAPI unsigned getSegmentNum(){			return m_segment_ptrs.size();		}
		virtual NSAPI bool tryMergeSegment( ISegment *op_segment_ptr, ENM_MergeStyle merge_style );
		virtual NSAPI off_t moveSegment( ISegment *op_segment_ptr, off_t new_base_position );
		virtual NSAPI void deleteSegment( ISegment *op_segment_ptr );

		virtual NSAPI off_t getDataSize();
		virtual NSAPI off_t resetDataSize( off_t new_data_size );
		virtual NSAPI void writeBack();

		//对于文件，必须调用close函数，否则可能会丢失数据
		virtual NSAPI void close( bool sync_mode, bool force_close_file );
		///@}

		CDataContainer( const char *path_string_ptr, off_t &data_size )
			: m_data_file( &CDataContainer::onFileIOCallback )
		{
			m_data_file.open( path_string_ptr, data_size );
			m_data_file.set_user_data_ptr( this );
		}

#ifdef EX_TEST
	protected:
		CDataContainer()
			: m_data_file( NULL )
		{}
#endif

	private:
		off_t get_container_border(){
			return m_data_file.get_file_size() - 1;
		}
		static void onFileIOCallback( CFile *file_ptr, const CFile::ST_ListIOItem &list_io_item ) {
			/* CDataContainer 中的 onFileIOCallback 只是将参数转移给 CSegment::onFileIOCallback */
			CSegment::onFileIOCallback( file_ptr, list_io_item );
		}
		struct sort_segment_less
		{
			bool operator()(CSegment * const&a, CSegment * const&b){
				return a->m_base_offset < b->m_base_offset;
			}
		};

		friend class				CSegment;
		CFile						m_data_file;
		std::set<CSegment*,sort_segment_less>	m_segment_ptrs;
		std::mutex m_mutex;
#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( DataContainer, DataContainer_CreateAndDestroy );
		FRIEND_TEST( DataContainer, DataContainer_AddAndDeleteSegment_Base );
		FRIEND_TEST( DataContainer, DataContainer_AddSegment_MiddleOfLastOne_MiddleOfBiggest );
	protected:
#endif
	/* 不能直接调用析构函数，使用 releaseReference() 替代 */
	~CDataContainer() {}	//只有在测试模式下才可以继承
};

///@}

}/* DataContainer **/ }/*KernelPlus**/ }/* Netspecters **/

#endif   /* ----- #ifndef DATACONTAINER_INC  ----- */
