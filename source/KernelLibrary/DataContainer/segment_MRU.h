#ifndef  segment_MRU_INC
#define  segment_MRU_INC

#include "Interfaces.h"
#include "segment.h"

namespace Netspecters{ namespace KernelPlus{ namespace DataContainer{

class CSegment_MRU : public CSegment
{
	public:
		CSegment_MRU( CDataContainer &parent, const ST_SegmentInfor &segment_create_infor );
};

}/* DataContainer **/}/*KernelPlus**/}/* Netspecters **/

#endif   /* ----- #ifndef segment_MRU_INC  ----- */
