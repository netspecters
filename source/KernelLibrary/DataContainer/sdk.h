#ifndef DATA_CONTAINER_SDK_H_INCLUDED
#define DATA_CONTAINER_SDK_H_INCLUDED

namespace Netspecters {
namespace KernelPlus {
namespace DataContainer {

template<typename TSubClass>
class ASegment
{
	public:
		int a_read(void *data_ptr, size_t data_size, off_t offset, long io_tag) {
			return getUpperClassPtr()->read(data_ptr, data_size, offset, io_tag);
		}
		int a_write(void *data_ptr, size_t data_size, off_t offset, long io_tag) {
			return getUpperClassPtr()->write(data_ptr, data_size, offset, io_tag);
		}
		void *a_getPage(bool base_offset, off_t offset) {
			return getUpperClassPtr()->getPage(base_offset, offset);
		}
		off_t a_getAbsolutePosition(void) {
			return getUpperClassPtr()->getAbsolutePosition();
		}
		off_t a_getSegmentPointerOffset(void) {
			return getUpperClassPtr()->getSegmentPointerOffset();
		}
		off_t a_getSegmentSize(void) {
			return getUpperClassPtr()->getSegmentSize();
		}
		void a_flushData(void) {
			getUpperClassPtr()->flushData();
		}
		void a_getSegmentInformation(ST_SegmentInfor &segment_infor) {
			getUpperClassPtr()->getSegmentInformation();
		}
		bool a_setDataWindowSize(size_t data_window_size) {
			return getUpperClassPtr()->setDataWindowSize();
		}
		off_t a_resetSegmentBaseOffset(off_t new_offset) {
			return getUpperClassPtr()->resetSegmentBaseOffset();
		}

	private:
		TSubClass *getUpperClassPtr(void)
		{
			return static_cast<TSubClass*>(this);
		}
};

}/* DataContainer **/

}/* KernelPlus **/

}/* Netspecters **/

#endif // DATA_CONTAINER_SDK_H_INCLUDED
