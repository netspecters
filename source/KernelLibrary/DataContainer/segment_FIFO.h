#ifndef  segment_fifo_INC
#define  segment_fifo_INC

#include <mutex>
#include "segment.h"
#include "nslib.h"

namespace Netspecters{ namespace KernelPlus{ namespace DataContainer{

using namespace Netspecters::NSStdLib;

class CSegment_FIFO : public CSegment
{
	public:
		CSegment_FIFO( CDataContainer &parent, const ISegment::ST_SegmentInfor &segment_create_infor );

		/**
		 * @name 实现 CSegment 没有实现的 ISegment 接口
		 * @{
		*/
		virtual NSAPI int read( void *data_ptr, size_t data_size, off_t offset, long io_tag );
		virtual NSAPI int write( const void *data_ptr, size_t data_size, off_t offset, long io_tag );
		virtual NSAPI void flushData( void );	/* 会将数据窗口内容写入磁盘，然后清空数据窗口 */

		virtual NSAPI off_t getIOPosition();
		virtual NSAPI void getSegmentInformation( ST_SegmentInfor &segment_infor );
		virtual NSAPI bool setDataWindowSize( size_t data_window_size );	//新数据窗口的尺寸必须大于原有数据窗口尺寸
		///@}


	private:
		struct ST_FIFOQueueItem
		{
			ST_FIFOQueueItem *next_item_ptr;
			const void *data_ptr;		//数据指针
			size_t data_size;			//数据块的大小一定要为os数据页的整数倍
			long io_tag;
		};

		/**
		 * @name 类內成员变量辅助函数
		 * @{
		*/
		///获取当前数据窗口中列队等待写入磁盘的项目数量
		size_t get_queue_item_count();
		///@}

		ST_FIFOQueueItem *m_queue_header_ptr, *m_queue_tail_ptr;	///< 列队头尾指针

		size_t m_data_window_base;			///< 数据窗口基址(初始化等于段基址偏移) @note 当数据项写入磁盘后会将此基址后移，但不会超过读写位置
		size_t m_data_window_io_position;	///< 数据窗口当前读写位置(相对于数据窗口基址的偏移) @note 读写位置以字节为单位
		size_t m_data_window_size;			///< 通用意义上的数据窗口大小(最大列队数据长度，也是数据窗口基址相对于数据窗口边界的偏移量)；数据窗口的边界=窗口基址+数据窗口大小 @note 以字节为单位
		std::mutex m_queue_mutex;

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( Segment, FIFO_CreateAndDestroy );
#endif
};

}/* DataContainer **/}/* KernelPlus**/}/* Netspecters **/

#endif   /* ----- #ifndef segment_fifo_INC  ----- */
