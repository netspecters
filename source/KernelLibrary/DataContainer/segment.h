#ifndef  segment_INC
#define  segment_INC

#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif
#include "Interfaces.h"
#include "file.h"
#include "../NetLayer/Interfaces.h"
#include "../NetLayer/socket.h"

namespace Netspecters { namespace KernelPlus { namespace DataContainer {

/**
 * @addtogroup DataContainer Data Container(数据容器)
 * @{
*/

using Netspecters::NSStdLib::CReferenceInterface;

/**
 * @brief 实现 ISegment 接口
 * @namespace Netspecters::KernelPlus::DataContainer
*/
class CDataContainer;
class CSegment : public ISegment, public CReferenceInterface
{
	public:
		CSegment( CDataContainer &parent, const ST_SegmentInfor &segment_create_infor );

		/**
		 * @name ISegment 接口
		 * @{
		*/
		virtual NSAPI off_t getSegmentOffset() {
			return m_base_offset;
		}
		virtual NSAPI off_t getSegmentSize() {
			return m_border - m_base_offset;
		}
		virtual NSAPI void getSegmentInformation( ST_SegmentInfor &segment_infor );
		///@}

	protected:
		/* 外界不能直接调用析构函数，使用 releaseReference() 替代 */
		virtual ~CSegment();

		/* 友元类是不可继承的，因此定义此函数访问数据文件可以防止在头文件中过多的声明友元类 */
		CFile &get_data_file();

		/**
		 * @name 提供子类获取边界的函数
		 * @{
		*/
		off_t get_base_offset() {
			return m_base_offset;
		}
		off_t get_border() {
			return m_border;
		}
		///@}

		/**
		 * @name 提供给子类的对CFile::io的包装函数
		 * @{
		*/
		int data_read( void *data_buffer, size_t data_size, off_t offset, long io_tag, bool force_sync ) {
			return io( CFile::ST_ListIOItem::ioRead, data_buffer, data_size, offset, io_tag, force_sync );
		}
		int data_write( const void *data_buffer, size_t data_size, off_t offset, long io_tag, bool force_sync ) {
			return io( CFile::ST_ListIOItem::ioWrite, ( void * )data_buffer, data_size, offset, io_tag, force_sync );
		}
		///@}

		///检测指定长度是否在界內(指定长度是否合法)
		bool checkSegmentRange( off_t test_segment_length ) {
			return ( test_segment_length >= m_base_offset ) && ( m_border >= ( m_base_offset + test_segment_length ) );
		}

		off_t	m_virtual_border;	/**< 区段结束位置相对文件头的（虚拟）偏移，默认初始化与 m_base_offset 相同值
										@note 这个边界是段必须维持的边界，DataContainer执行切分段操作时，会将分割点在此边界以后与 m_border 指定的边界以前
										@note 子类如果关心此边界就要随时调整，否则使用基类默认皆可
									*/
		ISegment::INotify	*m_io_notify_ptr;
		ISegment::ST_SegmentInfor::ENM_SegmentBufferType	m_segment_buffer_type;

	private:
		friend class 		CDataContainer;
		static void onFileIOCallback( CFile *file_ptr, const CFile::ST_ListIOItem &list_io_item );//由 CDataContainer 调用
		///CFile::io的包装函数
		int io( CFile::ST_ListIOItem::ENM_IOType io_type, void *data_buffer, size_t data_size, off_t offset, long io_tag, bool force_sync );

		CDataContainer		&m_parent;
		off_t 				m_base_offset;	///< 区段 起始 位置相对文件头的（物理）偏移，相对稳定
		off_t				m_border;		///< 区段 结束 位置相对文件头的（物理）偏移，相对稳定

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( Segment, Base_CreateAndDestroy );
		FRIEND_TEST( Segment, Base_GetSegmentInformation );
#endif
};

class CSegment_NULL : public CSegment
{
	public:
		CSegment_NULL( CDataContainer &parent, const ISegment::ST_SegmentInfor &segment_create_infor )
			: CSegment( parent, segment_create_infor )
		{}
		virtual NSAPI int read( void *data_ptr, size_t data_size, off_t offset, long io_tag ) {
			return 0;
		}
		virtual NSAPI int write( const void *data_ptr, size_t data_size, off_t offset, long io_tag ) {
			return 0;
		}
		virtual NSAPI void flushData( void ) {}
		virtual NSAPI off_t getIOPosition() {
			return 0;
		}
		virtual NSAPI bool setDataWindowSize( size_t data_window_size ) {
			return false;
		}
};

///@}

}/* DataContainer **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef segment_INC  ----- */
