#include "data_container_manager.h"

using namespace Netspecters::KernelPlus::DataContainer;

IDataContainer *CDataContainerManager::newDataContainer(const char *path_string_ptr, off_t &data_size)
{
	return new CDataContainer(path_string_ptr, data_size);
}

void CDataContainerManager::freeDataContainer(IDataContainer *data_container_ptr)
{
	delete data_container_ptr;
}

int CDataContainerManager::onLoad(IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param)
{
	//初始化状态表
	printk(KINFORMATION"DataContainerManager loaded.\n");
	return NSEC_SUCCESS;
}

void CDataContainerManager::onUnload(void)
{
	printk(KINFORMATION"DataContainerManager unloaded.\n");
}

int CDataContainerManager::getParam(const char *param_name, void *value_buffer_ptr)
{
	return 0;
}

void *CDataContainerManager::setParam(const char *param_name, void *value_ptr)
{
	return NULL;
}
