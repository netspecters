#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <alloca.h>
#include <fcntl.h>
#include <assert.h>
#include <aio.h>

#include "../../source/Core/KernelConsole.h"
#include "file.h"

using namespace Netspecters::KernelPlus::DataContainer;

struct aiocb_t : public aiocb
{
	CFile *file_ptr;
	uintptr_t io_key, io_tag;
	size_t real_buffer_size;
};

/* ============================= aio 相关函数 ============================= */

static void aio_completion_handler( sigval_t sigval )
{
	aiocb_t *aiocb_ptr = ( aiocb_t * ) sigval.sival_ptr;

	/* 请求是否完成? */
	errno = aio_error( aiocb_ptr );
	if( errno == 0 )
	{
		/* Request completed successfully, get the return status */
		size_t transfer_data_size = aio_return( aiocb_ptr );

		dbg( TEST_DBG_LEVEL, "Enter aio_complete_handler, address=%x, io_tag=%d\n", aiocb_ptr, aiocb_ptr->io_tag );

		//call back
		CFile::ST_ListIOItem list_io_item = {
			aiocb_ptr->aio_lio_opcode == LIO_READ ? CFile::ST_ListIOItem::ioRead : CFile::ST_ListIOItem::ioWrite,
			( void * )aiocb_ptr->aio_buf,
			transfer_data_size,
			aiocb_ptr->aio_offset,
			aiocb_ptr->io_key,
			aiocb_ptr->io_tag
		};
		aiocb_ptr->file_ptr->io_callback( list_io_item );
	}
	else
	{
		printk( KERROR"IOCenter recv an io error! Get last os error: (%d)%s\n", errno, strerror( errno ) );
	}

	delete aiocb_ptr;
}


/* ============================= file 相关函数 ============================= */

int CFile::open( const char *file_name_ptr, off_t &file_size )
{
	m_file_handle = ::open( file_name_ptr, O_RDWR | O_CREAT | O_LARGEFILE, S_IRUSR | S_IWUSR | S_IRGRP ); //O_DIRECT |
	if( m_file_handle >= 0 )
	{
		//获取文件大小
		struct stat file_status_struct;
		fstat( m_file_handle, &file_status_struct );
		m_file_size = file_size = file_status_struct.st_size;
		m_file_name.assign( file_name_ptr );
	}
	return m_file_handle;
}

void CFile::close()
{
	::close( m_file_handle );
	reset();
}

int CFile::resize( off_t new_file_size )
{
	if( new_file_size > m_file_size )
	{
		off_t old_file_size = m_file_size;
		m_file_size = lseek( m_file_handle, new_file_size - old_file_size, SEEK_END );
		if( m_file_size == -1 )
		{
#ifdef __i386
			if( errno == EOVERFLOW )	//32位cpu构架下可能虽然成功，但返回溢出失败
				m_file_size = new_file_size;
			else
#endif
			{
				printk( KERROR"COSIO can not reset file size! Get last os error: (%d)%s\n", errno, strerror( errno ) );
				m_file_size = old_file_size;
			}
		}
	}
	else if( new_file_size < m_file_size )
	{
		//保证页对齐
		new_file_size = ( new_file_size + OSFunc::OS_MEM_PAGE_SIZE ) / OSFunc::OS_MEM_PAGE_SIZE * OSFunc::OS_MEM_PAGE_SIZE;
		ftruncate( m_file_handle, new_file_size );
		new_file_size = new_file_size;
	}
	return m_file_size;
}

int CFile::list_io( const ST_ListIOItem *list_io_array, unsigned list_io_count, bool force_sync )
{
	unsigned action_io_count = 0;

	/* 声明 aiocb_t 数组 */
	aiocb_t *aiocb_ptr_list[list_io_count];
	memset( aiocb_ptr_list, 0, sizeof( aiocb_t * )*list_io_count );

	/* 将 ST_ListIOItem 结构数组转换成 aiocb_t（aiocb）结构数组 */
	aiocb_t *aiocb_item_ptr;
	for( int i = 0; i < list_io_count; i++ )
	{
		aiocb_item_ptr = new aiocb_t;
		memset( aiocb_item_ptr, 0, sizeof( aiocb_t ) );

		aiocb_item_ptr->aio_sigevent.sigev_notify = SIGEV_THREAD;
		aiocb_item_ptr->aio_sigevent.sigev_notify_function = &aio_completion_handler;
		aiocb_item_ptr->aio_sigevent.sigev_value.sival_ptr = aiocb_item_ptr;

		aiocb_item_ptr->aio_fildes = m_file_handle;
		aiocb_item_ptr->aio_buf = list_io_array->data_ptr;
		aiocb_item_ptr->aio_nbytes = list_io_array->data_size;
		aiocb_item_ptr->aio_offset = list_io_array->offset;
		aiocb_item_ptr->aio_lio_opcode = list_io_array->io_type == ST_ListIOItem::ioRead ? LIO_READ : LIO_WRITE;

		aiocb_item_ptr->real_buffer_size = list_io_array->data_size < OSFunc::OS_MEM_PAGE_SIZE ? OSFunc::OS_MEM_PAGE_SIZE : list_io_array->data_size;
		aiocb_item_ptr->file_ptr = this;
		aiocb_item_ptr->io_key = list_io_array->io_key;
		aiocb_item_ptr->io_tag = list_io_array->io_tag;

		aiocb_ptr_list[i] = aiocb_item_ptr;
		list_io_array++;
		action_io_count++;
	}

	int mode = force_sync ? LIO_WAIT : LIO_NOWAIT;

	if( lio_listio( mode, ( aiocb ** )aiocb_ptr_list, list_io_count, NULL ) == -1 )
	{
		/* 操作失败后，分配的 aiocb_t 数组没用了，释放掉 */
		for( int i = 0; i < list_io_count; i++ )
			delete aiocb_ptr_list[i];
		action_io_count = 0;
	}

	return action_io_count;
}

void CFile::fsync()
{
	::fsync( m_file_handle );
}
