#ifndef DATA_CONTAINER_FILE_H_INCLUDED
#define DATA_CONTAINER_FILE_H_INCLUDED

#include <string>
#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif
#include "nslib.h"

namespace Netspecters { namespace KernelPlus { namespace DataContainer {

using std::string;
using namespace Netspecters::NSStdLib;

class CFile
{
	public:
		struct ST_ListIOItem
		{
			enum ENM_IOType { ioRead, ioWrite } io_type;
			void *data_ptr;
			size_t data_size;
			off_t offset;
			uintptr_t io_key, io_tag;
		};
		typedef void ( *io_callback_t )( CFile *file_ptr, const ST_ListIOItem &list_io_item );

		CFile( io_callback_t io_callback )
			: m_file_size( -1 )
			, m_file_handle( 0 )
			, m_io_callback( io_callback )
		{}
		~CFile() {
			close();
		}

		/**
		 * @name 文件操作函数
		 * @{
		 */
		int open( const char *file_name_ptr, off_t &file_size );
		void close();
		int resize( off_t new_file_size );
		int list_io( const ST_ListIOItem *list_io_array, unsigned list_io_count, bool force_sync );
		void fsync();
		///@}

		/**
		 * @name 功能操作函数
		 * @{
		 */
		int get_file_handle() {
			return m_file_handle;
		}
		off_t get_file_size() {
			return m_file_size;
		}
		const char *get_file_name() {
			return m_file_name.length() > 0 ? m_file_name.c_str() : NULL;
		}
		void io_callback( const ST_ListIOItem &list_io_item ) {
			if( m_io_callback )
				m_io_callback( this, list_io_item );
		}
		void *get_user_data_ptr() {
			return m_user_data_ptr;
		}
		void set_user_data_ptr( void *user_data_ptr ) {
			m_user_data_ptr = user_data_ptr;
		}
		///@}

	private:
		///重置类的数据成员
		void reset()
		{
			m_file_handle = m_file_size = 0;
			m_file_name.clear();
		}

		string			m_file_name;
		off_t			m_file_size;
		int				m_file_handle;
		io_callback_t	m_io_callback;
		void			*m_user_data_ptr;				/* 用户数据指针 */

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( DataContainer, File_OpenAndClose );
		FRIEND_TEST( DataContainer, File_ResetFileSize );
		FRIEND_TEST( DataContainer, File_ReadAndWrite );
#endif
};

}/* DataContainer **/ }/* KernelPlus**/ }/* Netspecters **/

#endif // DATA_CONTAINER_FILE_H_INCLUDED
