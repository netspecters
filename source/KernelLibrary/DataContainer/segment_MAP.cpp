#include "segment_MAP.h"
#include "data_container.h"

using namespace Netspecters::KernelPlus::DataContainer;

CSegment_MAP::CSegment_MAP( CDataContainer &parent, const ISegment::ST_SegmentInfor &segment_create_infor )
	: CSegment( parent, segment_create_infor )
{
	m_segment_mapped_ptr = OSFunc::mapFile( get_data_file().get_file_handle(), get_base_offset(), get_border() );
}

int CSegment_MAP::read( void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	if( sizeof( void * ) == data_size )
	{
		offset += get_base_offset();

		/* 偏移量必须是页长的整数倍，规整偏移量 */
#ifdef __i386__
		offset |= 0x00000FFF;
#else
#	error __x86_64__ method has no implemented
#endif

		if(( offset + ( off_t )OSFunc::OS_MEM_PAGE_SIZE ) < get_border() )
			*( uintptr_t * )data_ptr = ( uintptr_t ) m_segment_mapped_ptr + offset;
		else
			return 0;
	}
	else
		return NSEC_PARAMERROR;
	return sizeof( void * );
}

CSegment_MAP::~CSegment_MAP()
{
	OSFunc::unmapFile( m_segment_mapped_ptr, getSegmentSize() );
}

void CSegment_MAP::getSegmentInformation( ST_SegmentInfor &segment_infor )
{
	segment_infor.segment_type = ST_SegmentInfor::stMAP;

	/* 调用基类同名函数，填写基本信息 */
	CSegment::getSegmentInformation( segment_infor );
}
