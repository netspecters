#ifndef  DATA_CONTAINER_INTERFACES_INC
#define  DATA_CONTAINER_INTERFACES_INC

#include <stddef.h>
#include <unistd.h>
#include <cstring>
#include "../../Core/PlusInterface.h"

namespace Netspecters { namespace KernelPlus { namespace DataContainer {

/**
 * @addtogroup DataContainer Data Container(数据容器)
 * @{
*/

/**
 * @brief 在 IDataContainer 中使用的Segment接口
 * @namespace Netspecters::KernelPlus::DataContainer
*/
class ISegment
{
	public:
		/**
		 * @brief 接收Segment事件的（回调）接口
		*/
		class INotify
		{
			public:
				enum ENM_IOErrorCode
				{
					ioe
				};
				virtual NSAPI int onIORealRead( void *data_buffer, size_t transfer_data_size, long io_tag ) = 0;
				virtual NSAPI int onIORealWrite( void *data_buffer, size_t transfer_data_size, long io_tag ) = 0;
				virtual NSAPI int onIOError( ENM_IOErrorCode io_error_code ) = 0;
				virtual NSAPI bool enableMerge( ISegment *request_segment_ptr ) = 0;
		};
		struct ST_SegmentInfor
		{
			///段结构类型
			enum ENM_SegmentType
			{
				stNULL,	///< 用于占位的空结构，没有具体功能
				stMRU,	///< 使用 MRU 算法控制缓存
				stFIFO, ///< 使用 FIFO 算法控制缓存
				stDRW,	///< 不使用缓存，直接操作数据
				stMAP	///< 使用内存映射的方法操作数据 @note 因map模式会将文件映射入内存，所有会增加内存使用量
			} segment_type;

			///段內使用的缓冲区类型(只对 stMRU, stFIFO 类型的段有效，其余段忽略此项设置 )
			enum ENM_SegmentBufferType
			{
				sbtInternal,		/**< 使用segment内部的缓存 */
				sbtManaged,		/**< 使用托管缓存，向data_buffer_manager申请和释放数据块 */
				sbtExternal		/**< 使用外部缓存 @note segment不负责数据块的分配与释放（如果希望使用map文件，则不能使用此参数）*/
			} segment_buffer_type;

			off_t segment_base_offset;	/**< 段首相对文件头的绝对偏移 */
			off_t segment_border;			/**< 段尾相对文件头的绝对偏移 @note -1表示到文件末尾 */
			size_t data_window_size;		/**< 数据窗口大小(缓冲列队长度)，如果设为0,系统会使用默认值,stDRW类型的Segment忽略此参数 */
			INotify *io_notify_ptr;		/**< 如果对事件感兴趣，可以实现此接口并在此注册 */
			size_t page_size;				/**< [out]数据页大小，ns会使用os设定的page大小 */
		};

		/**
		 * @name 数据操作函数
		 * @{
		 */
		/**
		 * @brief 读segment中的数据
		 * @param data_ptr 接受缓冲区指针; 对于使用内存映射的方法的段，缓冲区为 (void*) 指针
		 * @param data_size 缓冲区大小; 对于使用内存映射的方法的段，此参数必须为 sizeof(void*)
		 * @param offset 相对段基址的读出位置偏移，对于使用内存映射的方法的段，offset应该是页长的整数倍，否则本函数会向下自动规整到页长的整数倍（如，页长为4K，offset指定为4000时，会被规整到0）
		 * @param io_tag
		 * @return 如果成功，返回实际读出的字节数，否则返回0
		 * @note 请阅读具体段类的实现相关说明
		 * @note 如果在段构建时 segment_buffer_type 指定为 sbtExternal, 则必须保证data_ptr、data_size和offset页对齐，否则调用会失败
		 * @see ST_SegmentInfor
		*/
		virtual NSAPI int read( void *data_ptr, size_t data_size, off_t offset, long io_tag ) = 0;
		/**
		 * @brief 将数据写入segment中
		 * @param data_ptr 数据缓冲区指针
		 * @param data_size 缓冲区大小
		 * @param offset 相对段基址的写入位置偏移
		 * @param io_tag
		 * @return 如果成功，返回实际写入的字节数，否则返回0
		 * @note 请阅读具体段类的实现相关说明
		 * @note 如果在段构建时 segment_buffer_type 指定为 sbtExternal, 则必须保证data_ptr、data_size和offset页对齐，否则调用会失败
		 * @note 对于使用内存映射的方法的段，本函数只会返回失败(NSEC_NSERROR)
		 * @see ST_SegmentInfor
		*/
		virtual NSAPI int write( const void *data_ptr, size_t data_size, off_t offset, long io_tag ) = 0;
		///将段缓存中的数据（如果有）刷新到磁盘上
		virtual NSAPI void flushData() = 0;
		///@}

		/**
		 * @name 段属性
		 * @{
		*/
		///获取段起始(相对于文件头的绝对)偏移
		virtual NSAPI off_t getSegmentOffset() = 0;
		///获取段尺寸
		virtual NSAPI off_t getSegmentSize() = 0;
		///获取当前读写位置(相对于段基址)
		virtual NSAPI off_t getIOPosition() = 0;
		///获取段相关信息
		virtual NSAPI void getSegmentInformation( ST_SegmentInfor &segment_infor ) = 0;
		virtual NSAPI bool setDataWindowSize( size_t data_window_size ) = 0;
		///@}
};

/**
 * @brief 映射到文件的平台无关的数据容器接口
 * @namespace Netspecters::KernelPlus::DataContainer
*/
class IDataContainer
{
	public:
		enum ENM_OffsetType
		{
			otMiddleOfLastOne,	///< 将最后一个
			otMiddleOfBiggest,	///< 分割最大的一个段
			otCustom
		};
		enum ENM_MergeStyle { msCombineNext, msCombinePrivious };

		virtual NSAPI const char *getDataContainerName() = 0;
		virtual NSAPI unsigned dumpSegmentRanges( void *buffer ) = 0;

		/**
		 * @brief 在指定位置添加段
		 * @param offset_type 指定获取偏移的方式
		 * @param segment_create_infor 段构建参数；如果 offset_type 没有指定 otCustom, 成功返回后会在 segment_base_offset 和 segment_border 字段填入实际使用值
		 * @return 成功创建的段结构指针，或失败返回NULL
		 */
		virtual NSAPI ISegment *addSegment( ENM_OffsetType offset_type, ISegment::ST_SegmentInfor &segment_create_infor ) = 0;		virtual NSAPI unsigned getSegmentNum();
		/**
		 * @brief 尝试合并段
		 * @param op_segment_ptr 发出合并操作请求的段指针（往往是目的段）
		 * @param merge_style 合并方式（尝试向前或向后合并）
		 * @return 是否成功合并		 * @note 如果成功完成合并，被合并段会被自动关闭
		 */
		virtual NSAPI bool tryMergeSegment( ISegment *op_segment_ptr, ENM_MergeStyle merge_style ) = 0;
		///移动段，即：重新设置段起始(相对于文件头的绝对)偏移
		virtual NSAPI off_t moveSegment( ISegment *op_segment_ptr, off_t new_base_position ) = 0;
		virtual NSAPI void deleteSegment( ISegment *op_segment_ptr ) = 0;

		///获取数据容器整体尺寸
		virtual NSAPI off_t getDataSize() = 0;
		/**
		 * @brief 重新设定数据容器尺寸
		 * @param new_data_size 目的尺寸
		 * @return 调整前数据容器尺寸
		 */
		virtual NSAPI off_t resetDataSize( off_t new_data_size ) = 0;
		virtual NSAPI void writeBack() = 0;

		/**
		 * @brief 关闭数据容器对应的文件
		 * @param sync_mode 是否使用同步模式进行数据刷新
		 * @param force_close_file 是否强制关闭
		 * @note 对于文件，必须调用close函数，否则会数据丢失
		 */
		virtual NSAPI void close( bool sync_mode, bool force_close_file ) = 0;
};

class IDataContainerManager : public INSPlus
{
	public:
		virtual NSAPI IDataContainer *newDataContainer( const char *name_string_ptr, off_t &data_size ) = 0;
		virtual NSAPI void freeDataContainer( IDataContainer *data_container_ptr ) = 0;
};

///@}

}/* DataContainer **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef DATA_CONTAINER_INTERFACES_INC  ----- */
