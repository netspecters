#include "segment_DRW.h"
#include "data_container.h"

using namespace Netspecters::KernelPlus::DataContainer;

int CSegment_DRW::read( void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	return data_read(data_ptr, data_size, offset, 0, true);
}

int CSegment_DRW::write( const void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	return data_write(data_ptr, data_size, offset, 0, true);
}

void CSegment_DRW::getSegmentInformation( ST_SegmentInfor &segment_infor )
{
	segment_infor.segment_type = ST_SegmentInfor::stDRW;
	segment_infor.data_window_size = 0;

	/* 调用基类同名函数，填写基本信息 */
	CSegment::getSegmentInformation( segment_infor );
}

bool CSegment_DRW::setDataWindowSize(size_t data_window_size)
{
	return false;
}

