#include "segment.h"
#include "data_container.h"

using namespace Netspecters::KernelPlus::DataContainer;

CSegment::CSegment( CDataContainer &parent, const ST_SegmentInfor &segment_create_infor )
	: m_virtual_border(segment_create_infor.segment_base_offset)
	, m_io_notify_ptr( segment_create_infor.io_notify_ptr )
	, m_segment_buffer_type( segment_create_infor.segment_buffer_type )
	, m_parent( parent )
	, m_base_offset( m_virtual_border )
	, m_border( segment_create_infor.segment_border )
{
	/* 成功建立段后，必须增加数据容器的引用计数 */
	parent.addReference();
}

CFile &CSegment::get_data_file()
{
	return m_parent.m_data_file;
}

void CSegment::getSegmentInformation( ST_SegmentInfor &segment_infor )
{
	segment_infor.segment_base_offset = m_base_offset;
	segment_infor.segment_border = m_border;
	segment_infor.io_notify_ptr = m_io_notify_ptr;
}

int CSegment::io( CFile::ST_ListIOItem::ENM_IOType io_type, void *data_buffer, size_t data_size, off_t offset, long io_tag, bool force_sync )
{
	addReference();

	CFile::ST_ListIOItem list_io_item;
	list_io_item.io_type = io_type;
	list_io_item.data_ptr = data_buffer;
	list_io_item.data_size = data_size;
	list_io_item.offset = offset;
	list_io_item.io_key = ( uintptr_t )this;
	list_io_item.io_tag = ( uintptr_t )io_tag;
	return m_parent.m_data_file.list_io( &list_io_item, 1, force_sync );
}

CSegment::~CSegment()
{
	/* 减少数据容器的引用计数 */
	m_parent.releaseReference();
}

void CSegment::onFileIOCallback( CFile *file_ptr, const CFile::ST_ListIOItem &list_io_item )
{
	CSegment *segment_ptr = ( CSegment * )( list_io_item.io_key );
	if( segment_ptr->m_io_notify_ptr )
	{
		switch( list_io_item.io_type )
		{
			case CFile::ST_ListIOItem::ioRead:
				segment_ptr->m_io_notify_ptr->onIORealRead( list_io_item.data_ptr, list_io_item.data_size, list_io_item.io_tag );
				break;
			case CFile::ST_ListIOItem::ioWrite:
				segment_ptr->m_io_notify_ptr->onIORealWrite( list_io_item.data_ptr, list_io_item.data_size, list_io_item.io_tag );
				break;
		}
	}
	segment_ptr->releaseReference();
}
