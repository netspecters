#include <algorithm>
#include <boost/foreach.hpp>#include <pair>

#include "data_container.h"
#include "segment_DRW.h"
#include  "segment_FIFO.h"
#include "segment_MRU.h"
#include "segment_MAP.h"

using namespace Netspecters::KernelPlus::DataContainer;

void CDataContainer::close( bool sync_mode, bool force_close_file )
{
}

void CDataContainer::writeBack()
{
}

off_t CDataContainer::getDataSize()
{
	return m_data_file.get_file_size();
}

off_t CDataContainer::resetDataSize( off_t new_data_size )
{
	return m_data_file.resize( new_data_size );
}

off_t CDataContainer::moveSegment( ISegment *op_segment_ptr, off_t new_base_position )
{
	if( op_segment_ptr )
	{
		std::lock_guard<std::mutex> locker( m_mutex );
		CSegment *segment_ptr = static_cast<CSegment *>( op_segment_ptr );

		/* 保证新移动的位置不在任何一个段內 */
		for( auto itr = m_segment_ptrs.begin(); itr != m_segment_ptrs.end(); itr++ )
			if(( *itr != segment_ptr )/* 跳过自身 */
					&& ((( *itr )->m_base_offset < new_base_position ) && ( new_base_position < ( *itr )->m_border ) ) )
				return NSEC_PARAMERROR;

		off_t segment_size = segment_ptr->getSegmentSize();

		/* 调整段的起始和结束位置 */
		segment_ptr->m_base_offset = new_base_position;
		segment_ptr->m_border = new_base_position + segment_size;

		/* 调整结束位置，使之不会覆盖下一个段也不会越界 */
		auto next_seg_itr = m_segment_ptrs.find( segment_ptr );
		next_seg_itr++;
		if( next_seg_itr == m_segment_ptrs.end() ) /* 已经是最后一个段了，确保不会越界 */
		{
			if( segment_ptr->m_border >= get_container_border() )
				segment_ptr->m_border = get_container_border() - 1;
		}
		else	/* 不是最后一个段，确保不会覆盖下一个段 */
		{
			if( segment_ptr->m_border >= ( *next_seg_itr )->m_base_offset )
				segment_ptr->m_border = ( *next_seg_itr )->m_base_offset - 1;
		}
	}

	return new_base_position;
}

void CDataContainer::deleteSegment( ISegment *op_segment_ptr )
{
	if( op_segment_ptr )
	{
		CSegment *segment_ptr = static_cast<CSegment *>( op_segment_ptr );
		/* 将数据刷入磁盘 */
		segment_ptr->flushData();

		/* 移出vector */
		std::lock_guard<std::mutex> locker( m_mutex );
		m_segment_ptrs.erase( segment_ptr );

		segment_ptr->releaseReference();
	}
}

ISegment *CDataContainer::addSegment( ENM_OffsetType offset_type, ISegment::ST_SegmentInfor &segment_create_infor )
{
	/* 如果容器空，建立段是无意义的，故不允许建立段 */
	if( 0 == m_data_file.get_file_size() ) return NULL;

	CSegment *target_segment_ptr = NULL;

	/* 处理容器还没分段的情况下的非自定义位置分配，将范围设定为全容器 */
	if(( m_segment_ptrs.size() == 0 ) && ( otCustom != offset_type ) )
	{
		segment_create_infor.segment_base_offset = 0;
		segment_create_infor.segment_border = get_container_border();
	}
	else
		/* 在容器已分段的情况下，查找有效的目的分割段 */
		switch( offset_type )
		{
				/* 分割最后一个段 */
			case otMiddleOfLastOne:
				target_segment_ptr = *m_segment_ptrs.rbegin();
				break;

				/* 分割最大的一个段 */
			case otMiddleOfBiggest:
				{
					off_t max_size = 0;
					for( auto itr = m_segment_ptrs.begin(); itr != m_segment_ptrs.end(); ++itr )
					{
						off_t current_size = ( *itr )->getSegmentSize();
						if( current_size > max_size )
						{
							target_segment_ptr = *itr;
							max_size = current_size;
						}
					}
				}
				break;

				/* 自定义段位置 */
			case otCustom:
				/* 校验参数是否合法 */
				if(( segment_create_infor.segment_base_offset < 0 )
						|| ( segment_create_infor.segment_base_offset >= segment_create_infor.segment_border )
						|| ( segment_create_infor.segment_border > get_container_border() ) )
					return NULL;

				/* 由外部指定段位置，但又不关心段边界，只是说明段边界对齐文件边界，这时应该在此处用文件长度替代 "-1" */
				if( -1 == segment_create_infor.segment_border )
					segment_create_infor.segment_border = get_container_border();
				break;
		}

	/* 如果有有效的目的分割段，填写构建参数中实际分配的段偏移和段边界 */
	if( target_segment_ptr )
	{
		off_t availd_split_space_length = target_segment_ptr->m_border - target_segment_ptr->m_virtual_border;	//允许分割的空间大小
		/* 允许分割空间长度应该大于一个常量才分割，否则分割后的段长太小，没有意义 */
		if( availd_split_space_length > 0 )
		{
			off_t want_target_border = target_segment_ptr->m_virtual_border + availd_split_space_length / 2;

			/* 填写段构建参数，指定段的基址与边界 */
			segment_create_infor.segment_base_offset = want_target_border + 1;
			segment_create_infor.segment_border = target_segment_ptr->m_border;

			/* 调整目的段的边界 */
			target_segment_ptr->m_border = want_target_border;
		}
		else
			return NULL;	/* 段允许分割的长度太小，不能成功完成段的分割 */
	}

	CSegment *new_segment_ptr = NULL;
	switch( segment_create_infor.segment_type )
	{
		case ISegment::ST_SegmentInfor::stNULL:
			new_segment_ptr = new CSegment_NULL( *this,  segment_create_infor );
			break;
		case ISegment::ST_SegmentInfor::stMRU:
			//new_segment_ptr = new CSegment_MRU( *this,  segment_create_infor );
			break;
		case ISegment::ST_SegmentInfor::stFIFO:
			new_segment_ptr = new CSegment_FIFO( *this,  segment_create_infor );
			break;
		case ISegment::ST_SegmentInfor::stDRW:
			new_segment_ptr = new CSegment_DRW( *this,  segment_create_infor );
			break;
		case ISegment::ST_SegmentInfor::stMAP:
			new_segment_ptr = new CSegment_MAP( *this,  segment_create_infor );
			break;
	}
	if( new_segment_ptr )
	{
		std::lock_guard<std::mutex> locker( m_mutex );
		m_segment_ptrs.insert( new_segment_ptr );
	}
	return new_segment_ptr;
}

bool CDataContainer::tryMergeSegment( ISegment *op_segment_ptr, ENM_MergeStyle merge_style )
{
	bool merged = false;
	CSegment *segment_ptr = static_cast<CSegment *>( op_segment_ptr );

	std::lock_guard<std::mutex> locker( m_mutex );

	auto op_itr = m_segment_ptrs.find( segment_ptr );
	if( m_segment_ptrs.end() != op_itr )
		switch( merge_style )
		{
			case msCombineNext:	/* 向后合并 */
				if( ++op_itr != m_segment_ptrs.end() )
				{
					CSegment *target_segment_ptr = *op_itr;
					if( target_segment_ptr->m_io_notify_ptr && ( merged = target_segment_ptr->m_io_notify_ptr->enableMerge( op_segment_ptr ) ) )
					{
						/* 调整边界 */
						segment_ptr->m_border = target_segment_ptr->m_border;
						deleteSegment( target_segment_ptr );	//如果允许合并要删除合并后残留物
					}
				}
				break;

			case msCombinePrivious:	/* 向前合并 */
				if( --op_itr != m_segment_ptrs.begin() )
				{
					CSegment *target_segment_ptr = *op_itr;
					if( target_segment_ptr->m_io_notify_ptr && ( merged = target_segment_ptr->m_io_notify_ptr->enableMerge( op_segment_ptr ) ) )
					{
						/* 调整边界 */
						segment_ptr->m_base_offset = target_segment_ptr->m_base_offset;
						deleteSegment( target_segment_ptr );	//如果允许合并要删除合并后残留物
					}
				}
				break;
		}
	return merged;
}

const char *CDataContainer::getDataContainerName( void )
{
	return m_data_file.get_file_name();
}

unsigned CDataContainer::dumpSegmentRanges( void *buffer )
{
	std::pair<off_t, off_t> *range_buffer_ptr;
	if( buffer )
	{
		range_buffer_ptr = ( _ST_Range * )buffer;
		BOOST_FOREACH( CSegment * segment_ptr, m_segment_ptrs )
		{
			range_buffer_ptr->first = segment_ptr->getSegmentOffset();	//base
			range_buffer_ptr->second = segment_ptr->getSegmentSize();	//border
			range_buffer_ptr++;
		}
	}
	return m_segment_ptrs.size() * sizeof( _ST_Range );
}
