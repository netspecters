#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "GeneralTestDefine.hpp"
#include "../segment.h"
#include "../data_container.h"
#include "../segment_FIFO.h"

namespace Netspecters {
namespace KernelPlus {
namespace DataContainer {

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	__data_container_mock dc;\
	ISegment::ST_SegmentInfor segment_infor;\
	memset( &segment_infor, 0, sizeof( segment_infor ) );\
	segment_infor.segment_type = ISegment::ST_SegmentInfor::stNULL;\
	segment_infor.segment_base_offset = 0;\
	segment_infor.segment_border = 4095;\
	enable_continue = false
#define TEST_END()\
	enable_continue = true

TEST( Segment, Base_CreateAndDestroy )
{
	TEST_BEGIN();

	CSegment_NULL segement_base( dc, segment_infor );

	/* 测试 CSegment 对象成员 */
	EXPECT_EQ( 0, segement_base.m_base_offset );
	EXPECT_EQ( segement_base.m_base_offset, segement_base.m_virtual_border );
	EXPECT_EQ( 4095, segement_base.m_border );
	EXPECT_EQ( 0, ( intptr_t ) segement_base.m_io_notify_ptr );

	TEST_END();
}

TEST( Segment, Base_GetSegmentInformation )
{
	TEST_BEGIN();

	CSegment_NULL segement_base( dc, segment_infor );

	/* 测试 getSegmentInformation 函数是否正确获取信息 */
	ISegment::ST_SegmentInfor want_segment_infor;
	segement_base.getSegmentInformation( want_segment_infor );

	EXPECT_EQ( segment_infor.segment_base_offset, want_segment_infor.segment_base_offset );
	EXPECT_EQ( segment_infor.segment_border, want_segment_infor.segment_border );
	EXPECT_EQ( segment_infor.io_notify_ptr, want_segment_infor.io_notify_ptr );

	TEST_END();
}

TEST( Segment, FIFO_CreateAndDestroy )
{
	TEST_BEGIN();

	CSegment_FIFO segement_fifo( dc, segment_infor );

	/* 测试 CSegment_FIFO 对象成员 */
	EXPECT_EQ( 0, ( intptr_t ) segement_fifo.m_queue_header_ptr );
	EXPECT_EQ( 0, ( intptr_t ) segement_fifo.m_queue_tail_ptr );
	EXPECT_EQ( 0, segement_fifo.m_data_window_base );
	EXPECT_EQ( 0, segement_fifo.m_data_window_io_position );
	EXPECT_EQ( 40960, segement_fifo.m_data_window_size );

	TEST_END();
}

}/* DataContainer **/
}/*KernelPlus**/
}/* Netspecters **/
