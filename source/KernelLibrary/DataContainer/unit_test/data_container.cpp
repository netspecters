#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdio.h>
#include "../data_container.h"#include "GeneralTestDefine.hpp"
#define FILENAME_BASE "dc_unit_test.tmp"
#ifdef __linux
#	define FILENAME "/tmp/"FILENAME_BASE
#else
#	define FILENAME FILENAME_BASE
#endif

namespace Netspecters {
namespace KernelPlus {
namespace DataContainer {

struct __dc_helper
{
	__dc_helper( void ) : container_ptr( NULL )
	{
		while( 1 )
		{
			off_t dc_size = 0;
			container_ptr = new CDataContainer( FILENAME, dc_size );
			EXPECT_EQ( 0, dc_size );	/* 此前，文件不应该存在 */
			if( 0 == dc_size )
				break;
			else
			{
				container_ptr->releaseReference();
				remove( FILENAME );
			}
		}
		EXPECT_EQ( 4096, container_ptr->resetDataSize( 4096 ) ); //初始化文件长度为4096字节
	}
	~__dc_helper( void )
	{
		container_ptr->releaseReference();
		remove( FILENAME );
	}
	CDataContainer *container_ptr;
};

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	__dc_helper dc_helper;\
	CDataContainer &container = *(dc_helper.container_ptr);\
	enable_continue = false
#define TEST_END()\
	enable_continue = true

TEST( DataContainer, DataContainer_CreateAndDestroy )
{
	TEST_BEGIN();

	/* 测试对象成员 */
	ASSERT_EQ( 1, container.m_reference_count );
	ASSERT_EQ( 4096, container.m_data_file.get_file_size() );
	ASSERT_EQ( 0, container.m_segment_ptrs.size() );

	TEST_END();
}

TEST( DataContainer, DataContainer_AddAndDeleteSegment_Base )
{
	TEST_BEGIN();

	/* 使用错误参数插入段 */
	ISegment::ST_SegmentInfor error_segment_infor;
	error_segment_infor.segment_base_offset = 0;
	error_segment_infor.segment_border = 1024;
	ASSERT_EQ( 0, ( uintptr_t )container.addSegment( IDataContainer::otCustom, error_segment_infor ) );


	ISegment *segment_ptrs[5];
	struct {
		off_t base;
		off_t border;
	} segment_offsetes[5] = {{27, 52}, {0, 16}, {17, 26}, {53, 1000}, {1001, 1023}};

	/* 插入打乱顺序的 5 个段 */
	for( int i = 0; i < 5; i++ )
	{
		ISegment::ST_SegmentInfor segment_infor;
		memset( &segment_infor, 0, sizeof( segment_infor ) );
		segment_infor.segment_type = ISegment::ST_SegmentInfor::stNULL;
		segment_infor.segment_base_offset = segment_offsetes[i].base;
		segment_infor.segment_border = segment_offsetes[i].border;
		segment_ptrs[i] = container.addSegment( IDataContainer::otCustom, segment_infor );
		ASSERT_NE( 0, ( uintptr_t )segment_ptrs[i] );
	}

	/* 测试 container 对象成员 */
	ASSERT_EQ( 5, container.m_segment_ptrs.size() );

	/* 段应该按序排列 */
	{
		off_t last_border = -1;
		int index = 0;
		for( auto itr = container.m_segment_ptrs.begin(); itr != container.m_segment_ptrs.end(); ++itr )
		{
			ISegment::ST_SegmentInfor segment_infor;
			( *itr )->getSegmentInformation( segment_infor );
			EXPECT_EQ( last_border + 1, segment_infor.segment_base_offset ) << "In the segment " << index;
			last_border = segment_infor.segment_border;
			index++;
		}
		EXPECT_EQ( 1024, last_border + 1 );
	}

	/* 删除段 */
	for( int i = 0; i < 5; i++ )
		container.deleteSegment( segment_ptrs[i] );

	/* 测试 container 对象成员 */
	ASSERT_EQ( 0, container.m_segment_ptrs.size() );
	ASSERT_EQ( 1, container.m_reference_count );

	TEST_END();
}

/* 测试 otMiddleOfLastOne 和 otMiddleOfBiggest */
TEST( DataContainer, DataContainer_AddSegment_MiddleOfLastOne_MiddleOfBiggest )
{
	TEST_BEGIN();

	ISegment *segment_ptrs[2];

	/* 首先添加占位段，占据全部容器空间 */
	ISegment::ST_SegmentInfor segment_infor;
	memset( &segment_infor, 0, sizeof( segment_infor ) );
	segment_infor.segment_type = ISegment::ST_SegmentInfor::stNULL;
	segment_ptrs[0] = container.addSegment( IDataContainer::otMiddleOfLastOne, segment_infor );
	ASSERT_NE( 0, ( uintptr_t )( segment_ptrs[0] ) );

	/* 测试段位置 */
	ASSERT_EQ( 0, segment_infor.segment_base_offset );
	ASSERT_EQ( container.get_container_border(), segment_infor.segment_border );

	/* 然后再添加一个占位段，位置在 MiddleOfLastOne  */
	segment_ptrs[1] = container.addSegment( IDataContainer::otMiddleOfLastOne, segment_infor );
	ASSERT_NE( 0, ( uintptr_t )( segment_ptrs[0] ) );

	/* 测试段位置 */
	ASSERT_EQ( container.get_container_border() / 2 + 1, segment_infor.segment_base_offset );
	ASSERT_EQ( container.get_container_border(), segment_infor.segment_border );

	/* 测试 container 对象成员 */
	ASSERT_EQ( 2, container.m_segment_ptrs.size() );

	/* 测试刚才插入的两个段是否在正确位置 */
	struct {
		off_t base;
		off_t border;
	} segment_offsetes[2] = { {0, container.get_container_border() / 2}, {container.get_container_border() / 2 + 1, container.get_container_border()} };
	for( int i = 0; i < 2; i++ )
	{
		ISegment::ST_SegmentInfor segment_infor;
		segment_ptrs[i]->getSegmentInformation( segment_infor );
		EXPECT_EQ( segment_offsetes[i].base, segment_infor.segment_base_offset ) << "In the segment " << i;
		EXPECT_EQ( segment_offsetes[i].border, segment_infor.segment_border ) << "In the segment " << i;
	}

	/* 释放段 */
	for( int i = 0; i < 2; i++ )
		container.deleteSegment( segment_ptrs[i] );

	/* 测试 container 对象成员 */
	ASSERT_EQ( 0, container.m_segment_ptrs.size() );

	TEST_END();
}

}/* DataContainer **/
}/*KernelPlus**/
}/* Netspecters **/
