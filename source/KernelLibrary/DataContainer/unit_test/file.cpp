#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string.h>
#include "../file.h"

#define FILENAME_BASE "dc_osio_unit_test.tmp"
#ifdef __linux
#	define FILENAME "/tmp/"FILENAME_BASE
#else
#	define FILENAME FILENAME_BASE
#endif

namespace Netspecters {
namespace KernelPlus {
namespace DataContainer {

static void null_io_callback( CFile *file_ptr, const CFile::ST_ListIOItem &list_io_item )
{
	;
}

struct __dc_osio_helper
{
	__dc_osio_helper( void ) : size( 0 ), os_io( &null_io_callback )
	{
		os_io.open( FILENAME, size );
		while( 1 )
		{
			EXPECT_EQ( 0, size );	/* 此前，文件不应该存在 */
			if( size == 0 )
				break;
			else
				remove( FILENAME );
		}
	}
	~__dc_osio_helper( void )
	{
		remove( FILENAME );
	}

	off_t size;
	CFile os_io;
};

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	__dc_osio_helper dc_osio_helper;\
	CFile &os_io = dc_osio_helper.os_io;\
	enable_continue = false
#define TEST_END()\
	enable_continue = true

TEST( DataContainer, File_OpenAndClose )
{
	CFile os_io( &null_io_callback );
	off_t size = 0;
	enable_continue = false;

	/* 打开物理文件 */
	os_io.open( FILENAME, size );

	/* 测试对象成员 */
	EXPECT_EQ( 0, size );	/* 此前，文件不应该存在 */
	if( size == 0 )	remove( FILENAME );
	ASSERT_EQ( size, os_io.m_file_size );
	ASSERT_LE( 0, ( uintptr_t )os_io.m_file_handle );
	ASSERT_STREQ( FILENAME, os_io.m_file_name.c_str() );

	/* 关闭物理文件 */
	os_io.close();

	/* 测试对象成员 */
	ASSERT_EQ( 0, os_io.m_file_size );
	ASSERT_EQ( 0, ( uintptr_t )os_io.m_file_handle );
	ASSERT_EQ( 0, ( uintptr_t )os_io.get_file_name() );

	/* 删除物理文件 */
	remove( FILENAME );

	enable_continue = true;
}

TEST( DataContainer, File_ResetFileSize )
{
	TEST_BEGIN();

	/* 设置文件长度 */
	os_io.resize( 1024 );

	/* 检测对象成员 */
	ASSERT_EQ( 1024, os_io.m_file_size );

	TEST_END();
}

TEST( DataContainer, File_ReadAndWrite )
{
#define FIRST			4096
#define SECOND			4096
#define THRID			4096
#define FOURTH			4096
#define RAND_DATA_NUM	(4096 * 4)
#define RAND_DATA_SIZE	(RAND_DATA_NUM * sizeof(int))

	TEST_BEGIN();

	struct __io_notify
	{
		__io_notify( void ) : read_count( 0 ), write_count( 0 )
		{
			for( int i = 0; i < 4; i++ )
				write_tag[i] = false;
			for( int i = 0; i < 2; i++ )
				read_tag[i] = false;
		}

		void onIORead( void *data_buffer, size_t transfer_data_size, off_t abs_offset, size_t data_buffer_size, long io_tag )
		{
			ASSERT_LE( 0, io_tag );
			ASSERT_LE( io_tag, 1 );

			read_tag[io_tag] = true;
			read_count++;
		}
		void onIOWrite( void *data_buffer, size_t transfer_data_size, off_t abs_offset, size_t data_buffer_size, long io_tag )
		{
			ASSERT_LE( 0, io_tag );
			ASSERT_LE( io_tag, 3 );

			write_tag[io_tag] = true;
			write_count++;
		}

		int read_count, write_count;
		bool read_tag[2], write_tag[4];

		static void io_callback( CFile *file_ptr, const CFile::ST_ListIOItem &list_io_item )
		{
			__io_notify *io_notify_ptr = ( __io_notify * )( file_ptr->get_user_data_ptr() );
			if( list_io_item.io_type == CFile::ST_ListIOItem::ioRead )
				io_notify_ptr->onIORead( list_io_item.data_ptr, list_io_item.data_size, list_io_item.offset, list_io_item.data_size, list_io_item.io_tag );
			else
				io_notify_ptr->onIOWrite( list_io_item.data_ptr, list_io_item.data_size, list_io_item.offset, list_io_item.data_size, list_io_item.io_tag );
		}
	} io_notify;

	/* 重置回调函数 */
	os_io.m_io_callback = &__io_notify::io_callback;

	/* 设置用户数据 */
	os_io.set_user_data_ptr( &io_notify );

	/* 初始化随机数据区用于写入 */
	int write_datas[RAND_DATA_NUM];
	for( int i = 0; i < RAND_DATA_NUM; i++ )
		write_datas[i] = getRand();

	/* 分四次写入 */
	{
		off_t offset = 0;
#define CALL_WRITE(index, tag, sync)\
	do{\
		CFile::ST_ListIOItem list_io_item = {CFile::ST_ListIOItem::ioWrite, &(write_datas[offset]), index*sizeof(int), offset*sizeof(int), 0, tag};\
		os_io.list_io(&list_io_item, 1, sync);\
		offset += index;\
	}while(0)

		CALL_WRITE( FIRST, 0, false );
		CALL_WRITE( SECOND, 1, true );
		CALL_WRITE( THRID, 2, false );
		CALL_WRITE( FOURTH, 3, true );
#undef CALL_WRITE
	}

	/* 等待写入完成 */
	while( 4 != io_notify.write_count )
		std::this_thread::sleep_for(std::chrono::seconds(1));
	for( int i = 0; i < 4; i++ )
		ASSERT_TRUE( io_notify.write_tag[i] );

	/* 分两次读出 */
	int read_datas[RAND_DATA_NUM];
	{
		memset( read_datas, 0, sizeof( int )*RAND_DATA_NUM );
		int ONE = FIRST + SECOND;
		int TWO = THRID + FOURTH;
		CFile::ST_ListIOItem list_io_item_first = {CFile::ST_ListIOItem::ioRead, read_datas, ONE *sizeof( int ), 0, 0, 0 };
		CFile::ST_ListIOItem list_io_item_second = {CFile::ST_ListIOItem::ioRead, &( read_datas[ONE] ), TWO *sizeof( int ), ONE *sizeof( int ), 0, 1};

		EXPECT_EQ( NSEC_SUCCESS, os_io.list_io( &list_io_item_first, 1, true ) );
		EXPECT_EQ( NSEC_SUCCESS, os_io.list_io( &list_io_item_second, 1, false ) );
	}

	/* 等待读完 */
	while( 2 != io_notify.read_count )
		std::this_thread::sleep_for(std::chrono::seconds(1));
	for( int i = 0; i < 2; i++ )
		ASSERT_TRUE( io_notify.read_tag[i] );

	/* 校验读出数据 */
	ASSERT_EQ( 0, memcmp( write_datas, read_datas, RAND_DATA_SIZE ) );

	TEST_END();
}

}/* DataContainer **/
}/*KernelPlus**/
}/* Netspecters **/
