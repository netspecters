/*
 * =====================================================================================
 *
 *       Filename:  DataContainerManager.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2009-2-14 15:34:36
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (Tony), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */

#ifndef  DATACONTAINERMANAGER_INC
#define  DATACONTAINERMANAGER_INC

#include "../../Core/PlusInterface.h"

#include "Interfaces.h"
#include "data_container.h"

namespace Netspecters{ namespace KernelPlus{ namespace DataContainer{


class CDataContainerManager : public IDataContainerManager
{
	public:
		virtual NSAPI int onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param );
		virtual NSAPI void onUnload( void );
		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr);
		virtual NSAPI void *setParam( const char *param_name, void *value_ptr );

		virtual NSAPI IDataContainer *newDataContainer( const char *path_string_ptr, off_t &data_size );
		virtual NSAPI void freeDataContainer( IDataContainer *data_container_ptr );

		static inline CDataContainerManager &getStaticInstance()
		{
			static CDataContainerManager dcm_instance;
			return dcm_instance;
		}

	private:
};

#define DATA_CONTAINER_MANAGER_INSTANCE	CDataContainerManager::getStaticInstance()

}/* DataContainer **/}/*KernelPlus**/}/* Netspecters **/

#endif   /* ----- #ifndef DATACONTAINERMANAGER_INC  ----- */
