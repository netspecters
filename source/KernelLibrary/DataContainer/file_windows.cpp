#include <windows.h>
#include "file.h"
#include "convert_utf_unicode.hpp"

using namespace Netspecters::KernelPlus::DataContainer;

struct overlapped_t : public _OVERLAPPED
{
	ST_ListIOItem::ENM_IOType io_type;
	CFile *file_ptr;
	uintptr_t io_key, io_tag;
	void *data_buf_ptr;
	size_t real_buffer_size;
};

/* ============================= 完成实例 相关函数 ============================= */

static void file_io_completion_handler( DWORD dwErrorCode, DWORD transfer_data_size, LPOVERLAPPED lpOverlapped )
{
	overlapped_t *overlapped_st_ptr = static_cast<ST_Overlapped *>( lpOverlapped );

	/* 请求是否完成? */
	if( ERROR_SUCCESS == dwErrorCode )
	{
		//call back
		ST_ListIOItem list_io_item = {
			overlapped_st_ptr->io_type,
			overlapped_st_ptr->data_buf_ptr,
			transfer_data_size,
			overlapped_st_ptr->Offset,
			overlapped_st_ptr->io_tag
		};
		overlapped_st_ptr->file_ptr->io_callback( list_io_item );
	}
	else
	{
		DWORD translate_bytes_num;
		GetOverlappedResult( overlapped_st_ptr->file_handle, lpOverlapped, &translate_bytes_num, TRUE );
	}

	delete overlapped_st_ptr;
}

/* ============================= file 相关函数 ============================= */

int CFile::open( const char *file_name_ptr, off_t &file_size )
{
	/* utf8 to unicode */
	DECL_UTF8_UNICODE( file_name_ptr, file_name_unicode );

	//异步文件io不能与map file连用,否则会导致文件数据异常(调了一晚才得到的结论，该死的ms也不说清楚-_-!)
	//使用FILE_FLAG_NO_BUFFERING关闭windows系统的文件cache(注意地址对齐,配合netlayer库使用是不会有问题的)
	//在异步模式下使用windows系统的cache比自己实现的要慢约50%(知道fastcopy为什么快了，windows自己的文件cache很白痴:)
	//经测试：定义cache的大小为硬盘物理cache大小的两倍左右(xp和vista的大小不一样)，io性能达到最优
	//如果不使用IOCP，而使用IOCompletionRoutine Callback Function，不要设置FILE_FLAG_OVERLAPPED，否则必须设置FILE_FLAG_OVERLAPPED
	m_file_handle = CreateFileW( file_name_unicode, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_FLAG_NO_BUFFERING | FILE_ATTRIBUTE_NORMAL, 0 );
	if( INVALID_HANDLE_VALUE != m_file_handle )
	{
		/* 为了使磁盘效用达到最高，对于新建的文件要扩展文件的稀疏标志 */
		if( ERROR_ALREADY_EXISTS != GetLastError() )
		{
			DWORD temp;
			DeviceIoControl( m_file_handle, FSCTL_SET_SPARSE, NULL, 0, NULL, 0, &temp, NULL );
		}
		else
			m_file_size = GetFileSize( m_file_handle, NULL );

		/* 无论是创建新文件还是打开旧文件，只要 file_size 有效，都要根据 file_size 重新设定文件大小 */
		if( file_size > 0 )
			resize( file_size );
		else
			/* 将 file_size 设定为真实文件大小 */
			file_size = m_file_size;

		m_file_name.assign( file_name_ptr );
	}
	else
		m_file_handle = 0;

	return m_file_handle;
}

void CFile::close()
{
	CloseHandle( m_file_handle );
	reset();
}

int CFile::resize( off_t new_file_size )
{
	//保证页对齐
	new_file_size = ( new_file_size + OSFunc::OS_MEM_PAGE_SIZE ) / OSFunc::OS_MEM_PAGE_SIZE * OSFunc::OS_MEM_PAGE_SIZE;
	LARGE_INTEGER file_size = { new_file_size - m_file_size };
	SetFilePointerEx( m_file_handle, file_size, NULL, FILE_CURRENT );
	SetEndOfFile( m_file_handle );
	m_file_size = new_file_size;
}

int CFile::list_io( const ST_ListIOItem *list_io_array, unsigned list_io_count, bool force_sync )
{
	unsigned action_io_count = 0;

	const ST_ListIOItem *list_io_item_ptr = list_io_array;
	for( int i = 0; i < list_io_count; i++ )
	{
		/* 将 ST_ListIOItem 结构转换成 overlapped_t(overlapped) 结构 */
		overlapped_t *overlapped_st_ptr = new overlapped_t;
		memset( overlapped_st_ptr, 0, sizeof( overlapped_t ) );

		overlapped_st_ptr->Offset = offset;

		overlapped_st_ptr->io_type = list_io_item_ptr->io_type;
		overlapped_st_ptr->file_ptr = this;
		overlapped_st_ptr->io_tag = list_io_item_ptr->io_tag;
		overlapped_st_ptr->io_key = list_io_item_ptr->io_key;
		overlapped_st_ptr->data_buf_ptr = list_io_item_ptr->data_ptr;
		overlapped_st_ptr->real_buffer_size = list_io_item_ptr->data_size < OSFunc::OS_MEM_PAGE_SIZE ? OSFunc::OS_MEM_PAGE_SIZE : list_io_item_ptr->data_size;

		/* call os io function */
		typedef( *TIOFunc )( HANDLE, LPCVOID, DWORD, LPOVERLAPPED, LPOVERLAPPED_COMPLETION_ROUTINE );
		TIOFunc io_func = io_type == ( TIOFunc )( ST_ListIOItem::liotRead ? &::ReadFileEx : &::WriteFileEx );
		if( 0 == io_func( m_file_handle, data_ptr, data_size, overlapped_st_ptr, &file_io_completion_handler ) )
		{
			if( GetLastError() == WAIT_IO_COMPLETION )
			{
				if( force_sync )
				{
					SleepEx( INFINITE, TRUE );
					DWORD transfer_data_size;
					if( GetOverlappedResult( m_file_handle, overlapped_st_ptr, &transfer_data_size, TRUE ) )
						/* 成功阻塞执行，调用 file_io_completion_handler 统一处理 */
						file_io_completion_handler( ERROR_SUCCESS, transfer_data_size, overlapped_st_ptr );
					else
						delete overlapped_st_ptr;
				}
				action_io_count++;
			}
			else
				delete overlapped_st_ptr;
		}
	}

	return action_io_count;
}

void CFile::fsync()
{
	//TODO: finish fsync
}
