#include "segment_FIFO.h"
#include "data_container.h"

using namespace Netspecters::KernelPlus::DataContainer;

CSegment_FIFO::CSegment_FIFO( CDataContainer &parent, const ISegment::ST_SegmentInfor &segment_create_infor )
	: CSegment( parent, segment_create_infor )
	, m_queue_header_ptr( NULL )
	, m_queue_tail_ptr( NULL )
	, m_data_window_base( segment_create_infor.segment_base_offset )
	, m_data_window_io_position( 0 )
	, m_data_window_size( 40960 )
{}

bool CSegment_FIFO::setDataWindowSize( size_t data_window_size )
{
	if( data_window_size > m_data_window_size )
	{
		m_data_window_size = data_window_size;
		return true;
	}
	else
		return false;
}

int CSegment_FIFO::read( void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	if( !checkSegmentRange( offset ) || !checkSegmentRange( offset + data_size ) )
		return NSEC_NSERROR;

	/* 首先尝试从数据窗口中直接获得数据 */
	if(( offset > m_data_window_base ) && ( data_size <= m_data_window_io_position ) )
	{
		std::lock_guard<std::mutex> locker( m_queue_mutex );

		ST_FIFOQueueItem *item_ptr = m_queue_header_ptr;
		long current_offset = offset;
		while( item_ptr )
		{
			current_offset -= item_ptr->data_size;
			if( offset <= 0 )
			{
				char *current_data_buffer_ptr = static_cast<char *>( data_ptr );
				size_t remain_data_size = data_size;
				while( remain_data_size > 0 )
				{
					memcpy( current_data_buffer_ptr, item_ptr->data_ptr, item_ptr->data_size );
					remain_data_size -= item_ptr->data_size;
					current_data_buffer_ptr += item_ptr->data_size;
					item_ptr++;
				}
				return data_size;
			}
			item_ptr++;
		}
	}

	//read from disk file and read sync
	return data_read( data_ptr, data_size, offset, io_tag, true );
}

/**
 * @note 在此实现中忽略 offset 参数，写入位置永远为当前位置
 * @see ISegment::getIOPosition
*/
int CSegment_FIFO::write( const void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	( void ) offset;

	size_t want_border = m_data_window_io_position + data_size;

	/* 首先检查是否越过段边界 */
	if( !checkSegmentRange( want_border ) )
		return NSEC_NSERROR;

	/* 再次检测是否超过数据窗口边界 */
	if( want_border > m_data_window_size )
		flushData();	/* 当前数据窗口已满，回写数据而后清空数据窗口 */

	std::lock_guard<std::mutex> locker( m_queue_mutex );

	/* 填写数据项 */
	ST_FIFOQueueItem *new_queue_item_ptr = new ST_FIFOQueueItem;
	new_queue_item_ptr->data_size = data_size;
	new_queue_item_ptr->data_ptr = data_ptr;
	new_queue_item_ptr->next_item_ptr = NULL;
	new_queue_item_ptr->io_tag = io_tag;

	/* 挂到数据列队(窗口) */
	if( !m_queue_header_ptr )
		m_queue_header_ptr = new_queue_item_ptr;
	if( m_queue_tail_ptr )
		m_queue_tail_ptr->next_item_ptr = new_queue_item_ptr;
	m_queue_tail_ptr = new_queue_item_ptr;

	/* 移动读写位置 */
	m_data_window_io_position += data_size;

	/* 调整段虚拟边界到当前读写位置上 */
	m_virtual_border = m_data_window_base + m_data_window_io_position;

	return data_size;
}

off_t CSegment_FIFO::getIOPosition()
{
	return m_data_window_io_position;
}

void CSegment_FIFO::flushData()
{
	std::lock_guard<std::mutex> locker( m_queue_mutex );

	unsigned item_count = get_queue_item_count();
	if( 0 == item_count ) return;

	CFile::ST_ListIOItem list_io_item_array[item_count], *list_io_item_ptr = list_io_item_array;
	ST_FIFOQueueItem *item_ptr = m_queue_header_ptr;
	off_t current_offset = m_data_window_base;
	for( unsigned int i = 0; i < item_count; i++ )
	{
		list_io_item_ptr->io_type = CFile::ST_ListIOItem::ioWrite;
		list_io_item_ptr->data_ptr = ( void * )item_ptr->data_ptr;
		list_io_item_ptr->data_size = item_ptr->data_size;
		list_io_item_ptr->offset = current_offset;
		list_io_item_ptr->io_key = ( uintptr_t )( static_cast<CSegment *>( this ) );
		list_io_item_ptr->io_tag = item_ptr->io_tag;

		current_offset += item_ptr->data_size;

		list_io_item_ptr++;
		item_ptr = item_ptr->next_item_ptr;
	}

	addReference( item_count );
	if( get_data_file().list_io( list_io_item_array, item_count, false ) == NSEC_SUCCESS )
	{
		//释放所有数据以写入磁盘的数据块( FIXME: 本版本将数据块的缓冲工作交由c++库处理,如果需要则使用NS的ObjectPool全局管理)
		item_ptr = m_queue_header_ptr;
		while( item_ptr )
		{
			ST_FIFOQueueItem *tmp_ptr = item_ptr->next_item_ptr;
			delete item_ptr;
			item_ptr = tmp_ptr;
		}

		/* 调整数据窗口位置 */
		m_data_window_base += m_data_window_size;
		m_data_window_io_position = 0;

		/* 清空列队 */
		m_queue_header_ptr = m_queue_tail_ptr = NULL;
	}
	else
		for( unsigned int i = 0; i < item_count; i++ )
			releaseReference();
}

void CSegment_FIFO::getSegmentInformation( ST_SegmentInfor &segment_infor )
{
	segment_infor.segment_type = ST_SegmentInfor::stFIFO;
	segment_infor.data_window_size = m_data_window_size;

	/* 调用基类同名函数，填写基本信息 */
	CSegment::getSegmentInformation( segment_infor );
}

size_t CSegment_FIFO::get_queue_item_count()
{
	unsigned item_count = 0;
	ST_FIFOQueueItem *item_ptr = m_queue_header_ptr;
	while( item_ptr )
	{
		item_ptr = item_ptr->next_item_ptr;
		item_count++;
	}
	return item_count;
}
