#ifndef  segment_drw_INC
#define  segment_drw_INC

#include "segment.h"

namespace Netspecters { namespace KernelPlus { namespace DataContainer {

class CSegment_DRW : public CSegment
{
	public:
		CSegment_DRW( CDataContainer &parent, const ISegment::ST_SegmentInfor &segment_create_infor )
			: CSegment( parent, segment_create_infor )
		{}

		virtual NSAPI int read( void *data_ptr, size_t data_size, off_t offset, long io_tag );
		virtual NSAPI int write( const void *data_ptr, size_t data_size, off_t offset, long io_tag );
		virtual NSAPI void flushData( void ){}

		virtual NSAPI off_t getIOPosition(){}
		virtual NSAPI void getSegmentInformation( ST_SegmentInfor &segment_infor );
		virtual NSAPI bool setDataWindowSize( size_t data_window_size );

};

}/* DataContainer **/ }/* KernelPlus**/ }/* Netspecters **/

#endif   /* ----- #ifndef segment_drw_INC  ----- */
