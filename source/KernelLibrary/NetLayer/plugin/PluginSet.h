#ifndef PLUGINSET_H_INCLUDED
#define PLUGINSET_H_INCLUDED

namespace Netspecters{ namespace KernelPlus{ namespace NetLayer{

enum ENM_PluginName
{
	pnSock5
};

class CNode;
class CIOOperator;
struct ST_ErrorStatus;
class INodePlugin
{
	public:
		virtual bool nodeEventHook( CNode *active_node_ptr ) = 0;
		virtual bool nodeEventHook( CNode *active_node_ptr, void *user_buffer_ptr, void *buffer_ptr, size_t buffer_size, int io_code, uintptr_t io_tag ) = 0;
		virtual bool nodeEventHook( CNode *active_node_ptr, ST_ErrorStatus &error_status ) = 0;
};

}/* NetLayer **/}/* KernelPlus **/ }/* Netspecters **/

#endif // PLUGINSET_H_INCLUDED
