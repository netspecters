#if defined(_WIN32) || defined(_WIN64)
#	include <winsock2.h>
#	include <ws2tcpip.h>
#else
#	include <sys/types.h>
#	include <sys/socket.h>
#	include <netdb.h>
#endif

#include <new>
#include <utility>

#include "NetLayer.h"
#include "../../NSErrorCode.h"

using namespace Netspecters::KernelPlus::NetLayer;

int CNetLayer::onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param )
{
	printk( KINFORMATION"NetLayer loaded.\n" );
	return NSEC_SUCCESS;
}

void CNetLayer::onUnload()
{
	printk( KINFORMATION"NetLayer unloaded.\n" );
}

int CNetLayer::getParam( const char *param_name, void *value_buffer_ptr )
{
	return 0;
}

void *CNetLayer::setParam( const char *param_name, void *value_ptr )
{
	return NULL;
}

CNetLayer::CNetLayer()
{
#if defined _WIN32 || defined _WIN64
	WORD 	wVersionRequested;
	WSADATA wsaData;
	int 	err;

	wVersionRequested = MAKEWORD( 2, 2 );

	err = WSAStartup( wVersionRequested, &wsaData );

	/* Found a usable winsock dll? */
	if( err != 0 )
	{
		printk( KERROR"Can not load win32 socket library!\n" );
	}

	/*
	** Confirm that the WinSock DLL supports 2.2.
	** Note that if the DLL supports versions greater
	** than 2.2 in addition to 2.2, it will still return
	** 2.2 in wVersion since that is the version we
	** requested.
	*/

	if( LOBYTE( wsaData.wVersion ) != 2 || HIBYTE( wsaData.wVersion ) != 2 )
	{
		/*
		** Tell the user that we could not find a usable
		** WinSock DLL.
		*/
		WSACleanup( );
		printk( KERROR"Can not load win32 socket library!\n" );
	}

	/* The WinSock DLL is acceptable. Proceed. */
#endif
}

CNetLayer::~CNetLayer()
{}
CSocket* CNetLayer::create_socket(const CSocket::ST_Callback& callback)
{	return new CSocket(callback);
}

void CNetLayer::delete_socket(CSocket* socket_ptr)
{	delete socket_ptr;
}
