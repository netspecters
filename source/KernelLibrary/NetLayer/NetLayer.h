#ifndef  NETLAYER_INC
#define  NETLAYER_INC

#include "Interfaces.h"
#include "./socket.h"

namespace Netspecters{ namespace KernelPlus{ namespace NetLayer{

class CNetLayer : public INetLayer
{
	public:
		virtual NSAPI int onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param ) ;
		virtual NSAPI void onUnload() ;
		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr);
		virtual NSAPI void *setParam(const char *param_name, void *value_ptr);

		virtual NSAPI CSocket *create_socket( const CSocket::ST_Callback &callback );
		virtual NSAPI void delete_socket( CSocket *socket_ptr );

	public:
		static CNetLayer &getStaticInstance()
		{
			static CNetLayer static_netlayer;
			return static_netlayer;
		}

	private:
		CNetLayer();
		~CNetLayer();
};

#define NETLAYER_INSTANCE	CNetLayer::getStaticInstance()

} /* NetLayer **/}/* KernelPlus **/ } /* Netspecters **/

#endif   /* ----- #ifndef NETLAYER_INC  ----- */
