#ifndef  NETLAYER_INTERFACES_INC
#define  NETLAYER_INTERFACES_INC

#include "../../Core/PlusInterface.h"
#include "plugin/PluginSet.h"
#include "./socket.h"

namespace Netspecters { namespace KernelPlus { namespace NetLayer {

class INetLayer : public INSPlus
{
	public:
		virtual NSAPI CSocket *create_socket( const CSocket::ST_Callback &callback ) = 0;
		virtual NSAPI void delete_socket( CSocket *socket_ptr ) = 0;
};

} /* NetLayer **/ }/* KernelPlus **/ } /* Netspecters **/

#endif   /* ----- #ifndef NETLAYER_INTERFACES_INC  ----- */
