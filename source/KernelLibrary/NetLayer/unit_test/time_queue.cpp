#include <mutex>
#include <condition_variable>
#include <gtest/gtest.h>
#include "../time_queue.h"

namespace Netspecters {
namespace KernelPlus {
namespace NetLayer {

TEST( NetLayer, TimeQueue_RegisteTimer )
{
	std::mutex mutex;
	std::condition_variable cond_wait;
	CTimeQueue time_queue;

	/* 测试时差列队对象初始成员状态 */
	EXPECT_EQ( 0, ( intptr_t )time_queue.m_first_item_ptr );

	/* 注册超时回调 */
	CTimeQueue::TIME_HANDLE callback_handle = time_queue.registeTimer( 2000, [&mutex, &cond_wait]() {
		std::lock_guard<std::mutex> locker( mutex );
		cond_wait.notify_all();
	} );

	/* 测试时差列队对象成员状态 */
	EXPECT_EQ( callback_handle, time_queue.m_first_item_ptr );

	/* 等待超时 */
	std::unique_lock<std::mutex> locker( mutex );
	cond_wait.wait( locker );

	/* 测试超时列队在完成超时触发后，对象成员状态 */
	EXPECT_EQ( 0, ( intptr_t )time_queue.m_first_item_ptr );
}

TEST( NetLayer, TimeQueue_UnregisteTimer )
{
	CTimeQueue time_queue;
	bool time_out = false;

	/* 注册较长时间的超时回调 */
	CTimeQueue::TIME_HANDLE callback_handle = time_queue.registeTimer( 3000, [&time_out]() {
		time_out = true;
	} );

	/* 等待一小段时间后注销回调 */
	std::this_thread::sleep_for(std::chrono::seconds(2));
	time_queue.unregisteTimer( callback_handle );

	/* 等待时间耗尽后，检测状态 */
	std::this_thread::sleep_for(std::chrono::seconds(3));
	EXPECT_FALSE( time_out );
}

}/* NetLayer **/
}/* KernelPlus **/
}/* Netspecters **/
