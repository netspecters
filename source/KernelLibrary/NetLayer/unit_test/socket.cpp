#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../socket.h"

using ::testing::_;
using ::testing::Invoke;
using ::testing::Return;

namespace Netspecters {
namespace KernelPlus {
namespace NetLayer {

int on_connect_default( CSocket *socket_ptr, uintptr_t io_key ) {
	return 0;
}
int on_accept_default( CSocket *accepted_socket_ptr, const IP_Address &ip_address ) {
	return 0;
}
int on_send_default( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key ) {
	return 0;
}
int on_recv_default( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key ) {
	return 0;
}
int on_error_default( CSocket *socket_ptr, CSocket::ST_Callback::ENM_ErrorCode error_code, CSocket::ST_Buffer *buffer_ptr ) {
	return 0;
}

/* 测试用服务器 */
class __test_server
{
	public:
		static const unsigned SERVER_PORT = 12345u;
		static char *SERVER_ECHO_DATA;
		static const unsigned SERVER_ECHO_DATA_SIZE = 30;

		__test_server( bool enable_accept = true )
			: m_socket_handle( ::socket( AF_INET, SOCK_STREAM, IPPROTO_TCP ) )
			, m_goon( true )
			, m_enable_accept( enable_accept )
			, m_service_thread_ptr( NULL )
		{
			sockaddr_in servaddr;
			bzero( &servaddr, sizeof( servaddr ) );
			servaddr.sin_family = AF_INET;
			servaddr.sin_addr.s_addr = INADDR_ANY;
			servaddr.sin_port = htons( SERVER_PORT );
			::bind( m_socket_handle, ( struct sockaddr * ) &servaddr, sizeof( servaddr ) );

			if( enable_accept )
			{
				::listen( m_socket_handle, SOMAXCONN );
				m_service_thread_ptr = new std::thread( &__test_server::onExecute, this );
			}
		}
		~__test_server()
		{
			m_goon = false;
			struct linger so_linger = {1, 0};
			::setsockopt( m_socket_handle, SOL_SOCKET, SO_LINGER, ( const char * )( &so_linger ), sizeof( struct linger ) );
			close( m_socket_handle );
			if( m_service_thread_ptr )
			{
				m_service_thread_ptr->join();
				delete m_service_thread_ptr;
			}
		}
		void client_ok()
		{
			std::lock_guard<std::mutex> locker( m_mutex );
			m_cond_wait.notify_all();
		}

	private:
		/* 服务器运行过程，仅仅实现echo功能 */
		void onExecute()
		{
			while( m_goon )
			{
				sockaddr_in cliaddr;
				socklen_t length = sizeof( cliaddr );

				int client_socket_handle = ::accept( m_socket_handle, ( struct sockaddr * ) &cliaddr, &length );
				if( client_socket_handle >= 0 )
				{
					char recv_data[SERVER_ECHO_DATA_SIZE];
					memset( recv_data, 0, sizeof( char )*SERVER_ECHO_DATA_SIZE );
					::recv( client_socket_handle, recv_data, SERVER_ECHO_DATA_SIZE, 0 );
					::send( client_socket_handle, recv_data, SERVER_ECHO_DATA_SIZE, 0 );

					/* 等待客户端完成 */
					std::unique_lock<std::mutex> locker( m_mutex );
					m_cond_wait.wait( locker );

					/* 服务器端关闭socket */
					struct linger so_linger = {1, 0};
					::setsockopt( client_socket_handle, SOL_SOCKET, SO_LINGER, ( const char * )( &so_linger ), sizeof( struct linger ) );
					::close( client_socket_handle );
				}
			}
		}

		int m_socket_handle;
		bool m_goon, m_enable_accept;
		std::mutex m_mutex;
		std::condition_variable m_cond_wait;
		std::thread *m_service_thread_ptr;
};
char *__test_server::SERVER_ECHO_DATA = {"hello netspecters test client!"};

/* 测试用客户端 */
class __test_client
{
	public:
};

static bool enable_continue = true;
#define TEST_BEGIN()\
	ASSERT_TRUE( enable_continue );\
	enable_continue = false;\
	CSocket::ST_Callback __callback = {on_connect_default, on_accept_default, on_send_default, on_recv_default, on_error_default};\
	CSocket *socket_ptr = new CSocket( __callback );\
	IP_Address local_addr( IP_Address::addr2ip( "127.0.0.1" ), 0 );\
	socket_ptr->open(0,local_addr)

#define TEST_END()\
	socket_ptr->close();\
	delete socket_ptr;\
	enable_continue = true

TEST( NetLayer, Socket_CreateAndDestroy )
{
	enable_continue = false;
	CSocket::ST_Callback __callback = {on_connect_default, on_accept_default, on_send_default, on_recv_default, on_error_default};
	CSocket *socket_ptr = new CSocket( __callback );
	ASSERT_NE( 0, ( intptr_t ) socket_ptr );

	/* 检测成员变量是否被正确初始化 */
	EXPECT_EQ( 0, socket_ptr->m_socket_handle );
	EXPECT_EQ( CSocket::sIdle, socket_ptr->m_state );
	EXPECT_EQ( 0, ( intptr_t )socket_ptr->m_recv_buffer_ptr );
	EXPECT_EQ( 0, ( intptr_t )socket_ptr->m_send_buffer_ptr );
	EXPECT_EQ( 0, socket_ptr->m_key );

	delete socket_ptr;
	enable_continue = true;
}

TEST( NetLayer, Socket_OpenAndClose )
{
	TEST_BEGIN();

	EXPECT_NE( 0, socket_ptr->m_socket_handle );

	TEST_END();
}

/* 构建异常 tcp 协议客户端，测试错误处理函数（服务器拒绝连接） */
TEST( NetLayer, Socket_AsTCPClient_ConnectRefused )
{
	TEST_BEGIN();

	__test_server test_server( false );
	IP_Address server_addr( IP_Address::addr2ip( "127.0.0.1" ), htons( __test_server::SERVER_PORT ) );
	//EXPECT_EQ( NSEC_REFUSED, socket_ptr->connect( server_addr, 0 ) );

	TEST_END();
}

/* 构建正常 tcp 协议客户端 */
static bool __socket_tst_wait_;
static char __echo_data[__test_server::SERVER_ECHO_DATA_SIZE+1];
int on_connect_normal( CSocket *socket_ptr, uintptr_t io_key )
{
	/* 建立连接后，向服务器发送数据 */
	CSocket::ST_Buffer *send_buffer_ptr = CSocket::alloc_buffer( CSocket::ST_Buffer::ENM_Type::type_4K );
	memcpy( send_buffer_ptr->data, __test_server::SERVER_ECHO_DATA, __test_server::SERVER_ECHO_DATA_SIZE );
	send_buffer_ptr->size = __test_server::SERVER_ECHO_DATA_SIZE;
	socket_ptr->send( send_buffer_ptr, 0 );
	return NSEC_SUCCESS;
}
int on_send_normal( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key )
{
	/* 发送数据后，请求接收server的echo */
	buffer_ptr->size =  __test_server::SERVER_ECHO_DATA_SIZE;
	socket_ptr->recv( buffer_ptr, 0 );
	return NSEC_SUCCESS;
}
int on_recv_normal( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key )
{
	memcpy( __echo_data, buffer_ptr->data, __test_server::SERVER_ECHO_DATA_SIZE );
	CSocket::free_buffer( buffer_ptr );
	__socket_tst_wait_ = false;
	return NSEC_SUCCESS;
}
int on_error_normal( CSocket *socket_ptr, CSocket::ST_Callback::ENM_ErrorCode error_code, CSocket::ST_Buffer *buffer_ptr )
{
	socket_ptr->close();
	return NSEC_SUCCESS;
}
TEST( NetLayer, Socket_AsTCPClient_Normal )
{
	ASSERT_TRUE( enable_continue );
	enable_continue = false;
	CSocket::ST_Callback __callback = {&on_connect_normal, NULL, &on_send_normal, &on_recv_normal, &on_error_normal};
	CSocket *socket_ptr = new CSocket( __callback );
	IP_Address local_addr( IP_Address::addr2ip( "127.0.0.1" ), 0 );
	socket_ptr->open( 0, local_addr );
	__socket_tst_wait_ = true;

	__test_server test_server;

	/* 连接服务器 */
	IP_Address server_addr( IP_Address::addr2ip( "127.0.0.1" ), htons( __test_server::SERVER_PORT ) );
	socket_ptr->connect( server_addr, 0 );

	/* 等待服务器数据返回，为了多线程模式下有效，必须使用忙等 */
	while( __socket_tst_wait_ )
		std::this_thread::sleep_for( std::chrono::milliseconds( 20 ) );

	/* 通知服务器客户端处理完毕 */
	test_server.client_ok();

	/* 校验接收到的数据 */
	__echo_data[__test_server::SERVER_ECHO_DATA_SIZE] = '\0';
	ASSERT_STREQ( __echo_data, __test_server::SERVER_ECHO_DATA );

	socket_ptr->close();
	delete socket_ptr;
}

}/* NetLayer **/
}/* KernelPlus **/
}/* Netspecters **/
