#include "socket.h"
#include "nslib.h"

using namespace Netspecters::KernelPlus::NetLayer;

void CEventPoll::init()
{
	m_backend_fd = epoll_create( 256 );
	m_epoll_event_queue_ptr = new ST_EpollEventQueue[Netspecters::NSStdLib::OSFunc::getCPUNum()];
}

void CEventPoll::fina()
{
	delete[] m_epoll_event_queue_ptr;
	/* close epoll handle */
	::close( m_backend_fd );
}

void CEventPoll::poll( int thread_id )
{
	struct epoll_event *epoll_events = m_epoll_event_queue_ptr[thread_id].event_queue;
	int event_num = epoll_wait( m_backend_fd, epoll_events, 64, -1 );
	if( event_num <= 0 )	//==0时是timeout
	{
		switch( errno )
		{
			case EBADF:		//m_backend_fd 不是一个有效的文件描述符
			case EFAULT:
			case EINVAL:	//backend_fd不是一个有效的epoll描述符，或MAX_EVENTS<=0
			case EINTR:		//收到了系统中断信号
			default:
				return;
		}
	}

	std::lock_guard<std::recursive_mutex> locker( m_epoll_event_queue_ptr[thread_id].event_queue_mutex );
	/* epoll_wait sucess */
	struct epoll_event *current_event_ptr = epoll_events;
	for( int i = 0; i < event_num; i++ )
	{
		/* get the io that act this event */
		CSocket *socket_ptr = static_cast<CSocket *>( current_event_ptr->data.ptr );
		if( NULL == socket_ptr ) continue;

		int got  = current_event_ptr->events;

		if( got & ( EPOLLERR | EPOLLHUP | EPOLLRDHUP ) )	//some error occured on socket
		{
			if( got & ( EPOLLRDHUP | EPOLLHUP ) )
			{
				if( CSocket::sConnecting == socket_ptr->m_state )	/* connect error */
					socket_ptr->m_callback.error( socket_ptr, CSocket::ST_Callback::ecConnect, NULL ); // socket_error_handle( SOCK_ERROR_CONNECT );
				else if( got & EPOLLRDHUP )		/* socket closed by remote client */
					socket_ptr->m_callback.error( socket_ptr, CSocket::ST_Callback::ecConnrest, NULL ); //socket_error_handle( SOCK_ERROR_RST );
			}
			else	/* other error */
				;
		}
		else if( got & EPOLLPRI )	/* we got data */
		{
			/* TODO process socket */
		}
		else	/* socket got an io event */
		{
			/* socket can be readed */
			if( got & EPOLLIN )
			{
				if( CSocket::sListening == socket_ptr->m_state )
				{
					sockaddr_in sockaddr_struct;
					memset( &sockaddr_struct, 0, sizeof( sockaddr_in ) );
					socklen_t address_length = sizeof( struct sockaddr );
					::accept4( socket_ptr->m_socket_handle, ( sockaddr * )&sockaddr_struct, &address_length, SOCK_NONBLOCK );
					CSocket *accepted_socket_ptr = new CSocket( socket_ptr->m_callback );
					IP_Address ip_addr( sockaddr_struct.sin_addr.s_addr, sockaddr_struct.sin_port );
					socket_ptr->m_callback.accept( accepted_socket_ptr, ip_addr );
				}
				else if( socket_ptr->m_recv_buffer_ptr )
					socket_ptr->op_io(( CSocket::op_func_t )::recv, socket_ptr->m_recv_buffer_ptr );
			}
			/* socket can be writed */
			if( got & EPOLLOUT )
			{
				if(( CSocket::sConnecting == socket_ptr->m_state ) && socket_ptr->m_send_buffer_ptr )
				{
					/* the connected event only can be triggered once */
					if( CTimeQueue::TIME_HANDLE_INVAILD != socket_ptr->m_send_buffer_ptr->m_timer )
						CSocket::static_m_timeout_queue.unregisteTimer( socket_ptr->m_send_buffer_ptr->m_timer );//stop the timer
					size_t io_key = socket_ptr->m_send_buffer_ptr->m_io_key;
					delete socket_ptr->m_send_buffer_ptr;
					socket_ptr->m_send_buffer_ptr = NULL;
					socket_ptr->m_state = CSocket::sConnected;
					socket_ptr->m_callback.connect( socket_ptr, io_key );
				}
				else if( socket_ptr->m_send_buffer_ptr )
					socket_ptr->op_io(( CSocket::op_func_t )::send, socket_ptr->m_send_buffer_ptr );
			}
		}

		current_event_ptr->data.ptr = NULL;

		/* point to next event struct */
		current_event_ptr++;
	}
}

void CSocket::cancel( ST_Buffer *buffer_ptr )
{
	if( m_recv_buffer_ptr == buffer_ptr )
		m_recv_buffer_ptr = NULL;
	else
		m_send_buffer_ptr = NULL;
}

int CSocket::op_io( op_func_t op_func, CSocket::ST_Buffer *buffer_ptr )
{
	int result = 0;
	int transfer_length = op_func( m_socket_handle, buffer_ptr->data, buffer_ptr->waitting_size, 0 );
	switch( transfer_length )
	{
		case -1:	/* need wait on epoll */
			if(( errno != EAGAIN ) && ( errno != EWOULDBLOCK ) )
				result = -1;
			else
			{
				if( op_func == ( op_func_t )::recv )
					m_recv_buffer_ptr = buffer_ptr;
				else
					m_send_buffer_ptr = buffer_ptr;
			}
			break;

		case 0:		/* socket has been shutdown by one side */
			result = -1;
			break;

		default:
			buffer_ptr->waitting_size -= transfer_length;
			if( 0 == buffer_ptr->waitting_size )
			{
				result = buffer_ptr->size;
				if( op_func == ( op_func_t )::recv )
				{
					m_recv_buffer_ptr = NULL;
					recv_hook( buffer_ptr, buffer_ptr->m_io_key );
				}
				else
				{
					m_send_buffer_ptr = NULL;
					send_hook( buffer_ptr, buffer_ptr->m_io_key );
				}
			}
	}

	return result;
}

int CSocket::op_open( int type, const struct sockaddr *local_addr )
{
	int new_sock = ::socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if( new_sock )
	{
		m_socket_handle = new_sock;

		/* set the socket's flage which is 'SO_REUSEADDR' */
		int flage = 1;
		::setsockopt( new_sock, SOL_SOCKET, SO_REUSEADDR, ( const char * )&flage, sizeof( int ) );

		/* bind the socket */
		if( -1 == ::bind( new_sock, local_addr, sizeof( struct sockaddr ) ) )
			goto failed;

		/* set nonblock */
		::fcntl( new_sock, F_SETFL, fcntl( new_sock, F_GETFL, 0 ) | O_NONBLOCK );

		m_state = sOpen;
		/* set epoll */
		struct epoll_event event;
		memset( &event, 0, sizeof( event ) );
		event.data.ptr = this;
		event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET;	/* FIXME can not use EPOLLONESHOT here，else if epoll thread will suspend，why？ */
		epoll_ctl( CEventPoll::get_instance().m_backend_fd, EPOLL_CTL_ADD, m_socket_handle, &event );
	}
	return new_sock;

failed:
	m_socket_handle = 0;
	::close( new_sock );
	return 0;
}

int CSocket::op_connect( const struct sockaddr *serv_addr, int time_out, uintptr_t io_key )
{
	int rst = ::connect( m_socket_handle, serv_addr, sizeof( struct sockaddr ) );
	if( -1 == rst )
	{
		switch( errno )
		{
			case ECONNREFUSED:
				/* call error processer */
				rst = NSEC_REFUSED;
				break;

			case EINPROGRESS:
				{
					m_state = sConnecting;	//设置状态进入连接建立等待阶段
					m_send_buffer_ptr = new ST_Buffer;
					m_send_buffer_ptr->m_io_key = io_key;

					/* 设定连接等待超时 */
					if( time_out > 0 )
						m_send_buffer_ptr->m_timer = static_m_timeout_queue.registeTimer( time_out, std::bind( &CSocket::connect_timeout, this ) );
					else
						m_send_buffer_ptr->m_timer = CTimeQueue::TIME_HANDLE_INVAILD;

					rst = NSEC_ON_PROCESS;
				}
				break;
		}
	}
	else if( 0 == rst )
	{
		m_state = sConnected;
		m_callback.connect( this, io_key );
	}
	return rst;
}

void CSocket::close()
{
	if( (sFree == m_state) || sDisconnecting == m_state ) return;

	m_state = sDisconnecting;
	epoll_ctl( CEventPoll::get_instance().m_backend_fd, EPOLL_CTL_DEL, m_socket_handle, NULL );

	/* shutdown the socket gently */
	::shutdown( m_socket_handle, SHUT_RDWR );

	/* find unfinished event */
	for( unsigned i = 0; i < Netspecters::NSStdLib::OSFunc::getCPUNum(); i++ )
	{
		CEventPoll::ST_EpollEventQueue &epoll_event_queue = CEventPoll::get_instance().m_epoll_event_queue_ptr[i];
		/* lock the event queue */
		std::lock_guard<std::recursive_mutex> locker( epoll_event_queue.event_queue_mutex );

		/* enum the queue to find out this socket's event */
		struct epoll_event *current_event_ptr = epoll_event_queue.event_queue;
		for( int j = 0; j < 64; j++, current_event_ptr++ )
		{
			CSocket *socket_ptr = static_cast<CSocket *>( current_event_ptr->data.ptr );
			if( socket_ptr == this )
				current_event_ptr->data.ptr = NULL;
		}
	}

	/* use 'SO_LINGER' to avoid close timeout(TIME_WAIT) */
	struct linger so_linger = {1, 0};
	::setsockopt( m_socket_handle, SOL_SOCKET, SO_LINGER, ( const char * )( &so_linger ), sizeof( struct linger ) );

	/* now, we close the socket's handle */
	::close( m_socket_handle );

	m_state = sFree;
}
