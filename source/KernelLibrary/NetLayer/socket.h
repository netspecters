#ifndef SOCKET_H_INCLUDED
#define SOCKET_H_INCLUDED

#define PREALLOC_BUFFER_NUMBER 2

#include <thread>
#include <mutex>#include <set>

#include <string.h>
#if defined(__linux)
#	include <sys/types.h>
#	include <sys/socket.h>
#	include <sys/ioctl.h>
#	include <netinet/tcp.h>
#	include <unistd.h>
#	include <fcntl.h>
#	include <netdb.h>
#	include <errno.h>
#	include <stropts.h>
#	include <sys/epoll.h>
#	include <arpa/inet.h>
#	define EV_USE_EPOLL 1
#elif defined(_WIN32)
#	include <windows.h>
#	include <Winsock2.h>
#	include <Ws2tcpip.h>
#	include <cstddef>
#	define EV_USE_IOCP  1
#endif

#include "nslib.h"
#include "time_queue.h"

namespace Netspecters { namespace KernelPlus { namespace NetLayer {

using namespace Netspecters::NSStdLib;

struct IP_Address
{
#if defined( USE_IPV6 )
	typedef __uint128_t IP_Type;
#define AF_NET AF_INET6
#else
	typedef uint32_t IP_Type;
#define AF_NET AF_INET
#endif
	static IP_Type addr2ip( const char *addr_str )
	{
		return inet_addr( addr_str );
	}

	static uint16_t host2net( uint16_t host )
	{
		return htons( host );
	}

	static uint16_t net2host( uint16_t net )
	{
		return ntohs( net );
	}

	static IP_Address from_host_name( const char *host_name )	//dns
	{
		IP_Address result( 0, 0 );

		result.IP = inet_addr( host_name );
		if( INADDR_NONE == result.IP )	//直接转换二进制失败，dns解析
		{
			addrinfo *sys_query_addr_infor_ptr;
			addrinfo addr_hint;
			memset( &addr_hint, 0, sizeof( addr_hint ) );
			addr_hint.ai_family = AF_NET;

			if( getaddrinfo( host_name, NULL, &addr_hint, &sys_query_addr_infor_ptr ) == 0 )
			{
				for( addrinfo *rst_addr_infor_ptr = sys_query_addr_infor_ptr; rst_addr_infor_ptr != NULL; rst_addr_infor_ptr = rst_addr_infor_ptr->ai_next )
				{
					result.IP = reinterpret_cast<sockaddr_in *>( rst_addr_infor_ptr->ai_addr )->sin_addr.s_addr;
					break;//FIXME: 本版本只取第一个有效地址
				}
				freeaddrinfo( sys_query_addr_infor_ptr );
			}
		}
		return result;
	}

	IP_Address( void ) : IP( 0 ), Port( 0 ) {}
	explicit IP_Address( IP_Type ip, uint16_t port )
		: IP( ip ), Port( port )
	{}
	IP_Type IP;	//网络字节序
	uint16_t Port;	//网络字节序
};
class CSpeedGroup;
class CSocket
{
	friend class CEventPoll;	friend CSpeedGroup;
	public:
		struct ST_Buffer
#if EV_USE_IOCP
				: public OVERLAPPED
#endif
		{
				friend class CSocket;
				friend class CEventPoll;
				enum ENM_Type {
					type_4K, type_16K, type_64K, type_256K, type_count
				} type;
				size_t size;	///<实际数据占用的空间大小
				void *data;
				static size_t get_type_size(ENM_Type type){
					return  (ST_Buffer::type_4K ? 1 : ( 2 << ( 2 * type ) )) * OSFunc::OS_MEM_PAGE_SIZE;
				}
#ifndef EX_TEST
			private:
#endif
				ST_Buffer() :data(NULL) {}
				~ST_Buffer(){}

			public:
				void *operator new( size_t size ) {
					void *result = static_m_buffer_pool.allocObject();
					memset( result, 0, size );
					return result;
				}
				void operator delete( void *space_ptr ) {
					if( space_ptr )
						static_m_buffer_pool.freeObject(( ST_Buffer * )space_ptr );
				}

			private:
				static CObjectPool<ST_Buffer> static_m_buffer_pool;
				CTimeQueue::TIME_HANDLE m_timer;
				uintptr_t m_io_key;
				CSocket *m_socket_ptr;
#if EV_USE_EPOLL
				unsigned waitting_size;		///< 等待被传输的数据长度[how many bytes is waiting to transfer]
				unsigned filled_size;			/* point out how many bytes have been transfered. Only for internal using */
#endif
		};
		struct ST_Callback
		{
			int ( *connect )( CSocket *socket_ptr, uintptr_t io_key );
			int ( *accept )( CSocket *accepted_socket_ptr, const IP_Address &ip_address );
			int ( *send )( CSocket *socket_ptr, ST_Buffer *buffer_ptr, uintptr_t io_key );
			int ( *recv )( CSocket *socket_ptr, ST_Buffer *buffer_ptr, uintptr_t io_key );

			enum ENM_ErrorCode { ecConnect, ecConnRefused, ecConnrest, ecOPAborted, ecConnTimeout, ecRecvTimeout, ecSendTimeout };
			int ( *error )( CSocket *socket_ptr, ENM_ErrorCode error_code, ST_Buffer *buffer_ptr );
		};

		/** \brief 分配数据缓冲区
		 *
		 * \param type ST_Buffer::ENM_Type 指定类型（长度）
		 * \return 失败返回NULL
		 *
		 */
		static ST_Buffer *alloc_buffer( ST_Buffer::ENM_Type type );
		/** \brief 释放数据缓冲区
		 *
		 * \param buffer_ptr ST_Buffer* 使用 alloc_buffer 分配的缓冲区
		 *
		 */
		static void free_buffer( ST_Buffer *buffer_ptr );

		CSocket( const ST_Callback &callback );
		~CSocket();

		ST_Buffer::ENM_Type get_best_buffer_type( bool recv = true );
		int listen()
		{
			return ::listen( m_socket_handle,
#if EV_USE_EPOLL
							 SOMAXCONN
#endif
#if EV_USE_IOCP
							 30
#endif
						   );
		}
		int open( int type, const IP_Address &ip_address );
		int connect( const IP_Address &ip_address, uintptr_t io_key, int time_out = -1 );
		int send( ST_Buffer *buffer_ptr, uintptr_t io_key, int time_out = -1 );
		/** \brief 提出接收数据申请
		 *
		 * \param buffer_ptr ST_Buffer*
		 * \param io_key uintptr_t 与本次操作绑定的键值，由用户随意指定，系统只是负责传送。
		 * \param time_out int 指定接收超时，如果在指定时间內没有收到指定长度的数据，会触发超时错误。
									设定为-1，表示永久等待；
									设定为0，表示不等待，立即读取当前栈中所有可用数据而后返回。此时可以设置 buffer_ptr 为NULL，由返回值得到当前接收缓存內有多少数据。
		 * \return int 不小于零表示成功执行，为获取的数据长度
		 *
		 */
		int recv( ST_Buffer *buffer_ptr, uintptr_t io_key, int time_out = -1 );
        /** \brief 取消io操作
         *
         * \param buffer_ptr ST_Buffer* 指定需要取消的io buffer
         * \note 在windows系统家族内，此函数只在windows vista以上操作系统有效；对于linux系统恒有效
         *
         */
		void cancel( ST_Buffer *buffer_ptr );
        /** \brief 关闭socket
         *
         * \note 如果在对象生存周期内调用 open，则在释放前需要使用本函数释放操作系统的句柄
         *
         */
		void close();
		void set_socket_key(size_t key){
			m_key = key;
		}
		size_t get_socket_key(){
			return m_key;
		}
		///工具类函数
		struct utility
		{
            /** @brief
             *
             * @param socket const CSocket&
             * @param alive_time size_t 当keepalive 打开之后，TCP 要多久送出 keepalive 信息(tcp协议默认值为 2 小时)
             * @param alive_interval size_t 当一个探测(probe)没有获得确认之后，隔多久要被重送(tcp协议默认值为 75 秒)
             * @param alive_probes size_t 在决定挂断联机之前，要送出多少个 keepalive 探测(tcp协议默认值为 9，在Windows下被忽略)
             * @return int
             *
             */
			static int set_keepalive( const CSocket &socket, size_t alive_time, size_t alive_interval, size_t alive_probes );
			static int set_nagle( const CSocket &socket, bool enable );
			static int set_defer_accept_feature( const CSocket &socket, int time_out );/* 只支持*nix平台 */
			static int set_quick_ack( const CSocket &socket, bool enable );
			static int set_cork_feature( const CSocket &socket, bool enable );
			static int get_sock_addr( const CSocket &socket, struct sockaddr *local_addr );
			static int get_peer_addr( const CSocket &socket, struct sockaddr *peer_addr );
		};

	private:
		enum ENM_State
		{
			sIdle, sOpen, sConnecting, sListening, sConnected, sDisconnecting, sFree
		};
#if EV_USE_EPOLL
		typedef ssize_t ( *op_func_t )( int, void *, size_t, int );
#endif
#if EV_USE_IOCP
		typedef int ( *op_func_t )( SOCKET, LPWSABUF, DWORD, LPDWORD, LPDWORD, LPWSAOVERLAPPED, LPWSAOVERLAPPED_COMPLETION_ROUTINE );
#endif
		int op_io( op_func_t op_func, CSocket::ST_Buffer *buffer_ptr );
		int op_open( int type, const struct sockaddr *local_addr );
		int op_connect( const struct sockaddr *serv_addr, int time_out, uintptr_t io_key );

		void send_timeout();
		void recv_timeout();
		void connect_timeout();		int send_hook( ST_Buffer *buffer_ptr, uintptr_t io_key );
		int recv_hook( ST_Buffer *buffer_ptr, uintptr_t io_key );

		/**
		 * @brief 缓冲页面栈
		 * @note 使用列队方式,先进后出,非线程安全
		*/
		class CDataBufferStack
		{
			public:
				void *pop( ST_Buffer::ENM_Type  buffer_type );
				void push( ST_Buffer::ENM_Type buffer_type, void *buffer_ptr );
				CDataBufferStack() {
					memset( m_buffer_queues, 0, sizeof( ST_BufferHeader ) *ST_Buffer::type_count );
				}
			private:
				struct ST_BufferHeader
				{
					ST_BufferHeader() : m_next_buffer( NULL ) {}
					ST_BufferHeader *m_next_buffer;
				};
				ST_BufferHeader m_buffer_queues[ST_Buffer::type_count];
		};		static CDataBufferStack static_m_data_buffer_stack;
		static std::mutex static_m_lock;
		static CTimeQueue static_m_timeout_queue;

		int m_socket_handle;
		ENM_State m_state;
		ST_Buffer *m_recv_buffer_ptr, *m_send_buffer_ptr;
		ST_Callback m_callback;
		size_t m_key;		size_t m_send_size, m_recv_size;		CSpeedGroup *m_speed_group_ptr;

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( NetLayer, Socket_CreateAndDestroy );
		FRIEND_TEST( NetLayer, Socket_OpenAndClose );
		FRIEND_TEST( NetLayer, Socket_AsTCPClient_ConnectRefused );
		FRIEND_TEST( NetLayer, Socket_AsTCPClient_Normal );
#endif
};class CSpeedGroup{	public:		CSpeedGroup(unsigned interval = 1000);		~CSpeedGroup();		void append( CSocket *socket_ptr );		unsigned get_speed( CSocket *socket_ptr, bool recv = true );		void remove( CSocket *socket_ptr );		void set_speed_test_interval( unsigned interval );	private:		void on_tick_time();		static CTimeQueue static_m_ticktime_queue;		struct ST_Socket		{			CSocket *socket_ptr;			size_t send_speed, recv_speed;			bool operator < (const ST_Socket &socket){				return (uintptr_t)socket_ptr < (uintptr_t)socket.socket_ptr;			}		};		CTimeQueue::TIME_HANDLE m_tick_timer;		unsigned m_interval;		std::set<ST_Socket> m_sockets;};

class CEventPoll
{
		friend class CSocket;
	public:
		void poll( int thread_index );
#if EV_USE_IOCP
		HANDLE *get_event_objs() {
			return m_iocp_os_event_objs;
		}
		CSocket::ST_Buffer *get_buffers() {
			return m_iocp_buffers;
		}
#endif

		static CEventPoll &get_instance()
		{
			static CEventPoll instance;
			return instance;
		}
	private:
		CEventPoll()
		{
			init();
			unsigned cpu_num = Netspecters::NSStdLib::OSFunc::getCPUNum();
			for( unsigned i = 0; i < cpu_num; i++ )
			{
				std::thread service_thread( &CEventPoll::service_loop, this, i );
				service_thread.detach();
#if EV_USE_IOCP
				std::thread service_thread_ex( &CEventPoll::ex_service_loop, this, i );
				service_thread_ex.detach();
#endif
			}
		}
		~CEventPoll() {
			fina();
		}
		void init();
		void fina();
		void service_loop( int thread_index ) {
			while( 1 )
				poll( thread_index );
		}
#if EV_USE_IOCP
		void ex_service_loop( int thread_index ) {
			while( 1 )
				ex_poll( thread_index );
		}
#endif
#if EV_USE_EPOLL
		struct ST_EpollEventQueue
		{
			ST_EpollEventQueue() {
				memset( event_queue, 0, sizeof( epoll_event ) * 64 );
			}
			std::recursive_mutex event_queue_mutex;
			epoll_event event_queue[64];
		} *m_epoll_event_queue_ptr;
#endif
		int m_backend_fd;

#if EV_USE_IOCP
		void ex_poll();	//process selected event
		HANDLE m_iocp_os_event_objs[MAXIMUM_WAIT_OBJECTS];
		CSocket::ST_Buffer m_iocp_buffers[MAXIMUM_WAIT_OBJECTS];
#endif
};

} /* NetLayer **/ }/* KernelPlus **/ } /* Netspecters **/

#endif // SOCKET_H_INCLUDED
