#include "time_queue.h"
#include "../../NSErrorCode.h"

using namespace Netspecters::KernelPlus::NetLayer;

#define SC_Jiffies_Length 1000 ///<滴答声长度(时钟精度:ms)

#ifdef USE_STATIC_ARRAY
void *ST_TimeQueueItem::operator new( size_t )
{
	int new_id = -1;

	//查找空白项目
	for( int i = 0; i < ITEM_QUEUE_LENGTH; i++ )
	{
		if( !( static_m_item_array[i].m_jiffies ) )
		{
			new_id = i;
			break;
		}
	}

	//没有足够的空间返回空
	return ( new_id >= 0 ) ? &static_m_item_array[new_id] : NULL;
}

void ST_TimeQueueItem::operator delete( void *item_ptr )
{
	int new_id = reinterpret_cast<uint8_t *>( static_m_item_array ) - reinterpret_cast<uint8_t *>( item_ptr );
	static_m_item_array[new_id].m_callback_func = NULL;
	static_m_item_array[new_id].m_jiffies = 0;
}
#endif

void CTimeQueue::onExecute()
{
	while( m_running )
	{
		std::this_thread::sleep_for(std::chrono::milliseconds( SC_Jiffies_Length ) );
		JiffiesProcess();
	}
}

CTimeQueue::TIME_HANDLE CTimeQueue::registeTimer( int time_out, TTimeNotifyCallbackFunc callback_func )
{
	std::lock_guard<std::recursive_mutex> locker( m_mutex );

	ST_TimeQueueItem &item_struct = *( new ST_TimeQueueItem() );

	//构建时间项目
	item_struct.m_callback_func = callback_func;
	item_struct.m_jiffies = time_out / SC_Jiffies_Length;
	item_struct.next_item_ptr = m_first_item_ptr;
	m_first_item_ptr = &item_struct;

	//插入到时间列队（基于时间差）
	if( item_struct.next_item_ptr )
	{
		if( item_struct.m_jiffies <= item_struct.next_item_ptr->m_jiffies )
			item_struct.next_item_ptr->m_jiffies -= item_struct.m_jiffies;
		else
			while(( item_struct.next_item_ptr != NULL ) && ( item_struct.next_item_ptr->m_jiffies < item_struct.m_jiffies ) )
			{
				item_struct.m_jiffies  -= item_struct.next_item_ptr->m_jiffies;

				//交换参数
				item_struct.exchangeValue( item_struct.next_item_ptr );
			}
	}

	/* 安全激活计时器 */
	if(!m_service_thread_ptr)
	{
		m_running = true;
		m_service_thread_ptr = new std::thread(std::bind(&CTimeQueue::onExecute, this));
	}

	return &item_struct;
}

inline void CTimeQueue::JiffiesProcess()
{
	std::lock_guard<std::recursive_mutex> locker( m_mutex );

	if( m_first_item_ptr )
	{
		m_first_item_ptr->m_jiffies--;
		while(( NULL != m_first_item_ptr ) && ( m_first_item_ptr->m_jiffies <= 0 ) )
		{
			//是否为哑异常
			if( m_first_item_ptr->m_callback_func )
				m_first_item_ptr->m_callback_func();

			//释放
			ST_TimeQueueItem *tmp_item_ptr = m_first_item_ptr;
			m_first_item_ptr = m_first_item_ptr->next_item_ptr;
			delete tmp_item_ptr;
		}
	}
}

void CTimeQueue::unregisteTimer( TIME_HANDLE timer )
{
	std::lock_guard<std::recursive_mutex> locker( m_mutex );

	static_cast<ST_TimeQueueItem *>( timer )->m_callback_func = NULL;
}

CTimeQueue::~CTimeQueue()
{
	/* wait for thread terminate */
	if(m_running)
	{
		m_running = false;
		m_service_thread_ptr->join();
		delete m_service_thread_ptr;
	}

	/* delete all alloced memory block */
	while( m_first_item_ptr )
	{
		ST_TimeQueueItem *tmp_item_ptr = m_first_item_ptr;
		m_first_item_ptr = m_first_item_ptr->next_item_ptr;
		delete tmp_item_ptr;
	}
}
