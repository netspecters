#ifndef _TIMEQUEUE_H_
#define _TIMEQUEUE_H_

#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif
#include <mutex>
#include <thread>
#include <functional>
#include "nslib.h"

namespace Netspecters{ namespace KernelPlus{ namespace NetLayer{

typedef std::function<void(void)> TTimeNotifyCallbackFunc;

///在 CTimeQueue 中使用的队列项结构
struct ST_TimeQueueItem
{
	public:
		void exchangeValue(ST_TimeQueueItem *source_item_ptr)
		{
			int tmp_jiffies = m_jiffies;
			m_jiffies = source_item_ptr->m_jiffies;
			source_item_ptr->m_jiffies = tmp_jiffies;

			TTimeNotifyCallbackFunc tmp_callback_func = m_callback_func;
			m_callback_func = source_item_ptr->m_callback_func;
			source_item_ptr->m_callback_func = tmp_callback_func;
		}

		ST_TimeQueueItem *next_item_ptr;
		int m_jiffies;
		TTimeNotifyCallbackFunc m_callback_func;
//#define USE_STATIC_ARRAY
#ifdef USE_STATIC_ARRAY
		void *operator new(size_t);
		void operator delete(void *);
	private:
		static const int ITEM_QUEUE_LENGTH = 128;
		static ST_TimeQueueItem	static_m_item_array[ SC_ItemQueue_Length ];
#endif
};

///基于时差原理的通知队列
//在linux下可以用select() + timeval结构也可以（最多精确到微秒）或者用pselect函数+timespec(可以精确到纳秒，足够精确了)\n
//在windows下可以用内核定时器以获取更高精度(见delphi程序的实现)
//此版本只是简单的使用sleep实现
class CTimeQueue
{
	public:
		typedef ST_TimeQueueItem* TIME_HANDLE;
		static const unsigned TIME_HANDLE_INVAILD = 0u;

		/**
		 * @brief 注册超时回调函数
		 *
		 * @param time_out 超时时长，单位：毫秒(ms)
		 * @param callback_func 发送超时后的回调函数
		 *
		 * @return 控制句柄
		 * @see unregisteTimer
		 */
		TIME_HANDLE registeTimer(int time_out, TTimeNotifyCallbackFunc callback_func);
		void unregisteTimer(TIME_HANDLE timer);

	public:
		CTimeQueue() : m_first_item_ptr(NULL), m_service_thread_ptr(NULL), m_running(false) {}
		CTimeQueue( const CTimeQueue &) = delete;
		~CTimeQueue();

	protected:
		void onExecute();

	private:
		std::recursive_mutex m_mutex;
		ST_TimeQueueItem	*m_first_item_ptr;
		std::thread *m_service_thread_ptr;
		bool m_running;

		void TimeProcess();
		void JiffiesProcess();

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( NetLayer, TimeQueue_RegisteTimer );
		FRIEND_TEST( NetLayer, TimeQueue_UnregisteTimer );
#endif
};

} /* NetLayer **/} /* KernelPlus **/ } /* Netspecters **/

#endif//_TIMEQUEUE_H_
