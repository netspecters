#include <winsock2.h>
#include <mswsock.h>

#include "socket.h"

static int selectevent_add( HANDLE fd, CSocket::ST_Buffer *iocp_buffer, int type )
{
	/* find an unused event object */
	int index = -1;
	CSocket::ST_Buffer *buffers = CEventPoll::get_instance().get_buffers();
	for( i = 1; i < MAXIMUM_WAIT_OBJECTS; i++ )
		if( !buffers[i] )
		{
			index = i;
			break;
		}

	if( index > 0 )
	{
		buffers[index] = iocp_buffer;
		WSAEventSelect( fd, CEventPoll::get_instance().get_event_objs()[index], type );
	}
}

static void selectevent_del( int index )
{
	CEventPoll::get_instance().get_buffers()[index] = NULL;
}

void CEventPoll::init()
{
	m_backend_fd = ( int )CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 0 );
	for( unsigned i = 0; i < MAXIMUM_WAIT_OBJECTS; i++ )
		m_iocp_os_event_objs[i] = CreateEvent( NULL, FALSE, FALSE, NULL );
}

void CEventPoll::fina()
{
	CloseHandle( m_backend_fd );
}

CEventPoll::poll(int thread_index)
{
	DWORD bytes_transferred;
	DWORD fd;
	CSocket::ST_Buffer *iocp_buffer_ptr = NULL;
	BOOL success = GetQueuedCompletionStatus( m_backend_fd, &bytes_transferred, &fd, ( OVERLAPPED ** )&iocp_buffer_ptr, INFINITE );
	DWORD last_error = WSAGetLastError();

	if( !success )
	{
		if( NULL != iocp_buffer_ptr )	/* system dequeues a completion packet for a failed I/O operation from the completion port */
		{
			CSocket *socket_ptr = iocp_buffer_ptr->m_socket_ptr;
			if( ERROR_SUCCESS == last_error )	/* socket */
				;
			else
				switch( last_error )
				{
					case ERROR_NETNAME_DELETED:	/* client closed by remote */
						socket_ptr->m_callback.error( socket_ptr, ST_Callback::ecConnrest );
						break;
					case ERROR_OPERATION_ABORTED:
						if( ( CTimeQueue::TIME_HANDLE )0xffffffff != iocp_buffer_ptr->m_timer )
							socket_ptr->m_callback.error( socket_ptr, ST_Callback::ecOPAborted );
						break;
				}
		}
		else
		{}
	}
	else if (iocp_buffer_ptr)
	{
		if ( iocp_buffer_ptr == iocp_buffer_ptr->m_socket_ptr->m_recv_buffer_ptr)
			iocp_buffer_ptr->m_socket_ptr->m_callback.recv( iocp_buffer_ptr->m_socket_ptr, iocp_buffer_ptr, iocp_buffer_ptr->m_io_key );
		else if ( iocp_buffer_ptr == iocp_buffer_ptr->m_socket_ptr->m_send_buffer_ptr)
			iocp_buffer_ptr->m_socket_ptr->m_callback.send( iocp_buffer_ptr->m_socket_ptr, iocp_buffer_ptr, iocp_buffer_ptr->m_io_key );		else			delete iocp_buffer_ptr;
	}
}

void CEventPoll::ex_poll(int thread_index)
{
	int index = WSAWaitForMultipleEvents( MAXIMUM_WAIT_OBJECTS, m_iocp_os_event_objs, FALSE, -1, FALSE );
	if( WAIT_FAILED == index || WAIT_TIMEOUT == index )
	{
		if( WAIT_FAILED == index ) ev_syserr( "WSAWaitForMultipleEvents" );
		return;
	}

	index -= WSA_WAIT_EVENT_0;
	if( index > 0 )
	{
		struct select_event *select_event_ptr = ( struct select_event * )m_iocp_events[index];
		if( select_event_ptr )
		{
			CSocket *socket_ptr = select_event_ptr->m_socket_ptr;
			WSANETWORKEVENTS net_events;
			int net_errno = 0;
			if( SOCKET_ERROR != WSAEnumNetworkEvents( socket_ptr->m_socket_handle, m_iocp_os_event_objs[index], &net_events ) )
			{
				if( net_events.lNetworkEvents & FD_CONNECT )
				{
					net_errno = net_events.iErrorCode [FD_CONNECT_BIT];
					if( 0 == net_errno )
						socket_ptr->m_callback.connect(socket_ptr, select_event_ptr->m_io_key);
					else
						socket_ptr->m_callback.error(socket_ptr, CSocket::ST_Buffer::ecConnect);
				}
				else if( net_events.lNetworkEvents & FD_ACCEPT )
				{
					net_errno = net_events.iErrorCode [FD_ACCEPT_BIT];
					if( 0 == net_errno )
					{
						sockaddr_in sockaddr_struct;
						memset( &sockaddr_struct, 0, sizeof( sockaddr_in ) );
						int address_length = sizeof( struct sockaddr );
						::accept( socket_ptr->m_socket_handle, (sockaddr *)sockaddr_struct, &address_length );
						/* refresh attribute of socket (only use in acceptex call) */
						//setsockopt( m_fd, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, (char*)&remote_sock, sizeof(remote_sock) );
						CSocket *accepted_socket_ptr = new CSocket(socket_ptr->m_callback);
						IP_Address ip_addr = { sockaddr_struct.sin_addr.s_addr, sockaddr_struct.sin_port };
						socket_ptr->m_callback.accept( accepted_socket_ptr, ip_addr );
					}
				}
			}
		}
	}
}

int CSocket::op_open( int type, const struct sockaddr *local_addr )
{
	SOCKET new_sock = WSASocket( AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );
	if( new_sock )
	{
		m_socket_handle = new_sock;

		/* set the socket's flage which is 'SO_REUSEADDR' */
		int flage = 1;
		setsockopt( new_sock, SOL_SOCKET, SO_REUSEADDR, ( const char * )&flage, sizeof( int ) );

		/* bind the socket */
		if( SOCKET_ERROR == bind( new_sock, local_addr, sizeof( struct sockaddr ) ) )
			goto failed;

		/* assigned to iocp */
		CreateIoCompletionPort( new_sock, loop_ptr->backend_fd, 0, 0 );
	}
	return ( int )new_sock;

failed:
	m_socket_handle = 0;
	closesocket( new_sock );
	return 0;
}

int CSocket::op_connect( const struct sockaddr *serv_addr, int time_out, uintptr_t io_key )
{
	m_send_buffer_ptr = new ST_Buffer;
	m_send_buffer_ptr->m_io_key = io_key;

	/* listen socket's connect event */
	int rst = selectevent_add( m_socket_handle, m_send_buffer_ptr, FD_CONNECT );
	if( rst >= 0 )
	{
		if( ::connect( m_socket_handle, serv_addr, sizeof( struct sockaddr ) ) )
			if( WSAEWOULDBLOCK != WSAGetLastError() )
			{
				selectevent_del( rst );
				return -1;
			}

		/* set time out */
		if( time_out > 0 )
			m_send_buffer_ptr->m_timer = static_m_time_queue.registeTimer( time_out, std::bind( &CSocket::connect_timeout, this ) );
		else
			m_send_buffer_ptr->m_timer = CTimeQueue::TIME_HANDLE_INVAILD;
	}
	return rst;
}

void CSocket::cancel( ST_Buffer *buffer_ptr )
{
	void *func_cancelIO_ptr = ( void * ) GetProcAddress( GetModuleHandleA( "KERNEL32" ), "CancelIoEx" );
	if( func_cancelIO_ptr )
	{
		typedef bool ( WINAPI * TCancelIOEx )( SOCKET, void * );
		TCancelIOEx cancelIOExFunc = ( TCancelIOEx ) func_cancelIO_ptr;

		/* cancel recv operation */
		cancelIOExFunc( m_socket_handle, buffer_ptr );
	}
}

int CSocket::op_io( op_func_t op_func, CSocket::ST_Buffer *buffer_ptr )
{
	/* init iocp overlapped struct */
	struct iocp_event *iocp_event_ptr = ( struct iocp_event * )( op_func == WSARecv ? m_read_iocp_event_ptr : m_write_iocp_event_ptr );
	memset( iocp_event_ptr, 0, sizeof( struct iocp_event ) );
	iocp_event_ptr->overlapped.hEvent = m_ev_ctrl.m_iocp_os_event_objs[0];
	iocp_event_ptr->op_code = type;
	iocp_event_ptr->socket_ptr = this;

	/* do io operate */
	DWORD flags = 0, imp_size = 0;
	struct ev_buf *buf_ptr = type == EV_READ ? m_read_buf_ptr : m_write_buf_ptr;
	int wsa_result = op_func( m_socket_handle, &buf_ptr->wsa_buf, 1, &imp_size, &flags, &iocp_event_ptr, NULL );
	if((( 0 == wsa_result ) && ( imp_size > 0 ) ) || (( SOCKET_ERROR == wsa_result ) && ( WSAGetLastError() == WSA_IO_PENDING ) ) )
		return 1;
}

void CSocket::close()
{
	/* shutdown the socket gently */
	shutdown( m_socket_handle, SD_BOTH );

	/* use 'SO_LINGER' to avoid close timeout */
	linger so_linger = {1, 0};
	setsockopt( m_socket_handle, SOL_SOCKET, SO_LINGER, ( const char * )( &so_linger ), sizeof( linger ) );

	/* now, we close the socket's handle */
	closesocket( m_socket_handle );

	m_state = sFree;
}
