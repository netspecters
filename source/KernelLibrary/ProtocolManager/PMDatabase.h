#ifndef  pmdatabase_INC
#define  pmdatabase_INC

#include <string>
#include <stdio.h>
#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif

#include "nslib.h"
#include "Interfaces.h"

namespace Netspecters { namespace KernelPlus { namespace ProtocolManager {

using std::string;
using Netspecters::NSStdLib::CAlloctor;

class CPMDatabase : public CAlloctor
{
#define DB_SUPER_BLOCK_PTR ((ST_DBSuperBlock*)CAlloctor::get_ptr(CAlloctor::get_space_base()))
	public:
		typedef CAlloctor::ENM_FileState ENM_FileState;

		/* 任务控制块 */
		typedef struct ST_TaskControlBlockStore : public CAlloctor::ST_ListItem
		{
			TRelativeAddress	pcb_store;	///< 任务(所属)对应的协议
			TRelativeAddress	gcb_store;	///< 任务所属组
			size_t data_size;				///< 任务数据长度
			TRelativeAddress data;		///< 任务数据
		} ST_TCB_Store;

		/* 协议控制块 */
		typedef struct ST_ProtocolControlBlockStore : public CAlloctor::ST_ListItem
		{
			uint32_t protocol_id;
			TRelativeAddress task_list_header;
		} ST_PCB_Store;

		/* 组控制块 */
		typedef struct ST_GroupControlBlockStore : public CAlloctor::ST_ListItem
		{
			uint32_t			group_id;
			uintptr_t			user_data;		///< 由用户自定义的标识
			size_t				data_size;		///< 组数据长度
			TRelativeAddress	data;			///< 存储如下数据：数据段界限
			size_t				filename_len;
			char				filename[];	///< 组对应的文件(不包含NULL)
		} ST_GCB_Store;

		struct ST_DBSuperBlock
		{
			TRelativeAddress protocol_list_header, group_list_header;
		};

		ENM_FileState load_file( const string &file_name )
		{
			ENM_FileState result = CAlloctor::load_file( file_name );
			if( fsOpenNew == result )
			{
				/* 新建文件要初始化超级块，定义超级快就是第一个分配块 */
				ST_DBSuperBlock *super_block_ptr = ( ST_DBSuperBlock * )malloc( sizeof( ST_DBSuperBlock ) );
				memset( super_block_ptr, 0, sizeof( ST_DBSuperBlock ) );
			}
			return result;
		}

        /** @brief 分配协议存储结构
         *
         * @param protocol_id uint32_t 协议id(一般为协议标识的hash值)
         * @return ST_PCB_Store*
         * @note 分配的存储结构会自动的加入到协议列表中
         *
         */
		ST_PCB_Store *malloc_protocol_space( uint32_t protocol_id );
		ST_GCB_Store *malloc_group_space( uint32_t group_id, const string &group_filename );
		ST_TCB_Store *malloc_task_space( ST_PCB_Store *pcb_store_ptr, ST_GCB_Store *gcb_store_ptr, size_t init_data_size = 0 );

		/* 遍历相关链表 */
		static const long C_First_Cookie = -1;
		ST_PCB_Store *find_next_protocol( long &cookie ) {
			return ( ST_PCB_Store * )get_next_in_list( DB_SUPER_BLOCK_PTR->protocol_list_header, cookie );
		}
		ST_TCB_Store *find_next_task_in_protocol( ST_PCB_Store *pcb_store_ptr, long &cookie ) {
			return ( ST_TCB_Store * )get_next_in_list( pcb_store_ptr->task_list_header, cookie );
		}

		ST_PCB_Store *find_protocol( uint32_t protocol_id );

		/* 读写相关数据 */
		template <typename T> size_t read_data( T *store_ptr, void *buffer_ptr )
		{
			size_t read_size = store_ptr->data_size;
			if( buffer_ptr && read_size > 0 )
				memcpy( buffer_ptr, get_ptr( store_ptr->data ), read_size );
			return read_size;
		}
		template <typename T> size_t write_data( T *store_ptr, const void *data_ptr, size_t data_size )
		{
			void *data_space_ptr = get_ptr( store_ptr->data );
			if( store_ptr->data_size < data_size )
			{
				void *new_addr = realloc( data_space_ptr, data_size, false );
				store_ptr->data = get_relative_address( new_addr );
			}
			memcpy( data_space_ptr, data_ptr, data_size );
			store_ptr->data_size = data_size;
			return data_size;
		}

		void *lock_block( TRelativeAddress ra ){
			return get_ptr(ra);
		}

		void free_protocol_space( ST_PCB_Store *pcb_store_ptr );	//必须保证协议已经没有任务了，否则会导致失败
		void free_group_space( ST_GCB_Store *gcb_store_ptr );
		void free_task_space( ST_TCB_Store *tcb_store_ptr );

	private:
		CAlloctor::ST_ListItem *get_next_in_list( const TRelativeAddress header, long &cookie );

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( ProtocolManager, PMDatabase_Load );
		FRIEND_TEST( ProtocolManager, PMDatabase_AllocAndFreeBlock );
		FRIEND_TEST( ProtocolManager, PMDatabase_AddAndDeleteGroup_Protocol );
		FRIEND_TEST( ProtocolManager, PMDatabase_AddAndDeleteTask );
#endif
};

}/* ProtocolManager **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef pmdatabase_INC  ----- */
