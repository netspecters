#ifndef  PROTOCOL_MANAGER_INTERFACES_INC
#define  PROTOCOL_MANAGER_INTERFACES_INC

#include "../../Core/PlusInterface.h"
#include "../NetLayer/Interfaces.h"
#include "../DataContainer/Interfaces.h"
#include <cstring>
#include <unistd.h>
#include <stddef.h>

namespace Netspecters{ namespace KernelPlus{ namespace ProtocolManager{

using namespace Netspecters::KernelPlus;
using namespace DataContainer;
using namespace NetLayer;

typedef uintptr_t GROUP_HANDLE;
typedef uintptr_t GROUP_TASK_HANDLE;
typedef uintptr_t TASK_HANDLE;

class ITaskGroupManager;

/**
 * @brief 协议接口
 */
class IProtocol : public INSPlus
{
	public:
		/**
		 * @brief 任务状态监视器
		 * @note 如果不注册Monitor，task的状态要通过查询获得
		 */
		class ITaskMonitor
		{
			public:
				virtual NSAPI int onFinish(void) = 0;
				virtual NSAPI int onError(void) = 0;
		};

		///任务资源
		struct ST_TaskResource
		{
			int max_thread_num;
			int retry_limit;
			int proxy_type;
			int proxy_port;
			int host_port;

			unsigned res_path_offset, referer_uri_offset;
			unsigned login_usr_name_offset,login_passwd_offset;
			unsigned proxy_address_offset;
			unsigned cookie_data_offset;
			unsigned extend_data_offset;
			char resource_data[];
		};

		///任务状态数据(用于序列化)
		struct ST_TaskStateData
		{
			int data_size;	///< data_size指示data数组字节长度
			uint8_t data[];
		} __attribute__(( packed ));

		///任务初始化参数
		struct ST_TaskInitParam
		{
			int		selector_id;
			bool	enable_hooker;

			ST_TaskResource	*task_resource_ptr;
			ST_TaskStateData	*init_task_state_data_ptr;
		};

		enum ENM_TaskStopCode
		{
			tscRunning, tscError, tscPause
		};

		///任务信息
		struct ST_TaskInformation
		{
			unsigned thread_count;								///< 总线程计数（包含所有的线程）
			unsigned transfer_speed_up, transfer_speed_down;	/**< 任务运行速度 */
			unsigned send_data_size, recved_data_size;			/**< 从任务开始到现在总共传输的字节计数 */
		};

		/**
		 * @brief 启动任务
		 * @param task_init_param 任务初始化参数
		 * @param group_task_handle 任务所在组的任务组句柄
		 * @param task_group_manager_ptr 负责管理此任务组的任务组管理器指针
		 * @param data_container_ptr 任务组拥有的数据容器指针
		 * @param task_monitor_ptr 任务监视器指针
		 * @return 如果成功，返回任务句柄，否则返回 NS_INVALID_HANDLE
		 * @note 当 task_monitor_ptr 为空的时候，不使用状态回调接口，task的状态由 queryTaskInformation 得到
		 * @see queryTaskInformation
		 */
		virtual NSAPI TASK_HANDLE startTask( const ST_TaskInitParam &task_init_param,
												GROUP_TASK_HANDLE group_task_handle,
												IDataContainer *data_container_ptr,
												ITaskMonitor *task_monitor_ptr ) = 0;
		/**
		 * @brief 停止任务
		*/
		virtual NSAPI void stopTask( TASK_HANDLE task_handle ) = 0;
		virtual NSAPI ENM_TaskStopCode getStopCode( TASK_HANDLE task_handle ) = 0;
		/**
		 * @brief 查询任务信息
		 * @param task_handle 任务句柄
		 * @param task_infor_ptr 接收任务信息的数据区，如果为空，表示不查询任务信息
		 * @param thread_infor_ptr 接收线程信息的数据区，如果为空，表示不查询线程信息
		 * @return 成功查询后，返回 NSEC_SUCCESS， 否则返回 NSEC_NSERROR
		 * @note 首先令 thread_infor_ptr 为空，根据 task_infor_ptr 中指示的线程数开辟线程信息接收缓存，然后令 task_infor_ptr 为空，查询线程信息
		 */
		virtual NSAPI int queryTaskInformation( TASK_HANDLE task_handle, ST_TaskInformation *task_infor_ptr ) = 0;

		/**
		 * @brief 删除任务
		 * @param task_handle 任务句柄
		 * @note 对于没有停止的任务，本函数会先停止任务，然后再删除任务
		 * @note group调用此函数后将认为任务已经被成功删除，因此在实现中此函数应该是阻塞的，至少也应该是模拟阻塞
		 * @see startTask stopTask
		*/
		virtual NSAPI void delTask( TASK_HANDLE task_handle ) = 0;
};

/**
 * @brief 协议钩
*/
class IProtocolHook
{
	public:
		enum ENM_HookResult
		{
			hrPass,				/**< 略过本hook */
			hrNewParam,			/**< 使用 new_task_init_param 作为参数继续运行 */
			hrProtocolChange,	/**< 使用 new_task_init_param 参数，但协议已经改变，需要重新选择合适的协议 */
			hrStop				/**< 任务被终止 */
		};

		/* 如果hook成功，hooker负责返回new_task_init_param_ptr，ns系统将放弃使用old_task_init_param，转而使用new_task_init_param_ptr继续工作 */
		virtual ENM_HookResult onHookBegin( const IProtocol::ST_TaskInitParam &old_task_init_param, IProtocol::ST_TaskInitParam *&new_task_init_param_ptr ) = 0;
		virtual void onHookEnd(IProtocol::ST_TaskInitParam *new_task_init_param_ptr) = 0;
};

/**
 * @brief 任务组管理器
*/
class ITaskGroupManager
{
	public:
		static const int GROUPS_HANDLE = -1;

		enum ENM_GroupState
		{
			gsNone,		///< 用于获取组状态
			gsRunning,	///< 表明组在运行
			gsPasue,	///< 表明组暂停
			gsError		///< 有错误阻止组继续运行
		};

		///任务组信息
		struct ST_GroupInformation
		{
			uint32_t group_id;							///< TaskGroupManager为组分配的组id(id在组生存期内保证唯一)
			uintptr_t user_tag;							///< 在创建组时，用户自己为组设定的标识
			off_t group_file_size;						///< 组文件大小
			unsigned task_count;						/**< 组中任务的个数
																	@note 没有任务意味着组暂停、错误状态
																			错误状态：组的错误码不为 'None'
																			暂停状态：既不是错误状态也不是完成状态
														*/
			unsigned send_data_speed, recv_data_speed;	///< 发送和接收数据的整体瞬间速率
			off_t send_data_size, recved_data_size;		///< 自组建立以来发送和接收数据总长度
			char group_file_name_str[];					///< 组文件名(以NULL结尾的字符串)
		};

		/**
		 * @brief 添加一个新组
		 * @param group_file_name_str_ptr 任务组对应的文件在文件系统中的路径.NULL指示将文件保存在内存中.
		 * @param visable 任务组是否可见
		 * @param group_tag 用户定义数据，会被持久化
		 * @note 添加后是suspend的状态，需要手动resume\n
		 *	group在重启还原后是suspend状态，需要手动resume\n
		 *	非可视组（visable == false）NS系统不负责持久化\n
		 *	group_file_name_str_ptr必须是指向完整文件路径的utf-8字符数组指针
		*/
		virtual NSAPI GROUP_HANDLE addGroup( const char *group_file_name_str_ptr, bool visable, uintptr_t group_tag ) = 0;
		/**
		 * @brief 删除一个组
		 * @param group_handle 任务组句柄
		 * @param delete_group_file 是否在删除组的同时删除组对应的物理文件
		 * @note 要在suspend状态才可以delete，组完成后会自动删除自己
		 */
		virtual NSAPI void delGroup(GROUP_HANDLE group_handle, bool delete_group_file) = 0;
		/**
		 * @brief 获取有效组句柄列表
		 * @param group_handle_buf_ptr 接收缓冲区指针
		 *	- NULL：只返回有效组句柄个数
		 * @return 有效组句柄个数
		*/
		virtual NSAPI size_t getGroupHandleList( GROUP_HANDLE *group_handle_buf_ptr ) = 0;
		/**
		 * @brief 根据组句柄获得组ID
		 * @return 返回0，表示此组不存在
		 * @note 组ID在组的生命周期内不变(具有持久性)
		*/
		virtual NSAPI uint32_t getGroupID( GROUP_HANDLE group_handle ) = 0;
		/**
		 * @brief 统计组信息
		 * @param group_handle 指定需要获得的组句柄；使用‘NS_INVALID_HANDLE’，指示函数获取所有有效组信息
		 * @param group_infor_buf_ptr 接收信息缓冲区指针
		 *	- NULL：忽略group_handle参数，只返回有效组个数
		 * @return 有效组个数
		*/
		virtual NSAPI size_t getGroupStatistics( GROUP_HANDLE group_handle, ST_GroupInformation *group_infor_buf_ptr ) = 0;
        /**
         * @brief 获取组详细信息
         *
         * @param group_handle 指定需要获得的组句柄，NS_INVALID_HANDLE 为无效值
         * @param group_details_buf_ptr 接收组详细信息的缓冲区，缓冲区內数据格式：|task information|information for the threads of this task|...|task information|information for the threads of this task|
         * @return 如果 group_details_buf_ptr 为 NULL，返回需要的最大数据区长度（单位：字节），否则返回实际写入buffer的数据长度
         */
		virtual NSAPI size_t getGroupDetails( GROUP_HANDLE group_handle, void *group_details_buf_ptr ) = 0;
		///向组中添加一个协议任务,返回的句柄是 task group manager 使用的
		virtual NSAPI GROUP_TASK_HANDLE addProtocolTaskToGroup( GROUP_HANDLE group_handle, const IProtocol::ST_TaskInitParam &task_init_param ) = 0;
		///从组中移除一个协议任务
		virtual NSAPI void removeProtocolTask( GROUP_TASK_HANDLE group_task_handle ) = 0;
		///切换组运行状态 suspend/resume
		virtual NSAPI ENM_GroupState modifyGroupState( GROUP_HANDLE group_handle, ENM_GroupState new_group_state ) = 0;
		virtual NSAPI size_t getTaskStopCodes( GROUP_HANDLE group_handle, IProtocol::ENM_TaskStopCode *task_stop_code_buffer ) = 0;
		/**
		 * @brief 请求 task group manager 存储任务状态信息
		 * @param group_task_handle 组任务句柄（由 addProtocolTaskToGroup 返回）
		 * @param task_state_data_ptr 需要保存的任务数据指针，必须有效
		 * @return 实际保存的数据长度
		 * @see addProtocolTaskToGroup
		*/
		virtual NSAPI int storeTaskStateData( GROUP_TASK_HANDLE group_task_handle, IProtocol::ST_TaskStateData *task_state_data_ptr ) = 0;
        /** @brief 打开一个数据段
         * 请求管理器打开一个可用的数据段
         * @param segment_type 希望以何种模式打开数据段
         * @param segment_ptr [out]保存打开的数据段指针
         * @return
         *
         */
		virtual NSAPI int openSegment( GROUP_TASK_HANDLE group_task_handle, ISegment::ST_SegmentInfor::ENM_SegmentType segment_type, ISegment *&segment_ptr ) = 0;
		///关闭以打开的数据段
		virtual NSAPI void closeSegment( GROUP_TASK_HANDLE group_task_handle, ISegment *segment_ptr ) = 0;
};

class IProtocolManager : public INSPlus
{
	public:
		virtual NSAPI int registerProtocolHook( const void *protocol_tag_ptr, IProtocolHook *protocol_hook_ptr ) = 0;
		/**
		 * @brief 注册一个协议
		 * @param protocol_tag_ptr 协议标识的crc码.如HTTP协议的标识就是http
		 * @param protocol_selector_id 协议选择器的索引码
		 * @param protocol_ptr 协议接口指针.
		 * @return 如果成功，返回tag_hash_code，否则返回NSEC_NSERROR
		*/
		virtual NSAPI int registerProtocol( const void *protocol_tag_ptr, int protocol_selector_id, IProtocol *protocol_ptr ) = 0;
		virtual NSAPI void unregisterProtocol(void *protocol_tag_ptr, int protocol_selector_id) = 0;
		virtual NSAPI IProtocol *queryProtocol( const void *protocol_tag_ptr ) =  0;
		virtual NSAPI ITaskGroupManager *getTaskGroupManager(void) = 0;
};

}/* ProtocolManager **/}/* KernelPlus **/}/* Netspecters **/

#endif   /* ----- #ifndef PROTOCOL_MANAGER_INTERFACES_INC  ----- */
