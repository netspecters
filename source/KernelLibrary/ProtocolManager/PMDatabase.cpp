#include <cstdio>
#include <utility>
#include <assert.h>
#include "../../StdLib/NSFC.h"
#include "PMDatabase.h"

using namespace Netspecters::KernelPlus::ProtocolManager;

CPMDatabase::ST_PCB_Store *CPMDatabase::malloc_protocol_space( uint32_t protocol_id )
{
	ST_PCB_Store *pcb_store_ptr = ( ST_PCB_Store * )CAlloctor::malloc( sizeof( ST_PCB_Store ) );
	add_to_list( DB_SUPER_BLOCK_PTR->protocol_list_header, get_relative_address( pcb_store_ptr ) );
	pcb_store_ptr->protocol_id = protocol_id;
	pcb_store_ptr->task_list_header = 0;
	return pcb_store_ptr;
}

CPMDatabase::ST_GCB_Store *CPMDatabase::malloc_group_space( uint32_t group_id, const string &group_filename )
{
	ST_GCB_Store *gcb_store_ptr = ( ST_GCB_Store * )CAlloctor::malloc( sizeof( ST_GCB_Store ) + group_filename.size() );
	add_to_list( DB_SUPER_BLOCK_PTR->group_list_header, get_relative_address( gcb_store_ptr ) );
	gcb_store_ptr->group_id = group_id;
	gcb_store_ptr->data_size = 0;
	gcb_store_ptr->data = 0;
	gcb_store_ptr->filename_len = group_filename.size();
	memcpy( gcb_store_ptr->filename, group_filename.data(), gcb_store_ptr->filename_len );
	return gcb_store_ptr;
}

CPMDatabase::ST_TCB_Store *CPMDatabase::malloc_task_space( ST_PCB_Store *pcb_store_ptr, ST_GCB_Store *gcb_store_ptr, size_t init_data_size )
{
	ST_TCB_Store *tcb_store_ptr = ( ST_TCB_Store * )CAlloctor::malloc( sizeof( ST_TCB_Store ) + init_data_size );
	add_to_list( pcb_store_ptr->task_list_header, get_relative_address( tcb_store_ptr ) );	//挂入协议任务列表中
	tcb_store_ptr->pcb_store = get_relative_address( pcb_store_ptr );
	tcb_store_ptr->gcb_store = get_relative_address( gcb_store_ptr );
	tcb_store_ptr->data_size = init_data_size;
	tcb_store_ptr->data = init_data_size > 0 ? get_relative_address( CAlloctor::malloc( init_data_size ) ) : 0;
	return tcb_store_ptr;
}

CAlloctor::ST_ListItem *CPMDatabase::get_next_in_list( const TRelativeAddress header, long &cookie )
{
	CAlloctor::ST_ListItem *result = NULL;
	if( header > 0 )
	{
		if( C_First_Cookie == cookie )
		{
			result = ( CAlloctor::ST_ListItem * )get_ptr( header );
			cookie = result->next;
		}
		else if( cookie > 0 )
		{
			result = ( CAlloctor::ST_ListItem * )get_ptr( cookie );
			cookie = result->next;
		}
		else
			cookie = C_First_Cookie;
	}
	return result;
}

CPMDatabase::ST_PCB_Store *CPMDatabase::find_protocol( uint32_t protocol_id )
{
	long cookie = C_First_Cookie;
	ST_PCB_Store *result;
	while( result = find_next_protocol( cookie ) )
		if( result->protocol_id == protocol_id )
			break;

	return result;
}

void CPMDatabase::free_protocol_space( ST_PCB_Store *pcb_store_ptr )
{
	assert( pcb_store_ptr->task_list_header == 0 );
	remove_from_list( DB_SUPER_BLOCK_PTR->protocol_list_header, get_relative_address( pcb_store_ptr ) );
	CAlloctor::free( pcb_store_ptr );
}
void CPMDatabase::free_group_space( ST_GCB_Store *gcb_store_ptr )
{
	if( gcb_store_ptr->data > 0 )
		CAlloctor::free( get_ptr( gcb_store_ptr->data ) );
	remove_from_list( DB_SUPER_BLOCK_PTR->group_list_header, get_relative_address( gcb_store_ptr ) );
	CAlloctor::free( gcb_store_ptr );
}
void CPMDatabase::free_task_space( ST_TCB_Store *tcb_store_ptr )
{
	if( tcb_store_ptr->data > 0 )
		CAlloctor::free( get_ptr( tcb_store_ptr->data ) );
	remove_from_list((( ST_PCB_Store * )get_ptr( tcb_store_ptr->pcb_store ) )->task_list_header, get_relative_address( tcb_store_ptr ) );
	CAlloctor::free( tcb_store_ptr );
}
