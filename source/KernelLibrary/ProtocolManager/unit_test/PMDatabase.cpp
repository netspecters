#include <gtest/gtest.h>
#include <stdio.h>
#include "../PMDatabase.h"
#include "public.hpp"

namespace Netspecters {
namespace KernelPlus {
namespace ProtocolManager {

class __pm_db_helper
{
	public:
		__pm_db_helper( void )
		{
			while( 1 )
			{
				/* 加载数据库文件 */
				CPMDatabase::ENM_FileState file_state = database.load_file( DB_FILENAME );
				EXPECT_EQ( CPMDatabase::fsOpenNew, file_state );	/* 此前，数据库文件不应该存在 */
				if( CPMDatabase::fsOpenNew != file_state )
					remove( DB_FILENAME );
				else
					break;
			}
		}
		~__pm_db_helper( void )
		{
			/* 删除数据块文件 */
			remove( DB_FILENAME );
		}

		CPMDatabase database;
};

static bool enable_continue = true;

#define GET_BLOCK_HEADER(x)			(pm_database.getObjPtrFromOffset<CPMDatabase::ST_BlockHeader>(x))
#define DEFAULT_UNUSED_BLOCK_SIZE	(DEFAULT_FILE_SIZE - sizeof( CPMDatabase::ST_FileHeader ) - sizeof( CPMDatabase::ST_BlockHeader ))

#define INDEX_INFORMATION " with i = (" << i << ")"

#define TEST_BEGIN()\
	ASSERT_TRUE( enable_continue );\
	__pm_db_helper db_helper;\
	CPMDatabase &pm_database = db_helper.database;\
	enable_continue = false
#define TEST_END()\
	enable_continue = true

TEST( ProtocolManager, PMDatabase_Load )
{
	TEST_BEGIN();

	//CPMDatabase::ST_DBSuperBlock *db_super_block_ptr = DB_SUPER_BLOCK_PTR;

	TEST_END();
}

/* 添加删除组和协议的代码基本相同，操作组的代码更复杂些，只测试组操作代码就可以了 */
TEST( ProtocolManager, PMDatabase_AddAndDeleteGroup_Protocol )
{
	TEST_END();
}

TEST( ProtocolManager, PMDatabase_AddAndDeleteTask )
{
	TEST_BEGIN();


	TEST_END();
}

TEST( ProtocolManager, PMDatabase_FindGroup )
{
	TEST_BEGIN();


	TEST_END();
}

/* 枚举组和枚举协议的实现代码完全相同，只测试一个就行了 */
TEST( ProtocolManager, PMDatabase_EnumProtocol_Group )
{
	TEST_BEGIN();


	TEST_END();
}

#undef GET_BLOCK_HEADER

}/* ProtocolManager **/
}/* KernelPlus **/
}/* Netspecters **/
