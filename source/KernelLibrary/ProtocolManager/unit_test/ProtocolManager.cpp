#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <boost/format.hpp>
#include <stdio.h>
#include <string>
#include "../ProtocolManager.h"
#include "public.hpp"

namespace Netspecters {
namespace KernelPlus {
namespace ProtocolManager {

#define GROUP_FILENAME_BASE "pm_unit_test.tmp"
#ifdef __linux
#	define GROUP_FILENAME "/tmp/"GROUP_FILENAME_BASE
#else
#	define GROUP_FILENAME GROUP_FILENAME_BASE
#endif

using ::testing::_;
using ::testing::Invoke;
using ::testing::Return;

struct __pm_instance_helper
{
	__pm_instance_helper( void )
	{
		while( 1 )
		{
			/* 加载数据库文件 */
			CPMDatabase::ENM_FileState file_state = pm_instance.m_pm_database.load_file( DB_FILENAME );
			EXPECT_EQ( CPMDatabase::fsOpenNew, file_state );	/* 此前，数据库文件不应该存在 */
			if( CPMDatabase::fsOpenNew != file_state )
				remove( DB_FILENAME );
			else
				break;
		}
	}
	~__pm_instance_helper( void )
	{
		/* 删除数据块文件 */
		remove( DB_FILENAME );
	}

	CProtocolManager pm_instance;
};

struct __group_file_scope /* 用于确保在退出测试时删除组文件 */
{
	~__group_file_scope( void )
	{
		remove( GROUP_FILENAME );
	}
};

static bool enable_continue = true;
static const char protocol_tag[] = {"TestProtocol"};
static uint32_t protocol_tag_hash_code = 2173579351u;

#define TEST_BEGIN()\
	ASSERT_TRUE( enable_continue );\
	__pm_instance_helper pm_helper;\
	CProtocolManager &pm_instance = pm_helper.pm_instance;\
	enable_continue = false
#define TEST_END()\
	enable_continue = true

TEST( ProtocolManager, CreateAndDestroy )
{
	CProtocolManager pm_instance;
	ASSERT_EQ( 5000, pm_instance.m_default_time_out );

	enable_continue = true;
}

TEST( ProtocolManager, RegisterAndUnregisterProtocol )
{
	TEST_BEGIN();

	/* 构建mock对象 */
	__protocol_mock protocol_mock;

	/* 新注册一个协议 */
	ASSERT_NE( NSEC_NSERROR, pm_instance.registerProtocol( protocol_tag, 0, &protocol_mock ) );

	/* 检查 CProtocolManager 对象数据成员 */
	auto found_itr = pm_instance.m_protocol_control_block_table.find( protocol_tag_hash_code );
	if( pm_instance.m_protocol_control_block_table.end() == found_itr )
		ASSERT_TRUE( false );
	ASSERT_EQ(( *found_itr ).second->store_ptr->protocol_id, protocol_tag_hash_code );
	ASSERT_EQ(( *found_itr ).second->protocol_interface_ptr, &protocol_mock );

	/* 检测是否正确存入数据库中 */
	CPMDatabase::ST_PCB_Store *protocol_store_ptr = pm_instance.m_pm_database.find_protocol( protocol_tag_hash_code );
	ASSERT_NE( 0, ( uintptr_t )protocol_store_ptr );
	ASSERT_EQ( protocol_tag_hash_code , protocol_store_ptr->protocol_id );

	TEST_END();
}

TEST( ProtocolManager, QueryProtocol )
{
	TEST_BEGIN();

	/* 构建mock对象 */
	__protocol_mock protocol_mock;
	//EXPECT_CALL( protocol_mock, startTask ).WillOnce( Return( 0xc5c5c5c5 ) );
	//EXPECT_CALL( protocol_mock, queryTaskInformation( 0xc5c5c5c5, _ ) ).WillOnce( Return( NSEC_SUCCESS ) );

	/* 注册协议 */
	ASSERT_NE( NSEC_NSERROR, pm_instance.registerProtocol( protocol_tag, 0, &protocol_mock ) );

	/* 查询协议 */
	ASSERT_EQ( &protocol_mock, pm_instance.queryProtocol( protocol_tag ) );

	TEST_END();
}

TEST( ProtocolManager, AddAndDeleteGroup )
{
	TEST_BEGIN();
	__group_file_scope group_file_scope;

	/* 添加组 */
	GROUP_HANDLE group_handle = pm_instance.addGroup( GROUP_FILENAME, true, 0xc5c5c5c5 );
	ASSERT_NE( NS_INVALID_HANDLE, group_handle );

	/* 再次添加相同组应该为异常 */
	{
		GROUP_HANDLE e_group_handle = pm_instance.addGroup( GROUP_FILENAME, true, 0xc5c5c5c5 );
		ASSERT_EQ( NS_INVALID_HANDLE, e_group_handle );
	}

	/* 检查组对象 */
	CProtocolManager::ST_GCB *gcb_ptr = ( CProtocolManager::ST_GCB * )group_handle;
	ASSERT_EQ( gcb_ptr->store_ptr->user_data, 0xc5c5c5c5 );
	ASSERT_EQ( gcb_ptr->spacific_tag, SPACIFIC_TAG );

	/* 检查 CProtocolManager 对象数据成员 */
	uint32_t group_hash_code = CRC32( GROUP_FILENAME, strlen( GROUP_FILENAME ) );
	auto found_itr = pm_instance.m_group_control_block_table.find( group_hash_code );
	if( pm_instance.m_group_control_block_table.end() == found_itr )
		ASSERT_TRUE( false );
	ASSERT_EQ( found_itr->second, gcb_ptr );

	/* 检测是否正确存入数据库中 */
	//CPMDatabase::ST_GCB_Store *db_group_ptr = pm_instance.m_pm_database.findGroup( group_hash_code );
	//ASSERT_NE( 0, ( uintptr_t )db_group_ptr );

	/* 删除组 */
	pm_instance.delGroup( group_handle , false );

	/* 检查 CProtocolManager 对象数据成员 */
	found_itr = pm_instance.m_group_control_block_table.find( group_hash_code );
	if( pm_instance.m_group_control_block_table.end() != found_itr )
		ASSERT_TRUE( false );

	/* 检测是否正确从数据库中删除 */
	//ASSERT_EQ( 0, ( uintptr_t )pm_instance.m_pm_database.findGroup( group_hash_code ) );

	TEST_END();
}

TEST( ProtocolManager, GetGroupHandleList )
{
	TEST_BEGIN();

	/* 添加3个组 */
	GROUP_HANDLE group_handles[3];
	std::string file_names[3];
	for( int i = 0; i < 3; i++ )
	{
		file_names[i] = str( boost::format( GROUP_FILENAME"%d" ) % i );
		group_handles[i] = pm_instance.addGroup( file_names[i].c_str(), true, 0xc5c5c5c5 );
		ASSERT_NE( NS_INVALID_HANDLE, group_handles[i] ) << " with index = (" << i << ")";
	}

	/* 获取组列表 */
	GROUP_HANDLE group_handle_store[3];
	int count = pm_instance.getGroupHandleList( group_handle_store );
	EXPECT_EQ( 3, count );
	std::set<GROUP_HANDLE> handle_set( group_handle_store, group_handle_store + 3 );
	for( int i = 0; i < 3; i++ )
		if( handle_set.find( group_handles[i] ) == handle_set.end() )
			ASSERT_TRUE( false );

	/* 删除文件 */
	for( int i = 0; i < 3; i++ )
		remove( file_names[i].c_str() );

	/* 在结束之前不释放组是安全的 */
	TEST_END();
}

TEST( ProtocolManager, AddAndRemoveProtocolTaskOfGroup )
{
	TEST_BEGIN();
	__group_file_scope group_file_scope;

	/* 构建mock对象 */
	__protocol_mock protocol_mock;
	EXPECT_CALL( protocol_mock, startTask( _, _, _, _ ) ).WillOnce( Return( 25 ) );
	EXPECT_CALL( protocol_mock, delTask( _ ) );

	/* 新注册协议 */
	ASSERT_NE( NSEC_NSERROR, pm_instance.registerProtocol( protocol_tag, 0, &protocol_mock ) );

	/* 添加组 */
	GROUP_HANDLE group_handle = pm_instance.addGroup( GROUP_FILENAME, true, 0xc5c5c5c5 );
	ASSERT_NE( NS_INVALID_HANDLE, group_handle );

	/* 添加协议任务 */
#define URI "TestProtocol://test"
	IProtocol::ST_TaskResource *task_resource = ( IProtocol::ST_TaskResource * )alloca( sizeof( IProtocol::ST_TaskResource ) + strlen( URI ) + 1 );
	memset( task_resource, 0, sizeof( IProtocol::ST_TaskResource ) );
	strcpy( task_resource->resource_data, URI );

	IProtocol::ST_TaskInitParam task_init_param;
	task_init_param.selector_id = 0;
	task_init_param.enable_hooker = true;
	task_init_param.task_resource_ptr = task_resource;
	task_init_param.init_task_state_data_ptr = NULL;
	GROUP_TASK_HANDLE group_task_handle = pm_instance.addProtocolTaskToGroup( group_handle, task_init_param );
	ASSERT_NE( NS_INVALID_HANDLE, group_task_handle );

	/* 检查 CProtocolManager 对象数据成员 */
	CProtocolManager::ST_GroupControlBlock *gcb_ptr = reinterpret_cast<CProtocolManager::ST_GroupControlBlock *>( group_handle );
	CProtocolManager::ST_ProtocolControlBlock *pcb_ptr = pm_instance.pm_selectProtocol( task_resource->resource_data, 0 );
	ASSERT_NE( NULL, ( uintptr_t )pcb_ptr );
	ASSERT_EQ( 1, gcb_ptr->tcb_ptr_vector.size() );
	ASSERT_EQ( 1, pcb_ptr->tcb_ptr_vector.size() );

	/* 删除协议任务 */
	pm_instance.removeProtocolTask( group_task_handle );

	/* 检查 CProtocolManager 对象数据成员 */
	ASSERT_EQ( 0, gcb_ptr->tcb_ptr_vector.size() );
	ASSERT_EQ( 0, pcb_ptr->tcb_ptr_vector.size() );

	TEST_END();
}

TEST( ProtocolManager, GetGroupStatistics )
{
	TEST_BEGIN();
	__group_file_scope group_file_scope;

	/* 添加组 */
	GROUP_HANDLE group_handle = pm_instance.addGroup( GROUP_FILENAME, true, 0xc5c5c5c5 );
	EXPECT_NE( NS_INVALID_HANDLE, group_handle );

	/* 获取组的统计信息 */
	size_t size = sizeof( CProtocolManager::ST_GroupInformation ) + strlen( GROUP_FILENAME ) + 1;
	CProtocolManager::ST_GroupInformation *group_information = ( CProtocolManager::ST_GroupInformation * )alloca( size );
	EXPECT_EQ( size, pm_instance.getGroupStatistics( group_handle, group_information ) );

	/* 检测组统计信息 */
	EXPECT_STREQ( GROUP_FILENAME, group_information->group_file_name_str );
	EXPECT_EQ( 0, group_information->group_file_size );
	EXPECT_EQ( 0xc5c5c5c5, group_information->user_tag );
	EXPECT_EQ( 0, group_information->task_count );
	EXPECT_EQ( 0, group_information->send_data_speed );
	EXPECT_EQ( 0, group_information->recv_data_speed );
	EXPECT_EQ( 0, group_information->send_data_size );
	EXPECT_EQ( 0, group_information->recved_data_size );

	TEST_END();
}

}/* ProtocolManager **/
}/* KernelPlus **/
}/* Netspecters **/
