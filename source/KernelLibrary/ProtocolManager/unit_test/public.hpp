#ifndef  pm_ut_public_INC
#define  pm_ut_public_INC
#include "GeneralTestDefine.hpp"
#include <gtest/gtest.h>

namespace Netspecters {
namespace KernelPlus {
namespace ProtocolManager {

#define DB_FILENAME_BASE "pm_db_unit_test.tmp"
#ifdef __linux
#	define DB_FILENAME "/tmp/"DB_FILENAME_BASE
#else
#	define DB_FILENAME DB_FILENAME_BASE
#endif

}/* ProtocolManager **/
}/* KernelPlus **/
}/* Netspecters **/

#endif   /* ----- #ifndef pm_ut_public_INC  ----- */
