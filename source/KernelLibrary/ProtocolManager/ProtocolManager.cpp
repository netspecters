#ifdef __MINGW32__
#	include <malloc.h>
#else
#	include <alloca.h>
#endif
#include "../../StdLib/NSFC.h"
#include "ProtocolManager.h"

using namespace Netspecters::KernelPlus::ProtocolManager;

CProtocolManager::CProtocolManager()
	: m_default_time_out( 5000 )
	, m_restore_status_time( 2000 )
	, m_running( true )
	, m_store_thread( &CProtocolManager::onExecute, this )
{}

CProtocolManager::~CProtocolManager()
{
	m_running = false;
	m_store_thread.join();
}

int CProtocolManager::onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param )
{
	m_plus_dir_handle = plus_dir_handle;

	/* load database */
	char data_dir_path[ 256 ];
	plus_tree_ptr->getDataDirPath( data_dir_path, sizeof( data_dir_path ) );
	string database_file_name( data_dir_path );
	database_file_name.append( "ProtocolManager.db" );

	if( CPMDatabase::fsFail == m_pm_database.load_file( database_file_name ) )
	{
		printk( KINFORMATION "ProtocolManager can't load database at '%s'\n", database_file_name.c_str() );
		return NSEC_NSERROR;
	}

	/* register rpc call method */
	plus_tree_ptr->initInterfaceIPC( plus_dir_handle, ( void * )this, 9 );
#define REGISTER_METHOD(name) plus_tree_ptr->registerIPCMethod(plus_dir_handle,#name,reinterpret_cast<void*>(&CProtocolManager::name))
	REGISTER_METHOD( addGroup );
	REGISTER_METHOD( delGroup );
	REGISTER_METHOD( getGroupHandleList );
	REGISTER_METHOD( getGroupID );
	REGISTER_METHOD( getGroupStatistics );
	REGISTER_METHOD( addProtocolTaskToGroup );
	REGISTER_METHOD( removeProtocolTask );
	REGISTER_METHOD( modifyGroupState );
	REGISTER_METHOD( getTaskStopCodes );
#undef REGISTER_METHOD

	printk( KINFORMATION "ProtocolManager loaded.\n" );
	return NSEC_SUCCESS;
}

void CProtocolManager::onUnload()
{
	printk( KINFORMATION "ProtocolManager unloaded.\n" );
}

int CProtocolManager::getParam( const char *param_name, void *value_buffer_ptr )
{
	return 0;
}

void *CProtocolManager::setParam( const char *param_name, void *value_ptr )
{
	return NULL;
}

int CProtocolManager::registerProtocol( const void *protocol_tag_ptr, int protocol_selector_id, IProtocol *protocol_ptr )
{
	if( protocol_selector_id != 0 )
		RET_NS_ERR( NSEC_PARAMERROR );

	/*
		caculate tag code which identify a protocol.
		we use crc32 for caculation now.
	 */
	uint32_t protocol_id = CRC32_STR( protocol_tag_ptr );

	/* 构建协议控制块 */
	ST_ProtocolControlBlock *new_pcb_ptr = new ST_ProtocolControlBlock;

	/* 用唯一tag码做索引，插入到hash map中 */
	new_pcb_ptr->protocol_interface_ptr = protocol_ptr;
	m_protocol_control_block_table.insert( std::make_pair( protocol_id, new_pcb_ptr ) );

	/* try to load protocol from database */
	CPMDatabase::ST_PCB_Store *pcb_store_ptr = m_pm_database.find_protocol( protocol_id );
	if( pcb_store_ptr )
	{
		new_pcb_ptr->store_ptr = pcb_store_ptr;

		long cookie = CPMDatabase::C_First_Cookie;
		CPMDatabase::ST_TCB_Store *tcb_store_ptr;
		/* 遍历协议中的任务 */
		while(( tcb_store_ptr = m_pm_database.find_next_task_in_protocol( pcb_store_ptr, cookie ) ) )
		{
			ST_TaskControlBlock *tcb_ptr = new ST_TaskControlBlock;
			tcb_ptr->spacific_tag = SPACIFIC_TAG;
			tcb_ptr->pcb_ptr = new_pcb_ptr;
			tcb_ptr->store_ptr = tcb_store_ptr;

			/* 查找任务对应的组，如果组只存在于数据库中，则需要创建组控制块实例 */
			CPMDatabase::ST_GCB_Store *gcb_store_ptr = ( CPMDatabase::ST_GCB_Store * )m_pm_database.lock_block( tcb_store_ptr->gcb_store );
			ST_GroupControlBlock *gcb_ptr;
			auto group_itr = m_group_control_block_table.find( gcb_store_ptr->group_id );
			if( group_itr != m_group_control_block_table.end() )
				gcb_ptr = group_itr->second;
			else	/* group instance need to be created */
			{
				/* 从数据库中读取组信息，重新构建组(控制块) */
				gcb_ptr = new ST_GroupControlBlock;
				gcb_ptr->spacific_tag = SPACIFIC_TAG;

				/* 重建数据容器 */
				char group_file_name[gcb_store_ptr->filename_len+1];
				memcpy( group_file_name, gcb_store_ptr->filename, gcb_store_ptr->filename_len );
				group_file_name[gcb_store_ptr->filename_len] = '\0';
				off_t data_container_size;
				gcb_ptr->data_container_ptr = DATA_CONTAINER_MANAGER_INSTANCE.newDataContainer( group_file_name, data_container_size );

				/* 重建数据段边界栈 */
				gcb_ptr->restore_data_segment_stack( m_pm_database );

				/* 将组控制块插入组索引表中 */
				auto gcb_inster_result = m_group_control_block_table.insert( std::make_pair( gcb_store_ptr->group_id, gcb_ptr ) );
				if( !gcb_inster_result.second )
				{
					DATA_CONTAINER_MANAGER_INSTANCE.freeDataContainer( gcb_ptr->data_container_ptr );
					delete gcb_ptr;
					delete tcb_ptr;
					continue;	/* 任务组重建失败，跳过此任务的重建 */
				}
			}
			gcb_ptr->store_ptr = gcb_store_ptr;

			/* 链接组与任务 */
			tcb_ptr->gcb_ptr = gcb_ptr;
			gcb_ptr->tcb_ptr_vector.push_back( tcb_ptr );

			//将tcb放入 protocol control block 的 tcb_vector 中
			new_pcb_ptr->tcb_ptr_vector.push_back( tcb_ptr );

			/* 读取任务数据，重启任务 */
			tgm_resumeTask( tcb_ptr );
		}
	}
	else	/* can not find protocol in database, so we register it in database */
	{
		new_pcb_ptr->store_ptr = m_pm_database.malloc_protocol_space( protocol_id );
		if( NULL == new_pcb_ptr->store_ptr )
		{
			delete new_pcb_ptr;
			RET_NS_ERR( NSEC_NSERROR );
		}
	}
	return NSEC_SUCCESS;
}

int CProtocolManager::registerProtocolHook( const void *protocol_tag_ptr, IProtocolHook *protocol_hook_ptr )
{
	//计算唯一tag码，用来标识protocol(本版使用crc32的方法获取)
	uint32_t tag_hash_code = CRC32_STR( protocol_tag_ptr );

	/* 用唯一tag码做key，查找协议描述符 */
	auto found_itr = m_protocol_control_block_table.find( tag_hash_code );
	if( found_itr != m_protocol_control_block_table.end() )
	{
		found_itr->second->hook_array.push_back( protocol_hook_ptr );
		return NSEC_SUCCESS;
	}
	return NSEC_NSERROR;
}

void CProtocolManager::unregisterProtocol( void *protocol_tag_ptr, int protocol_selector_id )
{
	/* 计算唯一tag码，用来标识protocol(本版使用crc32的方法获取) */
	uint32_t protocol_id = CRC32_STR( protocol_tag_ptr );

	/* 用唯一tag码做key，查找协议数据库指针 */
	auto protocol_ptr = m_pm_database.find_protocol( protocol_id );
	if( protocol_ptr )
		m_pm_database.free_protocol_space( protocol_ptr );
}

IProtocol *CProtocolManager::queryProtocol( const void *protocol_tag_ptr )
{
	IProtocol *result = NULL;
	uint32_t hash_code = CRC32_STR( protocol_tag_ptr );
	if( hash_code > 0 )
	{
		//查找协议
		auto found_itr = m_protocol_control_block_table.find( hash_code );
		if( found_itr != m_protocol_control_block_table.end() )
			result = found_itr->second->protocol_interface_ptr;
	}
	return result;
}

void CProtocolManager::onExecute()
{
	while( m_running )
	{
		std::this_thread::sleep_for( std::chrono::milliseconds( m_restore_status_time ) );
		//
	}
}

CProtocolManager::ST_ProtocolControlBlock *CProtocolManager::pm_selectProtocol( const char *protocol_address, int selector_id )
{
	/* check parameter */
	if( !protocol_address || ( selector_id != 0 ) ) return NULL;

	ST_ProtocolControlBlock *found_pcb_ptr = NULL;
	uint32_t protocol_tag = pm_getProtocolHashTagFromAddress( protocol_address );
	if( protocol_tag > 0 )
	{
		/* find protocol in table */
		auto found_itr = m_protocol_control_block_table.find( protocol_tag );
		if( found_itr != m_protocol_control_block_table.end() )
			found_pcb_ptr = found_itr->second;
	}
	return found_pcb_ptr;
}

uint32_t CProtocolManager::pm_getProtocolHashTagFromAddress( const char *protocol_address_ptr )
{
	int tag_len = 0;

	const char *current_pos_ptr = protocol_address_ptr;
	for( int protocol_address_len = strlen( protocol_address_ptr );
			( *current_pos_ptr != ':' ) && ( protocol_address_len > 0 );
			protocol_address_len--, current_pos_ptr++, tag_len++ );

	return tag_len > 0 ? CRC32( protocol_address_ptr, tag_len ) : 0;
}

size_t CProtocolManager::getTaskStopCodes( GROUP_HANDLE group_handle, IProtocol::ENM_TaskStopCode *task_stop_code_buffer )
{
	ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle, -1 );
	int task_num = gcb_ptr->tcb_ptr_vector.size();
	if( task_stop_code_buffer )
		for( auto itr = gcb_ptr->tcb_ptr_vector.begin(); itr != gcb_ptr->tcb_ptr_vector.end(); ++itr, task_stop_code_buffer++ )
		{
			ST_TaskControlBlock *tcb_ptr = *itr;
			*task_stop_code_buffer = tcb_ptr->pcb_ptr->protocol_interface_ptr->getStopCode( tcb_ptr->task_handle );
		}
	return task_num * sizeof( IProtocol::ENM_TaskStopCode );
}
