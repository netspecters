#include <algorithm>
#include "ProtocolManager.h"

using namespace Netspecters::KernelPlus::ProtocolManager;

inline uint32_t CProtocolManager::ST_GroupControlBlock::getGroupHashCode()
{
	const char *file_name_ptr = data_container_ptr->getDataContainerName();
	return CRC32( file_name_ptr, strlen( file_name_ptr ) );
}

void CProtocolManager::ST_GroupControlBlock::save_data_segment_stack( CPMDatabase &db )
{	/*		保存的数据范围包括：分配栈中未分配的数据段，数据容器里分配的数据段	*/	typedef std::pair<off_t, off_t> __segment_range_t;	int segment_num = data_segment_stack.size() + data_container_ptr->getSegmentNum() ;	__segment_range_t segment_ranges[segment_num];
	if( !data_segment_stack.empty() )
		for( int i = 0; i < data_segment_stack.size(); ++i )
			segment_ranges[i] = data_segment_stack[i];
	if( data_container_ptr->getSegmentNum() > 0 )		data_container_ptr->dumpSegmentRanges(&(segment_ranges[data_segment_stack.size()]));	db.write_data( store_ptr, segment_ranges, sizeof( __segment_range_t ) * segment_num );
}

void CProtocolManager::ST_GroupControlBlock::restore_data_segment_stack( CPMDatabase &db )
{
	if( store_ptr->data_size > 0 )
	{
		typedef std::pair<off_t, off_t> __segment_range_t;
		int segment_num = store_ptr->data_size / sizeof( __segment_range_t );
		__segment_range_t segment_ranges[segment_num];
		db.read_data( store_ptr, segment_ranges );
		for( int i = segment_num - 1; i >= 0; i-- )
			data_segment_stack.push_back( segment_ranges[i] );
	}
}

GROUP_HANDLE CProtocolManager::addGroup( const char *group_file_name_str_ptr, bool visable, uintptr_t group_tag )
{
	/* group ctrl block */
	ST_GroupControlBlock *new_gcb_ptr = new ST_GroupControlBlock;
	new_gcb_ptr->spacific_tag = SPACIFIC_TAG;

	do
	{
		/* 建立data container */
		off_t data_container_size = 0;
		new_gcb_ptr->data_container_ptr = DATA_CONTAINER_MANAGER_INSTANCE.newDataContainer( group_file_name_str_ptr, data_container_size );
		if( data_container_size > 0 )	/* 文件已经存在，冲突 */
		{
			break;
		}

		/* 加入到任务组列表中 */
		uint32_t group_id = new_gcb_ptr->getGroupHashCode();
		auto gcb_itr_pair = m_group_control_block_table.insert( std::make_pair( group_id, new_gcb_ptr ) );
		if( !gcb_itr_pair.second )
			break;

		/* 将组写入数据库（为了持久化操作） */
		if( group_file_name_str_ptr )	/* 如果是内存文件则不需要持久化 */
		{
			new_gcb_ptr->store_ptr = m_pm_database.malloc_group_space( group_id, std::string( group_file_name_str_ptr ) );
			if( !new_gcb_ptr->store_ptr )
			{
				m_group_control_block_table.erase( gcb_itr_pair.first );
				break;
			}
			new_gcb_ptr->store_ptr->group_id = group_id;
			new_gcb_ptr->store_ptr->user_data = group_tag;
		}

		return PTR_TO_HANDLE( new_gcb_ptr );
	} while( false );

	DATA_CONTAINER_MANAGER_INSTANCE.freeDataContainer( new_gcb_ptr->data_container_ptr );
	delete new_gcb_ptr;
	return NS_INVALID_HANDLE;
}

void CProtocolManager::delGroup( GROUP_HANDLE group_handle, bool delete_group_file )
{
	ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle );

	/* 从任务组中删除所有（协议）任务 */
	if( gcb_ptr->tcb_ptr_vector.size() )
		for( auto tcb_ptr_itr = gcb_ptr->tcb_ptr_vector.begin(); tcb_ptr_itr != gcb_ptr->tcb_ptr_vector.end(); ++tcb_ptr_itr )
		{
			ST_TaskControlBlock *tcb_ptr = *tcb_ptr_itr;
			tcb_ptr->pcb_ptr->protocol_interface_ptr->delTask( tcb_ptr->task_handle );

			/* 从数据库中删除task记录 */
			m_pm_database.free_task_space( tcb_ptr->store_ptr );
		}

	/* 从数据库中删除 group 纪录 */
	m_pm_database.free_group_space( gcb_ptr->store_ptr );

	/* 从任务组列表中移除 group 纪录 */
	m_group_control_block_table.erase( gcb_ptr->getGroupHashCode() );
	gcb_ptr->spacific_tag = 0;
	delete gcb_ptr;
}

size_t CProtocolManager::getGroupHandleList( GROUP_HANDLE *group_handle_buf_ptr )
{
	size_t handle_count = m_group_control_block_table.size();
	if( group_handle_buf_ptr )
		for( auto gcb_ptr_itr = m_group_control_block_table.begin(); gcb_ptr_itr != m_group_control_block_table.end(); ++gcb_ptr_itr )
		{
			*group_handle_buf_ptr = PTR_TO_HANDLE( gcb_ptr_itr->second );
			++group_handle_buf_ptr;
		}
	return handle_count;
}

uint32_t CProtocolManager::getGroupID( GROUP_HANDLE group_handle )
{
	ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle, 0 );
	return gcb_ptr->store_ptr->group_id;
}

size_t CProtocolManager::getGroupStatistics( GROUP_HANDLE group_handle, ST_GroupInformation *group_infor_buf_ptr )
{
	size_t total_size = 0;

	UNSAFE_CODE_BEGIN
	{
		if( NS_INVALID_HANDLE == group_handle )
		{
			for( auto gcb_ptr_itr = m_group_control_block_table.begin(); gcb_ptr_itr != m_group_control_block_table.end(); ++gcb_ptr_itr )
			{
				ST_GroupControlBlock *gcb_ptr = gcb_ptr_itr->second;
				size_t group_infor_size = tgm_getGroupInformation( gcb_ptr, group_infor_buf_ptr );
				total_size += group_infor_size;
				if( group_infor_buf_ptr )
					group_infor_buf_ptr = ( ST_GroupInformation * )(( uintptr_t )group_infor_buf_ptr + group_infor_size );
			}
		}
		else
		{
			/* param check */
			ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle, NSEC_PARAMERROR );

			total_size = tgm_getGroupInformation( gcb_ptr, group_infor_buf_ptr );
		}
	}
	UNSAFE_CODE_END( RET_NS_ERR( NSEC_PARAMERROR ) )
	return total_size;
}

size_t CProtocolManager::getGroupDetails( GROUP_HANDLE group_handle, void *group_details_buf_ptr )
{
	ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle, NSEC_PARAMERROR );
	UNSAFE_CODE_BEGIN
	{
	}
	UNSAFE_CODE_END( RET_NS_ERR( NSEC_PARAMERROR ) )
}

GROUP_TASK_HANDLE CProtocolManager::addProtocolTaskToGroup( GROUP_HANDLE group_handle, const IProtocol::ST_TaskInitParam &task_init_param )
{
	ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle, NS_INVALID_HANDLE );

	GROUP_TASK_HANDLE new_group_task_handle = NS_INVALID_HANDLE;
	const IProtocol::ST_TaskInitParam *active_task_init_param_ptr = &task_init_param;
	IProtocolHook *hooker_ptr = NULL;

	/* 选择协议 */
	ST_ProtocolControlBlock *pcb_ptr = pm_selectProtocol( &( task_init_param.task_resource_ptr->resource_data[task_init_param.task_resource_ptr->res_path_offset] ), task_init_param.selector_id );
	if( !pcb_ptr )
		return NS_INVALID_HANDLE;

	/* 查询Hook */
	if( task_init_param.enable_hooker )
	{
		IProtocol::ST_TaskInitParam *new_task_init_param_ptr = NULL;
		auto itr = pcb_ptr->hook_array.begin();
		bool continue_enum = true;	//是否要继续递归
		while( continue_enum && itr != pcb_ptr->hook_array.end() )
		{
			continue_enum = false;
			switch(( *itr )->onHookBegin( task_init_param, new_task_init_param_ptr ) )
			{
				case IProtocolHook::hrPass:
					continue_enum = true;
					break;

				case IProtocolHook::hrNewParam:
					hooker_ptr = *itr;
					active_task_init_param_ptr = new_task_init_param_ptr;
					break;

					/* 需要重新选择协议，递归执行 */
				case IProtocolHook::hrProtocolChange:
					hooker_ptr = *itr;
					active_task_init_param_ptr = new_task_init_param_ptr;
					new_group_task_handle = addProtocolTaskToGroup( group_handle , *new_task_init_param_ptr );
					break;

				case IProtocolHook::hrStop:
					break;
			}
			++itr;
		}
	}

	/* 建立协议任务 */
	if( NS_INVALID_HANDLE == new_group_task_handle )
	{
		/* 构建任务控制块 */
		ST_TaskControlBlock *tcb_ptr = new ST_TaskControlBlock;
		new_group_task_handle = PTR_TO_HANDLE( tcb_ptr );
		tcb_ptr->spacific_tag = SPACIFIC_TAG;
		tcb_ptr->pcb_ptr = pcb_ptr;
		tcb_ptr->gcb_ptr = gcb_ptr;
		/* 如果内存文件不需要持久化，则不允许其拥有任务数据 */
		tcb_ptr->store_ptr = gcb_ptr->data_container_ptr->getDataContainerName() ? m_pm_database.malloc_task_space( pcb_ptr->store_ptr, gcb_ptr->store_ptr ) : NULL;
		tcb_ptr->task_handle = pcb_ptr->protocol_interface_ptr->startTask( *active_task_init_param_ptr, new_group_task_handle, gcb_ptr->data_container_ptr, NULL );

		/* 将任务控制块放到group及protocol中 */
		gcb_ptr->tcb_ptr_vector.push_back( tcb_ptr );
		pcb_ptr->tcb_ptr_vector.push_back( tcb_ptr );
	}

	/* 将new task param反给preprocessor，以便preprocessor回收相关数据结构 */
	if( hooker_ptr )
		hooker_ptr->onHookEnd( const_cast<IProtocol::ST_TaskInitParam *>( active_task_init_param_ptr ) );

	return new_group_task_handle;
}

void CProtocolManager::removeProtocolTask( GROUP_TASK_HANDLE group_task_handle )
{
	ST_TaskControlBlock *tcb_ptr = HANDLE_TO_TCB_PTR( group_task_handle );
	ST_GroupControlBlock *gcb_ptr = tcb_ptr->gcb_ptr;
	ST_ProtocolControlBlock *pcb_ptr = tcb_ptr->pcb_ptr;
	IProtocol *protocol_interface_ptr = pcb_ptr->protocol_interface_ptr;

	/* 请求协议删除任务 */
	protocol_interface_ptr->delTask( tcb_ptr->task_handle );

	/* 消除task所在组中记录 */
	gcb_ptr->tcb_ptr_vector.erase( std::remove( gcb_ptr->tcb_ptr_vector.begin(), gcb_ptr->tcb_ptr_vector.end(), tcb_ptr ) );
	/* 消除task所在协议中记录 */
	pcb_ptr->tcb_ptr_vector.erase( std::remove( pcb_ptr->tcb_ptr_vector.begin(), pcb_ptr->tcb_ptr_vector.end(), tcb_ptr ) );
	/* 清除task的数据库入口 */
	m_pm_database.free_task_space( tcb_ptr->store_ptr );

	tcb_ptr->spacific_tag = 0;
	delete tcb_ptr;	/* 释放task占用的内存空间 */

	/* 检验任务对应的组是不是已经空了，如果空了并且没有错误发生，表示此组已经完成类既定任务，此组可以删除了 */
	if( 0 == gcb_ptr->tcb_ptr_vector.size() )
		delGroup( PTR_TO_HANDLE( gcb_ptr ) , false );
}

CProtocolManager::ENM_GroupState CProtocolManager::modifyGroupState( GROUP_HANDLE group_handle, ENM_GroupState new_group_state )
{
	ST_GroupControlBlock *gcb_ptr = HANDLE_TO_GCB_PTR( group_handle, gsNone );
	ENM_GroupState old_group_state;
	if( gcb_ptr->tcb_ptr_vector.size() > 0 )
		old_group_state = gsRunning;
	else
	{
		bool group_error = true;
		for( auto itr = gcb_ptr->tcb_ptr_vector.begin(); itr != gcb_ptr->tcb_ptr_vector.end(); ++itr )
			group_error &= ( *itr )->pcb_ptr->protocol_interface_ptr->getStopCode(( *itr )->task_handle ) == IProtocol::tscError;
		old_group_state = group_error ? gsError : gsPasue ;
	}

	switch( new_group_state )
	{
		case gsNone:
			break;
		case gsRunning:
			for( auto tcb_itr = gcb_ptr->tcb_ptr_vector.begin(); tcb_itr != gcb_ptr->tcb_ptr_vector.end(); ++tcb_itr )
			{
				ST_TaskControlBlock *tcb_ptr = *tcb_itr;
				if( 0 == tcb_ptr->task_handle )
					tgm_resumeTask( tcb_ptr );
			}
			break;
		case gsPasue:
			for( auto tcb_itr = gcb_ptr->tcb_ptr_vector.begin(); tcb_itr != gcb_ptr->tcb_ptr_vector.end(); ++tcb_itr )
			{
				ST_TaskControlBlock *tcb_ptr = *tcb_itr;
				if( tcb_ptr->task_handle )
				{
					tcb_ptr->pcb_ptr->protocol_interface_ptr->stopTask( tcb_ptr->task_handle );
					tcb_ptr->task_handle = 0;
				}
			}
		default:
			old_group_state = gsNone;
	}
	return old_group_state;
}

int CProtocolManager::storeTaskStateData( GROUP_TASK_HANDLE group_task_handle, IProtocol::ST_TaskStateData *task_state_data_ptr )
{
	ST_TaskControlBlock *tcb_ptr = HANDLE_TO_TCB_PTR( group_task_handle, NSEC_PARAMERROR );
	return tcb_ptr->store_ptr ? m_pm_database.write_data( tcb_ptr->store_ptr, task_state_data_ptr->data, task_state_data_ptr->data_size ) : 0;
}

int CProtocolManager::openSegment( GROUP_TASK_HANDLE group_task_handle, ISegment::ST_SegmentInfor::ENM_SegmentType segment_type, ISegment *&segment_ptr )
{
	ST_TaskControlBlock *tcb_ptr = HANDLE_TO_TCB_PTR( group_task_handle, NSEC_PARAMERROR );
	ST_GroupControlBlock *gcb_ptr = tcb_ptr->gcb_ptr;
	ISegment::ST_SegmentInfor segment_info = {segment_type, ISegment::ST_SegmentInfor::sbtManaged, -1, -1, 0, NULL};
	bool has_range = !gcb_ptr->data_segment_stack.empty();
	/* 在栈不空的情况下，从分段栈中获取一个未完成的段 */
	if( has_range )
	{
		auto segment_range = gcb_ptr->data_segment_stack.back();
		gcb_ptr->data_segment_stack.pop_back();
		segment_info.segment_base_offset = segment_range.first;
		segment_info.segment_border = segment_range.second;		/* 回刷段数据到数据库 */		gcb_ptr->save_data_segment_stack( m_pm_database );
	}
	segment_ptr = NULL;
	if( gcb_ptr->data_container_ptr->getDataSize() > 0 )
		segment_ptr = gcb_ptr->data_container_ptr->addSegment( has_range ? IDataContainer::otCustom : IDataContainer::otMiddleOfBiggest, segment_info );
	return NSEC_SUCCESS;
}

void CProtocolManager::closeSegment( GROUP_TASK_HANDLE group_task_handle, ISegment *segment_ptr )
{
	ST_TaskControlBlock *tcb_ptr = HANDLE_TO_TCB_PTR( group_task_handle );
	ST_GroupControlBlock *gcb_ptr = tcb_ptr->gcb_ptr;
	/* 如果段没有完成，则将段压入分段栈 */
	if( segment_ptr->getSegmentSize() != segment_ptr->getIOPosition() )
	{		gcb_ptr->data_segment_stack.push_back(std::make_pair(segment_ptr->getSegmentOffset(),segment_ptr->getSegmentSize()));		/* 回刷段数据到数据库 */		gcb_ptr->save_data_segment_stack( m_pm_database );
	}	gcb_ptr->data_container_ptr->deleteSegment( segment_ptr );
}

size_t CProtocolManager::tgm_getGroupInformation( ST_GroupControlBlock *gcb_ptr, ST_GroupInformation *group_infor_ptr )
{
	size_t total_size = sizeof( ST_GroupInformation ) + strlen( gcb_ptr->data_container_ptr->getDataContainerName() ) + 1;
	if( group_infor_ptr )
	{
		memset( group_infor_ptr, 0, total_size );
		group_infor_ptr->group_file_size = gcb_ptr->data_container_ptr->getDataSize();
		group_infor_ptr->user_tag = gcb_ptr->store_ptr->user_data;
		group_infor_ptr->group_id = gcb_ptr->store_ptr->group_id;
		strcpy( group_infor_ptr->group_file_name_str, gcb_ptr->data_container_ptr->getDataContainerName() );

		/* 统计组內每个任务 */
		group_infor_ptr->recved_data_size = group_infor_ptr->send_data_size = 0;
		group_infor_ptr->recv_data_speed = group_infor_ptr->send_data_speed = 0;
		group_infor_ptr->task_count = 0;
		for( auto tcb_ptr_itr = gcb_ptr->tcb_ptr_vector.begin(); tcb_ptr_itr != gcb_ptr->tcb_ptr_vector.end(); ++tcb_ptr_itr )
		{
			ST_TaskControlBlock *tcb_ptr = *tcb_ptr_itr;
			if( tcb_ptr->task_handle )
			{
				IProtocol::ST_TaskInformation task_infor;
				tcb_ptr->pcb_ptr->protocol_interface_ptr->queryTaskInformation( tcb_ptr->task_handle, &task_infor );

				/* 累加 */
				group_infor_ptr->recved_data_size += task_infor.recved_data_size;
				group_infor_ptr->send_data_size += task_infor.send_data_size;
				group_infor_ptr->recv_data_speed += task_infor.transfer_speed_down;
				group_infor_ptr->send_data_speed += task_infor.transfer_speed_up;

				group_infor_ptr->task_count++;
			}
		}
	}
	return total_size;
}

/**
 * @brief 读取任务数据，重启任务
 * @param tcb_ptr 任务控制块指针
 * @note 任务控制块的 pcb_ptr、gcb_ptr、db_task_ptr 域必须为有效值
*/
void CProtocolManager::tgm_resumeTask( ST_TaskControlBlock *tcb_ptr )
{
	IProtocol::ST_TaskInitParam task_init_param;
	memset( &task_init_param, 0, sizeof( task_init_param ) );

	int task_state_data_size = tcb_ptr->store_ptr->data_size;
	IProtocol::ST_TaskStateData *task_state_data_ptr = ( IProtocol::ST_TaskStateData * )alloca( sizeof( IProtocol::ST_TaskStateData ) + task_state_data_size );
	task_state_data_ptr->data_size = task_state_data_size;
	m_pm_database.read_data<CPMDatabase::ST_TCB_Store>( tcb_ptr->store_ptr, task_state_data_ptr->data );
	task_init_param.init_task_state_data_ptr = task_state_data_ptr;

	tcb_ptr->task_handle = tcb_ptr->pcb_ptr->protocol_interface_ptr->startTask( task_init_param, PTR_TO_HANDLE( tcb_ptr ), tcb_ptr->gcb_ptr->data_container_ptr, NULL );
}
