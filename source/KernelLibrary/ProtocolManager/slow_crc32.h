
#ifndef  SLOW_CRC32_INC
#define  SLOW_CRC32_INC

#include <stdint.h>
#include <stddef.h>

static uint32_t cal_crc(unsigned char *ptr, size_t len)
{
  unsigned char i;
  uint32_t crc=0;
  while(len--!=0)
  {
    for(i=0x80; i!=0; i/=2) {
      if((crc&0x8000)!=0) {crc*=2; crc^=0x1021;}   /* 余式CRC乘以2再求CRC  */
        else crc*=2;
	if((*ptr&i)!=0) crc^=0x1021;                /* 再加上本位的CRC */
    }
    ptr++;
  }
  return(crc);
}

#endif   /* ----- #ifndef SLOW_CRC32_INC  ----- */
