/*
 * =====================================================================================
 *
 *       Filename:  ProtocolManager.h
 *
 *    Description:  管理应用层协议族，提供透明传输服务
 *
 *        Version:  1.0
 *        Created:  2009-1-24 20:35:07
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (WG), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */
#ifndef  PROTOCOLMANAGER_INC
#define  PROTOCOLMANAGER_INC

#include <string>
#include <vector>
#include <map>
#include <thread>

#ifdef __MINGW32__
#	undef USE_ZLIB_CRC32
#endif

#include "crc32.h"
#define CRC32(x,y) chksum_crc32(( unsigned char* )(x), y)
#define CRC32_STR(x) CRC32(x,strlen((const char*)x))

#define SPACIFIC_TAG	0x5f5f5f5f

#include "../NetLayer/Interfaces.h"
#include "../DataContainer/data_container_manager.h"
#include "../../NSErrorCode.h"
#include "nslib.h"
#include "../../StdLib/NSFC.h"

#include "Interfaces.h"
#include "PMDatabase.h"

namespace Netspecters { namespace KernelPlus { namespace ProtocolManager {

#define PTR_TO_HANDLE(x)		(reinterpret_cast<uintptr_t>(x))

/* check param */
#define CHECK_GCB_PTR(ptr,err_msg...)\
	{\
		try\
		{\
			if ( !ptr || ( ptr->spacific_tag != SPACIFIC_TAG ) )\
				return err_msg;\
		}\
		catch ( ... )\
		{\
			return err_msg;\
		}\
	}

#define HANDLE_TO_PTR(ptr_type, handle, err_msg...)\
({\
	if( NS_INVALID_HANDLE == handle ) return err_msg;\
	ptr_type *ptr = reinterpret_cast<ptr_type*>(handle);\
	CHECK_GCB_PTR(ptr, err_msg)\
	ptr;\
})

#define HANDLE_TO_GCB_PTR(handle, err_msg...) HANDLE_TO_PTR(ST_GroupControlBlock, handle, err_msg)
#define HANDLE_TO_TCB_PTR(handle, err_msg...) HANDLE_TO_PTR(ST_TaskControlBlock, handle, err_msg)

using namespace Netspecters::NSStdLib;
using Netspecters::KernelPlus::DataContainer::CDataContainer;
using std::string;

class CProtocolManager : public IProtocolManager, public ITaskGroupManager
{
	public:
		/* INSPlugin */
		virtual NSAPI int onLoad ( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param );
		virtual NSAPI void onUnload ( void );
		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr);
		virtual NSAPI void *setParam(const char *param_name, void *value_ptr);

		/* IProtocolManager */
		virtual NSAPI int registerProtocolHook ( const void *protocol_tag_ptr, IProtocolHook *protocol_hook_ptr );
		virtual NSAPI int registerProtocol ( const void *protocol_tag_ptr, int protocol_selector_id, IProtocol *protocol_ptr );
		virtual NSAPI void unregisterProtocol(void *protocol_tag_ptr, int protocol_selector_id);
		virtual NSAPI IProtocol *queryProtocol( const void *protocol_tag_ptr );
		virtual NSAPI ITaskGroupManager *getTaskGroupManager ( void ) { return this; }

		/**
		 * @name ITaskGroupManager
		 * @{
		 */
		virtual NSAPI GROUP_HANDLE addGroup ( const char *group_file_name_str_ptr, bool visable, uintptr_t group_tag );
		virtual NSAPI void delGroup ( GROUP_HANDLE group_handle, bool delete_group_file );
		virtual NSAPI size_t getGroupHandleList( GROUP_HANDLE *group_handle_buf_ptr );
		virtual NSAPI uint32_t getGroupID( GROUP_HANDLE group_handle );
		virtual NSAPI size_t getGroupStatistics ( GROUP_HANDLE group_handle, ST_GroupInformation *group_infor_buf_ptr );
		virtual NSAPI size_t getGroupDetails( GROUP_HANDLE group_handle, void *group_details_buf_ptr );
		virtual NSAPI GROUP_TASK_HANDLE addProtocolTaskToGroup ( GROUP_HANDLE group_handle, const IProtocol::ST_TaskInitParam &task_init_param );
		virtual NSAPI void removeProtocolTask( GROUP_TASK_HANDLE group_task_handle );
		virtual NSAPI ENM_GroupState modifyGroupState( GROUP_HANDLE group_handle, ENM_GroupState new_group_state );
		virtual NSAPI size_t getTaskStopCodes( GROUP_HANDLE group_handle, IProtocol::ENM_TaskStopCode *task_stop_code_buffer );
		virtual NSAPI int storeTaskStateData( GROUP_TASK_HANDLE group_task_handle, IProtocol::ST_TaskStateData *task_state_data_ptr );
		virtual NSAPI int openSegment( GROUP_TASK_HANDLE group_task_handle, ISegment::ST_SegmentInfor::ENM_SegmentType segment_type, ISegment *&segment_ptr );
		virtual NSAPI void closeSegment( GROUP_TASK_HANDLE group_task_handle, ISegment *segment_ptr );
		/// @}

	public:
		CProtocolManager();
		~CProtocolManager();

	protected:
		void onExecute();

	private:
		/* 协议控制块 */
		struct ST_TaskControlBlock;
		typedef struct ST_ProtocolControlBlock
		{
			IProtocol		*protocol_interface_ptr;
			CSimpleArray<IProtocolHook*>			hook_array;	//协议预处理器数组
			std::vector<ST_TaskControlBlock*>	tcb_ptr_vector;
			CPMDatabase::ST_PCB_Store *store_ptr;	///< 对应数据库项
		} ST_PCB;

		/* 任务控制块 */
		struct ST_GroupControlBlock;
		typedef struct ST_TaskControlBlock
		{
			uint32_t					spacific_tag;
			ST_ProtocolControlBlock	*pcb_ptr;		///< 任务(所属)对应的协议
			ST_GroupControlBlock		*gcb_ptr;		///< 任务所属组
			TASK_HANDLE				task_handle;	///< 协议相关的任务句柄(是启动任务时协议函数的返回值),用于以后查询协议任务信息
			CPMDatabase::ST_TCB_Store *store_ptr;	///< 对应数据库项
		} ST_TCB;

		/* 组控制块 */
		typedef struct ST_GroupControlBlock
		{
			/**
			 * @brief 获取group_id
			 * @note 使用crc32对 data contianer 的 file name 作哈希得到
			 */
			uint32_t getGroupHashCode();
			void save_data_segment_stack(CPMDatabase &db);
			void restore_data_segment_stack(CPMDatabase &db);

			uint32_t			spacific_tag;
			IDataContainer		*data_container_ptr;
			std::vector<ST_TaskControlBlock*>	tcb_ptr_vector;	///< 组內所有任务的控制块指针集合
			std::vector<std::pair<off_t, off_t>> data_segment_stack;	///< 数据段边界(<起始偏移，边界>)栈
			CPMDatabase::ST_GCB_Store *store_ptr;					///< 对应数据库项
		} ST_GCB;

	private:
		uint32_t	pm_getProtocolHashTagFromAddress ( const char *protocol_address_ptr );
		ST_PCB		*pm_selectProtocol ( const char *protocol_address, int selector_id );

		size_t tgm_getGroupInformation( ST_GroupControlBlock *gcb_ptr, ST_GroupInformation *group_infor_ptr );
		void tgm_resumeTask( ST_TaskControlBlock *tcb_ptr );

	private:
		HDIR											m_plus_dir_handle;
		CPMDatabase									m_pm_database;
		int												m_default_time_out, m_restore_status_time;
		std::map<uint32_t, ST_ProtocolControlBlock*>	m_protocol_control_block_table;	//使用 map（红黑树） 就足够了,不需要使用散列表
		std::map<uint32_t, ST_GroupControlBlock*>	m_group_control_block_table;
		bool											m_running;
		std::thread									m_store_thread;

#ifdef EX_TEST
	private:/* only for test */
		friend struct __pm_instance_helper;
		FRIEND_TEST( ProtocolManager, CreateAndDestroy );
		FRIEND_TEST( ProtocolManager, RegisterAndUnregisterProtocol );
		FRIEND_TEST( ProtocolManager, AddAndDeleteGroup );
		FRIEND_TEST( ProtocolManager, AddAndRemoveProtocolTaskOfGroup );
#endif
};

}/* ProtocolManager **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef PROTOCOLMANAGER_INC  ----- */
