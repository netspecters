#ifndef  TOKENTREE_INC
#define  TOKENTREE_INC

#include <cstring>

template < int INDEX_LEN >
class CTokenTree
{
	public:

		CTokenTree( ) : m_token_num(0)
		{}

		int searchToken( char *&token_ptr, char stop_char ) //在扫描的同时会修改指针
		{
			ST_TokenNode *current_token_node_ptr = &m_first_token_node;
			int char_index = 0;
			while( *token_ptr != stop_char )
			{
				//根据http和ftp协议的要求，对token的大小写不敏感，全部转换成小写字母进行比对
				char current_char = *token_ptr;
				lowChar( current_char );

				char_index =  get_index( current_char );
				ST_TokenNode *next_token_node_ptr = current_token_node_ptr->next_token_nodes[ char_index ];
				if( !next_token_node_ptr )
					break;
				current_token_node_ptr = next_token_node_ptr;

				token_ptr++;
			}
			if ( (current_token_node_ptr->node_id[char_index] == 0) )	//没有找到对应项
			{
				while(*token_ptr++ != stop_char);	//仍然要到 stopchar
				return -1;
			}
			else
			{
				return current_token_node_ptr->node_id[char_index];
			}
		}

		int addToken(const char *token_ptr )
		{
			ST_TokenNode *current_token_node_ptr = &m_first_token_node;
			int char_index = 0;
			while( *token_ptr )
			{
				//根据http和ftp协议的要求，对token的大小写不敏感，全部转换成小写字母进行比对
				char current_char = *token_ptr;
				lowChar( current_char );

				char_index = get_index( current_char );
				ST_TokenNode *&next_token_node_ptr = current_token_node_ptr->next_token_nodes[char_index];
				if( next_token_node_ptr == NULL )
				{
					next_token_node_ptr = new ST_TokenNode();
				}
				current_token_node_ptr = next_token_node_ptr;

				token_ptr++;
			}
			current_token_node_ptr->node_id[ char_index ] = ++m_token_num;
			return m_token_num;
		}

	protected:
		virtual int get_index(char c) = 0;

	private:
		struct ST_TokenNode
		{
			ST_TokenNode(void)
			{
				memset(next_token_nodes, 0, sizeof(ST_TokenNode*)*INDEX_LEN);
				memset(node_id, 0, sizeof(int)*INDEX_LEN);
			}
			ST_TokenNode	*next_token_nodes[INDEX_LEN];
			int				node_id[INDEX_LEN];
		};

		void lowChar(char &c)
		{
			if (c>='A' && c<='Z')
				c += 32;
		}

		ST_TokenNode m_first_token_node;
		int m_token_num;
};

#endif   /* ----- #ifndef TOKENTREE_INC  ----- */
