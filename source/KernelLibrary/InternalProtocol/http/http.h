/*
 * =====================================================================================
 *
 *       Filename:  http.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2009年10月15日 23时54分02秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */

#ifndef  http_INC
#define  http_INC

#include "../../ProtocolManager/Interfaces.h"
#include "../../DataContainer/Interfaces.h"
#include "nslib.h"

#include "http_task.h"

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace HTTP {

using namespace Netspecters::KernelPlus::ProtocolManager;
using namespace Netspecters::KernelPlus::DataContainer;
using namespace Netspecters::NSStdLib;

class CHttpProtocol : public IProtocol
{
	public:
		/* INSPlugin */
		virtual NSAPI int onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param );
		virtual NSAPI void onUnload( void );
		virtual NSAPI int getParam( const char *param_name, void *value_buffer_ptr );
		virtual NSAPI void *setParam( const char *param_name, void *value_ptr );

		/* IProtocol */
		virtual NSAPI TASK_HANDLE startTask( const ST_TaskInitParam &task_init_param,
											 GROUP_TASK_HANDLE group_task_handle,
											 IDataContainer *data_container_ptr,
											 ITaskMonitor *task_monitor_ptr );
		virtual NSAPI void stopTask( TASK_HANDLE task_handle );
		virtual NSAPI ENM_TaskStopCode getStopCode( TASK_HANDLE task_handle );
		virtual NSAPI int queryTaskInformation( TASK_HANDLE task_handle, ST_TaskInformation *task_infor_ptr );
		virtual NSAPI void delTask( TASK_HANDLE task_handle );

	private:
		CThreadedList<CHttpTask *> m_task_list;
		IProtocolManager *m_protocol_manager_ptr;
};

}/* HTTP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef http_INC  ----- */
