#ifndef  HTTP_TASK_INC
#define  HTTP_TASK_INC

#include <list>
#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif

#include "../../DataContainer/data_container_manager.h"
#include "nslib.h"

#include "Interfaces.h"
#include "task_thread.h"

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace HTTP {

using namespace Netspecters::NSStdLib;
using namespace Netspecters::KernelPlus::NetLayer;
using namespace Netspecters::KernelPlus::DataContainer;
using namespace Netspecters::KernelPlus::ProtocolManager;

class CHttpTask
{
	public:
		CHttpTask( const IProtocol::ST_TaskInitParam &task_init_param,
				   GROUP_TASK_HANDLE group_task_handle,
				   ITaskGroupManager *task_group_manager_ptr,
				   IDataContainer *data_container_ptr,
				   IProtocol::ITaskMonitor *task_monitor_ptr );
		~CHttpTask();
		IProtocol::ENM_TaskStopCode getStopCode(){
			if(m_running_thread_ptrs.size() > 0)
				return IProtocol::tscRunning;
			else if( m_thread_count > 0 )
				return IProtocol::tscError;
			else
				return IProtocol::tscPause;
		}
		void saveTaskState();
		void readTaskInformation( IProtocol::ST_TaskInformation &task_information );

	private:
		struct ST_StateData : public IProtocol::ST_TaskStateData
		{
			int 	thread_num;
			off_t	abs_recved_data_size;
			int		std_url_length;
			uint8_t state_data[0];//url_string
		} __attribute__(( packed ) );

	private:
		friend class CTaskThread;
		CTaskThread::ST_ThreadEnvironment	m_thread_environment;
		std::list<CTaskThread *>	m_running_thread_ptrs;
		IProtocol::ITaskMonitor	*m_task_monitor_ptr;
		unsigned m_thread_count;	//成功创建计数加一，成功完成计数减一
		off_t	m_abs_recved_data_size;
		int		m_max_thread_count;
#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( InternalProtocol_HTTP, Task_CreateAndDestroy );
		FRIEND_TEST( InternalProtocol_HTTP, Task_SaveTaskState );
		FRIEND_TEST( InternalProtocol_HTTP, Task_ReadTaskInformation );
#endif
};

}/* HTTP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef HTTP_TASK_INC  ----- */
