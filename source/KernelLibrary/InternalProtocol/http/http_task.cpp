#include <string>
#ifdef __MINGW32__
#	include <malloc.h>
#else
#	include <alloca.h>
#endif

#include "http_task.h"

using namespace Netspecters::KernelPlus::InternalProtocol::HTTP;

CHttpTask::CHttpTask( const IProtocol::ST_TaskInitParam &task_init_param, GROUP_TASK_HANDLE group_task_handle, ITaskGroupManager *task_group_manager_ptr,
					  IDataContainer *data_container_ptr, IProtocol::ITaskMonitor *task_monitor_ptr )
	: m_task_monitor_ptr( task_monitor_ptr ), m_abs_recved_data_size( 0 )
	, m_max_thread_count( 0 )
{
	/* 初始化公共线程环境 */
	m_thread_environment.http_task_ptr = this;
	m_thread_environment.data_container_ptr = data_container_ptr;
	m_thread_environment.task_group_manager_ptr = task_group_manager_ptr;
	m_thread_environment.group_task_handle = group_task_handle;

	/* 如果 init_task_state_data_ptr 不为空，表示本任务是重启任务 */
	if( task_init_param.init_task_state_data_ptr )
	{
		const ST_StateData *state_data_ptr = reinterpret_cast<const ST_StateData *>( task_init_param.init_task_state_data_ptr );

		/* 还原全局变量 */
		m_abs_recved_data_size = state_data_ptr->abs_recved_data_size;

		/* 初始化公共http请求头 */
		std::string url( reinterpret_cast<const char *>( state_data_ptr->state_data ), state_data_ptr->std_url_length );		unsigned buf_size = sizeof( IProtocol::ST_TaskResource ) + url.size() + 1;
		IProtocol::ST_TaskResource *request_init_param = ( IProtocol::ST_TaskResource * )alloca( buf_size );
		memset( request_init_param, 0, buf_size );
		memcpy( request_init_param->resource_data, url.data(), url.size() );
		m_thread_environment.request_header_ptr = new CHTTPRequest( *request_init_param );

		/* 还原线程 */
		m_max_thread_count = state_data_ptr->thread_num;
		for( int i = 0; i < state_data_ptr->thread_num; i++ )
		{
			m_thread_count++;
			m_running_thread_ptrs.push_back( new CTaskThread( m_thread_environment ) );
		}

		/* 初始化第一个线程 */
		( *m_running_thread_ptrs.begin() )->connect();
	}
	else
	{
		/* 获取变量 */
		m_max_thread_count = task_init_param.task_resource_ptr->max_thread_num;

		/* 初始化公共http请求头 */
		m_thread_environment.request_header_ptr = new CHTTPRequest( *task_init_param.task_resource_ptr );

		/* 构建第一个线程并初始化 */
		CTaskThread *new_thread_ptr = new CTaskThread( m_thread_environment );
		m_thread_count++;
		m_running_thread_ptrs.push_back( new_thread_ptr );
		new_thread_ptr->connect();
	}

	/* 第一次存储线程状态 */
	saveTaskState();
}

CHttpTask::~CHttpTask()
{
	/* 释放所有线程 */
	for( auto thread_ptr_itr = m_running_thread_ptrs.begin(); thread_ptr_itr != m_running_thread_ptrs.end(); ++thread_ptr_itr )
		delete( *thread_ptr_itr );
}

void CHttpTask::saveTaskState()
{
	size_t url_string_len = m_thread_environment.request_header_ptr->getOrgURI().size();
	/* 计算task state data size */
	size_t task_state_data_size = sizeof( ST_StateData ) + url_string_len;

	ST_StateData *state_data_ptr = reinterpret_cast<ST_StateData *>( alloca( task_state_data_size ) );
	state_data_ptr->data_size = task_state_data_size - sizeof( IProtocol::ST_TaskStateData );
	state_data_ptr->thread_num = m_running_thread_ptrs.size();
	state_data_ptr->abs_recved_data_size = m_abs_recved_data_size;

	/* write url string */
	state_data_ptr->std_url_length = url_string_len;
	memcpy( state_data_ptr->state_data, m_thread_environment.request_header_ptr->getOrgURI().data(), url_string_len );

	m_thread_environment.task_group_manager_ptr->storeTaskStateData( m_thread_environment.group_task_handle, state_data_ptr );
}

void CHttpTask::readTaskInformation( IProtocol::ST_TaskInformation &task_information )
{
	task_information.thread_count = m_running_thread_ptrs.size();
	task_information.transfer_speed_up = 0;
	task_information.transfer_speed_down = m_thread_environment.speed_group.get_speed();
	task_information.send_data_size = 0;
	task_information.recved_data_size = m_abs_recved_data_size;
}
