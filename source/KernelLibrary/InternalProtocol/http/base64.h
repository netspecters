/*
 * =====================================================================================
 *
 *       Filename:  base64.h
 *
 *    Description:  base64编码解码单元
 *
 *        Version:  1.0
 *        Created:  2009-1-27 23:34:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (WG), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */

#ifndef  BASE64_INC
#define  BASE64_INC

#include <string>

using std::string;

void base64_encode(const string &decode_string, string &base64_string);
void base64_decode(const string &encoded_string, string &decode_string);

#endif   /* ----- #ifndef BASE64_INC  ----- */
