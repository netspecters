#include "http.h"

#include "../../../NSErrorCode.h"
#include "../../../StdLib/OSFunc.hpp"

#define PTR_TO_HANDLE(x)	reinterpret_cast<uintptr_t>(x)
#define HANDLE_TO_TASK_PTR(x) reinterpret_cast<CHttpTask*>(x)

using namespace Netspecters::NSStdLib;
using namespace Netspecters::KernelPlus::InternalProtocol::HTTP;

int CHttpProtocol::onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param )
{
	//获取协议管理器的指针，注册协议
	m_protocol_manager_ptr = static_cast<IProtocolManager *>( parent_plus_ptr );
	m_protocol_manager_ptr->registerProtocol( const_cast<char *>( "http" ), 0, this );

	printk( KINFORMATION"HTTP protocol loaded.\n" );
	return NSEC_SUCCESS;
}

void CHttpProtocol::onUnload( void )
{
	printk( KINFORMATION"HTTP protocol unloaded.\n" );
}

int CHttpProtocol::getParam( const char *param_name, void *value_buffer_ptr )
{
	return 0;
}

void *CHttpProtocol::setParam( const char *param_name, void *value_ptr )
{
	return NULL;
}

TASK_HANDLE CHttpProtocol::startTask( const ST_TaskInitParam &task_init_param, GROUP_TASK_HANDLE group_task_handle, IDataContainer *data_container_ptr, ITaskMonitor *task_monitor_ptr )
{
	ITaskGroupManager *task_group_manager_ptr = m_protocol_manager_ptr->getTaskGroupManager();
	CHttpTask *http_task_ptr = new CHttpTask( task_init_param, group_task_handle, task_group_manager_ptr, data_container_ptr, task_monitor_ptr );
	return PTR_TO_HANDLE( http_task_ptr );
}

void CHttpProtocol::stopTask( TASK_HANDLE task_handle )
{
	HANDLE_TO_TASK_PTR( task_handle )->saveTaskState();
	delete HANDLE_TO_TASK_PTR( task_handle );
}

IProtocol::ENM_TaskStopCode CHttpProtocol::getStopCode( TASK_HANDLE task_handle )
{
	return HANDLE_TO_TASK_PTR( task_handle )->getStopCode();
}

int CHttpProtocol::queryTaskInformation( TASK_HANDLE task_handle, ST_TaskInformation *task_infor_ptr )
{
	CHttpTask *task_ptr = HANDLE_TO_TASK_PTR( task_handle );
	if( task_infor_ptr )
		task_ptr->readTaskInformation( *task_infor_ptr );
	return NSEC_SUCCESS;
}

void CHttpProtocol::delTask( TASK_HANDLE task_handle )
{
	delete HANDLE_TO_TASK_PTR( task_handle );
}
