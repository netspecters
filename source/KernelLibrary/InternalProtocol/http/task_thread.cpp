#include <assert.h>
#include "task_thread.h"
#include "http_task.h"

using namespace Netspecters::KernelPlus::InternalProtocol::HTTP;

#define PTR_TO_TAG(x) reinterpret_cast<long>(x)

CTaskThread::CTaskThread( const ST_ThreadEnvironment &shared_thread_environment )
	: m_thread_environment( shared_thread_environment )	, m_data_segment_ptr( NULL )
	, m_respond_parser_ptr( NULL )
	, m_state( tsNone )
	, m_max_retry_count( 100 )	/* 默认重试次数为99次 */
#ifdef EX_TEST	//测试状态下用不到真正的socket
	, m_socket_ptr( NULL )
#else
	, m_socket_ptr( new CSocket(( CSocket::ST_Callback ) {
	on_connect, NULL, on_send, on_recv, on_error
} ) )
#endif
{
	/* 请求分配segment */
	shared_thread_environment.task_group_manager_ptr->openSegment( shared_thread_environment.group_task_handle, ISegment::ST_SegmentInfor::stFIFO, m_data_segment_ptr );
#ifndef EX_TEST	/* 设置socket的key为task thread的实例 */
	m_socket_ptr->set_socket_key(( size_t )this );	/* 加入速度测量组 */	shared_thread_environment.speed_group.append( m_socket_ptr );
#endif
}

CTaskThread::~CTaskThread()
{
	if( m_data_segment_ptr )
		m_thread_environment.task_group_manager_ptr->closeSegment( m_thread_environment.group_task_handle, m_data_segment_ptr );
	delete m_respond_parser_ptr;#ifndef EX_TEST	delete m_socket_ptr;	m_thread_environment.speed_group.remove( m_socket_ptr );#endif
}

void CTaskThread::connect()
{
	/* 连接到服务器 */
	IP_Address server_address = IP_Address::from_host_name( m_thread_environment.request_header_ptr->getHostName() );
	if( server_address.IP )	/* 做单元测试的时候返回恒为0,因此不会真正连接服务器 */
	{
		server_address.Port = IP_Address::host2net( m_thread_environment.request_header_ptr->getHostPort() );
#ifndef EX_TEST
		m_socket_ptr->connect( server_address, 0, -1 );
#endif
	}
}

void CTaskThread::sendRequest()
{
	off_t segment_offset = 0; /* 如果数据容器空，在http协议获取数据长度后再分配segment */

	if( m_data_segment_ptr )
		/* 数据容器非空，获取起始段偏移，以便设置http协议中的range字段 */
		segment_offset = m_data_segment_ptr->getSegmentOffset();

	/* 设置range字段 */
	CHTTPRequest::ST_OPTHeaderValues opt_request_header = { segment_offset, -1 };

	/* 使用托管内存缓冲区，取出请求头数据 */
	CSocket::ST_Buffer *head_buf_ptr = CSocket::alloc_buffer( CSocket::ST_Buffer::type_4K );
	head_buf_ptr->size = m_thread_environment.request_header_ptr->dumpHeaderData( static_cast<char *>( head_buf_ptr->data ), 4096, opt_request_header );

	/* 释放以前的应答解析器 */
	delete m_respond_parser_ptr;
#ifdef EX_TEST	//测试状态下解析器由测试用例构建
	m_respond_parser_ptr = NULL;
#else
	/* 建立新的应答解析器 */
	m_respond_parser_ptr = new CHTTPRespond;
#endif

	/* 送出http header */
	m_state = tsCommand;
#ifndef EX_TEST	//测试状态下不应该真实发送数据
	m_socket_ptr->send( head_buf_ptr, 0, -1 );
#endif
}

off_t CTaskThread::getThreadRecvedDataSize()
{
	ISegment::ST_SegmentInfor seg_infor;
	m_data_segment_ptr->getSegmentInformation( seg_infor );
	return m_data_segment_ptr->getIOPosition() - seg_infor.segment_base_offset;
}

void CTaskThread::freeMe()
{
#ifndef EX_TEST	//测试状态下
	m_socket_ptr->close();
	m_thread_environment.http_task_ptr->m_running_thread_ptrs.remove( this );
	delete this;
#endif
}

/* ================================ 事件处理 ===================================== */

int CTaskThread::on_connect( CSocket *socket_ptr, uintptr_t io_key )
{
	CTaskThread *task_thread_ptr = ( CTaskThread * )( socket_ptr->get_socket_key() );
	task_thread_ptr->sendRequest();
	return NSEC_SUCCESS;
}
int CTaskThread::on_send( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key )
{
	/* 使用接受缓冲区作为发送缓冲区 */
	buffer_ptr->size = 32;

	/* 接收应答(PEEK) */
	CTaskThread *task_thread_ptr = ( CTaskThread * )( socket_ptr->get_socket_key() );
	task_thread_ptr->m_socket_ptr->recv( buffer_ptr, 0, -1 );
	return NSEC_SUCCESS;
}

int CTaskThread::on_recv( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key )
{
	CTaskThread *task_thread_ptr = ( CTaskThread * )( socket_ptr->get_socket_key() );
	task_thread_ptr->onNodeRecvData( buffer_ptr );
	return NSEC_SUCCESS;
}

void CTaskThread::onNodeRecvData( CSocket::ST_Buffer *buffer_ptr )
{
	void *data_ptr = buffer_ptr->data;
	size_t data_size = buffer_ptr->size;
	size_t next_recv_data_size = 0;

	switch( m_state )
	{
		case tsCommand:
			{
				size_t command_size = data_size;
				if( m_respond_parser_ptr->appendRespondData( static_cast<char *>( data_ptr ), command_size ) )
				{
					int respond_extend_code = 0;
					CHTTPRespond::ENM_RespondMainCode respond_main_code = m_respond_parser_ptr->getRespondMainCode( respond_extend_code );

					/* 检测服务器返回的状态码 */
					switch( respond_main_code )
					{
							/* 服务器接受请求 */
						case CHTTPRespond::rmcOK:
							/* 如今还没有分配segment表示data_container还未知大小,数据容器为空 */
							if( !m_data_segment_ptr )
							{
								m_thread_environment.data_container_ptr->resetDataSize( atoi( m_respond_parser_ptr->getParam( CHTTPRespond::ptcContentLength ) ) );
								/* 分配segment */
								m_thread_environment.task_group_manager_ptr->openSegment( m_thread_environment.group_task_handle, ISegment::ST_SegmentInfor::stFIFO, m_data_segment_ptr );
							}

							/* 检测缓冲区里接收完命令后剩余的数据 */
							if( command_size > 0 )
							{
								char *rest_data_ptr = ( char * )data_ptr + data_size - command_size;
								m_data_segment_ptr->write( rest_data_ptr, command_size, 0, 0 );
							}

							/* 计算应该接收的数据量 */
							next_recv_data_size = m_data_segment_ptr->getSegmentSize() - m_data_segment_ptr->getIOPosition();

							/* 进入 '接收数据' 状态 */
							m_state = tsTransferData;

							/* 释放应答解析器 */
							delete m_respond_parser_ptr;
							m_respond_parser_ptr = NULL;
							break;

							/* 服务器指示地址重定位 */
						case CHTTPRespond::rmcRedirect:	//将再次进入命令状态
							m_thread_environment.request_header_ptr->resetURLAddress( m_respond_parser_ptr->getParam( CHTTPRespond::ptcLocation ) );
							sendRequest();	//再次发送协议头
							break;

							/* 服务器应答失败 */
						case CHTTPRespond::rmcInvaildRequest:
						case CHTTPRespond::rmcServerError:
							if( --m_max_retry_count > 0 )
								sendRequest();
							else	//线程意外终止
							{
								m_state = tsAbort;
								/* 释放应答解析器 */
								delete m_respond_parser_ptr;
								m_respond_parser_ptr = NULL;
								freeMe();
							}
							break;
					}
				}
				else	//继续预取
				{
					if( m_socket_ptr->recv( buffer_ptr, 0, 0 ) > 0 )
						onNodeRecvData( buffer_ptr );
					else
					{
						/* 数据缓冲区內已经没有数据了，需要非阻塞等待 */
						buffer_ptr->size = 1;
						m_socket_ptr->recv( buffer_ptr, 0, -1 );
					}
				}
			}
			break;

		case tsTransferData:
			m_data_segment_ptr->write( data_ptr, data_size, 0, 0 );

			/* 计算下次应该接受的数据量 */
			if( 0 == ( next_recv_data_size = m_data_segment_ptr->getSegmentSize() - m_data_segment_ptr->getIOPosition() ) )
			{
				/* 此段已经传输完毕，尝试合并下一段 */
				if( !m_thread_environment.data_container_ptr->tryMergeSegment( m_data_segment_ptr, IDataContainer::msCombineNext ) )
				{
					/* 合并数据段失败后，此线程的任务已经完成，释放所占用的资源 */
					m_thread_environment.task_group_manager_ptr->closeSegment( m_thread_environment.group_task_handle, m_data_segment_ptr );
					m_data_segment_ptr = NULL;

					m_thread_environment.http_task_ptr->m_thread_count--;
					freeMe();
				}
			}
			break;

		default:
			assert( false );
			break;
	}

	/* 请求socket接收指定长度数据 */
	if( next_recv_data_size != 0 )
	{
		CSocket::ST_Buffer::ENM_Type best_buffer_type = m_socket_ptr->get_best_buffer_type();	//获取最佳数据区类型
		if( best_buffer_type != buffer_ptr->type )
		{
			/* 最佳数据区类型跟当前使用的数据区类型不相同，需要重新分配数据区 */
			CSocket::free_buffer( buffer_ptr );
			buffer_ptr = CSocket::alloc_buffer( best_buffer_type );
			buffer_ptr->size = CSocket::ST_Buffer::get_type_size( best_buffer_type );
		}
		buffer_ptr->size = buffer_ptr->size <= next_recv_data_size ? : next_recv_data_size;
		m_socket_ptr->recv( buffer_ptr, 0, -1 );
	}
}

int CTaskThread::on_error( CSocket *socket_ptr, CSocket::ST_Callback::ENM_ErrorCode error_code, CSocket::ST_Buffer *buffer_ptr )
{	/* 所有的socket错误都应该关闭对应线程 */	CTaskThread *task_thread_ptr = ( CTaskThread * )( socket_ptr->get_socket_key() );	task_thread_ptr->freeMe();}

int CTaskThread::onIORealRead( void *data_buffer, size_t transfer_data_size, long io_tag )
{
	assert( false );
	//never occur!!
	return NSEC_SUCCESS;
}

int CTaskThread::onIORealWrite( void *data_buffer, size_t transfer_data_size, long io_tag )
{
	assert( false );
	//never occur!!
	return NSEC_SUCCESS;
}

int CTaskThread::onIOError( ENM_IOErrorCode io_error_code )
{
	m_state = tsAbort;
	return NSEC_SUCCESS;
}

bool CTaskThread::enableMerge( ISegment *request_segment_ptr )
{
	bool enable_merge = (0 == m_data_segment_ptr->getIOPosition());
	if( enable_merge )
	{		m_data_segment_ptr = NULL;
		freeMe();
	}
	return enable_merge;
}
