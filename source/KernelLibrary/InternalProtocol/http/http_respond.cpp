#include "http_respond.h"
#include <string>

using namespace Netspecters::KernelPlus::InternalProtocol::HTTP;
using std::string;

CHTTPRespond::CHttpTokenTree CHTTPRespond::static_m_token_tree;

void CHTTPRespond::reset()
{
	memset( m_respond_buffer, 0, C_MAX_BUFFER_SIZE );
}

bool CHTTPRespond::appendRespondData( const char *data_buffer_ptr, size_t &buffer_size )
{
	//得到当前位置指针
	char *respond_buffer_ptr = m_respond_buffer + m_current_data_pos;

	//拷贝数据的同时分析处理
	int data_size = buffer_size;
	bool go_on = true;
	while( data_size > 0 )
	{
		switch( m_parase_status )
		{
			case psCommand_Line:
				if( '\n' == *data_buffer_ptr )
					m_parase_status = psCommand_Line_End;
				break;

			case psCommand_Line_End:	//'\n'
				if( '\r' == *data_buffer_ptr )	/* 遇到最后一行结束符 */
				{
					m_parase_status = psCommand_Finish;

					/* 跳过外界给出的结束符，在结束后由本函数再加上结束符，防止畸形缓冲区造成程序崩溃 */
					data_size -= 2;
					if( data_size == -1 )
					{
						m_parase_status = psCommand_Finish0;
						data_size = 0;
					}
				}
				else
					m_parase_status = psCommand_Line;
				break;

			case psCommand_Finish0:
				m_parase_status = psCommand_Finish;
				data_size--;
				break;
		}

		/* 没有结束的时候要拷贝数据 */
		if( m_parase_status != psCommand_Finish )
		{
			*respond_buffer_ptr++ = *data_buffer_ptr++;
			data_size--;
		}
		else
		{
			/* 添加结束符 */
			*respond_buffer_ptr++ = '\r';
			*respond_buffer_ptr++ = '\n';
			break;
		}
	}

	//调整拷贝大小
	buffer_size -= data_size;
	m_current_data_pos += buffer_size;

	if( m_parase_status == psCommand_Finish )
	{
		parseBuffer();
		return true;
	}
	else
		return false;
}

void CHTTPRespond::parseBuffer()
{
	memset( m_param_pos, 0, sizeof( char* )*ptcCount );
	char *respond_buffer_ptr = m_respond_buffer;

	/* 跳过状态行 */
	while( *respond_buffer_ptr++ != '\n' );

	//解析参数(为了加快速度，使用时间复杂度为O(n)的编码树)
	enum ENM_ProcessStatus { psStart, psCheckName, psCheckValue, psFinish };
	ENM_ProcessStatus next_status = psStart, current_status;
	while(( current_status = next_status ) != psFinish )
	{
		switch( current_status )
		{
			case psStart:	//提取参数标识
				next_status = *respond_buffer_ptr == '\r' ? psFinish : psCheckName;		//最后一行以'\r\n'结束
				break;

			case psCheckName:
				{
					respond_buffer_ptr--;
					int token_id = static_m_token_tree.searchToken( respond_buffer_ptr, ':' );
					if( token_id > 0 )
						m_param_pos[ token_id - 1 ] = ++respond_buffer_ptr;
					next_status = psCheckValue;
				}
				break;

			case psCheckValue:
				if( *respond_buffer_ptr == '\r' )
					*respond_buffer_ptr = '\0';			//行结束时，将'\r'换成'\0'
				else if( *respond_buffer_ptr == '\n' )	//每行最后以'\r\n'结束
					next_status = psStart;
				break;
		}
		respond_buffer_ptr++;
	}
}

const char *CHTTPRespond::getParam( ENM_ParamTypeCode param_type_code )
{
	char *result = m_param_pos[param_type_code];

	/* 跳过头部空格（如果有） */
	if( result )
		while( *result == ' ' )
			result++;

	return result;
}

CHTTPRespond::ENM_RespondMainCode CHTTPRespond::getRespondMainCode( int &extend_code )
{
	char *current_pos_ptr = m_respond_buffer + 9;	//跳过 "HTTP/1.1 "
	int respond_code = *current_pos_ptr++ - '0';	//ascii字符变数字

	int first_code = ( *current_pos_ptr++ - '0' );
	int second_code = ( *current_pos_ptr - '0' );
	extend_code =  first_code * 10 + second_code;	//扩展状态码只有两位
	return ( ENM_RespondMainCode )respond_code;
}
