#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../http_respond.h"

namespace Netspecters {
namespace KernelPlus {
namespace InternalProtocol {
namespace HTTP {

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	CHTTPRespond http_respond;\
	enable_continue = false

#define TEST_END()\
	enable_continue = true

/* 定义测试用的应答数据 */
#define COOKIE "asdfeas2%das&*^dfshdg124235!#@234wiorjgjsdkjf;lasjdfj;hhg"
#define LOCATION "http://www.refer.org/path/to/file.tmp"
static const char *g_respond_data[] = {
	"HTTP/1.1 302 OK\r\nContent-Type: text/html",
	"\r\nServer: Netspecters Test Case\r\nDate: Tue, 18 May 2010 21:35:10 GMT\r\n",
	"Content-Length: 0\r\nLocation: "LOCATION,
	"\r\nCache-control: private\r\nConnection: keep-alive\r\n",
	"Set-Cookie: "COOKIE"\r\n",
	"\r\n"
};

TEST( InternalProtocol_HTTP, Respond_AppendRespondData )
{
	TEST_BEGIN();

	/* 输入应答数据 */
	for( int i = 0; i < 6; i++ )
	{
		size_t len = strlen( g_respond_data[i] );
		bool ok = http_respond.appendRespondData( g_respond_data[i], len );
		ASSERT_EQ( strlen( g_respond_data[i] ), len ) << "respond data index is " << i;
		if( i == 5 )
			ASSERT_TRUE( ok );
		else
			ASSERT_FALSE( ok );
	}

	/* 检测应答数据解析结果 */
	{
		const char *expect_buffer_data[] = {
			"HTTP/1.1 302 OK\r\nContent-Type: text/html",
			"\nServer: Netspecters Test Case",
			"\nDate: Tue, 18 May 2010 21:35:10 GMT",
			"\nContent-Length: 0",
			"\nLocation: "LOCATION,
			"\nCache-control: private\0\nConnection: keep-alive",
			"\nSet-Cookie: "COOKIE,
			"\n\r\n"
		};
		int pos = 0;
		for( int i = 0; i < 6; i++ )
		{
			size_t len = strlen( expect_buffer_data[i] );
			char actual[len+1];
			memcpy( actual, &http_respond.m_respond_buffer[pos], len );
			actual[len] = '\0';
			EXPECT_STREQ( expect_buffer_data[i], actual );
			pos += len + 1;
		}
	}

	TEST_END();
}

TEST( InternalProtocol_HTTP, Respond_GetRespondMainCode )
{
	TEST_BEGIN();

	/* 输入应答数据 */
	for( int i = 0; i < 6; i++ )
	{
		size_t len = strlen( g_respond_data[i] );
		http_respond.appendRespondData( g_respond_data[i], len );
	}

	/* 获取解析后的应答码 */
	int ext_code;
	EXPECT_EQ( 3, http_respond.getRespondMainCode( ext_code ) );
	EXPECT_EQ( 2, ext_code );

	TEST_END();
}

TEST( InternalProtocol_HTTP, Respond_GetParam )
{
	TEST_BEGIN();

	/* 输入应答数据 */
	for( int i = 0; i < 6; i++ )
	{
		size_t len = strlen( g_respond_data[i] );
		http_respond.appendRespondData( g_respond_data[i], len );
	}

	/* 获取并验证参数 */
	EXPECT_STREQ( "0", http_respond.getParam( CHTTPRespond::ptcContentLength ) );
	EXPECT_EQ( 0, ( uintptr_t )http_respond.getParam( CHTTPRespond::ptcTransferEncoding ) );
	EXPECT_STREQ( LOCATION, http_respond.getParam( CHTTPRespond::ptcLocation ) );
	EXPECT_STREQ( COOKIE, http_respond.getParam( CHTTPRespond::ptcSetCookie ) );

	TEST_END();
}

}/* HTTP **/
}/* InternalProtocol **/
}/* KernelPlus **/
}/* Netspecters **/
