#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "../http_request.h"

namespace Netspecters {
namespace KernelPlus {
namespace InternalProtocol {
namespace HTTP {

static bool enable_continue = true;
extern const char g_res_path[];

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	size_t __size = sizeof(IProtocol::ST_TaskResource) + strlen(g_res_path) + 1;\
	IProtocol::ST_TaskResource *task_res = (IProtocol::ST_TaskResource*)alloca(__size);\
	memset( task_res, 0, __size );\
	strcpy(task_res->resource_data, g_res_path);\
	CHTTPRequest http_request( *task_res );\
	enable_continue = false

#define TEST_END()\
	enable_continue = true

TEST( InternalProtocol_HTTP, Request_CreateAndDestroy )
{
	enable_continue = false;

	size_t size = sizeof(IProtocol::ST_TaskResource) + strlen(g_res_path) + 1;
	IProtocol::ST_TaskResource *task_res = (IProtocol::ST_TaskResource*)alloca(size);
	memset( task_res, 0, size );
	strcpy(task_res->resource_data, g_res_path);
	CHTTPRequest http_request( *task_res );

	/* 测试对象成员 */
	EXPECT_STREQ( g_res_path, http_request.m_org_uri.c_str() );
	EXPECT_EQ( 80, http_request.m_host_port );
	EXPECT_EQ( 199, http_request.m_http_header_size );
	EXPECT_EQ( 7, http_request.m_header_value_count );

	enable_continue = true;
}

TEST( InternalProtocol_HTTP, Request_DumpHeaderData )
{
	TEST_BEGIN();

	/* 测试不同的可选头部(Range)参数 */
	CHTTPRequest::ST_OPTHeaderValues op_headers[3] = { {256, 1024}, {0, 0}, {1024, -1}};
	const char *range_values[3] = { "Range: bytes=256-1024\r\n", "", "Range: bytes=1024-\r\n" };
	bool need_value[3] = { true, false, true };
	int expect_size[3] = {254, 231, 251};
	for( int i = 0; i < 3; i++ )
	{
		/* 获取数据长度 */
		size_t data_size = http_request.dumpHeaderData( NULL, 0, op_headers[i] );

		/* 检验长度是否正确 */
		ASSERT_EQ( expect_size[i], data_size );

		/* 获取数据 */
		char data_buf[data_size+1];
		ASSERT_EQ( data_size, http_request.dumpHeaderData( data_buf, data_size, op_headers[i] ) );
		data_buf[data_size] = '\0';

		/* 验证数据 */
		int pos = 0;
#define _CHECK(x)\
	do\
	{\
		const char expect[]={x};\
		unsigned len = strlen( x );\
		char actual[len+1];\
		actual[len] = '\0';\
		memcpy(actual, &data_buf[pos], len);\
		EXPECT_STREQ( expect, actual );\
		pos += len;\
	}while(0)
#define __CHECK(x)\
	do\
	{\
		const char *&expect=x;\
		unsigned len = strlen( x );\
		char actual[len+1];\
		actual[len] = '\0';\
		memcpy(actual, &data_buf[pos], len);\
		EXPECT_STREQ( expect, actual );\
		pos += len;\
	}while(0)
#define CHECK(x) _CHECK(x"\r\n")
		CHECK( "GET path/to/file.tmp HTTP/1.0" );
		CHECK( "Host: www.test.org" );
		CHECK( "Referer: http://www.test.org/path/to" );
		CHECK( "Accept: */*" );
		CHECK( "User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US) NetSp Alpha" );
		CHECK( "Pragma: no-cache" );
		CHECK( "Cache-Control: no-cache" );
		CHECK( "Connection: close" );
		if( need_value[i] )
			__CHECK( range_values[i] );
		CHECK( "" );
#undef CHECK
#undef _CHECK
	}

	TEST_END();
}

TEST( InternalProtocol_HTTP, Request_ResetURLAddress )
{
	TEST_BEGIN();

	http_request.resetURLAddress( "http://www.reset.org/path/to/file.tmp" );

	CHTTPRequest::ST_OPTHeaderValues op_header {0, 0};

	/* 获取数据长度 */
	size_t data_size = http_request.dumpHeaderData( NULL, 0, op_header );

	/* 检验长度是否正确 */
	ASSERT_EQ( 232, data_size );

	/* 获取数据 */
	char data_buf[data_size+1];
	http_request.dumpHeaderData( data_buf, data_size, op_header );
	data_buf[data_size] = '\0';

	/* 验证数据 */
	int pos = 0;
#define _CHECK(x)\
	do\
	{\
		const char expect[]={x};\
		unsigned len = strlen( x );\
		char actual[len+1];\
		actual[len] = '\0';\
		memcpy(actual, &data_buf[pos], len);\
		EXPECT_STREQ( expect, actual );\
		pos += len;\
	}while(0)
#define CHECK(x) _CHECK(x"\r\n")
	CHECK( "GET path/to/file.tmp HTTP/1.0" );
	CHECK( "Host: www.reset.org" );
	CHECK( "Referer: http://www.test.org/path/to" );
	CHECK( "Accept: */*" );
	CHECK( "User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US) NetSp Alpha" );
	CHECK( "Pragma: no-cache" );
	CHECK( "Cache-Control: no-cache" );
	CHECK( "Connection: close" );
	CHECK( "" );
#undef CHECK
#undef _CHECK

	TEST_END();
}

}/* HTTP **/
}/* InternalProtocol **/
}/* KernelPlus **/
}/* Netspecters **/
