#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <alloca.h>#include "GeneralTestDefine.hpp"
#include "../http_task.h"

namespace Netspecters {
namespace KernelPlus {
namespace InternalProtocol {
namespace HTTP {

using ::testing::_;
using ::testing::Invoke;
using ::testing::Return;

extern const char g_res_path[] = {"http://www.test.org/path/to/file.tmp"};
const unsigned g_res_len = 36u;

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	enable_continue = false;\
	\
	__segment_mock seg_mock;\
	__task_group_manager_mock tgm_mock;\
	__data_container_mock dc_mock;\
	EXPECT_CALL( tgm_mock, storeTaskStateData( _, _ ) );\
	EXPECT_CALL( tgm_mock, openSegment( _,_,_ ) )\
	.WillOnce( Invoke( [&seg_mock]( GROUP_TASK_HANDLE, ISegment::ST_SegmentInfor::ENM_SegmentType, ISegment *&segment_ptr )->int {\
		segment_ptr = static_cast<ISegment *>( &seg_mock );\
		return 0;\
	} ) );\
	EXPECT_CALL( tgm_mock, closeSegment( _, _ ) );\
	\
	IProtocol::ST_TaskResource *task_res = ( IProtocol::ST_TaskResource * )alloca( sizeof( IProtocol::ST_TaskResource ) + strlen( g_res_path ) + 1 );\
	memset( task_res, 0, sizeof( IProtocol::ST_TaskResource ) );\
	strcpy( task_res->resource_data, g_res_path );\
	IProtocol::ST_TaskInitParam task_init_param {0, false, task_res, NULL};\
	CHttpTask http_task( task_init_param, 1, &tgm_mock, &dc_mock, NULL )

#define TEST_END()\
	enable_continue = true

TEST( InternalProtocol_HTTP, Task_CreateAndDestroy )
{
	enable_continue = false;

	/* 建立mock */
	__segment_mock seg_mock;
	__task_group_manager_mock tgm_mock;
	__data_container_mock dc_mock;
	EXPECT_CALL( tgm_mock, storeTaskStateData( _, _ ) ).Times( 2 );
	EXPECT_CALL( tgm_mock, openSegment( _, _, _ ) )
	.Times( 3 )
	.WillRepeatedly( Invoke( [&seg_mock]( GROUP_TASK_HANDLE, ISegment::ST_SegmentInfor::ENM_SegmentType, ISegment *&segment_ptr )->int {
		segment_ptr = static_cast<ISegment *>( &seg_mock );
		return 0;
	} ) );
	EXPECT_CALL( tgm_mock, closeSegment( _, _ ) ).Times( 3 );

	/* 使用创建参数构建task */
	{
		IProtocol::ST_TaskResource *task_res = ( IProtocol::ST_TaskResource * )alloca( sizeof( IProtocol::ST_TaskResource ) + strlen( g_res_path ) + 1 );
		memset( task_res, 0, sizeof( IProtocol::ST_TaskResource ) );
		strcpy( task_res->resource_data, g_res_path );
		IProtocol::ST_TaskInitParam task_init_param {0, false, task_res, NULL};
		CHttpTask http_task( task_init_param, 1, &tgm_mock, &dc_mock, NULL );//建立任务时的connect会调用失败，不会真正创建node

		/* 检测对象参数是否成功初始化 */
		ASSERT_EQ( static_cast<IDataContainer *>( &dc_mock ), http_task.m_thread_environment.data_container_ptr );
		ASSERT_NE( 0, ( uintptr_t )http_task.m_thread_environment.request_header_ptr );
		ASSERT_EQ( 1, http_task.m_running_thread_ptrs.size() );
		ASSERT_EQ( 0, ( uintptr_t )http_task.m_task_monitor_ptr );
		EXPECT_EQ( 0, http_task.m_abs_recved_data_size );
		EXPECT_EQ( 0, http_task.m_max_thread_count );
	}

	/* 使用重启参数构建task */
	{
		size_t data_size = sizeof( CHttpTask::ST_StateData ) + g_res_len;
		CHttpTask::ST_StateData *task_state_data_ptr = ( CHttpTask::ST_StateData * )alloca( data_size );
		task_state_data_ptr->data_size = data_size - sizeof( IProtocol::ST_TaskStateData );
		task_state_data_ptr->thread_num = 2;
		task_state_data_ptr->abs_recved_data_size = 0;
		task_state_data_ptr->std_url_length = g_res_len;
		memcpy( task_state_data_ptr->state_data, g_res_path, g_res_len );

		IProtocol::ST_TaskInitParam task_init_param {0, false, NULL, task_state_data_ptr};
		CHttpTask http_task( task_init_param, 1, &tgm_mock, &dc_mock, NULL );

		/* 检测对象参数是否成功初始化 */
		EXPECT_EQ( static_cast<IDataContainer *>( &dc_mock ), http_task.m_thread_environment.data_container_ptr );
		EXPECT_NE( 0, ( uintptr_t )http_task.m_thread_environment.request_header_ptr );
		EXPECT_EQ( 2, http_task.m_running_thread_ptrs.size() );
		EXPECT_EQ( 0, ( uintptr_t )http_task.m_task_monitor_ptr );
		EXPECT_EQ( 0, http_task.m_abs_recved_data_size );
		EXPECT_EQ( 2, http_task.m_max_thread_count );
	}

	enable_continue = true;
}

TEST( InternalProtocol_HTTP, Task_SaveTaskState )
{
	TEST_BEGIN();

	CHttpTask::ST_StateData *state_data_ptr = NULL;

	/* 设置mock */
	EXPECT_CALL( tgm_mock, storeTaskStateData( _, _ ) )
	.WillOnce( Invoke( [&state_data_ptr]( GROUP_TASK_HANDLE handle, IProtocol::ST_TaskStateData * data_ptr )->int {
		/* 拷贝数据 */
		size_t data_size = sizeof( IProtocol::ST_TaskStateData ) + data_ptr->data_size;
		void *void_data_ptr = malloc( data_size );
		memcpy( void_data_ptr, data_ptr, data_size );
		state_data_ptr = static_cast<CHttpTask::ST_StateData *>( void_data_ptr );
		return data_size;
	} ) );

	/* 调用保存状态函数 */
	http_task.saveTaskState();

	/* 检验需要保存数据是否正确 */
	ASSERT_NE( 0, ( uintptr_t )state_data_ptr );
	EXPECT_NE( 0, state_data_ptr->data_size );
	ASSERT_EQ( 1, state_data_ptr->thread_num );
	EXPECT_EQ( 0, state_data_ptr->abs_recved_data_size );
	ASSERT_EQ( g_res_len, state_data_ptr->std_url_length );

	/* 释放数据 */
	free( state_data_ptr );

	TEST_END();
}

TEST( InternalProtocol_HTTP, Task_ReadTaskInformation )
{
	TEST_BEGIN();

	/* task graph mock */

	/* 读取信息 */
	IProtocol::ST_TaskInformation task_information;
	http_task.readTaskInformation( task_information );

	/* 验证读取结果 */
	EXPECT_EQ( 0, task_information.transfer_speed_up );
	//EXPECT_EQ( 0, task_information.transfer_speed_down );
	EXPECT_EQ( 0, task_information.send_data_size );
	EXPECT_EQ( http_task.m_abs_recved_data_size, task_information.recved_data_size );

	TEST_END();
}

}/* HTTP **/
}/* InternalProtocol **/
}/* KernelPlus **/
}/* Netspecters **/
