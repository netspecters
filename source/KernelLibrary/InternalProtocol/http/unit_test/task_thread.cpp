#ifndef  task_thread_test_demo_INC
#define  task_thread_test_demo_INC
#include "GeneralTestDefine.hpp"
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include "../task_thread.h"

namespace Netspecters {
namespace KernelPlus {
namespace InternalProtocol {
namespace HTTP {

using ::testing::_;
using ::testing::Invoke;
using ::testing::Return;
using std::string;

extern const char g_host_name[] = {"www.test.org"};
extern const char g_res_path[];	//from http_task.cpp test demo
extern const char g_host_name[];//from task_thread.cpp test demo
class __http_request_mock : public CHTTPRequest
{
	public:

		__http_request_mock() : m_res_path( g_res_path ) {}
		const string &getOrgURI( void ) {
			return m_res_path;
		}
		size_t dumpHeaderData( char *buffer_ptr, size_t buffer_size, const ST_OPTHeaderValues & ) //忽略可选头参数
		{
			return 0;
		}
		const char *getHostName( void ) {
			return g_host_name;
		}
		uint16_t getHostPort( void ) {
			return 80;
		}
		void resetURLAddress( const char * ) {}

	private:
		string m_res_path;
};

class __http_respond_mock : public CHTTPRespond
{
	public:
		__http_respond_mock() : m_respond_main_code( rmcOK ) {}
		void reset() {
			m_respond_main_code = rmcOK;
		}
		bool appendRespondData( const char *, size_t &buffer_size )	//测试用函数是不关心具体缓冲区内容的
		{
			/* 根据传输字节数确定应答码 */
			m_respond_main_code = ( ENM_RespondMainCode )buffer_size;
			return true;
		}

		ENM_RespondMainCode getRespondMainCode( int &extend_code )
		{
			extend_code = 0;
			return m_respond_main_code;
		}
		const char *getParam( ENM_ParamTypeCode param_type_code )
		{
			switch( param_type_code )
			{
				case ptcContentLength:
					return "2048";	//测试用文件长度
				case ptcTransferEncoding:
					return NULL;
				case ptcLocation:
					return "http://local/test";
				case ptcSetCookie:
					return NULL;
				case ptcCount:
					return NULL;
			}
		}

	private:
		ENM_RespondMainCode m_respond_main_code;
};

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE(enable_continue);\
	enable_continue = false;\
	\
	__segment_mock seg_mock;\
	__data_container_mock dc_mock;\
	__http_request_mock hr_mock;\
	__task_group_manager_mock tg_mock;\
	EXPECT_CALL( tg_mock, openSegment( _,_,_ ) )\
	.WillOnce( Invoke( [&seg_mock]( GROUP_TASK_HANDLE, ISegment::ST_SegmentInfor::ENM_SegmentType, ISegment *&segment_ptr )->int {\
		segment_ptr = static_cast<ISegment *>( &seg_mock );\
		return 0;\
	} ) );\
	EXPECT_CALL( tg_mock, closeSegment( _, _ ) );\
	\
	CTaskThread::ST_ThreadEnvironment thread_env {NULL, &dc_mock, &hr_mock, &tg_mock};\
	CTaskThread task_thread( thread_env )

#define TEST_END()\
	enable_continue = true

TEST( InternalProtocol_HTTP, Thread_CreateAndDestroy )
{
	enable_continue = false;

	/* 初始化mock */
	__segment_mock seg_mock;
	__data_container_mock dc_mock;
	__http_request_mock hr_mock;
	__task_group_manager_mock tg_mock;
	EXPECT_CALL( tg_mock, openSegment( _, _, _ ) )
	.WillOnce( Invoke( [&seg_mock]( GROUP_TASK_HANDLE, ISegment::ST_SegmentInfor::ENM_SegmentType, ISegment *&segment_ptr )->int {
		segment_ptr = static_cast<ISegment *>( &seg_mock );
		return 0;
	} ) );
	EXPECT_CALL( tg_mock, closeSegment( _, _ ) );

	/* 初始化环境 */
	CTaskThread::ST_ThreadEnvironment thread_env {NULL, &dc_mock, &hr_mock, &tg_mock};

	/* 使用带范围参数的创建函数 */
	{
		CTaskThread task_thread( thread_env );

		/* 检验对象成员 */
		ASSERT_EQ( static_cast<ISegment *>( &seg_mock ), task_thread.m_data_segment_ptr );
		EXPECT_EQ( 0, ( uintptr_t )task_thread.m_socket_ptr );
		EXPECT_EQ( 0, ( uintptr_t )task_thread.m_respond_parser_ptr );
		EXPECT_EQ( CTaskThread::tsNone, task_thread.m_state );
	}

	enable_continue = true;
}

#define RECV_TEST_BEGIN()\
	TEST_BEGIN();\
	/* 手动设置状态到接收命令状态 */\
	task_thread.m_state = CTaskThread::tsCommand;\
	/* 手动创建应答解析器 */\
	task_thread.m_respond_parser_ptr = new __http_respond_mock

#define RECV_TEST_END()\
	TEST_END()

/* 测试接收到 2xx 命令 */
TEST( InternalProtocol_HTTP, Thread_OnRecvData_2xx )
{
	RECV_TEST_BEGIN();

	/* 设置mock */
	EXPECT_CALL( seg_mock, write( _, _, _, _ ) ).WillOnce( Return( 0 ) );
	EXPECT_CALL( seg_mock, getIOPosition() ).WillOnce( Return( 0 ) );
	EXPECT_CALL( seg_mock, getSegmentSize() ).WillOnce( Return( 0 ) );

	/* 调用接收数据函数，送入触发数据 */
	CSocket::ST_Buffer recv_buffer;	//伪造一个接收缓存
	recv_buffer.size = 2;
	task_thread.onNodeRecvData( &recv_buffer );

	/* 检测状态 */
	ASSERT_EQ( CTaskThread::tsTransferData, task_thread.m_state );

	RECV_TEST_END();
}

/* 测试接收到 3xx 命令 */
TEST( InternalProtocol_HTTP, Thread_OnRecvData_3xx )
{
	RECV_TEST_BEGIN();

	EXPECT_CALL( seg_mock, getSegmentOffset() ).WillOnce( Return( 0 ) );

	/* 调用接收数据函数，送入触发数据 */
	CSocket::ST_Buffer recv_buffer;	//伪造一个接收缓存
	recv_buffer.size = 3;
	task_thread.onNodeRecvData( &recv_buffer );

	/* 检测状态 */
	ASSERT_EQ( CTaskThread::tsCommand, task_thread.m_state );

	RECV_TEST_END();
}

/* 测试接收到 4xx 和 5xx 命令 */
TEST( InternalProtocol_HTTP, Thread_OnRecvData_4xx_5xx )
{
#define TEST_COUNT 3
	RECV_TEST_BEGIN();

	/* 手动设置请求重试次数 */
	task_thread.m_max_retry_count = TEST_COUNT;

	EXPECT_CALL( seg_mock, getSegmentOffset() ).Times( TEST_COUNT - 1 ).WillRepeatedly( Return( 0 ) );

	/* 调用接收数据函数，送入触发数据 */
	int actual_test_count = 0;
	while( task_thread.m_max_retry_count > 0 )
	{
		actual_test_count++;
		if( !task_thread.m_respond_parser_ptr )
			task_thread.m_respond_parser_ptr = new __http_respond_mock;
		CSocket::ST_Buffer recv_buffer;	//伪造一个接收缓存
		recv_buffer.size = 4;
		task_thread.onNodeRecvData( &recv_buffer );
	}
	EXPECT_EQ( TEST_COUNT, actual_test_count );

	/* 检测状态 */
	EXPECT_EQ( CTaskThread::tsAbort, task_thread.m_state );
	EXPECT_EQ( 0, task_thread.m_max_retry_count );

	RECV_TEST_END();
#undef TEST_COUNT
}

}/* HTTP **/
}/* InternalProtocol **/
}/* KernelPlus **/
}/* Netspecters **/

#endif   /* ----- #ifndef task_thread_test_demo_INC  ----- */
