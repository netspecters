#ifndef  task_thread_INC
#define  task_thread_INC

#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif

#include "../../NetLayer/socket.h"
#include "../../DataContainer/data_container_manager.h"


#include "http_request.h"
#include "http_respond.h"

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace HTTP {

using namespace Netspecters::KernelPlus::NetLayer;
using namespace Netspecters::KernelPlus::DataContainer;

class CHttpTask;
class CTaskThread : public ISegment::INotify
{
	public:
		virtual NSAPI int onIORealRead( void *data_buffer, size_t transfer_data_size, long io_tag );
		virtual NSAPI int onIORealWrite( void *data_buffer, size_t transfer_data_size, long io_tag );
		virtual NSAPI int onIOError( ENM_IOErrorCode io_error_code );
		virtual NSAPI bool enableMerge( ISegment *request_segment_ptr );

	public:
		enum ENM_State
		{
			tsNone,			///< 初始化状态
			tsCommand,
			tsTransferData,
			tsAbort
		};
		enum ENM_ErrorCode
		{
			ecInvaildRequest,
			ecServerInternalError
		};
		struct ST_ThreadEnvironment
		{
			CHttpTask *http_task_ptr;
			IDataContainer *data_container_ptr;
			CHTTPRequest *request_header_ptr;
			ITaskGroupManager *task_group_manager_ptr;
			GROUP_TASK_HANDLE group_task_handle;			CSpeedGroup speed_group;
		};

		CTaskThread( const ST_ThreadEnvironment &shared_thread_environment );
		~CTaskThread();

		///连接到远程服务器(运输层连接)
		void connect();

		///获取线程工作状态
		ENM_State getState() {
			return m_state;
		}
		///获取线程从开始到现在已经接收的数据的长度
		off_t getThreadRecvedDataSize();

	private:
		off_t allocSegment( off_t segment_offset = -1, off_t segment_border = -1 );
		//发送http请求，并等待服务器应答(开始执行http协议，接收数据[应该在node建立之后运行])
		void sendRequest();
		void onNodeRecvData( CSocket::ST_Buffer *buffer_ptr );
		void freeMe();

		/**
		 * @name node事件通知
		 * @{
		*/
		static int on_connect( CSocket *socket_ptr, uintptr_t io_key );
		static int on_send( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key );
		static int on_recv( CSocket *socket_ptr, CSocket::ST_Buffer *buffer_ptr, uintptr_t io_key );
		static int on_error( CSocket *socket_ptr, CSocket::ST_Callback::ENM_ErrorCode error_code, CSocket::ST_Buffer *buffer_ptr );
		///@}

	private:
		const ST_ThreadEnvironment	&m_thread_environment;
		ISegment					*m_data_segment_ptr;
		CHTTPRespond				*m_respond_parser_ptr;
		ENM_State					m_state;
		int							m_max_retry_count;
		CSocket					*m_socket_ptr;
#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( InternalProtocol_HTTP, Thread_CreateAndDestroy );
		FRIEND_TEST( InternalProtocol_HTTP, Thread_OnRecvData_2xx );
		FRIEND_TEST( InternalProtocol_HTTP, Thread_OnRecvData_3xx );
		FRIEND_TEST( InternalProtocol_HTTP, Thread_OnRecvData_4xx_5xx );
#endif
};

}/* HTTP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef task_thread_INC  ----- */
