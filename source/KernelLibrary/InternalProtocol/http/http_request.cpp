#include "http_request.h"
#include "../../../NSErrorCode.h"
#include <cstdio>

using namespace Netspecters::KernelPlus::InternalProtocol::HTTP;
using std::string;

static inline void g_strcpy( char *&dst_str_ptr, const char *src_str_ptr, int len )
{
	for( int i = 0; i < len; i++ )
		*dst_str_ptr++ = *src_str_ptr++;
}

static inline int g_int2str( char *str_buffer, size_t i )
{
	return sprintf( str_buffer, "%d", i );
}

CHTTPRequest::CHTTPRequest( const IProtocol::ST_TaskResource &request_init_param )
	: m_http_header_size( 0 )
	, m_header_value_count( 0 )
{
	const char *res_path_ptr = &( request_init_param.resource_data[request_init_param.res_path_offset] );

	//提取url信息
	CURI uri( res_path_ptr );
	m_host_port = uri.getPort( 80 );

	//定义http头部
	makeGeneralHeader( uri, request_init_param );

	/* 保存原始uri */
	m_org_uri.assign( res_path_ptr );
}

void CHTTPRequest::resetURLAddress( const char *new_url_address )
{
	//指定referer
	setValue( "Referer", m_header_value_array[1].second );//1号索引为reference

	//提取新url信息
	CURI uri( new_url_address );
	m_host_port = uri.getPort( 80 );

	//构建新命令行
	string new_cmd_line( "GET " );
	new_cmd_line.append( uri.getPath() ).append( " HTTP/1.0" );

	//刷新命令行
	m_http_header_size += new_cmd_line.length() - m_method_value.length();
	m_method_value = new_cmd_line;

	//刷新host
	setValue( "Host", uri.getHost() );
}

void CHTTPRequest::addValue( const char *name_str_ptr, const string &value_str )
{
	string name_str( name_str_ptr );
	m_header_value_array[m_header_value_count++] = std::make_pair( name_str, value_str );
	m_http_header_size += name_str.size() + value_str.size();
}

void CHTTPRequest::addValue( const char *name_str_ptr, const char *value_str_ptr )
{
	addValue( name_str_ptr, string( value_str_ptr ) );
}

void CHTTPRequest::addValue( const char *name_str_ptr, int value )
{
	char int_buffer[64];
	g_int2str( int_buffer, value );
	addValue( name_str_ptr, int_buffer );
}

bool CHTTPRequest::setValue( const char *name_str_ptr, const string &new_value_str )
{
	bool result = false;
	for( int i = 0; i < m_header_value_count; i++ )
		if( m_header_value_array[i].first == name_str_ptr )
		{
			string &old_value = m_header_value_array[i].second;

			m_http_header_size += new_value_str.size() - old_value.size();
			old_value = new_value_str;

			result = true;
			break;
		}
	return result;
}

void CHTTPRequest::makeGeneralHeader( const CURI &uri, const IProtocol::ST_TaskResource &request_init_param )
{
	//填写命令行
	m_method_value.assign( "GET " ); //只使用GET方法
	m_method_value.append( uri.getPath() ).append( " HTTP/1.0" );
	m_http_header_size = m_method_value.size();

	//填写请求报头参数: Host
	addValue( "Host", uri.getHost() );	// !!保证0号索引为host name!!

	//填写请求报头参数: Referer
	if( request_init_param.referer_uri_offset )	//根据结构的设计，如果referer_uri有效，则referer_uri_offset一定大于0
		addValue( "Referer", &( request_init_param.resource_data[request_init_param.referer_uri_offset] ) );
	else
	{
		//提取referer
		addValue( "Referer", uri.getReferer() );
	}

	//填写请求报头参数: cookie
	if( request_init_param.cookie_data_offset )
		addValue( "Cookie", &( request_init_param.resource_data[request_init_param.cookie_data_offset] ) );

	//填写请求报头参数: Accept
	addValue( "Accept", "*/*" );

	//填写请求报头参数: User-Agent
	addValue( "User-Agent", "Mozilla/5.0 (X11; U; Linux i686; en-US) NetSp Alpha" );

	//填写请求报头参数: Pragma
	addValue( "Pragma", "no-cache" );

	//填写请求报头参数: Cache-Control
	addValue( "Cache-Control", "no-cache" );

	//填写请求报头参数: Connection
	addValue( "Connection", "close" );

	//填写请求报头参数: Authorization
	if( request_init_param.login_usr_name_offset )
	{
		//base64编码(user_name:passwd)
		string user( &( request_init_param.resource_data[request_init_param.login_usr_name_offset] ) );
		string passwd( &( request_init_param.resource_data[request_init_param.login_passwd_offset] ) );
		user += ':' + passwd;
		string credentials;
		base64_encode( user, credentials );

		string value( "Basic " );
		value += credentials;

		addValue( "Authorization", value );
	}
}

inline size_t CHTTPRequest::getHeaderDataSize()
{
	return m_header_value_count == 0 ? : m_http_header_size + 2 + ( m_header_value_count ) * 4 + 2;
}

size_t CHTTPRequest::dumpHeaderData( char *buffer_ptr, size_t buffer_size, const ST_OPTHeaderValues &opt_header_values )
{
#define __APPEND_CRLF(buf_ptr)\
	*buf_ptr++ = '\r';\
	*buf_ptr++ = '\n'

	/* 处理可选头部参数: Range */
	string range_value( "bytes=" );
	if( opt_header_values.range_start > 0 )
	{
		char range_buffer[64];
		int range_str_len = g_int2str( range_buffer, opt_header_values.range_start );
		range_buffer[range_str_len++] = '-';
		if( opt_header_values.range_end > 0 )
			range_str_len += g_int2str( range_buffer + range_str_len, opt_header_values.range_end );
		range_buffer[range_str_len] = '\0';

		range_value += range_buffer;
	}

	size_t header_size = getHeaderDataSize();
	if( buffer_ptr && ( header_size <= buffer_size ) )
	{
		//处理第一行
		g_strcpy( buffer_ptr, m_method_value.data(), m_method_value.size() );
		__APPEND_CRLF( buffer_ptr );

		//处理其他行
		for( int i = 0; i < m_header_value_count; i++ )
		{
			CHeaderValues::value_type &item = m_header_value_array[i];
			if( item.first.size() > 0 )
			{
				g_strcpy( buffer_ptr, item.first.data(), item.first.size() );
				*buffer_ptr++ = ':';
				*buffer_ptr++ = ' ';
				g_strcpy( buffer_ptr, item.second.data(), item.second.size() );
				__APPEND_CRLF( buffer_ptr );
			}
		}

		/* 添加可选头部参数: Range */
		if( opt_header_values.range_start > 0 )
		{
			g_strcpy( buffer_ptr, "Range: ", 7 );
			g_strcpy( buffer_ptr, range_value.c_str(), range_value.size() );
			__APPEND_CRLF( buffer_ptr );
		};

		/* 添加最后一行结束标志 */
		__APPEND_CRLF( buffer_ptr );
	}

	if( opt_header_values.range_start > 0 )
		header_size += 7 + range_value.size() + 2;

	return header_size;
#undef __APPEND_CRLF
}
