/*
 * =====================================================================================
 *
 *       Filename:  http_request.h
 *
 *    Description:  http 请求管理
 *
 *        Version:  1.0
 *        Created:  2009-2-20 20:05:07
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (Tony), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */

#ifndef  HTTP_REQUEST_INC
#define  HTTP_REQUEST_INC

#include <string>
#include <array>
#include <utility>
#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#	define VIRTUAL virtual
#else
#	define VIRTUAL
#endif

#include "../../ProtocolManager/Interfaces.h"
#include "nslib.h"
#include "../uri.h"

#include "base64.h"

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace HTTP {

using namespace Netspecters::KernelPlus::ProtocolManager;
using std::string;

#define MAX_HEADER_ITEM_NUM	16

class CHTTPRequest
{
	public:
		struct ST_OPTHeaderValues	//可选头
		{
			off_t range_start, range_end;
		};

#ifdef EX_TEST
		CHTTPRequest() = default;
#endif
		CHTTPRequest( const IProtocol::ST_TaskResource &request_init_param );
		VIRTUAL const string &getOrgURI( void ) {
			return m_org_uri;
		}
		/**
		 * @brief 拷贝（符合http协议规定的）请求头到指定缓冲区
		 * @param buffer_ptr 目的缓冲区指针
		 * @param buffer_size 目的缓冲区大小
		 * @param opt_header_values 附加的可选头
		 * @return 实际拷贝数据的长度
		*/
		VIRTUAL size_t dumpHeaderData( char *buffer_ptr, size_t buffer_size, const ST_OPTHeaderValues &opt_header_values );

		VIRTUAL const char *getHostName( void ) {
			return m_header_value_array[0].second.c_str();
		}
		VIRTUAL uint16_t getHostPort( void ) {
			return m_host_port;
		}
		VIRTUAL void resetURLAddress( const char *new_url_address );

	private:
		typedef std::array< std::pair<string, string>, MAX_HEADER_ITEM_NUM > CHeaderValues;	//Header中的字段基本固定，用定长数组时空效率好些

	private:
		bool setValue( const char *name_str_ptr, const string &new_value_str );
		void addValue( const char *name_str_ptr, const char *value_str_ptr );
		void addValue( const char *name_str_ptr, const string &value_str );
		void addValue( const char *name_str_ptr, int new_value );
		size_t getHeaderDataSize( void );
		void makeGeneralHeader( const CURI &uri, const IProtocol::ST_TaskResource &request_init_param );

		string			m_org_uri;
		string			m_method_value;	//http GET
		uint16_t		m_host_port;
		int				m_http_header_size;
		int				m_header_value_count;
		CHeaderValues	m_header_value_array;
#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( InternalProtocol_HTTP, Request_CreateAndDestroy );
#endif
};

}/* HTTP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef HTTP_REQUEST_INC  ----- */

