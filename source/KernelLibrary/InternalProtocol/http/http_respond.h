/*
 * =====================================================================================
 *
 *       Filename:  http_respond.h
 *
 *    Description:  http 应答管理
 *
 *        Version:  1.0
 *        Created:  2009-2-20 20:07:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (Tony), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */


#ifndef  HTTP_RESPOND_INC
#define  HTTP_RESPOND_INC

#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#	define VIRTUAL virtual
#else
#	define VIRTUAL
#endif

#include "TokenTree.hpp"

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace HTTP {

static const int INDEX_TABLE_LENGTH = 27;

class CHTTPRespond
{
		class CHTTPRespond_TokenTree : public CTokenTree<INDEX_TABLE_LENGTH>
		{
			public:
				CHTTPRespond_TokenTree( void )
				{
					//将http应答的相关信息添加到编码树中
					addToken( "Content-Length" );
					addToken( "Transfer-Encoding" );
					addToken( "Location" );
					addToken( "Set-Cookie" );
				}

			protected:
				virtual int get_index( char c )
				{
					return c == '-' ? 26 : c - 97;
				}
		};

	public:
		enum ENM_ParamTypeCode { ptcContentLength, ptcTransferEncoding, ptcLocation, ptcSetCookie, ptcCount };
		enum ENM_RespondMainCode { rmcOK = 2, rmcRedirect, rmcInvaildRequest, rmcServerError };
		static const int MIN_RESPOND_SIZE = 1;

		CHTTPRespond()
			: m_parase_status( psCommand_Line ), m_current_data_pos( 0 )
		{
			memset( m_param_pos, 0, sizeof( char* )*ptcCount );
		}
		VIRTUAL void reset();
		//将数据添加到缓存区
		//buffer_size 指示data_buffer_ptr有多大，结束后将返回成功拷贝的数据大小
		//返回值：是否已经获得了足够的应答信息:true，已经完成了参数解析工作
		VIRTUAL bool appendRespondData( const char *data_buffer_ptr, size_t &buffer_size );

		VIRTUAL ENM_RespondMainCode getRespondMainCode( int &extend_code );
		VIRTUAL const char *getParam( ENM_ParamTypeCode param_type_code );

	private:
		typedef CHTTPRespond_TokenTree CHttpTokenTree;
		enum ENM_ParseStatus { psCommand_Line, psCommand_Line_End, psCommand_Finish0, psCommand_Finish };
		static const int C_MAX_BUFFER_SIZE = 1024;

	private:
		void parseBuffer(); //会修改buffer中的内容

		ENM_ParseStatus m_parase_status;
		char	m_respond_buffer[C_MAX_BUFFER_SIZE];
		size_t	m_current_data_pos;	//指示当前数据长度
		char	*m_param_pos[ptcCount];

		static CHttpTokenTree static_m_token_tree;
#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( InternalProtocol_HTTP, Respond_AppendRespondData );
#endif
};

}/* HTTP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif   /* ----- #ifndef HTTP_RESPOND_INC  ----- */
