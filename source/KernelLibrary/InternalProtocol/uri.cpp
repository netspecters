/*
 * =====================================================================================
 *
 *       Filename:  uri.c
 *
 *    Description:  implement URI.H
 *
 *        Version:  1.0
 *        Created:  2010年03月13日 14时47分06秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */
#include "uri.h"

CURI::CURI(string uri_string)
	: m_uri_string(uri_string)
{
	enum ENM_Status { sScheme, sHost, sUserName, sTransfer, sPasswd, sPort, sPath };
	enum ENM_SubStatus { ss1, ss2, ss3 };

	/* reserve buffer space */
	m_scheme.reserve(128);
	m_host_name.reserve(128);
	m_host_port.reserve(128);
	m_user_name.reserve(128);
	m_user_passwd.reserve(128);
	m_path.reserve(128);

	ENM_Status current_status = sScheme;
	ENM_SubStatus sub_status = ss1;

	const char *src_pos_ptr = uri_string.c_str();
	char current_char = *src_pos_ptr;
	while(current_char)
	{
		switch(current_status)
		{
			case sScheme:
				switch(sub_status)
				{
					case ss1:
						if(current_char == ':')
							sub_status = ss2;
						else
							m_scheme += current_char;
						break;

					case ss2:
						if(current_char == '/')
							sub_status = ss3;
						else
						{
							/* error format */
							return;
						}
						break;

					case ss3:
						if(current_char == '/')
						{
							sub_status = ss1;
							current_status = sUserName;
						}
						else
						{
							/* error format */
							return;
						}
				}
				break;

			case sUserName:
				if(current_char == ':')
					current_status = sPasswd;
				else if(current_char == '/')
					current_status = sTransfer;
				else
					m_user_name += current_char;
				break;

			case sPasswd:
				if(current_char == '@')
					current_status = sHost;
				else if(current_char == '/')
					current_status = sTransfer;
				else
					m_user_passwd += current_char;
				break;

			case sTransfer:
				m_host_name = m_user_name;
				m_host_port = m_user_passwd;
				m_user_name.clear();
				m_user_passwd.clear();

				/* 转换到路径状态 */
				m_path += current_char;
				current_status = sPath;
				break;

			case sHost:
				if(current_char == ':')
					current_status = sPort;
				else if(current_char == '/')
					current_status = sPath;
				else
					m_host_name += current_char;
				break;

			case sPort:
				if(current_char == '/')
					current_status = sPath;
				else
					m_host_port += current_char;
				break;

			case sPath:
				m_path += current_char;
				break;
		}
		current_char = *++src_pos_ptr;
	}
}

string CURI::getReferer(void) const
{
	return std::move(m_uri_string.substr(0, m_uri_string.rfind('/')));
}
