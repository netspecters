#ifndef CAP_H_INCLUDED
#define CAP_H_INCLUDED

#include "Interfaces.h"

#include <string>

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace CAP {

using std::string;

class CCap : public INSCap
{
	public:
		/* INSPlugin */
		virtual NSAPI int onLoad(IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param);
		virtual NSAPI void onUnload(void);
		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr);
		virtual NSAPI void *setParam(const char *param_name, void *value_ptr);

		/* IProtocol */
		/**
		 * @brief 添加任务
		 * @param task_init_param 任务初始化参数
		 * @note task_init_param中res_path_ptr参数格式为: nscap://interface-name/protocol-family[:port]/job-control-block-address
		*/
		virtual NSAPI TASK_HANDLE startTask(const ST_TaskInitParam &task_init_param,
											  GROUP_TASK_HANDLE group_task_handle,
											  IDataContainer *data_container_ptr,
											  ITaskMonitor *task_monitor_ptr);
		virtual NSAPI void stopTask(TASK_HANDLE task_handle);
		virtual NSAPI ENM_TaskStopCode getStopCode( TASK_HANDLE task_handle );
		virtual NSAPI int queryTaskInformation(TASK_HANDLE task_handle, ST_TaskInformation *task_infor_ptr);
		virtual NSAPI void delTask(TASK_HANDLE task_handle);

		/* INSCap */
		int enumInterface(TENMInterfaceCallback callback_func, void *client_data);
		int doCap(const char *address_ptr, ICapNotify *notify_ptr, size_t size_filte, bool copy_stream);

	private:
		string m_cap_lib_path;
};

}/* CAP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif // CAP_H_INCLUDED
