#ifndef NSCAP_INTERFACES_H_INCLUDED
#define NSCAP_INTERFACES_H_INCLUDED

#include "../../ProtocolManager/Interfaces.h"
#include "../../DataContainer/Interfaces.h"
#include "nslib.h"
#include "../../../ExtenalLibrary/Cap/Interfaces.h"

namespace Netspecters { namespace KernelPlus { namespace InternalProtocol { namespace CAP {

using namespace Netspecters::KernelPlus::ProtocolManager;
using namespace Netspecters::KernelPlus::DataContainer;
using namespace Netspecters::NSStdLib;

class ICapNotify
{
	public:
		virtual void onGetURL(const char *url_str_ptr, uintptr_t id) = 0;
};

class INSCap : public IProtocol
{
	public:
		virtual int enumInterface(TENMInterfaceCallback callback_func, void *client_data) = 0;
		virtual int doCap(const char *address_ptr, ICapNotify *notify_ptr, size_t size_filte, bool copy_stream) = 0;
};

}/* CAP **/ }/* InternalProtocol **/ }/* KernelPlus **/ }/* Netspecters **/

#endif // NSCAP_INTERFACES_H_INCLUDED
