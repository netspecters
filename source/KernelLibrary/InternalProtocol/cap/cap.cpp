#include "cap.h"
#include <vector>

using namespace Netspecters::KernelPlus::InternalProtocol::CAP;

TEnumInterface m_enum_interface_func;
TDoCap m_do_cap_func;
TGetURL g_get_url_func;

struct ST_DataChunk
{
	size_t size;
	void *data_ptr;
};

struct ST_URL
{
	string url_string;
	std::vector<ST_DataChunk> data_chunk_vector;
};

struct ST_CallbackDC
{
	CCap *cap_obj_ptr;
	ICapNotify *notify_ptr;
	bool copy_stream;
	size_t size_filte;
	bool cap_header;
	std::vector<ST_URL> url_vector;
};

static int header_magic_match(ST_Packet &packet, ST_CallbackDC *callback_dc_ptr)
{
	int url_index = -1;
	size_t data_size = packet.count - packet.offset;
	if(data_size >= callback_dc_ptr->size_filte)
	{
		char url_buf[256];
		int data_offset = g_get_url_func(packet.raw_data_ptr, data_size, url_buf, 256);
		if(data_offset > 0)
		{
			ST_DataChunk data_chunk;
			data_chunk.size = data_size - data_offset;
			data_chunk.data_ptr = malloc(data_chunk.size);
			memcpy(data_chunk.data_ptr, packet.raw_data_ptr, data_chunk.size);

			ST_URL url;
			url.url_string.assign(url_buf);
			url.data_chunk_vector.push_back(data_chunk);

			/* callback */
			url_index = callback_dc_ptr->url_vector.size() - 1;
			callback_dc_ptr->url_vector.push_back(url);
			callback_dc_ptr->notify_ptr->onGetURL(url_buf, url_index);
		}
	}
	return url_index;
}

static int tcp_cap_data(ST_Packet &packet, ST_PeerSocket *peer_socket_infor_ptr, ST_CallbackDC *callback_dc_ptr)
{
	if(callback_dc_ptr->cap_header)
	{
		int url_index = header_magic_match(packet, callback_dc_ptr);
		if(url_index >= 0)
		{
			peer_socket_infor_ptr->user_data = (void*)url_index;
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		ST_DataChunk data_chunk;
		data_chunk.size = packet.count_new;
		data_chunk.data_ptr = malloc(data_chunk.size);
		memcpy(data_chunk.data_ptr, packet.raw_data_ptr, data_chunk.size);
		callback_dc_ptr->url_vector[(int)(peer_socket_infor_ptr->user_data)].data_chunk_vector.push_back(data_chunk);
	}
}

static int tcp_callback(ST_TcpCapPackage *package_ptr, void *client_data, size_t *discard_len)
{
	ST_CallbackDC *callback_dc_ptr = reinterpret_cast<ST_CallbackDC*>(client_data);

	if(package_ptr->client_packet.count_new > 0)
	{
		return tcp_cap_data(package_ptr->client_packet, package_ptr->peer_socket_infor_ptr, callback_dc_ptr);
	}
	else if(package_ptr->server_packet.count_new > 0)
	{
		return tcp_cap_data(package_ptr->server_packet, package_ptr->peer_socket_infor_ptr, callback_dc_ptr);
	}
}

static void udp_callback(ST_UdpCapPackage *package_ptr, void *client_data)
{
	ST_CallbackDC *callback_dc_ptr = reinterpret_cast<ST_CallbackDC*>(client_data);
}

//=======================================================================================
//====================================== CCap ===========================================
//=======================================================================================

void CCap::delTask(TASK_HANDLE task_handle)
{

}

IProtocol::ENM_TaskStopCode CCap::getStopCode( TASK_HANDLE task_handle )
{

}

int CCap::queryTaskInformation(TASK_HANDLE task_handle, ST_TaskInformation *task_infor_ptr)
{

}

TASK_HANDLE CCap::startTask(const ST_TaskInitParam &task_init_param,
						  GROUP_TASK_HANDLE group_task_handle,
						  IDataContainer *data_container_ptr,
						  ITaskMonitor *task_monitor_ptr)
{

}

NSAPI void CCap::stopTask(TASK_HANDLE task_handle)
{

}

void *CCap::setParam(const char *param_name, void *value_ptr)
{

}

int CCap::getParam(const char *param_name, void *value_buffer_ptr)
{
	if(strcasecmp("caplibrary_path", param_name))
	{
		if(value_buffer_ptr && !m_cap_lib_path.empty())
			memcpy(value_buffer_ptr, m_cap_lib_path.data(), m_cap_lib_path.size());
		return m_cap_lib_path.size();
	}
	else if(strcasecmp("cap_interface", param_name))
	{
		*static_cast<INSCap **>(value_buffer_ptr) = static_cast<INSCap*>(this);
	}
	return NSEC_SUCCESS;
}

int CCap::doCap(const char *address_ptr, ICapNotify *notify_ptr, size_t size_filte, bool copy_stream)
{
	ST_CallbackDC callback_dc = {this, notify_ptr, copy_stream, size_filte, true};
	return m_do_cap_func(address_ptr, tcp_callback, udp_callback, &callback_dc);
}

int CCap::enumInterface(TENMInterfaceCallback callback_func, void *client_data)
{
	return m_enum_interface_func(callback_func, client_data);
}

void CCap::onUnload(void)
{

}

int CCap::onLoad(IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param)
{
	{
		/* 检验是否有libcap库 */
#if defined(_WIN32) || defined(_WIN64)
		std::wstring libcap_name(L"libcap.dll");
		HANDLE lib_handle = LoadLibrary(libcap_name.c_str());
#else
		string libcap_name("libcap.so");
		void *lib_handle = dlopen(libcap_name.c_str(), RTLD_NOW);
#endif
		if(!lib_handle)
			return NSEC_INVALID_CALL;
	}

	/* 加载捕获库 */
	size_t path_len = plus_tree_ptr->getPlusTreeWorkPath(NULL, 0);
	char plus_path_buffer[path_len + 1];
	plus_tree_ptr->getPlusTreeWorkPath(plus_path_buffer, path_len);
	m_cap_lib_path.assign(plus_path_buffer);

#if defined(_WIN32) || defined(_WIN64)
	m_cap_lib_path += "Cap.dll";
	DECL_UTF8_UNICODE(m_cap_lib_path.c_str(), cap_lib_name_unicode)
	HMODULE lib_handle = LoadLibrary(cap_lib_name_unicode);

	/* 获取接口 */
	m_enum_interface_func = (TEnumInterface)GetProcAddress(lib_handle, L"enumInterface");
	m_do_cap_func = (TDoCap)GetProcAddress(lib_handle, L"doCap");
	g_get_url_func = (TGetURL)dlsym(lib_handle, "getURL");

#else
	m_cap_lib_path += "Cap.so";
	void *lib_handle = dlopen(m_cap_lib_path.c_str(), RTLD_NOW);

	/* 获取接口 */
	m_enum_interface_func = (TEnumInterface)dlsym(lib_handle, "enumInterface");
	m_do_cap_func = (TDoCap)dlsym(lib_handle, "doCap");
	g_get_url_func = (TGetURL)dlsym(lib_handle, "getURL");
#endif

	//获取协议管理器的指针，注册协议
	reinterpret_cast<IProtocolManager*>(parent_plus_ptr)->registerProtocol(const_cast<char*>("nscap"), 0, this);

	return NSEC_SUCCESS;
}
