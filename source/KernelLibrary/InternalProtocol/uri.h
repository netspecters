#include <string>
#include <stdlib.h>
#include <utility>

using std::string;

class CURI
{
	public:
		CURI(string uri_string);
		string getScheme(void) const
		{
			return m_scheme;
		}
		string getHost(void) const
		{
			return m_host_name;
		}
		uint16_t getPort(uint16_t default_value) const
		{
			return m_host_port.empty() ? default_value : atoi(m_host_port.c_str());
		}
		string getUserName(void) const
		{
			return m_user_name;
		}
		string getPasswd(void) const
		{
			return m_user_passwd;
		}
		string getPath(void) const
		{
			return m_path;
		}
		string getReferer(void) const;

	private:
		string m_uri_string;
		string m_scheme, m_host_name, m_host_port, m_user_name, m_user_passwd, m_path;
};
