#include <sys/time.h>
#include <cstring>

#include "WebServer.h"
#include "../../NSErrorCode.h"

using namespace Netspecters::KernelPlus::WebServer;

#define TEST_PAGE "<HTML><BODY><H1>Netspecters' web GUI server works fine!</H1></BODY>"

void CWebServer::errorHandler( httpd *server, int error )
{
	switch( error )
	{
		case 404:
			httpdOutput( server, "Netspecters' Web Server can not find the page! <P><BR><BR>\n" );
			httpdOutput( server, "<B>Error 404</B>\n\n" );
			break;
	}
}

void CWebServer::killMe( httpd *server )
{
	httpdOutput( server, "<H1>Netspecters Process has KILLED !!</H1>\n" );
}

void CWebServer::processPlus( httpd *server )
{
	CWebServer *web_server_ptr = static_cast<CWebServer *>( server->extendPtr );
	httpVar	*var_action = httpdGetVariableByName( server, "action" );
	if( var_action )
	{
		if( strcmp( var_action->value, "install" ) == 0 )
		{
			httpVar *var_library = httpdGetVariableByName( server, "library" );
			httpVar *var_path = httpdGetVariableByName( server, "path" );
			httpVar *var_name = httpdGetVariableByName( server, "name" );
			if( var_library && var_path && var_name )
			{
				////http://127.0.0.1:6327/plus?action=install&&library=GUI&&path=/sys/protocol_manager/&&name=GUI
				switch( web_server_ptr->m_plus_tree_ptr->installPlus( var_library->value, var_path->value, var_name->value, 0 ) )
				{
					case NSEC_SUCCESS:
						httpdOutput( server,
									 "Plus \"$name\" been installed successful.\n"
									 "Please restart netspects for enable this plus." );
						break;
					case NSEC_CONFLICT:
						httpdPrintf( server, "Plus has already exist under %s.", var_path->value );
						break;
					default:
						httpdOutput( server, "An error occured when install \"$name\"" );
						break;
				}
			}
			else
				httpdOutput( server, "<H1>Plus install param error!</H1>\n" );
		}
		else if( strcmp( var_action->value, "query" ) == 0 )
		{
		}
		else if( strcmp( var_action->value, "scan" ) == 0 )
		{
			int new_plus_count = web_server_ptr->m_plus_tree_ptr->scanPlusDir();
			httpdPrintf( server, "%d plugines has been found in plugin dir, and all the new plugin has been installed.", new_plus_count );
		}
		else
			httpdPrintf( server, "<H1>Unknown action value \"%s\"!!</H1>\n", var_action->value );
	}
	else
		httpdOutput( server, "<H1>Invalid action!" );
}

CWebServer::CWebServer()
	: m_running( true )
	, m_service_thread_ptr( NULL )
{
	//创建http服务器
	m_server_handle = httpdCreate( NULL, WEB_SERVER_PORT );
	if( !m_server_handle )
		return;
	m_server_handle->extendPtr = reinterpret_cast<void *>( this );

	//使用自定义错误页面
	httpdSetErrorFunction( m_server_handle, 404, CWebServer::errorHandler );
}

CWebServer::~CWebServer()
{
	m_service_thread_ptr->join();
	delete m_service_thread_ptr;
}

int CWebServer::onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param )
{
	m_plus_tree_ptr = plus_tree_ptr;

#define MAX_WORK_PATH_LEN 256
	char work_path[MAX_WORK_PATH_LEN];
	plus_tree_ptr->getPlusTreeWorkPath( work_path, MAX_WORK_PATH_LEN );
	strcat( work_path, "web" );
#undef MAX_WORK_PATH_LEN

	//获取UIHost指针
	m_UIHost_ptr = static_cast<Netspecters::KernelPlus::UIHost::IUIHost *>( plus_tree_ptr->getMainPlus() );

	//设置服务器相对路径
	httpdSetFileBase( m_server_handle, work_path );

	//设置跟路径
	httpdAddWildcardContent( m_server_handle, "/", NULL, "" );

	//添加内部页
	httpdAddStaticContent( m_server_handle, "/", "server_test", HTTP_TRUE, NULL, TEST_PAGE );  //默认页
	httpdAddCContent( m_server_handle, "/", "kill", HTTP_FALSE, NULL, reinterpret_cast < void ( * )() > ( CWebServer::killMe ) );
	httpdAddCContent( m_server_handle, "/", "plus", HTTP_FALSE, NULL, reinterpret_cast < void ( * )() > ( CWebServer::processPlus ) );

	//启动线程
	m_service_thread_ptr = new std::thread( &CWebServer::onExecute, this );

	printk( KINFORMATION"Web server loaded.\n" );
	return NSEC_SUCCESS;
}

void CWebServer::onUnload()
{
	m_running = false;
	printk( KINFORMATION"Web server unloaded.\n" );
}

int CWebServer::getParam( const char *param_name, void *value_buffer_ptr )
{
	return 0;
}

void *CWebServer::setParam( const char *param_name, void *value_ptr )
{
	return NULL;
}

void CWebServer::onExecute()
{
	timeval timeout;

	while( m_running )
	{
		//虽然大多数unix系统实现的select调用不会修改timeval结构，但linux会
		timeout.tv_sec = 5;
		timeout.tv_usec = 0;

		if( httpdGetConnection( m_server_handle, &timeout ) <= 0 )
			continue;

		if( httpdReadRequest( m_server_handle ) < 0 )
		{
			httpdEndRequest( m_server_handle );
			continue;
		}
		//TODO CWebServer解析参数

		//应答
		httpdProcessRequest( m_server_handle );
		httpdEndRequest( m_server_handle );
	}
	m_UIHost_ptr->kill( 0 );
}

