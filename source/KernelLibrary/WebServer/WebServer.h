/*
 * =====================================================================================
 *
 *       Filename:  WebServer.h
 *
 *    Description:  netspecters' web GUI
 *
 *        Version:  1.0
 *        Created:  2009-1-18 23:30:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (WG), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */


#ifndef  WEBSERVER_INC
#define  WEBSERVER_INC

#include <thread>
#include "httpd.h"
#include "../../Core/PlusInterface.h"
#include "../../KernelLibrary/UIHost/Interfaces.h"

namespace Netspecters{ namespace KernelPlus{ namespace WebServer{

class IWebServer : public INSPlus
{
	public:
		virtual ~IWebServer(){}
};

class CWebServer : public IWebServer
{
	public:
		virtual NSAPI int onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param );
		virtual NSAPI void onUnload();
		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr);
		virtual NSAPI void *setParam(const char *param_name, void *value_ptr);

		void onExecute();

		CWebServer();
		~CWebServer();
	private:
		static const int WEB_SERVER_PORT = 32786;
		void httpServer();
		static void errorHandler(httpd *server, int error);
		static void killMe(httpd *server);
		static void processPlus(httpd *server);

		bool m_running;
		httpd*	m_server_handle;
		IPlusTree *m_plus_tree_ptr;
		Netspecters::KernelPlus::UIHost::IUIHost *m_UIHost_ptr;
#ifdef _WIN32
		DWORD m_msg_queue_thread_id;
#endif
		std::thread *m_service_thread_ptr;
};

}/* WebServer **/}/* KernelPlus **/}/* Netspecters **/

#endif   /* ----- #ifndef WEBSERVER_INC  ----- */
