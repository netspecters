#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if !(defined(_WIN32) || defined(_WIN64))
#	include <unistd.h>
#endif

#include "httpd.h"
#include "httpd_priv.h"

/* NOTE : This ifdef is for the entire file! */

#ifdef HAVE_EMBER
#include <ember.h>

/**************************************************************************
** GLOBAL VARIABLES
**************************************************************************/


/**************************************************************************
** PRIVATE ROUTINES
**************************************************************************/

int _httpd_executeEmber(httpd *server, char *data)
{
	ember	*script;

	script = eCreateScript();
	script->www.parseHtml = 0;
	script->www.outputHtml = 1;
	script->www.rawHTTP = 1;
	script->useRuntimeConfig = 1;
	script->stdoutFD = server->clientSock;
	eBufferSource(script, data);
	if (eParseScript(script) < 0)
	{
		fprintf(stdout,"Error at line %d of script '%s'.\n",
			eGetLineNum(script), eGetSourceName(script));
		fprintf(stdout,"Error is '%s'\n\n", eGetErrorMsg(script));
		return -1;
	}
	if (eRunScript(script) < 0)
	{
		fprintf(stdout,"Runtime error at line %d of script '%s'.\n",
			eGetLineNum(script), eGetCurFileName(script));
		fprintf(stdout,"Error is '%s'\n\n", eGetErrorMsg(script));
	}
	eFreeScript(script);
	return 0;
}

/**************************************************************************
** PUBLIC ROUTINES
**************************************************************************/



#endif /* HAVE_EMBER*/
