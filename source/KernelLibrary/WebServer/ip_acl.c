#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(_WIN32) || defined(_WIN64)
#else
#	include <unistd.h>
#endif

#include "httpd.h"
#include "httpd_priv.h"


/**************************************************************************
** GLOBAL VARIABLES
**************************************************************************/


/**************************************************************************
** PRIVATE ROUTINES
**************************************************************************/

static int scanCidr(char *val, int *result, int *length)
{
	int res, res1, res2, res3, res4, res5;
	char *cp;

	cp = val;
	res1 = atoi(cp);
	cp = strchr(cp,'.');
	if (!cp) return -1;
	cp++;
	res2 = atoi(cp);
	cp = strchr(cp,'.');
	if (!cp) return -1;
	cp++;
	res3 = atoi(cp);
	cp = strchr(cp,'.');
	if (!cp) return -1;
	cp++;
	res4 = atoi(cp);
	cp = strchr(cp,'/');
	if (!cp)
		res5 = 32;
	else
	{
		cp++;
		res5 = atoi(cp);
	}

	if (res1>255 || res2>255 || res3>255 || res4>255 || res5>32)
		return -1;

	res = (res1 << 24) + (res2 << 16) + (res3 << 8) + res4;
	*result = res;
	*length = res5;
	return 0;
}

static int _isInCidrBlock(httpd	 *server, int addr1, int len1, int addr2, int len2)
{
	int	count, mask;

	/* if (addr1 == 0 && len1 == 0)
	{
		return(1);
	}*/

	if(len2 < len1)
	{
		_httpd_writeErrorLog(server,LEVEL_ERROR, "IP Address must be more specific than network block");
		return 0;
	}

	mask = count = 0;
	while(count < len1)
	{
		mask = (mask << 1) + 1;
		count++;
	}
	mask = mask << (32 - len1);
	return (addr1 & mask) == (addr2 & mask) ? 1 : 0;
}


/**************************************************************************
** PUBLIC ROUTINES
**************************************************************************/

httpAcl *httpdAddAcl(httpd *server, httpAcl *acl, char *cidr, int action)
{
	httpAcl	*cur;
	int	addr, len;

	/*
	** Check the ACL info is reasonable
	*/
	if(scanCidr(cidr, &addr, &len) < 0)
	{
		_httpd_writeErrorLog(server,LEVEL_ERROR, "Invalid IP address format");
		return NULL;
	}
	if (action != HTTP_ACL_PERMIT && action != HTTP_ACL_DENY)
	{
		_httpd_writeErrorLog(server,LEVEL_ERROR, "Invalid acl action");
		return NULL;
	}

	/*
	** Find a spot to put this ACE
	*/
	if (acl)
	{
		cur = acl;
		while(cur->next)
		{
			cur = cur->next;
		}
		cur->next = (httpAcl*)malloc(sizeof(httpAcl));
		cur = cur->next;
	}
	else
	{
		cur = (httpAcl*)malloc(sizeof(httpAcl));
		acl = cur;
	}

	/*
	** Add the details and return
	*/
	cur->addr = addr;
	cur->len = len;
	cur->action = action;
	cur->next = NULL;
	return(acl);
}

int httpdCheckAcl(httpd *server, httpAcl *acl)
{
	httpAcl	*cur;
	int	addr, len, res, action;

	action = HTTP_ACL_DENY;
	scanCidr(server->clientAddr, &addr, &len);
	cur = acl;
	while(cur)
	{
		res = _isInCidrBlock(server, cur->addr, cur->len, addr, len);
		if (res == 1)
		{
			action = cur->action;
			break;
		}
		cur = cur->next;
	}
	if (action == HTTP_ACL_DENY)
	{
		_httpd_send403(server);
		_httpd_writeErrorLog(server,LEVEL_ERROR, "Access denied by ACL");
	}
	return action;
}

void httpdSetDefaultAcl(httpd *server, httpAcl *acl)
{
	server->defaultAcl = acl;
}
