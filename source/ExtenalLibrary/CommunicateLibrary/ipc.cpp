#if defined _WIN32 || defined _WIN64
#	include <windows.h>
#else
#	include <unistd.h>
#	include <sys/socket.h>
#	include <netinet/in.h>
#	include <arpa/inet.h>
#endif
#include <time.h>

#include "ipc.h"
#include "nslib.h"

typedef CMessageSocket::CMessage CMessage;

CIPCClient::CIPCClient()
{
	using namespace Netspecters::NSStdLib;

	CMessageSocket::socket_t socket_handle = m_msg_socket.get_socket_fd();

	/* 设置为非阻塞模式(为了使用select的超时设置) */
	int socket_flags = fcntl( socket_handle, F_GETFL );
	if( fcntl( socket_handle, F_SETFL, socket_flags | O_NONBLOCK ) < 0 )
		throw std::system_error( OSFunc::get_errno(), std::system_category() );

	/* 连接主程序 */
	sockaddr_in addr;
	memset( &addr, 0, sizeof( addr ) );
	addr.sin_family = AF_INET;
	addr.sin_port = htons( 32785 );	//连接端口 32785
	addr.sin_addr.s_addr = ::inet_addr( "127.0.0.1" );
	if( ::connect( socket_handle, ( struct sockaddr * )&addr, sizeof( addr ) ) < 0 ) //connect 会自动绑定(bind)到本地随机端口上
	{
		if( OSFunc::get_errno() == EINPROGRESS )
		{
			/* 使用select等待 */
			fd_set ev_set_w;
			FD_ZERO( &ev_set_w );
			FD_SET( socket_handle, &ev_set_w );
			timeval tv = {5, 0};	//设置时间为5秒
			if( select( socket_handle + 1, NULL, &ev_set_w, NULL, &tv ) <= 0 )	//==0超时，<0出错
				throw std::system_error( OSFunc::get_errno(), std::system_category() );
			else
			{
				/* 检查连接过程是否有问题 */
				int valopt;
				socklen_t lon = sizeof( valopt );
				getsockopt( socket_handle, SOL_SOCKET, SO_ERROR, ( void * )( &valopt ), &lon );
				/* 如果调试需要可以屏蔽以下判断 */
				if( valopt )
					throw std::system_error( valopt, std::system_category() );
			}
		}
		else
			throw std::system_error( OSFunc::get_errno(), std::system_category() );
	}

	/* 成功连接，恢复套接字阻塞状态 */
	if( fcntl( socket_handle, F_SETFL, socket_flags ) < 0 )
		throw std::system_error( OSFunc::get_errno(), std::system_category() );
}

void *CIPCClient::call_method( const char *object_path, const char *method_name,
							   unsigned arg_size, const void *arg_list,
							   const TAddressTranslateTable &att,
							   unsigned data_area_size, void *data_area_ptr )
{
	/* 建立消息 */
	CMessage call_method_msg( CMessage::cmdCallMethod );

	/* 写入路径跟方法 */
	string object_path_str( object_path ), method_name_str( method_name );
	call_method_msg << object_path_str << method_name_str;

	/* 写入参数长度 */
	call_method_msg << arg_size;
	if( arg_size > 0 )
	{
		/* 写入参数表 */
		call_method_msg.write_body_raw_data( arg_list, arg_size );

		/* 写入地址变换表 */
		call_method_msg << att.size();
		call_method_msg.write_body_raw_data( att.data(), att.size() * sizeof( TOffsetType ) );

		/* 写入数据区 */
		call_method_msg << data_area_size;
		call_method_msg.write_body_raw_data( data_area_ptr, data_area_size );
	}
	m_msg_socket.write_message( call_method_msg );

	/* 读取返回值 */
	void *result = NULL;
	m_msg_socket.read_message( call_method_msg );
	char *tmp = ( char * )data_area_ptr;
	call_method_msg.read_body_raw_data( tmp, data_area_size );
	call_method_msg.read_body_raw_data( &result, sizeof( void * ) );
	return result;
}
