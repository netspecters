#ifndef NS_GUI_IPC_H_INCLUDED
#define NS_GUI_IPC_H_INCLUDED

#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/interprocess/shared_memory_object.hpp>	//使用boost的share memory
#include <boost/interprocess/mapped_region.hpp>
#include "../../Core/message_pipe.hpp"

using std::string;

class CIPCClient
{
	public:
		CIPCClient();
		typedef std::vector<uintptr_t> TAddressTranslateTable;	//地址翻译表
		typedef CIPCClient::TAddressTranslateTable::value_type TOffsetType;
		void *call_method( const char *object_path, const char *method_name,
						   unsigned arg_size, const void *arg_list,
						   const TAddressTranslateTable &att,
						   unsigned data_area_size, void *data_area_ptr );

	private:
		CMessageSocket m_msg_socket;
};

#endif // NS_GUI_IPC_H_INCLUDED
