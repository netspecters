unit Window_APIs;

interface

uses Windows;

type
  ST_WndProc = packed record
    PopEax: Byte;
    PushSelf: Byte;
    SelfPointer: Pointer;
    PushEax: Byte;
    JmpCode: Byte;
    JmpAddress: Pointer;
  end;

function CreateKOLObjectWindow(const strCaption: string; const stDefPos: TRect; var stWndProc: ST_WndProc; lpObj, lpObjWndProc: Pointer;
  const hParent: THandle = 0; const hMenu: THandle = 0;
  dwParam: Cardinal = WS_TILEDWINDOW or WS_VISIBLE or WS_POPUP or WS_SIZEBOX or WS_TABSTOP;
  dwParamEx: Cardinal = WS_EX_WINDOWEDGE or WS_EX_CLIENTEDGE): THandle;

implementation

const
  C_WndProc         : ST_WndProc = (
    PopEax: $58;
    PushSelf: $68;
    SelfPointer: nil;
    PushEax: $50;
    JmpCode: $E9;
    JmpAddress: nil;
    );

function WndCallBack(hWnd: THandle; uMsg: Cardinal; wParam, lParam: Integer): Cardinal; stdcall;
begin
  Result := DefWindowProc(hWnd, uMsg, wParam, lParam);
end;

var
  Win32FormClass    : TWndClass =
    (Style: CS_HREDRAW or CS_VREDRAW or CS_OWNDC or CS_BYTEALIGNCLIENT or CS_DBLCLKS or CS_BYTEALIGNWINDOW;
    lpfnWndProc: @WndCallBack;
    cbClsExtra: 0;
    cbWndExtra: 0;
    hInstance: 0;
    hIcon: 0;
    hCursor: 0;
    hbrBackground: COLOR_WINDOW;
    lpszMenuName: nil;
    lpszClassName: 'Win32Form');

function CreateKOLObjectWindow(const strCaption: string; const stDefPos: TRect; var stWndProc: ST_WndProc; lpObj, lpObjWndProc: Pointer;
  const hParent: THandle = 0; const hMenu: THandle = 0;
  dwParam: Cardinal = WS_TILEDWINDOW or WS_VISIBLE or WS_POPUP or WS_SIZEBOX or WS_TABSTOP;
  dwParamEx: Cardinal = WS_EX_WINDOWEDGE or WS_EX_CLIENTEDGE): THandle;
var
  L_CodeOffset      : Cardinal;
begin
  Move(C_WndProc, stWndProc, SizeOf(C_WndProc));
  stWndProc.SelfPointer := lpObj;

  with stDefPos do
    Result := Windows.CreateWindowEx(dwParamEx, PChar(Win32FormClass.lpszClassName), PChar(strCaption), dwParam,
      Left, Top, Right, Bottom,
      hParent, hMenu, hInstance, nil);

  L_CodeOffset := Cardinal(Cardinal(lpObjWndProc) - Cardinal(@stWndProc.JmpAddress) - 4);
  stWndProc.JmpAddress := Pointer(L_CodeOffset);
  SetWindowLong(Result, GWL_WNDPROC, Integer(@stWndProc));
end;

initialization
  Win32FormClass.hInstance := hInstance;
  Win32FormClass.hCursor := LoadCursor(0, IDC_Arrow);

  RegisterClass(Win32FormClass);

end.

