unit ProgressBar;

interface

uses
  Windows, KOL;

procedure drawPrograssBar(canvas: PCanvas; PaintRect: TRect; cur_value, min_value, max_value: Longint);

implementation

{ This function solves for x in the equation "x is y% of z". }

function SolveForX(Y, Z: Longint): Longint;
begin
  Result := Longint(Trunc(Z * (Y * 0.01)));
end;

{ This function solves for y in the equation "x is y% of z". }

function SolveForY(X, Z: Longint): Longint;
begin
  if Z = 0 then
    Result := 0
  else
    Result := Longint(Trunc((X * 100.0) / Z));
end;

function GetPercentDone(cur_value, max_value, min_value: Longint): Longint;
begin
  Result := SolveForY(cur_value - min_value, max_value - min_value);
end;

procedure drawPrograssBar(canvas: PCanvas; PaintRect: TRect; cur_value, min_value, max_value: Longint);
var
  FillSize          : Longint;
  W, H              : Integer;
  X, Y              : Integer;
  RealWidth, RealHeight: Integer;
  R                 : TRect;
  PercentDone       : Longint;
begin
  X := PaintRect.Left;
  Y := PaintRect.Top;
  W := PaintRect.Right - PaintRect.Left + 1;
  H := PaintRect.Bottom - PaintRect.Top + 1;

  with canvas^ do
  begin
    Brush.Color := clWhite;
    FillRect(PaintRect);

    //Draw Base Image
    Pen.Color := $00636563;
    RoundRect(X, Y, X + W, Y + H, 2, 2);
    Pen.PenWidth := 1;
    Pen.Color := $00BDBEBD;
    MoveTo(X + 1, Y + 1);
    LineTo(X + 1, Y + H - 1);
    MoveTo(X + 1, Y + 1);
    LineTo(X + W - 1, Y + 1);
    Pen.Color := clWhite;
    MoveTo(X + W - 2, Y + 2);
    LineTo(X + W - 2, Y + H - 2);
    LineTo(X + 2, Y + H - 2);

    PercentDone := GetPercentDone(cur_value, max_value, min_value);
    FillSize := SolveForX(PercentDone, W);
    if FillSize > W then FillSize := W;
    if FillSize > 0 then
    begin
      RealWidth := Trunc((W - 3) * PercentDone / 100) + 1;
      R := MakeRect(X + 2, Y + 2, X + RealWidth + 1, Y + H - 2);
      Brush.Color := $00BD8A52;
      FillRect(R);
      Pen.Color := $00CEA684;
      MoveTo(X + 1, Y + 1);
      LineTo(X + 1, Y + H - 1);
      MoveTo(X + 1, Y + 1);
      LineTo(X + RealWidth + 1, Y + 1);
      Pen.Color := $007B4910;
      MoveTo(X + 2, Y + H - 2);
      LineTo(X + RealWidth + 1, Y + H - 2);
    end;
  end;
end;

end.

