unit Gauges2;
(*
安装：在File菜单下，选择OPEN...打开这个Gauges2_D7.dpk文件，在出现的Package窗口中先Compile一下，
      然后单击Install。你可以在Gauges2 Tab下找到控件。
      最后在Tools->Environment Options->Library Tab，Library path里增加Gauges2.pas所在目录，就OK了。

说明：Gauges2扩展了Delphi7中Source/Sample/Gauges.pas的功能，增加了自定义文字显示和RealOne Bar格式
(改编自jacky_zz的RealOneProgressBar), 而且还增加了对鼠标的响应事件。

具体用法如下：
  1。ShowText=True时，如果ShowPercent=True，只显示xx%；如果ShowPercent=False，显示Text属性中的string；
  2。Kind属性为gkHorizontalBar或gkVerticalBar时，如果RealOneStyle=True，显示RealOne Bar效果；
  3。增加了对鼠标的响应事件：
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp; 

                                                              Luke Wang 2008.9.10
*)

interface

uses SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, StdCtrls;

type

  TGaugeKind = (gkText, gkHorizontalBar, gkVerticalBar, gkPie, gkNeedle);

  TGauge2 = class(TGraphicControl)
  private
    FMinValue: Longint;
    FMaxValue: Longint;
    FCurValue: Longint;
    FKind: TGaugeKind;
    FText: String;    
    FShowText: Boolean;
    FShowPercent: Boolean;
    FRealOneStyle: Boolean;
    FBorderStyle: TBorderStyle;
    FForeColor: TColor;
    FBackColor: TColor;
    FBorderColor: TColor;
    FBaseInnerHighColor: TColor;
    FBaseInnerLowColor: TColor;
    FInnerHighColor: TColor;
    FInnerLowColor: TColor;    
    procedure PaintBackground(AnImage: TBitmap);
    procedure PaintAsText(AnImage: TBitmap; PaintRect: TRect);
    procedure PaintAsNothing(AnImage: TBitmap; PaintRect: TRect);
    procedure PaintAsBar(AnImage: TBitmap; PaintRect: TRect);
    procedure PaintAsPie(AnImage: TBitmap; PaintRect: TRect);
    procedure PaintAsNeedle(AnImage: TBitmap; PaintRect: TRect);
    procedure SetGaugeKind(Value: TGaugeKind);
    procedure SetText(Value: String);
    procedure SetShowText(Value: Boolean);
    procedure SetShowPercent(Value: Boolean);
    procedure SetRealOneStyle(Value: Boolean);
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetForeColor(Value: TColor);
    procedure SetBackColor(Value: TColor);
    procedure SetBorderColor(Value: TColor);
    procedure SetBaseInnerHighColor(Value: TColor);
    procedure SetBaseInnerLowCOlor(Value: TColor);
    procedure SetInnerHighColor(Value: TColor);
    procedure SetInnerLowCOlor(Value: TColor);    
    procedure SetMinValue(Value: Longint);
    procedure SetMaxValue(Value: Longint);
    procedure SetProgress(Value: Longint);
    function GetPercentDone: Longint;
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure AddProgress(Value: Longint);
    property PercentDone: Longint read GetPercentDone;
  published
    property Align;
    property Anchors;
    property BackColor: TColor read FBackColor write SetBackColor default clWhite;
    property BorderColor: TColor read FBorderColor write SetBorderColor default $00636563;
    property BaseInnerHighColor: TColor read FBaseInnerHighColor write SetBaseInnerHighColor default $00BDBEBD;
    property BaseInnerLowColor: TColor read FBaseInnerLowColor write SetBaseInnerLowColor default clWhite;
    property InnerHighColor: TColor read FInnerHighColor write SetInnerHighColor default $00CEA684;
    property InnerLowCOlor: TColor read FInnerLowColor write SetInnerLowColor default $007B4910;    
    property RealOneStyle: Boolean read FRealOneStyle write SetRealOneStyle default False;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
    property Color;
    property Constraints;
    property Enabled;
    property ForeColor: TColor read FForeColor write SetForeColor default clBlack;
    property Font;
    property Kind: TGaugeKind read FKind write SetGaugeKind default gkHorizontalBar;
    property MinValue: Longint read FMinValue write SetMinValue default 0;
    property MaxValue: Longint read FMaxValue write SetMaxValue default 100;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property Progress: Longint read FCurValue write SetProgress;
    property ShowHint;
    property Text: String read FText write SetText;
    property ShowText: Boolean read FShowText write SetShowText default True;
    property ShowPercent: Boolean read FShowPercent write SetShowPercent default False;
    property Visible;

    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;

procedure Register;

implementation

uses Consts;

procedure Register;
begin
  RegisterComponents('Gauges2', [TGauge2]);
end;


type
  TBltBitmap = class(TBitmap)
    procedure MakeLike(ATemplate: TBitmap);
  end;

{ TBltBitmap }

procedure TBltBitmap.MakeLike(ATemplate: TBitmap);
begin
  Width := ATemplate.Width;
  Height := ATemplate.Height;
  Canvas.Brush.Color := clWindowFrame;
  Canvas.Brush.Style := bsSolid;
  Canvas.FillRect(Rect(0, 0, Width, Height));
end;

{ This function solves for x in the equation "x is y% of z". }
function SolveForX(Y, Z: Longint): Longint;
begin
  Result := Longint(Trunc( Z * (Y * 0.01) ));
end;

{ This function solves for y in the equation "x is y% of z". }
function SolveForY(X, Z: Longint): Longint;
begin
  if Z = 0 then Result := 0
  else Result := Longint(Trunc( (X * 100.0) / Z ));
end;

{ TGauge2 }

constructor TGauge2.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := ControlStyle + [csFramed, csOpaque];
  { default values }
  FMinValue := 0;
  FMaxValue := 100;
  FCurValue := 0;
  FKind := gkHorizontalBar;
  FText:= '';
  FShowText := True;
  FShowPercent := False;
  FRealOneStyle := False;
  FBorderStyle := bsSingle;
  FForeColor := clBlue;    //$00BD8A52;;
  FBackColor := clWhite;   //$00E7E7E7;
  BorderColor := $00636563;
  BaseInnerHighColor := $00BDBEBD;
  BaseInnerLowColor := clWhite;
  InnerHighColor := $00CEA684;
  InnerLowColor := $007B4910;  
  Width := 100;
  Height := 100;
end;

function TGauge2.GetPercentDone: Longint;
begin
  Result := SolveForY(FCurValue - FMinValue, FMaxValue - FMinValue);
end;

procedure TGauge2.Paint;
var
  TheImage: TBitmap;
  OverlayImage: TBltBitmap;
  PaintRect: TRect;
begin
  with Canvas do
  begin
    TheImage := TBitmap.Create;
    try
      TheImage.Height := Height;
      TheImage.Width := Width;
      PaintBackground(TheImage);
      PaintRect := ClientRect;
      if FBorderStyle = bsSingle then InflateRect(PaintRect, -1, -1);
      OverlayImage := TBltBitmap.Create;
      try
        OverlayImage.MakeLike(TheImage);
        PaintBackground(OverlayImage);
        case FKind of
          gkText: PaintAsNothing(OverlayImage, PaintRect);
          gkHorizontalBar, gkVerticalBar: PaintAsBar(OverlayImage, PaintRect);
          gkPie: PaintAsPie(OverlayImage, PaintRect);
          gkNeedle: PaintAsNeedle(OverlayImage, PaintRect);
        end;
        TheImage.Canvas.CopyMode := cmSrcInvert;
        TheImage.Canvas.Draw(0, 0, OverlayImage);
        TheImage.Canvas.CopyMode := cmSrcCopy;
        if ShowText then PaintAsText(TheImage, PaintRect);
      finally
        OverlayImage.Free;
      end;
      Canvas.CopyMode := cmSrcCopy;
      Canvas.Draw(0, 0, TheImage);
    finally
      TheImage.Destroy;
    end;
  end;
end;

procedure TGauge2.PaintBackground(AnImage: TBitmap);
var
  ARect: TRect;
begin
  with AnImage.Canvas do
  begin
    CopyMode := cmBlackness;
    ARect := Rect(0, 0, Width, Height);
    CopyRect(ARect, Animage.Canvas, ARect);
    CopyMode := cmSrcCopy;
  end;
end;

procedure TGauge2.PaintAsText(AnImage: TBitmap; PaintRect: TRect);
var
  S: string;
  X, Y: Integer;
  OverRect: TBltBitmap;
begin
  OverRect := TBltBitmap.Create;
  try
    OverRect.MakeLike(AnImage);
    PaintBackground(OverRect);
    if ShowPercent then
      S := Format('%d%%', [PercentDone])
    else
      S := Self.Text;
    with OverRect.Canvas do
    begin
      Brush.Style := bsClear;
      Font := Self.Font;
      Font.Color := clWhite;
      with PaintRect do
      begin
        X := (Right - Left + 1 - TextWidth(S)) div 2;
        Y := (Bottom - Top + 1 - TextHeight(S)) div 2;
      end;
      TextRect(PaintRect, X, Y, S);
    end;
    AnImage.Canvas.CopyMode := cmSrcInvert;
    AnImage.Canvas.Draw(0, 0, OverRect);
  finally
    OverRect.Free;
  end;
end;

procedure TGauge2.PaintAsNothing(AnImage: TBitmap; PaintRect: TRect);
begin
  with AnImage do
  begin
    Canvas.Brush.Color := BackColor;
    Canvas.FillRect(PaintRect);
  end;
end;

procedure TGauge2.PaintAsBar(AnImage: TBitmap; PaintRect: TRect);
var
  FillSize: Longint;
  W, H: Integer;
  RealWidth, RealHeight: Integer;
  R: TRect;
begin
  W := PaintRect.Right - PaintRect.Left + 1;
  H := PaintRect.Bottom - PaintRect.Top + 1;
  with AnImage.Canvas do
  begin
    Brush.Color := BackColor;
    FillRect(PaintRect);
    
    if RealOneStyle then
    begin
      //以下code改编自jacky_zz的RealOneProgressBar 
      //Brush.Style := bsClear;
   
      //Draw Base Image
      Pen.Color := BorderColor;
      RoundRect(0, 0, W, H, 2, 2);
      Pen.Width := 1;
      Pen.Color := BaseInnerHighColor;
      MoveTo(1, 1);
      LineTo(1, H - 1);
      MoveTo(1, 1);
      LineTo(W - 1, 1);
      Pen.Color := BaseInnerLowColor;
      MoveTo(W - 2, 2);
      LineTo(W - 2, H - 2);
      LineTo(2, H - 2);
    
      //Draw Bar
      case FKind of
        gkHorizontalBar:
          begin
            FillSize := SolveForX(PercentDone, W);
            if FillSize > W then FillSize := W;
            if FillSize > 0 then
            begin
              RealWidth := Trunc((W - 3) * PercentDone / 100) + 1;
              R := Rect(2,2,RealWidth+1, H-2);
              Brush.Color := ForeColor;
              FillRect(R);
              Pen.Color := InnerHighColor;
              MoveTo(1, 1);
              LineTo(1, H - 1);
              MoveTo(1, 1);
              LineTo(RealWidth + 1, 1);
              Pen.Color := InnerLowCOlor;
              MoveTo(2, H - 2);
              LineTo(RealWidth + 1, H - 2);
            end;  
          end;
        gkVerticalBar:
          begin
            FillSize := SolveForX(PercentDone, W);
            if FillSize > W then FillSize := W;
            if FillSize > 0 then
            begin
              RealHeight := Trunc((H - 3) * PercentDone / 100) + 1;
              R := Rect(2,H - RealHeight - 1, W-2, H-2);
              Brush.Color := ForeColor;
              FillRect(R);
              Pen.Color := InnerHighColor;
              MoveTo(1, H - RealHeight - 1);
              LineTo(1, H - 2);
              LineTo(W-2, H-2);
              Pen.Color := InnerLowCOlor;
              MoveTo(W-2, H - RealHeight - 1);
              LineTo(W-2, H-2);
            end;          
          end; 
      end; 
    end
    else begin
      Pen.Color := ForeColor;
      Pen.Width := 1;
      Brush.Color := ForeColor;
      case FKind of
        gkHorizontalBar:
          begin
            FillSize := SolveForX(PercentDone, W);
            if FillSize > W then FillSize := W;
            if FillSize > 0 then FillRect(Rect(PaintRect.Left, PaintRect.Top,
              FillSize, H));
          end;
        gkVerticalBar:
          begin
            FillSize := SolveForX(PercentDone, H);
            if FillSize >= H then FillSize := H - 1;
            FillRect(Rect(PaintRect.Left, H - FillSize, W, H));
          end;
      end;
    end;
  end;
end;

procedure TGauge2.PaintAsPie(AnImage: TBitmap; PaintRect: TRect);
var
  MiddleX, MiddleY: Integer;
  Angle: Double;
  W, H: Integer;
begin
  W := PaintRect.Right - PaintRect.Left;
  H := PaintRect.Bottom - PaintRect.Top;
  if FBorderStyle = bsSingle then
  begin
    Inc(W);
    Inc(H);
  end;
  with AnImage.Canvas do
  begin
    Brush.Color := Color;
    FillRect(PaintRect);
    Brush.Color := BackColor;
    Pen.Color := ForeColor;
    Pen.Width := 1;
    Ellipse(PaintRect.Left, PaintRect.Top, W, H);
    if PercentDone > 0 then
    begin
      Brush.Color := ForeColor;
      MiddleX := W div 2;
      MiddleY := H div 2;
      Angle := (Pi * ((PercentDone / 50) + 0.5));
      Pie(PaintRect.Left, PaintRect.Top, W, H,
        Integer(Round(MiddleX * (1 - Cos(Angle)))),
        Integer(Round(MiddleY * (1 - Sin(Angle)))), MiddleX, 0);
    end;
  end;
end;

procedure TGauge2.PaintAsNeedle(AnImage: TBitmap; PaintRect: TRect);
var
  MiddleX: Integer;
  Angle: Double;
  X, Y, W, H: Integer;
begin
  with PaintRect do
  begin
    X := Left;
    Y := Top;
    W := Right - Left;
    H := Bottom - Top;
    if FBorderStyle = bsSingle then
    begin
      Inc(W);
      Inc(H);
    end;
  end;
  with AnImage.Canvas do
  begin
    Brush.Color := Color;
    FillRect(PaintRect);
    Brush.Color := BackColor;
    Pen.Color := ForeColor;
    Pen.Width := 1;
    Pie(X, Y, W, H * 2 - 1, X + W, PaintRect.Bottom - 1, X, PaintRect.Bottom - 1);
    MoveTo(X, PaintRect.Bottom);
    LineTo(X + W, PaintRect.Bottom);
    if PercentDone > 0 then
    begin
      Pen.Color := ForeColor;
      MiddleX := Width div 2;
      MoveTo(MiddleX, PaintRect.Bottom - 1);
      Angle := (Pi * ((PercentDone / 100)));
      LineTo(Integer(Round(MiddleX * (1 - Cos(Angle)))),
        Integer(Round((PaintRect.Bottom - 1) * (1 - Sin(Angle)))));
    end;
  end;
end;

procedure TGauge2.SetGaugeKind(Value: TGaugeKind);
begin
  if Value <> FKind then
  begin
    FKind := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetText(Value: String);
begin
  if Value <> FText then
  begin
    FText := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetShowText(Value: Boolean);
begin
  if Value <> FShowText then
  begin
    FShowText := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetShowPercent(Value: Boolean);
begin
  if Value <> FShowPercent then
  begin
    FShowPercent := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetRealOneStyle(Value: Boolean);
begin
  if Value <> FRealOneStyle then
  begin
    FRealOneStyle := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetBorderStyle(Value: TBorderStyle);
begin
  if Value <> FBorderStyle then
  begin
    FBorderStyle := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetForeColor(Value: TColor);
begin
  if Value <> FForeColor then
  begin
    FForeColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetBackColor(Value: TColor);
begin
  if Value <> FBackColor then
  begin
    FBackColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetBorderColor(Value: TColor);
begin
  if Value <> FBorderColor then
  begin
    FBorderColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetBaseInnerHighColor(Value: TColor);
begin
  if Value <> FBaseInnerHighColor then
  begin
    FBaseInnerHighColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetBaseInnerLowColor(Value: TColor);
begin
  if Value <> FBaseInnerLowColor then
  begin
    FBaseInnerLowColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetInnerHighColor(Value: TColor);
begin
  if Value <> FInnerHighColor then
  begin
    FInnerHighColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetInnerLowColor(Value: TColor);
begin
  if Value <> FInnerLowColor then
  begin
    FInnerLowColor := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetMinValue(Value: Longint);
begin
  if Value <> FMinValue then
  begin
    if Value > FMaxValue then
      if not (csLoading in ComponentState) then
        raise EInvalidOperation.CreateFmt(SOutOfRange, [-MaxInt, FMaxValue - 1]);
    FMinValue := Value;
    if FCurValue < Value then FCurValue := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetMaxValue(Value: Longint);
begin
  if Value <> FMaxValue then
  begin
    if Value < FMinValue then
      if not (csLoading in ComponentState) then
        raise EInvalidOperation.CreateFmt(SOutOfRange, [FMinValue + 1, MaxInt]);
    FMaxValue := Value;
    if FCurValue > Value then FCurValue := Value;
    Refresh;
  end;
end;

procedure TGauge2.SetProgress(Value: Longint);
var
  TempPercent: Longint;
begin
  TempPercent := GetPercentDone;  { remember where we were }
  if Value < FMinValue then
    Value := FMinValue
  else if Value > FMaxValue then
    Value := FMaxValue;
  if FCurValue <> Value then
  begin
    FCurValue := Value;
    if TempPercent <> GetPercentDone then { only refresh if percentage changed }
      Refresh;
  end;
end;

procedure TGauge2.AddProgress(Value: Longint);
begin
  Progress := FCurValue + Value;
  Refresh;
end;

end.
