unit ExPanel;

interface

uses
  Windows, Messages, KOL
  , Window_APIs;

type
  PExPanel = ^TExPanel;                 //本对象需要手动释放,会创建一个KOL的Panel
  TExPanel = object(TObj)
  private

    m_title_caption: KOLString;
    FMouseInButtonIdx: Byte;            //指示鼠标当前在哪个按钮上(0:不在按钮区域 1:缩进按钮 2:关闭按钮)

    FOnAfterPaint: TOnPaint;
    m_base_panel: PControl;
  protected
    function onBasePanelMsg(var Msg: TMsg; var Rslt: Integer): Boolean;
    procedure onBasePanelMouseMove(Sender: PControl; var Mouse: TMouseEventData);
    procedure onBasePanelMouseLeave(Sender: PObj);
    procedure onPaint(Sender: PControl; DC: HDC);
  public
    destructor Destroy; virtual;

    property BasePanel: PControl read m_base_panel;
    property TitleCaption: KOLString read m_title_caption write m_title_caption;
    property OnAfterPaint: TOnPaint read FOnAfterPaint write FOnAfterPaint;
  end;

function NewExPanel(parent: PControl; align: TControlAlign = caNone): PExPanel;

implementation

const
  _C_TitleButtonTopOffset = 3;          //按钮距顶端的偏移
  _C_TitleButtonWidth = 15;
  _C_TitleButtonHeight = _C_TitleButtonWidth + _C_TitleButtonTopOffset; //实际是按钮的底段距窗口顶端的偏移
  _C_TitleHeight    = _C_TitleButtonHeight + _C_TitleButtonTopOffset; //标题栏的高度
  _C_WidthBtwButton = 1;                //按钮间距

function NewExPanel(parent: PControl; align: TControlAlign = caNone): PExPanel;
begin
  New(Result, Create);
  if Result = nil then Exit;
  with Result^ do
  begin
    m_base_panel := NewPanel(parent, esNone);
    with m_base_panel^ do
    begin
      Font.Assign(parent.Font);
      SetAlign(align);

      OnMouseMove := onBasePanelMouseMove;
      OnMouseLeave := onBasePanelMouseLeave;
      OnMessage := onBasePanelMsg;
      OnShow:=OnShow;
      m_base_panel.onPaint := Result.onPaint;

      Show();
    end;
  end;
end;

{ TExPanel }

destructor TExPanel.Destroy;
begin
  Free_And_Nil(m_base_panel);
  inherited;
end;

procedure TExPanel.onBasePanelMouseLeave(Sender: PObj);
var
  L_DC              : HDC;
  tmpBaseOffset     : Cardinal;
  L_UsedRect        : TRect;
begin
  L_DC := m_base_panel.Canvas.Handle;

  if FMouseInButtonIdx <> 0 then
  begin
    tmpBaseOffset := m_base_panel.Width - (_C_TitleButtonWidth * 2 + _C_WidthBtwButton);

    L_UsedRect.Top := _C_TitleButtonTopOffset;
    L_UsedRect.Bottom := _C_TitleButtonHeight;
    L_UsedRect.Left := tmpBaseOffset + (_C_TitleButtonWidth + 1) * (FMouseInButtonIdx - 1);
    L_UsedRect.Right := L_UsedRect.Left + _C_TitleButtonWidth;
    DrawFrameControl(L_DC, L_UsedRect, DFC_BUTTON, DFCS_BUTTONPUSH);

    FMouseInButtonIdx := 0;
  end;
end;

procedure TExPanel.onBasePanelMouseMove(Sender: PControl; var Mouse: TMouseEventData);
var
  L_DC              : HDC;
  tmpBaseOffset     : Cardinal;
  tmpMousePos_X     : SmallInt;
  L_MouseInButtonIdxNow: Byte;
  L_UsedRect        : TRect;
begin
  tmpBaseOffset := m_base_panel.Width - (_C_TitleButtonWidth * 2 + _C_WidthBtwButton);
  tmpMousePos_X := Mouse.X;
  Dec(tmpMousePos_X, tmpBaseOffset);
  if (tmpMousePos_X > 0) and (Mouse.Y < _C_TitleHeight) then
  begin
    L_MouseInButtonIdxNow := (tmpMousePos_X div _C_TitleButtonWidth) + 1;
    if L_MouseInButtonIdxNow <> FMouseInButtonIdx then
    begin
      L_DC := m_base_panel.Canvas.Handle;

      L_UsedRect.Top := _C_TitleButtonTopOffset;
      L_UsedRect.Bottom := _C_TitleButtonHeight;
      if FMouseInButtonIdx <> 0 then    //还原前一个按钮
      begin
        L_UsedRect.Left := tmpBaseOffset + (_C_TitleButtonWidth + _C_WidthBtwButton) * (FMouseInButtonIdx - 1);
        L_UsedRect.Right := L_UsedRect.Left + _C_TitleButtonWidth;
        DrawFrameControl(L_DC, L_UsedRect, DFC_BUTTON, DFCS_BUTTONPUSH);
      end;

      L_UsedRect.Left := tmpBaseOffset + (_C_TitleButtonWidth + 1) * (L_MouseInButtonIdxNow - 1);
      L_UsedRect.Right := L_UsedRect.Left + _C_TitleButtonWidth;
      DrawFrameControl(L_DC, L_UsedRect, DFC_BUTTON, DFCS_BUTTONPUSH or DFCS_FLAT);

      FMouseInButtonIdx := L_MouseInButtonIdxNow;
    end;
  end
  else
  begin
    if FMouseInButtonIdx <> 0 then
    begin
      L_DC := m_base_panel.Canvas.Handle;

      L_UsedRect.Top := _C_TitleButtonTopOffset;
      L_UsedRect.Bottom := _C_TitleButtonHeight;
      L_UsedRect.Left := tmpBaseOffset + (_C_TitleButtonWidth + 1) * (FMouseInButtonIdx - 1);
      L_UsedRect.Right := L_UsedRect.Left + _C_TitleButtonWidth;
      DrawFrameControl(L_DC, L_UsedRect, DFC_BUTTON, DFCS_BUTTONPUSH);

      FMouseInButtonIdx := 0;
    end;
  end;
end;

function TExPanel.onBasePanelMsg(var Msg: TMsg; var Rslt: Integer): Boolean;
begin
  Result := False;
  case Msg.message of
    WM_LBUTTONUP:
      begin
        if FMouseInButtonIdx = 1 then
        begin

        end
        else
          if FMouseInButtonIdx = 2 then
            m_base_panel.Hide;
      end;

    WM_LBUTTONDOWN:
      begin
        if FMouseInButtonIdx = 1 then
        begin

        end
        else
          if FMouseInButtonIdx = 2 then
            m_base_panel.Show;
      end;
  end;
end;

procedure TExPanel.onPaint(Sender: PControl; DC: HDC);
var
  local_rect        : TRect;
  local_canvas      : PCanvas;
begin
  local_canvas := m_base_panel.Canvas;

  {* 绘制缩进和关闭按钮 *}
  local_rect.Top := _C_TitleButtonTopOffset;
  local_rect.Bottom := _C_TitleButtonHeight;

  local_rect.Left := m_base_panel.Width - _C_TitleButtonWidth;
  DrawFrameControl(DC, local_rect, DFC_BUTTON, DFCS_BUTTONPUSH);

  local_rect.Right := local_rect.Left - _C_WidthBtwButton;
  local_rect.Left := m_base_panel.Width - _C_TitleButtonWidth * 2 - _C_WidthBtwButton;
  DrawFrameControl(DC, local_rect, DFC_BUTTON, DFCS_BUTTONPUSH);

  {* 绘制出头标题 *}
  local_rect.Right := local_rect.Left;
  local_rect.Left := 0;
  //DrawText(DC, PKOLChar(m_title_caption), Length(m_title_caption), local_rect, DT_CENTER or DT_SINGLELINE or DT_VCENTER or DT_NOCLIP);
  local_canvas.TextRect(local_rect, 0, 0, m_title_caption);

  {* 绘制头标题下面的分割线 *}
  local_canvas.MoveTo(local_rect.Left, _C_TitleHeight);
  local_canvas.LineTo(m_base_panel.Width, _C_TitleHeight);

  if Assigned(FOnAfterPaint) then
    FOnAfterPaint(@Self, DC);
end;

end.

