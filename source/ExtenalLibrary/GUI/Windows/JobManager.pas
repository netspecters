{***************************************************************************}
{                                                                           }
{       TaskView                                                            }
{                                                                           }
{       版权所有 (C) 2006 王冠                                              }
{                                                                           }
{       功能说明:   数据传输窗体使用的任务观察器                            }
{       说明:       专为NS系统的DataTransfer窗体设计的,没有通用性           }
{                                                                           }
{       版本历史:                                                           }
{               1:2006-8-16至                                               }
{***************************************************************************}

unit JobManager;

interface
uses
  Windows, KOL, pascal_PlusInterface, ProgressBar, NSErrorCode, SysUtils;

type
  //job 构建信息
  ST_JobCreateParam = record
    url: AnsiString;
    file_path, file_name: KOLString;
    thread_num: Integer;                //不包含主线程在内的线程数量
    user_name, passwd, cookie: AnsiString;
    start_now: Boolean;                 //是不是建立就运行
  end;
  TOnMenuUp = procedure(x, y: Word; item_id: Integer) of object;

type
  ST_JobInformation = packed record
    job_handle: Cardinal;
    total_data_size, recved_data_size, sended_data_size: Cardinal;
    total_thread_count, actived_thread_count: SmallInt;
    up_speed, down_speed: SmallInt;
    name_str_ptr: PKOLChar;

    thread_infor_ptr: Pointer;
  end;

  TJobStatus = (jsConnect, jsSync, jsDataTransfering, jsSuspend, jsError, jsFinish);
  PJobStatus = ^ST_JobStatus;
  ST_JobStatus = record
    status: TJobStatus;
    job_status_id: Cardinal;
    kernel_job_handle: Cardinal;
    user_need_thread_num: Cardinal;     //用户需要的线程数量(剩余)
    kenrel_message: KOLString;          //NS内核通过回调返回的信息
  end;

  CJobManager = class(IProtocolEventListener)
  private
    m_protocol_manager_ptr: IProtocolManager;
    m_main_window: PControl;
    m_auto_refresh_interval: Cardinal;

    m_base_listbox: PControl;
    m_update_timer: PTimer;
    m_item_height: Integer;
    m_on_menu_up: TOnMenuUp;

    procedure resumeJob(job_status_ptr: PJobStatus); overload;
    function superCall(call_type: ENM_PM_InformationType; ctrl_code: Cardinal; wparam, lparam: LongInt; data_ptr: Pointer): Integer;
    function getOnSelChange: TOnEvent;
    procedure setOnSelChange(const Value: TOnEvent);
    function getCurIndex: Integer;
    procedure setCurIndex(const Value: Integer);
    function getJobCount: Cardinal;

  protected
    function onDrawItem(Sender: PObj; DC: HDC; const Rect: TRect; ItemIdx: Integer; DrawAction: TDrawAction; ItemState: TDrawState): Boolean;
    function onMeasureItem(Sender: PObj; Idx: Integer): Integer;
    procedure onMouseEvent(Sender: PControl; var Mouse: TMouseEventData);
    procedure onAutoRefreshTime(Sender: PObj);

    function getJob(index: Integer): PJobStatus;
    function onEvent(event_id: ENM_Event_ID; job_handle: LongInt; param: Integer; data: Pointer; job_tag: LongInt): Integer; override; stdcall;
  public
    procedure Refresh();

    procedure addJob(const base_url: string = '');
    procedure resumeJob(job_index: Integer); overload;
    procedure moveJob(offset: Integer);
    procedure delJob(job_index: Integer);
    constructor create(main_window: PControl; list_parent: PControl; protocol_manager_ptr: IProtocolManager);
    destructor Destroy(); override;

    property ProtocolManager: IProtocolManager read m_protocol_manager_ptr;
    property Jobs[index: Integer]: PJobStatus read getJob;
    property AutoRefreshInterval: Cardinal read m_auto_refresh_interval write m_auto_refresh_interval;
    property CurIndex: Integer read getCurIndex write setCurIndex;
    property onMenuUp: TOnMenuUp read m_on_menu_up write m_on_menu_up;
    property onSelChange: TOnEvent read getOnSelChange write setOnSelChange;
    property JobCount: Cardinal read getJobCount;
  end;

implementation

uses
  NewWorkDlg;

const
  _CS_Status_Infor  : array[TJobStatus] of KOLString = ('正在连接远程主机...', '正在与远程主机通信...',
    '数据传输中...', '任务暂停', '任务错误', '任务完成！');
  _CS_Units         : array[0..3] of KOLString = ('B', 'KB', 'MB', 'GB');
  _CS_ITEM_HEIGH    = 48 + 2;

  { CJobManager }

procedure CJobManager.addJob(const base_url: string = '');
var
  job_create_param  : ST_JobCreateParam;
  new_job_status_ptr: PJobStatus;
  job_param         : ST_JobParam;
  extend_data       : ST_ProtocolJob_Extend_Data;
  ctrl_infor        : ST_PM_CtrlInformation;
  new_job_dlg       : PCreateWorkDlg;

  function setValue(Value: string): PChar;
  begin
    if Value <> '' then
      Result := PChar(Value)
    else
      Result := nil;
  end;
begin
  //开启新建对话框
  new_job_dlg := NewCreateWorkDlg(m_main_window, base_url);
  if new_job_dlg.ShowModle(job_create_param) then
  begin
    //建立job status描述符
    GetMem(new_job_status_ptr, SizeOf(ST_JobStatus));

    //初始化job status
    FillChar(new_job_status_ptr^, SizeOf(ST_JobStatus), 0);
    new_job_status_ptr.user_need_thread_num := job_create_param.thread_num;
    if job_create_param.start_now then
      new_job_status_ptr.status := jsConnect
    else
      new_job_status_ptr.status := jsSuspend;

    //分离出协议附加数据
    extend_data.size := SizeOf(ST_ProtocolJob_Extend_Data);
    extend_data.usr_name := setValue(job_create_param.user_name);
    extend_data.usr_passwd := setValue(job_create_param.passwd);
    extend_data.cookie := setValue(job_create_param.cookie);

    //初始化协议job参数
    FillChar(job_param, SizeOf(job_param), 0);
    with job_param do
    begin
      with protocol_param do
      begin
        protocol_address := PAnsiChar(job_create_param.url);
        extend_data_ptr := Pointer(@extend_data);
      end;
      enable_preprocessor:=True;
      local_file_path := PWideChar(job_create_param.file_path + job_create_param.file_name);
    end;

    //请求manager添加job
    FillChar(ctrl_infor, SizeOf(ctrl_infor), 0);
    with ctrl_infor do
    begin
      infor_size := SizeOf(ctrl_infor);
      pm_information_type := Ord(imJob);
      ctrl_code := Ord(jcAdd);
      wparam := LongInt(new_job_status_ptr); //tag
      data_ptr := PByte(@job_param);
    end;
    m_protocol_manager_ptr.superCall(ctrl_infor);

    //启动定时器
    m_update_timer.Enabled := True;
  end;
  Free_And_Nil(new_job_dlg);
end;

constructor CJobManager.create(main_window: PControl; list_parent: PControl; protocol_manager_ptr: IProtocolManager);
var
  job_informations_ptr: ^ST_JobInformation;
  new_job_status_ptr: PJobStatus;
  job_count, i      : Integer;
  item_index        : Integer;
begin
  m_main_window := main_window;
  m_auto_refresh_interval := 1000;
  m_protocol_manager_ptr := protocol_manager_ptr;

  //显示列表
  m_base_listbox := NewListbox(list_parent, [loNoIntegralHeight, loOwnerDrawFixed, loOwnerDrawVariable, loNoData]);
  m_base_listbox.EraseBackground := False;
  m_base_listbox.onDrawItem := onDrawItem;
  m_base_listbox.onMeasureItem := onMeasureItem;
  m_base_listbox.OnMouseUp := onMouseEvent;
  m_base_listbox.SetAlign(caClient);
  //m_base_listbox.SetUnicode(True);

  //刷新timer
  m_update_timer := NewTimer(1000);
  with m_update_timer^ do
  begin
    OnTimer := onAutoRefreshTime;
    Enabled := False;
  end;

  //计算面板高度
  m_item_height := _CS_ITEM_HEIGH;

  //请求protocol manager，获取当前job列表
  job_count := superCall(imJob, Ord(jcStatistic), 0, 0, nil);
  if job_count > 0 then
  begin
    GetMem(job_informations_ptr, SizeOf(ST_JobInformation) * job_count);
    superCall(imJob, Ord(jcStatistic), job_count, 0, Pointer(job_informations_ptr));

    for i := 0 to job_count do
    begin
      GetMem(new_job_status_ptr, SizeOf(ST_JobStatus));
      FillChar(new_job_status_ptr^, SizeOf(ST_JobStatus), 0);
      new_job_status_ptr.kernel_job_handle := job_informations_ptr.job_handle;
      new_job_status_ptr.status := jsSuspend;

      item_index := m_base_listbox.Add('');
      m_base_listbox.ItemData[item_index] := Cardinal(new_job_status_ptr);
    end;

    FreeMem(job_informations_ptr);
  end;
end;

procedure CJobManager.delJob(job_index: Integer);
begin

end;

destructor CJobManager.Destroy;
begin
  m_base_listbox.Free;
  inherited;
end;

function CJobManager.getCurIndex: Integer;
begin
  Result := m_base_listbox.CurIndex;
end;

function CJobManager.getJob(index: Integer): PJobStatus;
begin
  Result := PJobStatus(m_base_listbox.ItemData[index]);
end;

function CJobManager.getJobCount: Cardinal;
begin
  Result := m_base_listbox.Count;
end;

function CJobManager.getOnSelChange: TOnEvent;
begin
  Result := m_base_listbox.onSelChange;
end;

function transSizeToTextSelector(speed: Cardinal; var text_selector: Integer): Single;
begin
  if speed < 1024 then
    text_selector := 0                  //B
  else if speed < 1024 * 1024 then
    text_selector := 1                  //KB
  else if speed < 1024 * 1024 * 1024 then
    text_selector := 2                  //MB
  else
    text_selector := 3;                 //GB
  Result := speed;
  if text_selector > 0 then
    Result := Result / (text_selector * 1024);
end;

procedure CJobManager.moveJob(offset: Integer);
var
  cur_idx           : Integer;
  next_idx          : Integer;
  cur_name          : KOLString;
  cur_value         : Cardinal;
begin
  with m_base_listbox^ do
  begin
    BeginUpdate;

    cur_idx := CurIndex;
    next_idx := cur_idx + offset;

    cur_name := Items[cur_idx];
    Items[cur_idx] := Items[next_idx];
    Items[next_idx] := cur_name;

    cur_value := ItemData[cur_idx];
    ItemData[cur_idx] := ItemData[next_idx];
    ItemData[next_idx] := cur_value;

    CurIndex := next_idx;

    EndUpdate;
  end;
end;

procedure CJobManager.onAutoRefreshTime(Sender: PObj);
begin
  Refresh();
end;

function CJobManager.onDrawItem(Sender: PObj; DC: HDC; const Rect: TRect; ItemIdx: Integer; DrawAction: TDrawAction; ItemState: TDrawState): Boolean;
const
  _C_LINE_PEND      = 2;                //行间距
  _CS_IconSize      = _CS_ITEM_HEIGH;
var
  job_information   : ST_JobInformation;
  text_rect, prograss_bar_rect: TRect;
  current_job_status_ptr: PJobStatus;
  line_height       : Integer;
  text_string       : KOLString;        //需要绘制的字符串

  recv_size_float, total_size_float, persent, up_speed_float, down_speed_float: Single;
  recv_size_select, total_size_select, up_speed_select, down_speed_select: Integer;
begin
  Result := True;
  if ItemIdx < 0 then
    Exit;

  //job的上下文
  current_job_status_ptr := Jobs[ItemIdx];

  {* 获取指定job的信息 *}
  superCall(imJob, Ord(jcStatistic), 1, current_job_status_ptr.kernel_job_handle, Pointer(@job_information));

  {* 绘制面板背景 *}
  m_base_listbox.Canvas.Brush.BrushStyle := bsSolid;
  //if odsSelected in ItemState then      //！！有问题！！
  if ItemIdx = m_base_listbox.CurIndex then //当前项被选择,填充选择背景
  begin
    m_base_listbox.Canvas.Brush.Color := clBackground;
    m_base_listbox.Canvas.Font.Color := clWhite;
  end
  else                                  //填充正常背景
  begin
    m_base_listbox.Canvas.Brush.Color := clWindow;
    m_base_listbox.Canvas.Font.Color := clMenuText;
  end;
  m_base_listbox.Canvas.FillRect(Rect);

  {* 绘制项目 *}
  with current_job_status_ptr^, job_information do
  begin
    if status = jsError then
      m_base_listbox.Canvas.Font.Color := clRed;

    //绘制文件名
    text_rect := MakeRect(Rect.Left + _CS_IconSize + 2, Rect.Top + _C_LINE_PEND, Rect.Right, Rect.Bottom);
    line_height := m_base_listbox.Canvas.DrawTextEx(m_base_listbox.Items[ItemIdx], text_rect, DT_SINGLELINE or DT_LEFT) + _C_LINE_PEND * 2;

    //绘制状态信息
    text_rect.Top := text_rect.Top + line_height;
    text_string := _CS_Status_Infor[status];
    if kenrel_message <> '' then
      text_string := text_string + SysUtils.WideFormat('(%s)', [kenrel_message]);
    m_base_listbox.Canvas.DrawTextEx(text_string, text_rect, DT_SINGLELINE or DT_LEFT);

    //绘制线程信息

    //绘制时间信息

    //计算/绘制速度
    up_speed_float := transSizeToTextSelector(up_speed, up_speed_select);
    down_speed_float := transSizeToTextSelector(down_speed, down_speed_select);

    text_string := SysUtils.WideFormat('上行: %n%s/S - 下行: %n%s/S',
      [up_speed_float, _CS_Units[up_speed_select],
      down_speed_float, _CS_Units[down_speed_select]]);
    text_rect := MakeRect(Rect.Right - 200, Rect.Top + line_height, Rect.Right - 2, Rect.Bottom - 2);
    m_base_listbox.Canvas.DrawTextEx(text_string, text_rect, DT_SINGLELINE or DT_RIGHT);

    //计算/绘制大小、进度
    recv_size_float := transSizeToTextSelector(recved_data_size, recv_size_select);
    total_size_float := transSizeToTextSelector(total_data_size, total_size_select);
    if total_data_size > 0 then
      persent := (recved_data_size / total_data_size) * 100
    else
      persent := 0;

    text_string := SysUtils.WideFormat('%n%% (%n%s/%n%s)', [persent, recv_size_float, _CS_Units[recv_size_select], total_size_float, _CS_Units[total_size_select]]);
    text_rect := MakeRect(Rect.Right - 150, Rect.Top + line_height * 2, Rect.Right - 2, Rect.Bottom - 2);
    m_base_listbox.Canvas.DrawTextEx(text_string, text_rect, DT_SINGLELINE or DT_RIGHT);

    //绘制进度条
    prograss_bar_rect := MakeRect(_CS_IconSize + 2, text_rect.Top - 2, text_rect.Left - 5, Rect.Bottom - 2);
    drawPrograssBar(m_base_listbox.Canvas, prograss_bar_rect, recved_data_size, 0, total_data_size);
  end;
end;

function CJobManager.onEvent(event_id: ENM_Event_ID; job_handle: LongInt; param: Integer; data: Pointer; job_tag: LongInt): Integer;
var
  job_index         : Integer;
begin
  begin
    case event_id of
      eidJobAdded:
        begin
          //加入job list
          job_index := m_base_listbox.Add('');
          m_base_listbox.ItemData[job_index] := job_tag;
          with Jobs[job_index]^ do
          begin
            kernel_job_handle := job_handle;
            if status = jsConnect then
              resumeJob(job_index);
          end;
        end;

      eidJobDeleted:
        begin
          for job_index := 0 to m_base_listbox.Count - 1 do
            if m_base_listbox.ItemData[job_index] = job_tag then
            begin
              FreeMem(Pointer(job_tag));
              m_base_listbox.Delete(job_index);

              m_update_timer.Enabled := m_base_listbox.Count > 0;
              Break;
            end;
        end;

      eidJobSuspend:
        begin

        end;

      eidJobResume:
        begin

        end;

      eidPeerAdded:
        begin

        end;
    end;
  end;
  Result := NSEC_SUCCESS;
end;

function CJobManager.onMeasureItem(Sender: PObj; Idx: Integer): Integer;
begin
  Result := m_item_height;
end;

procedure CJobManager.onMouseEvent(Sender: PControl; var Mouse: TMouseEventData);
var
  screen_point      : TPoint;
begin
  case Mouse.Button of
    mbRight:
      if Assigned(m_on_menu_up) then
      begin
        screen_point := MakePoint(Mouse.x, Mouse.y);
        screen_point := m_base_listbox.Client2Screen(screen_point);
        m_on_menu_up(screen_point.x, screen_point.y, m_base_listbox.LBItemAtPos(Mouse.x, Mouse.y));
      end;
  end;
end;

procedure CJobManager.Refresh;
begin
  m_base_listbox.InvalidateEx;
end;

procedure CJobManager.resumeJob(job_index: Integer);
begin
  resumeJob(Jobs[job_index]);
end;

procedure CJobManager.resumeJob(job_status_ptr: PJobStatus);
begin
  superCall(imJob, Cardinal(jcResume), 0, job_status_ptr.kernel_job_handle, nil);
end;

procedure CJobManager.setCurIndex(const Value: Integer);
begin
  m_base_listbox.CurIndex := Value;
end;

procedure CJobManager.setOnSelChange(const Value: TOnEvent);
begin
  m_base_listbox.onSelChange := Value;
end;

function CJobManager.superCall(call_type: ENM_PM_InformationType; ctrl_code: Cardinal; wparam, lparam: Integer; data_ptr: Pointer): Integer;
var
  job_ctrl_infor    : ST_PM_CtrlInformation;
begin
  with job_ctrl_infor do
  begin
    infor_size := SizeOf(ST_PM_CtrlInformation);
    pm_information_type := Ord(call_type);
  end;
  job_ctrl_infor.wparam := wparam;
  job_ctrl_infor.lparam := lparam;
  job_ctrl_infor.ctrl_code := ctrl_code;
  job_ctrl_infor.data_ptr := data_ptr;
  Result := m_protocol_manager_ptr.superCall(job_ctrl_infor);
end;

end.

