{***************************************************************************}
{                                                                           }
{       NS 数据流量曲线窗口类实现单元                                       }
{                                                                           }
{       版权所有 (C) 2006 王冠                                              }
{                                                                           }
{       功能说明:                                                           }
{                能截获拖拽的URL...                                         }
{       版本历史:                                                           }
{               1、2006-1-18至2006-1-20                                     }
{               2、2006-1-26                                                }
{                 使用PatBlt代替FillRect填充背景色(黑色),提高效率           }
{***************************************************************************}

{.$DEFINE BUILD_TEST}

unit DataFluxWindow_Implement;

interface

uses
  Windows, Messages,
  Kol, Err,
  DragTarget_Implement, NSErrorCode, Window_APIs;

const
  C_WindowWidth     = 40;
  C_WindowHeight    = 36;
  C_BorderOffset    = 3;
  C_FontEareHeight  = 10;
  C_FontHeight_Width = -10;
  C_WindowAlpha     = 150;              //窗口的透明度
  C_TimerID         = 1;                //定时器标识码
  C_TimeOut         = {$IFDEF BUILD_TEST}1000{$ELSE}1000{$ENDIF}; //定时时间
  C_HintOffset      = 2;
  C_HintPointHeight_With = C_HintOffset + 5;

type
  TURLDropEvent = procedure(const stURL: string) of object;
  TDataFluxWindowMouseEvent = procedure(dwMouseEvent: Cardinal; wParam, lParam: Integer) of object;
  TDataFluxWindowDataLineDrawing = function(const dwTimeCount: Cardinal; var dwCurrentJobPercent, dwGlobalSpeed: Cardinal; var bJobHealth: Boolean): Boolean of object;
  {
   dwTimeCount:工作时序计数(再FinishDrawing调用后将被置零)
   dwCurrentJobPercent:当前工作完成的百分比，用于绘制流量窗口的百分比
   dwGlobalSpeed:全局速度，用于传输速度数据曲线
   bJobHealth:表明任务是否健康，如果为false，则将提示点绘制成红色
   返回的结果表明是否发生了任务切换
  }
type
  PDataFluxWindow = ^TDataFluxWindow;
  TDataFluxWindow = object(TObj)
  private
    FOnDrawing: Boolean;

    FDragControl: TURLDragTarget;

    (*背景窗体*)
    FBackgroundJmpCode: ST_WndProc;
    FBackgroundWindow: THandle;
    FBackgroundWindowDC: HDC;           //开始做图时才会开辟,做图完毕后就会释放掉

    (*伪窗口(内存窗口)*)
    FMemoryWindowDC: HDC;
    FMemoryWindowHandle: THandle;
    FMemoryWindowOldObject: THandle;
    FMemoryWindowBdBrush: THandle;      //背景画刷

    FMemoryWindowHintPointBrush_Red,    //红色提示点画刷
    FMemoryWindowHintPointBrush_Green: THandle; //绿色提示点的画刷
    FMemoryWindowOldBrush: THandle;

    FMemoryWindowPen, FMemoryWindowOldPen: THandle; //画笔
    FMemoryWindowFont, FMemoryWindowOldFont: THandle;

    FTextEareRect: TRect;               //显示当前作业完成百分比数的区域

    FClientRect: TRect;                 //客户区大小位置信息 表示的是相对Window的大小
    FWindowSize: TRect;                 //窗口大小位置信息 表示的是相对Screen大小

    FMaxRange: Cardinal;

    FOnMouseEvent: TDataFluxWindowMouseEvent;
    FOnDataLineDrawing: TDataFluxWindowDataLineDrawing;
  private
    function GetOnGetURL(): TURLDropEvent;
    procedure SetOnGetURL(const Value: TURLDropEvent);
  private
    FTimeCount: Cardinal;
    FCanDrawHintPoint: Boolean;         //表明是否可以绘制提示点(每2个计时点变为true)
    FJobSwitch: Boolean;                //是否发生了任务切换
    {$IFDEF BUILD_TEST}
    FPercentCount: Cardinal;
    function GetCurrentData(): Cardinal;
    {$ENDIF}                            //得到当前数据
    procedure UpdateMemoryWindowToFrontWindow(dwCurrentJobPercent, dwGlobalSpeed: Cardinal; bJobHealth: Boolean); //根据当前的时序关系将背景图画到前景窗体上
  protected
    procedure DrawBackgroundWindow(dwBkDC: HDC);
    function BackgroundWndProc(hWnd: THandle; uMsg: Cardinal; wParam, lParam:
      Integer): LongWord; stdcall;
  public
    destructor Destroy; virtual;

    (*窗口状态*)
    procedure RefreshWindow();
    procedure Show();
    procedure Hide();
    procedure readWindowPosition(out Left, Top: Word);

    (*工作状态控制*)
    function StartDrawing(): Integer;
    procedure Reset();
    procedure FinishDrawing();
  public
    property MaxRange: Cardinal read FMaxRange write FMaxRange;
    property isWorking: Boolean read FOnDrawing;
    property OnGetURL: TURLDropEvent read GetOnGetURL write SetOnGetURL; //当窗口截获URL时，将会触发此事件
    property OnMouseEvent: TDataFluxWindowMouseEvent read FOnMouseEvent write FOnMouseEvent; //当鼠标事件来到时触发
    property OnDataLineDrawing: TDataFluxWindowDataLineDrawing read FOnDataLineDrawing write FOnDataLineDrawing;
  end;

function NewDataFluxWindow(const InitTop, InitLeft: Word): PDataFluxWindow;

implementation

function NewDataFluxWindow(const InitTop, InitLeft: Word): PDataFluxWindow;
begin
  New(Result, Create);

  with Result^ do
  begin
    FWindowSize := MakeRect(InitLeft, InitTop, C_WindowWidth, C_WindowHeight);
    with FClientRect do
    begin
      Left := C_BorderOffset;
      Top := C_BorderOffset;
      Right := C_WindowWidth - C_BorderOffset;
      Bottom := C_WindowHeight - C_BorderOffset;
    end;
    FBackgroundWindow := Window_APIs.CreateKOLObjectWindow(
      '', FWindowSize, FBackgroundJmpCode, Pointer(Result), @TDataFluxWindow.BackgroundWndProc,
      0, 0,
      WS_POPUP or WS_VISIBLE,
      WS_EX_ACCEPTFILES or WS_EX_LAYERED or WS_EX_TOPMOST or WS_EX_TOOLWINDOW);
    SetLayeredWindowAttributes(FBackgroundWindow, 0, C_WindowAlpha, LWA_ALPHA);

    RefreshWindow;

    {创建拖拽控制类}
    FDragControl := TURLDragTarget.Create;
    FDragControl.RegisterDestWindow(FBackgroundWindow);
  end;
end;

{ TDataFluxWindow }

function TDataFluxWindow.BackgroundWndProc(hWnd: THandle; uMsg: Cardinal;
  wParam, lParam: Integer): LongWord;
var
  L_PaintStruct     : TPaintStruct;
  L_CurrentJobPercent, L_GlobalSpeed: Cardinal;
  L_JobHealth       : Boolean;
begin
  case uMsg of
    WM_SYSCOMMAND:
      begin
        case wParam of
          SC_CLOSE:
            begin
              Result := 0;
              Destroy;
            end;
        else
          Result := DefWindowProc(hWnd, uMsg, wParam, lParam);
        end;
      end;
    WM_NCHITTEST:
      begin
        Result := HTCAPTION;
      end;
    WM_PAINT:
      begin
        wParam := BeginPaint(hWnd, L_PaintStruct);
        try
          DrawBackgroundWindow(wParam);
        finally
          EndPaint(hWnd, L_PaintStruct);
        end;
        Result := 0;
      end;
    WM_TIMER:
      begin
        Inc(FTimeCount);
        FCanDrawHintPoint := not FCanDrawHintPoint;
        if Assigned(FOnDataLineDrawing) then
        begin
          L_JobHealth := True;
          L_GlobalSpeed := 0;
          L_CurrentJobPercent := 0;
          FJobSwitch := FOnDataLineDrawing(FTimeCount, L_CurrentJobPercent, L_GlobalSpeed, L_JobHealth);
          UpdateMemoryWindowToFrontWindow(L_CurrentJobPercent, L_GlobalSpeed, L_JobHealth);
        end
        else
          {$IFDEF BUILD_TEST}
        begin
          UpdateMemoryWindowToFrontWindow(FPercentCount, 0, True);
          Inc(FPercentCount);
        end;
        {$ELSE}
          UpdateMemoryWindowToFrontWindow(0, 0, False);
        {$ENDIF};
        Result := 0;
      end;
    WM_DESTROY:
      begin
        Result := 0;
      end;
    WM_NCLBUTTONDBLCLK..WM_NCRBUTTONDBLCLK:
      begin
        if Assigned(FOnMouseEvent) then
        begin
          FOnMouseEvent(uMsg + 352, wParam, lParam);
          Result := 0;
        end
        else
          Result := DefWindowProc(hWnd, uMsg, wParam, lParam);
      end;
  else
    Result := DefWindowProc(hWnd, uMsg, wParam, lParam);
  end;
end;

destructor TDataFluxWindow.Destroy;
var
  tmpHandle         : THandle;
begin
  FinishDrawing();

  FDragControl.Free;

  if FBackgroundWindow <> 0 then
  begin
    tmpHandle := FBackgroundWindow;
    FBackgroundWindow := 0;
    DestroyWindow(tmpHandle);
  end;

  inherited;
end;

procedure TDataFluxWindow.DrawBackgroundWindow(dwBkDC: HDC);
var
  L_Brush           : THandle;
begin
  {填充背景色}
  L_Brush := GetStockObject(DKGRAY_BRUSH);
  FillRect(dwBkDC, FWindowSize, L_Brush);
  DeleteObject(L_Brush);

  {用黑色画边框和填充工作区}
  L_Brush := GetStockObject(BLACK_BRUSH);
  FrameRect(dwBkDC, MakeRect(0, 0, C_WindowWidth, C_WindowHeight), L_Brush); {画边框}
  FillRect(dwBkDC, FClientRect, L_Brush); {填充工作区}
  DeleteObject(L_Brush);
end;

procedure TDataFluxWindow.FinishDrawing;
begin
  if FOnDrawing then
  begin
    {释放定时器}
    KillTimer(FBackgroundWindow, C_TimerID);

    {释放伪窗口}
    SelectObject(FMemoryWindowDC, FMemoryWindowOldObject);
    DeleteObject(FMemoryWindowHandle);
    DeleteObject(FMemoryWindowBdBrush);
    SelectObject(FMemoryWindowDC, FMemoryWindowOldBrush);
    DeleteObject(FMemoryWindowHintPointBrush_Green);
    DeleteObject(FMemoryWindowHintPointBrush_Red);
    SelectObject(FMemoryWindowDC, FMemoryWindowOldPen);
    DeleteObject(FMemoryWindowPen);
    SelectObject(FMemoryWindowDC, FMemoryWindowOldFont);
    DeleteObject(FMemoryWindowFont);

    DeleteDC(FMemoryWindowDC);

    {释放前景窗体DC}
    ReleaseDC(FBackgroundWindow, FBackgroundWindowDC);

    FOnDrawing := False;

    {清空前景窗口}
    RefreshWindow;

    FTimeCount := 0;

    {$IFDEF BUILD_TEST}
    FPercentCount := 0;
    {$ENDIF}
  end;
end;

{$IFDEF BUILD_TEST}

function TDataFluxWindow.GetCurrentData: Cardinal;
begin
  Result := Random(128);
end;
{$ENDIF}

function TDataFluxWindow.GetOnGetURL: TURLDropEvent;
begin
  Result := FDragControl.OnURLDroped;
end;

procedure TDataFluxWindow.Hide;
begin
  ShowWindow(FBackgroundWindow, SW_HIDE);
end;

procedure TDataFluxWindow.readWindowPosition(out Left, Top: Word);
var
  window_rect       : TRect;
begin
  GetWindowRect(FBackgroundWindow, window_rect);
  Left := window_rect.Left;
  Top := window_rect.Top;
end;

procedure TDataFluxWindow.RefreshWindow;
var
  L_DC              : HDC;
begin
  //RedrawWindow(FBackgroundWindow, Nil, 0, RDW_UPDATENOW);
  L_DC := GetDC(FBackgroundWindow);
  try
    DrawBackgroundWindow(L_DC);
  finally
    ReleaseDC(FBackgroundWindow, L_DC);
  end;
end;

procedure TDataFluxWindow.Reset;
begin
  if FOnDrawing then
  begin
    FillRect(FBackgroundWindowDC, FClientRect, FMemoryWindowBdBrush);
  end;
end;

procedure TDataFluxWindow.SetOnGetURL(const Value: TURLDropEvent);
begin
  FDragControl.OnURLDroped := Value;
end;

procedure TDataFluxWindow.Show;
begin
  ShowWindow(FBackgroundWindow, SW_SHOW);
end;

function TDataFluxWindow.StartDrawing: Integer;
begin
  Result := NSEC_NORMALERROR;
  if not FOnDrawing then
  begin
    {设置定时器}
    SetTimer(FBackgroundWindow, C_TimerID, C_TimeOut, @FBackgroundJmpCode);

    {建立伪窗口}
    FMemoryWindowDC := CreateCompatibleDC(0);

    //FMemoryWindowHandle := CreateCompatibleBitmap(FMemoryWindowDC, C_WindowWidth - 2 * C_BorderOffset, C_WindowHeight - 2 * C_BorderOffset);
    FMemoryWindowHandle := CreateBitmap(C_WindowWidth - 2 * C_BorderOffset, C_WindowHeight - 2 * C_BorderOffset, 1, 32, nil); //创建宽度、高度与客户区相同的内存位图作为伪窗口
    FMemoryWindowOldObject := SelectObject(FMemoryWindowDC, FMemoryWindowHandle);

    FMemoryWindowBdBrush := GetStockObject(BLACK_BRUSH); //建立背景画刷

    FMemoryWindowHintPointBrush_Red := CreateSolidBrush(RGB(255, 0, 0));
    FMemoryWindowHintPointBrush_Green := CreateSolidBrush(RGB(0, 255, 0));
    FMemoryWindowOldBrush := SelectObject(FMemoryWindowDC, FMemoryWindowHintPointBrush_Green); //将绿色画刷作为默认的画刷(健康情况较多)

    FMemoryWindowPen := CreatePen(PS_SOLID, 1, RGB(0, 255, 0)); //建立数据曲线画笔
    FMemoryWindowOldPen := SelectObject(FMemoryWindowDC, FMemoryWindowPen); //将画笔选到DC中

    FMemoryWindowFont := Windows.CreateFont(C_FontHeight_Width, 0, 0, 0, FW_THIN, Cardinal(False), Cardinal(False), Cardinal(False),
      DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
      PROOF_QUALITY, FF_SWISS or VARIABLE_PITCH, PChar('Arial'));
    FMemoryWindowOldFont := SelectObject(FMemoryWindowDC, FMemoryWindowFont);
    SetBkColor(FMemoryWindowDC, RGB(0, 0, 0));
    SetTextColor(FMemoryWindowDC, RGB(0, 255, 0)); //设置前景文字颜色
    SetBkMode(FMemoryWindowDC, TRANSPARENT);

    //FillRect( FMemoryWindowDC, MakeRect( 0, 0, C_WindowWidth - 2 * C_BorderOffset, C_WindowHeight - 2 * C_BorderOffset ), FMemoryWindowBdBrush ); //清背景
    {2++}PatBlt(FMemoryWindowDC, 0, 0, C_WindowWidth - C_BorderOffset * 2, C_WindowHeight - C_BorderOffset * 2, BLACKNESS); {2++}

    {设置文本区域}
    with FTextEareRect do
    begin
      Top := 0;
      Left := 0;
      Right := C_WindowWidth - 2 * C_BorderOffset;
      Bottom := C_BorderOffset + C_FontEareHeight - 2; //为了美观，多处来2个像素大小
    end;

    {得到前景窗体DC}
    FBackgroundWindowDC := GetDC(FBackgroundWindow);

    FOnDrawing := True;
    Result := NSEC_SUCCESS;
  end;
end;

procedure TDataFluxWindow.UpdateMemoryWindowToFrontWindow(dwCurrentJobPercent, dwGlobalSpeed: Cardinal; bJobHealth: Boolean);
var
  L_HeightPos       : Cardinal;
  L_OldBrush        : THandle;
begin
  {清空背景窗体}
  //FillRect(FMemoryWindowDC, MakeRect(0, 0, C_WindowWidth - C_BorderOffset * 2, C_WindowHeight - C_BorderOffset * 2), FMemoryWindowBdBrush);
  {2++}PatBlt(FMemoryWindowDC, 0, 0, C_WindowWidth - C_BorderOffset * 2, C_WindowHeight - C_BorderOffset * 2, BLACKNESS); {2++}

  {从前景窗体中拷贝数据(不包含文字区域)}
  BitBlt(FMemoryWindowDC, 0, C_FontEareHeight,
    C_WindowWidth - C_BorderOffset * 2 - 1,
    C_WindowHeight - C_BorderOffset * 2 - C_FontEareHeight,
    FBackgroundWindowDC, C_BorderOffset + 1, C_BorderOffset + C_FontEareHeight, SRCCOPY);

  {画出当前数据线}
  {$IFDEF BUILD_TEST}
  L_HeightPos := (C_WindowHeight - C_BorderOffset * 2) - Round((GetCurrentData() / MaxRange) * (C_WindowHeight - C_BorderOffset * 2 - C_FontEareHeight));
  {$ELSE}
  L_HeightPos := (C_WindowHeight - C_BorderOffset * 2) - Round((dwGlobalSpeed / MaxRange) * (C_WindowHeight - C_BorderOffset * 2 - C_FontEareHeight));
  {$ENDIF}
  MoveToEx(FMemoryWindowDC, C_WindowWidth - C_BorderOffset * 2 - 1, L_HeightPos, nil); //移动Pen指针到相应位置
  LineTo(FMemoryWindowDC, C_WindowWidth - C_BorderOffset * 2 - 1, C_WindowHeight - C_BorderOffset * 2);

  {绘制提示点}
  if FCanDrawHintPoint then
    case bJobHealth of
      False:                            //工作不健康时不输出百分比
        begin
          L_OldBrush := SelectObject(FMemoryWindowDC, FMemoryWindowHintPointBrush_Red);
          Windows.Ellipse(FMemoryWindowDC, C_HintOffset, C_HintOffset, C_HintPointHeight_With, C_HintPointHeight_With);
          SelectObject(FMemoryWindowDC, L_OldBrush);
        end;
      True:
        begin
          {写百分比文字信息}
          if dwCurrentJobPercent = 0 then
          begin
            SetTextColor(FMemoryWindowDC, RGB(0, 255, 255));
            DrawText(FMemoryWindowDC, PKOLChar(Format('%d%%', [dwCurrentJobPercent])), -1, FTextEareRect, DT_RIGHT or DT_NOCLIP or DT_SINGLELINE or DT_VCENTER);
            SetTextColor(FMemoryWindowDC, RGB(0, 255, 0));
          end
          else
            DrawText(FMemoryWindowDC, PKOLChar(Format('%d%%', [dwCurrentJobPercent])), -1, FTextEareRect, DT_RIGHT or DT_NOCLIP or DT_SINGLELINE or DT_VCENTER);

          Windows.Ellipse(FMemoryWindowDC, C_HintOffset, C_HintOffset, C_HintPointHeight_With, C_HintPointHeight_With);
        end;
    end
  else
  begin
    if bJobHealth then
      {写百分比文字信息}
      DrawText(FMemoryWindowDC, PKOLChar(Format('%d%%', [dwCurrentJobPercent])), -1, FTextEareRect, DT_RIGHT or DT_NOCLIP or DT_SINGLELINE or DT_VCENTER);
  end;

  {将伪窗体的内容拷贝到前景窗体中}
  BitBlt(FBackgroundWindowDC,
    C_BorderOffset, C_BorderOffset,
    C_WindowWidth - 2 * C_BorderOffset, C_WindowHeight - 2 * C_BorderOffset,
    FMemoryWindowDC, 0, 0, SRCCOPY);
end;

end.

