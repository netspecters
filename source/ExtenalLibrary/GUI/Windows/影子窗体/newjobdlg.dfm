object Form1: TForm1
  Left = 263
  Top = 231
  Width = 388
  Height = 432
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 15
    Width = 48
    Height = 13
    Caption = #36164#28304#22320#22336
  end
  object Label2: TLabel
    Left = 10
    Top = 41
    Width = 48
    Height = 13
    Caption = #23384#20648#20301#32622
  end
  object Label3: TLabel
    Left = 10
    Top = 67
    Width = 32
    Height = 13
    Caption = 'Label3'
  end
  object Label4: TLabel
    Left = 15
    Top = 101
    Width = 32
    Height = 13
    Caption = 'Label4'
  end
  object Label5: TLabel
    Left = 313
    Top = 69
    Width = 55
    Height = 13
    AutoSize = False
    Caption = '4096MB'
  end
  object Edit1: TEdit
    Left = 70
    Top = 11
    Width = 300
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 73
    Top = 136
    Width = 221
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
  end
  object Button1: TButton
    Left = 285
    Top = 62
    Width = 25
    Height = 21
    Caption = '...'
    TabOrder = 2
  end
  object PageControl1: TPageControl
    Left = -1
    Top = 162
    Width = 381
    Height = 232
    ActivePage = TabSheet1
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Label7: TLabel
        Left = 16
        Top = 106
        Width = 32
        Height = 13
        Caption = 'Label7'
      end
      object Label8: TLabel
        Left = 17
        Top = 134
        Width = 32
        Height = 13
        Caption = 'Label8'
      end
      object GroupBox1: TGroupBox
        Left = 3
        Top = 2
        Width = 366
        Height = 75
        Caption = 'GroupBox1'
        TabOrder = 0
      end
      object CheckBox2: TCheckBox
        Left = 11
        Top = 83
        Width = 97
        Height = 17
        Caption = 'CheckBox2'
        TabOrder = 1
      end
      object Edit5: TEdit
        Left = 56
        Top = 101
        Width = 86
        Height = 21
        TabOrder = 2
        Text = 'Edit5'
      end
      object Edit6: TEdit
        Left = 56
        Top = 131
        Width = 86
        Height = 21
        TabOrder = 3
        Text = 'Edit6'
      end
      object Memo1: TMemo
        Left = 202
        Top = 101
        Width = 164
        Height = 95
        Lines.Strings = (
          'Memo1')
        TabOrder = 4
      end
      object GroupBox2: TGroupBox
        Left = 5
        Top = 153
        Width = 175
        Height = 41
        Caption = 'GroupBox2'
        TabOrder = 5
        object RadioButton1: TRadioButton
          Left = 8
          Top = 15
          Width = 113
          Height = 17
          Caption = 'RadioButton1'
          TabOrder = 0
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Label6: TLabel
        Left = 159
        Top = 6
        Width = 127
        Height = 13
        AutoSize = False
        Caption = #21333#22320#22336#26368#39640#32447#31243#25968#65306
      end
      object CheckBox1: TCheckBox
        Left = 8
        Top = 4
        Width = 148
        Height = 17
        Caption = #27599#20010#22336#20351#29992#21333#32447#31243
        TabOrder = 0
      end
      object Edit4: TEdit
        Left = 287
        Top = 2
        Width = 66
        Height = 21
        TabOrder = 1
        Text = 'Edit4'
      end
      object UpDown1: TUpDown
        Left = 336
        Top = 3
        Width = 17
        Height = 20
        TabOrder = 2
      end
    end
  end
  object Edit3: TEdit
    Left = 70
    Top = 92
    Width = 211
    Height = 21
    TabOrder = 4
    Text = 'Edit3'
  end
  object ComboBox1: TComboBox
    Left = 70
    Top = 38
    Width = 301
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    Text = 'ComboBox1'
  end
  object ComboBox2: TComboBox
    Left = 70
    Top = 65
    Width = 212
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = 'ComboBox1'
  end
  object XPManifest1: TXPManifest
    Left = 236
    Top = 180
  end
end
