library WinGui;

uses
  Windows,
  Messages,
  KOL,
  pascal_PlusInterface,
  DragTarget_Implement in 'DragTarget_Implement.pas',
  ExPanel in 'ExPanel.pas',
  InformationView in 'InformationView.pas',
  MainForm in 'MainForm.pas',
  NewWorkDlg in 'NewWorkDlg.pas',
  Window_APIs in 'Window_APIs.pas',
  ProgressBar in 'ProgressBar.pas',
  DataFluxWindow_Implement in 'DataFluxWindow_Implement.pas',
  ComboboxTreeView in 'ComboboxTreeView.pas',
  JobManager in 'JobManager.pas',
  NSErrorCode in '..\..\NSErrorCode.pas';

type
  TWinGui = class(INSPlus)
  public
    function onLoad(plus_tree_ptr: IPlusTree; parent_plus_ptr: INSPlus; self_dir_handle: HDIR; default_param: Integer): Integer; override; stdcall;
    procedure onUnload(); override; stdcall;

  protected
    procedure onStatusMustBeStoreEvent(style: TStoreEventStyle);

  private
    m_main_window: TMainWindow;
    m_plus_tree_ptr: IPlusTree;
    m_protocol_manager_ptr: IProtocolManager;
    m_self_dir_handle: HDIR;
  end;

function CreateInterface(interface_id: Integer; out plus_instance: INSPlus): Integer; stdcall; export;
begin
  plus_instance := INSPlus(TWinGui.Create());
  Result := 1;
end;

function DestroyInterface(plus_instance: INSPlus): Integer; stdcall; export;
begin
  TWinGui(plus_instance).Free;
  Result := 1;
end;

{ TWinGui }

function TWinGui.onLoad(plus_tree_ptr: IPlusTree; parent_plus_ptr: INSPlus; self_dir_handle: HDIR; default_param: Integer): Integer;
var
  main_form_infor   : ST_MainForm_Infor;
  record_handle     : HRECORD;
begin
  m_plus_tree_ptr := plus_tree_ptr;
  m_self_dir_handle := self_dir_handle;
  m_protocol_manager_ptr := IProtocolManager(parent_plus_ptr);

  //主窗口位置信息
  FillChar(main_form_infor, SizeOf(main_form_infor), 0);
  record_handle := plus_tree_ptr.openRecord(self_dir_handle, PChar(0), True);
  plus_tree_ptr.readRecord(record_handle, PByte(@main_form_infor), SizeOf(main_form_infor));
  plus_tree_ptr.closeRecord(record_handle);

  //检查信息的有效性
  if main_form_infor.flux_window.top = 0 then
    with main_form_infor.flux_window do
    begin
      max_range := 256 * 1024;
    end;

  //建立主窗体
  m_main_window := TMainWindow.Create(main_form_infor, m_protocol_manager_ptr);
  m_main_window.onStatusMustBeStore := onStatusMustBeStoreEvent;

  //注册主窗口
  m_protocol_manager_ptr.registerListener(m_main_window.ListenerInterface);

  Result := 1;
end;

procedure TWinGui.onUnload();
begin
  //
  m_main_window.Free;
end;

procedure TWinGui.onStatusMustBeStoreEvent(style: TStoreEventStyle);
var
  main_form_infor   : ST_MainForm_Infor;
  record_handle     : HRECORD;
begin
  case style of
    sesPosition:
      begin
        //写入状态:主窗口位置信息
        m_main_window.storeStatus(main_form_infor);
        record_handle := m_plus_tree_ptr.openRecord(m_self_dir_handle, PChar(0), True);
        m_plus_tree_ptr.writeRecord(record_handle, PByte(@main_form_infor), SizeOf(main_form_infor));
        m_plus_tree_ptr.closeRecord(record_handle);
      end;

    sesConfigure:
      begin
        record_handle := m_plus_tree_ptr.openRecord(m_self_dir_handle, PChar(1), True);
        m_plus_tree_ptr.closeRecord(record_handle);
      end;
  end;
end;

exports
  CreateInterface, DestroyInterface;

begin
end.

