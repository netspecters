unit InformationView;

interface

uses
  Windows, KOL, KOLadd;

type
  PInformationView = ^TInformationView; //本对象不会挂接父窗口的消息过程,需要通知改变
  TInformationView = object(TObj)       //本对象不会开辟新窗口,而是使用父窗口的DC作图
  private
    m_base_listbox: PControl;
    m_name_eare_len: Integer;

    m_values: PWStrList;
  protected
    function onDrawItem(Sender: PObj; DC: HDC; const Rect: TRect; ItemIdx: Integer; DrawAction: TDrawAction; ItemState: TDrawState): Boolean;
  public

    procedure BeginUpdata();            //开始更新相关数据
    procedure EndUpdata();              //相关数据更新完毕

    procedure clear();
    procedure addItem(name: KOLString; value: KOLString);

    destructor Destroy; virtual;
  end;

function NewInformationView(parent: PControl; name_eare_len_percent: Single): PInformationView;

implementation

{-------------------------------------------------------------------------------
  过程名:    NewInformationView
  所属工程:  DataTransfer
  功能:      构建一个(详细)信息显示器
  作者:      王冠
  创建日期:  2006.08.16
  参数:      cLineFinishChar: Char     规定行结束字符
  返回值:    PInformationView
  注意:
  版本历史:
-------------------------------------------------------------------------------}

function NewInformationView(parent: PControl; name_eare_len_percent: Single): PInformationView;
begin
  New(Result, Create);
  if Result = nil then Exit;

  with Result^ do
  begin
    m_base_listbox := NewListbox(parent, [loOwnerDrawVariable]);
    m_base_listbox.SetAlign(caClient).OnDrawItem := onDrawItem;
    m_base_listbox.Font.Assign(parent.Font);

    m_values := NewWStrList;
    m_name_eare_len := trunc(name_eare_len_percent * m_base_listbox.Width);
  end;
end;

{ TInformationView }

procedure TInformationView.addItem(name, value: KOLString);
begin
  m_base_listbox.Add(name);
end;

procedure TInformationView.BeginUpdata;
begin

end;

procedure TInformationView.clear;
begin
  m_base_listbox.Clear;
  m_values.Clear;
end;

destructor TInformationView.Destroy;
begin
  m_base_listbox.Free;
  m_values.Free;

  inherited;
end;

procedure TInformationView.EndUpdata;
begin

end;

function TInformationView.onDrawItem(Sender: PObj; DC: HDC;
  const Rect: TRect; ItemIdx: Integer; DrawAction: TDrawAction;
  ItemState: TDrawState): Boolean;
var
  local_rect        : TRect;
begin
  local_rect := Rect;

  //写name
  local_rect.Right := m_name_eare_len;
  DrawText(DC, PKOLChar(m_base_listbox.Items[ItemIdx]), Length(m_base_listbox.Items[ItemIdx]), local_rect
    , DT_LEFT or DT_NOCLIP or DT_VCENTER or DT_SINGLELINE);

  //写value
  with m_values^ do
  begin
    with local_rect do
    begin
      Left := m_name_eare_len;
      Right := Rect.Right;
    end;

    DrawText(DC, ItemPtrs[ItemIdx], Length(Items[ItemIdx]), local_rect
      , DT_LEFT or DT_NOCLIP or DT_VCENTER or DT_SINGLELINE);
  end;
end;

end.

