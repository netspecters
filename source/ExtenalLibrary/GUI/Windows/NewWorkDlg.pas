unit NewWorkDlg;

interface

uses
  Windows, KOL, JobManager;

type
  ST_JobDefaultConfigure = packed record

  end;

  ST_NewJobInfor = record
    main_url, file_path, file_name: KOLString;
    para_num: Integer;
    user_name, passwd, cookie: string;
  end;

  TOnNewJob = function(const new_job_infor: ST_NewJobInfor): Boolean of object;
  PCreateWorkDlg = ^TCreateWorkDlg;
  TCreateWorkDlg = object(TObj)
  private
    m_base_window: PControl;
    m_ok: Boolean;
    FPages: PControl;

    m_sys_buttons: array[0..2] of PControl;
    m_config_buttons: array[0..1] of PControl;

    m_infor_labels: array[0..9] of PControl;
    m_input_edits: array[0..4] of PControl;
    m_check_box: array[0..2] of PControl;
    m_radio_box: array[0..2] of PControl;
    m_group: array[0..2] of PControl;

    m_store_pos_combobox, m_store_dir_combobox: PControl;
    m_common: PControl;

    m_job_create_param: ST_JobCreateParam;
  protected
    procedure OnButtonClick(Sender: PObj);
    procedure onConfigButtonClick(Sender: PObj);
    procedure onURLChange(Sender: PObj);
    procedure onCheckBoxClick(Sender: PObj);
  public
    destructor Destroy; virtual;

    property BaseWindow: PControl read m_base_window;
    function ShowModle(var job_create_param: ST_JobCreateParam): Boolean;
    function AddPage(pwstrCaption: PWideChar): Cardinal;

  end;

function NewCreateWorkDlg(parent: PControl; url: KOLString = ''): PCreateWorkDlg;

implementation

const
  _C_DefDlgWidth    = 380;
  _C_DefDlgHeight   = 150;
  _C_MaxDlgHeight   = 405;
  _C_ExtendPanel_Height = _C_MaxDlgHeight - 150;
  _C_ButtonWidth    = 75;
  _C_ButtonHeight   = 25;
  _C_Label_Height   = 13 + 1;

function NewCreateWorkDlg(parent: PControl; url: KOLString = ''): PCreateWorkDlg;
var
  i                 : Integer;
begin
  New(Result, Create);

  with Result^ do
  begin
    m_base_window := NewForm(parent, '新建任务').SetClientSize(_C_DefDlgWidth, _C_DefDlgHeight - 2).CenterOnParent;
    m_base_window.Top := m_base_window.Top - 100;
    m_base_window.ExStyle := m_base_window.ExStyle or WS_EX_DLGMODALFRAME or WS_EX_WINDOWEDGE;
    m_base_window.Style := m_base_window.Style and not (WS_SIZEBOX or WS_MINIMIZEBOX or WS_MAXIMIZEBOX);

    //系统按钮
    m_sys_buttons[1] := NewButton(m_base_window, '确定').SetPosition(218, 120).SetSize(_C_ButtonWidth, _C_ButtonHeight);
    m_sys_buttons[0] := NewButton(m_base_window, '更多选项').SetPosition(10, 120).SetSize(_C_ButtonWidth + _C_ButtonHeight, _C_ButtonHeight);
    m_sys_buttons[2] := NewButton(m_base_window, '取消').SetPosition(304, 120).SetSize(_C_ButtonWidth, _C_ButtonHeight);
    m_sys_buttons[1].DefaultBtn := true;
    for i := 0 to High(m_sys_buttons) do
      with m_sys_buttons[i]^ do
      begin
        OnClick := OnButtonClick;
        Tag := i;
      end;

    //扩展页面
    FPages := NewTabControl(m_base_window, ['常规', '镜像', '文件', '环境'], [tcoHotTrack], nil, 0).SetPosition(0, 150).SetSize(m_base_window.Width, _C_ExtendPanel_Height);

    //组
    m_group[0] := NewGroupbox(FPages.Pages[0], '线程控制').SetPosition(1, 2).SetSize(FPages.Pages[0].Width - 10, 75);
    m_group[1] := NewGroupbox(FPages.Pages[0], '开始').SetPosition(1, 153).SetSize(175, 67);

    //标签
    m_infor_labels[0] := NewLabel(m_base_window, '资源地址').SetPosition(9, 15).SetSize(58, _C_Label_Height);
    m_infor_labels[1] := NewLabel(m_base_window, '存储位置').SetPosition(9, 41).SetSize(58, _C_Label_Height);
    m_infor_labels[2] := NewLabel(m_base_window, '存储路径').SetPosition(9, 67).SetSize(58, _C_Label_Height);
    m_infor_labels[3] := NewLabel(m_base_window, '另存名称').SetPosition(9, 93).SetSize(58, _C_Label_Height);
    m_infor_labels[4] := NewLabel(m_base_window, '--').SetPosition(317, 67).SetSize(58, 13); //磁盘空间
    m_infor_labels[5] := NewLabel(m_group[0], '每个地址线程数：').SetPosition(160, 24).SetSize(127, _C_Label_Height);
    m_infor_labels[6] := NewLabel(m_group[0], '每个任务最大线程数：').SetPosition(6, 50).SetSize(127, _C_Label_Height);
    m_infor_labels[7] := NewLabel(FPages.Pages[0], '用户名：').SetPosition(18, 107).SetSize(50, _C_Label_Height);
    m_infor_labels[8] := NewLabel(FPages.Pages[0], '密码：').SetPosition(18, 132).SetSize(50, _C_Label_Height);
    m_infor_labels[9] := NewLabel(FPages.Pages[0], '注释：').SetPosition(182, 85).SetSize(50, _C_Label_Height);
    for i := 0 to High(m_infor_labels) do
      with m_infor_labels[i]^ do
      begin
        TextAlign := taLeft;
      end;
    m_infor_labels[4].TextAlign := taCenter;

    //输入框
    m_input_edits[0] := NewEditbox(m_base_window, []).SetPosition(70, 11).SetSize(300, 21); //资源地址
    m_input_edits[1] := NewEditbox(m_base_window, []).SetPosition(70, 92).SetSize(212, 21); //另存名称
    m_input_edits[2] := NewEditbox(m_group[0], []).SetPosition(287, 20).SetSize(66, 21); //每个地址线程数
    m_input_edits[3] := NewEditbox(FPages.Pages[0], []).SetPosition(70, 102).SetSize(86, 21); //用户名
    m_input_edits[4] := NewEditbox(FPages.Pages[0], []).SetPosition(70, 129).SetSize(86, 21); //密码
    m_input_edits[0].Text := url;
    m_input_edits[0].OnChange := onURLChange;
    for i := 0 to High(m_input_edits) do
      with m_input_edits[i]^ do
      begin
        Tag := i;
      end;

    //组合框
    m_store_pos_combobox := NewCombobox(m_base_window, []).SetPosition(70, 37).SetSize(301, 21);
    m_store_dir_combobox := NewCombobox(m_base_window, []).SetPosition(70, 64).SetSize(212, 21);
    m_store_dir_combobox.CreateWindow;
    m_store_dir_combobox.Caption := KOLString('c:\');

    //选择框
    m_check_box[0] := NewCheckbox(m_group[0], '每个地址使用单线程').SetPosition(6, 20).SetSize(148, 17);
    m_check_box[1] := NewCheckbox(m_group[0], '智能线程控制').SetPosition(250, 47).SetSize(100, 17);
    m_check_box[2] := NewCheckbox(FPages.Pages[0], '登录到服务器').SetPosition(11, 83).SetSize(97, 17);
    for i := 0 to High(m_check_box) do
      with m_check_box[i]^ do
      begin
        Tag := i;
        OnClick := onCheckBoxClick;
      end;

    //单选框
    m_radio_box[0] := NewRadiobox(m_group[1], '立即').SetPosition(6, 15).SetSize(50, 17);
    m_radio_box[1] := NewRadiobox(m_group[1], '手动').SetPosition(80, 15).SetSize(50, 17);
    m_radio_box[2] := NewRadiobox(m_group[1], '计划').SetPosition(6, 45).SetSize(50, 17);

    //注释
    m_common := NewEditbox(FPages.Pages[0], [eoMultiline, eoNoHScroll]).SetPosition(182, 101).SetSize(184, 118);

    //按钮
    m_config_buttons[0] := NewButton(m_base_window, '...').SetPosition(285, 64).SetSize(25, 21);
    m_config_buttons[1] := NewButton(m_group[1], '计划任务...').SetPosition(60, 40).SetSize(_C_ButtonWidth + _C_ButtonHeight, _C_ButtonHeight);
    for i := 0 to High(m_config_buttons) do
    begin
      with m_config_buttons[i]^ do
      begin
        OnClick := onConfigButtonClick;
        Tag := i;
      end;
    end;

  end;
end;

{ TCreateWorkDlg }

function TCreateWorkDlg.AddPage(pwstrCaption: PWideChar): Cardinal;
begin
  Result := FPages.TC_Insert(-1, pwstrCaption, 0).GetWindowHandle
end;

destructor TCreateWorkDlg.Destroy;
begin
  FPages.Free;
  m_sys_buttons[0].Free;
  m_sys_buttons[1].Free;
  m_base_window.Free;

  inherited;
end;

procedure TCreateWorkDlg.OnButtonClick(Sender: PObj);
begin
  with PControl(Sender)^ do
    case Tag of
      0:                                //更多信息 按钮
        if (m_base_window.ClientHeight <> _C_MaxDlgHeight) then
        begin
          m_base_window.ClientHeight := _C_MaxDlgHeight;
        end
        else
        begin
          m_base_window.ClientHeight := _C_DefDlgHeight;
        end;
      1:                                //确定 按钮
        begin
          m_job_create_param.url := m_input_edits[0].Text;
          m_job_create_param.file_path := m_store_dir_combobox.Text;
          m_job_create_param.file_name := m_input_edits[1].Text;
          m_job_create_param.thread_num := 1;
          m_job_create_param.start_now := m_radio_box[0].Checked;
          m_ok := true;
          m_base_window.Close;
        end;
      2:
        m_base_window.Close;
    end;
end;

procedure TCreateWorkDlg.onCheckBoxClick(Sender: PObj);
begin

end;

procedure TCreateWorkDlg.onConfigButtonClick(Sender: PObj);
var
  store_location_dir: POpenDirDialog;
begin
  with PControl(Sender)^ do
    case Tag of
      0:
        begin
          store_location_dir := NewOpenDirDialog('选择存储路径', [odNewDialogStyle]);
          store_location_dir.CenterOnScreen := true;
          if store_location_dir.Execute then
          begin
            m_store_dir_combobox.Text := store_location_dir.Path;
          end;
          store_location_dir.Free;
        end;
    end;
end;

procedure TCreateWorkDlg.onURLChange(Sender: PObj);
var
  i                 : Integer;
  start_pos, end_pos: Integer;
  current_char      : KOLChar;

  function __isRightChar(ch: WideChar): Boolean;
  var
    ch_index        : Cardinal;
  begin
    ch_index := ord(ch);
    Result := ((ch_index >= $30) and (ch_index <= $39)) or ((ch_index >= $41) and (ch_index <= $7A)) or (ch_index = $2E);
  end;
begin
  if Length(m_input_edits[0].Text) = 0 then Exit;

  //解析文件名称
  start_pos := 0;
  end_pos := Length(m_input_edits[0].Text);

  //先找到最后一个'/'
  for i := end_pos downto 0 do
  begin
    current_char := m_input_edits[0].Text[i];
    if current_char = '/' then
      if (i > 0) and __isRightChar(m_input_edits[0].Text[i - 1]) then
      begin
        start_pos := i + 1;
        break;
      end;
  end;

  //拷贝文件名
  if start_pos > 0 then
  begin
    for i := start_pos to end_pos do
      if not __isRightChar(m_input_edits[0].Text[i]) then
      begin
        end_pos := i - 1;
        break;
      end;

    m_input_edits[1].Text := Copy(m_input_edits[0].Text, start_pos, end_pos - start_pos + 1);
  end;
end;

function TCreateWorkDlg.ShowModle(var job_create_param: ST_JobCreateParam): Boolean;
begin
  m_base_window.ShowModal;
  job_create_param := m_job_create_param;
  Result := m_ok;
end;

end.

