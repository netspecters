unit MainForm;

interface

uses
  Windows, Messages, KOL, pascal_PlusInterface, JobManager, ExPanel, InformationView, DataFluxWindow_Implement, NewWorkDlg;

type
  TStoreEventStyle = (sesPosition, sesConfigure);
  TStatusMustBeStoreEvent = procedure(style: TStoreEventStyle) of object;
  //持久化数据
  ST_MainForm_Infor = packed record
    left, top: Word;
    right, bottom: Word;
    visible: WordBool;
    flux_window: packed record
      left, top: Word;
      max_range: Cardinal;
    end;
  end;

  ST_ConfigureInfor = packed record

  end;

  TMainWindow = class
  private
    //状态信息
    m_total_data_size, m_finished_data_size: Int64;
    m_global_down_speed: Cardinal;

    //控件
    m_main_window: PControl;
    m_main_menu: PMenu;
    m_sys_tray: PTrayIcon;
    m_main_tab_ctrl: PControl;
    m_work_eare_toobar, m_finish_eare_toobar: PControl;
    m_flux_window: PDataFluxWindow;
    m_float_menu: PMenu;
    m_taskview_menu: PMenu;
    m_catalog_tree: PControl;
    m_spliter: PControl;
    m_job_manager: CJobManager;
    m_finish_eare_splite_panel: array[0..1] of PControl;
    m_fake_treeview: PControl;

    procedure OnMainMenuItem(Sender: PMenu; Item: Integer);
    procedure onTrayMouse(Sender: PObj; Message: Word);
    procedure onFluxWindowGetURL(const stURL: string);
    procedure onFluxWindowMouseEvent(dwMouseEvent: Cardinal; wParam, lParam:
      Integer);
    function OnFluxWindowDataLineDrawing(const dwTimeCount: Cardinal; var
      dwCurrentJobPercent, dwGlobalSpeed: Cardinal; var bJobHealth: Boolean):
      Boolean;
    procedure onFloatMenuItem(Sender: PMenu; Item: Integer);
    procedure onWorkEareToobarButtonClick(Sender: PObj);
    procedure onFinishEareToobarButtonClick(Sender: PObj);
    procedure onTaskViewMenuUp(x, y: Word; item_id: Integer);
    procedure onTaskViewSelChange(Sender: PObj);
    procedure onMainFormClose(Sender: PObj; var Accept: Boolean);
    procedure onMainFormQueryEndSession(Sender: PObj; var Accept: Boolean);
    function getListenerInterface: IProtocolEventListener;

  protected
    procedure setWorkEareToolbarState(job_index: Integer);
    //根据指定的job index设置工具栏按钮状态

  public
    onStatusMustBeStore: TStatusMustBeStoreEvent;
    procedure storeStatus(var main_form_infor: ST_MainForm_Infor);
    constructor Create(main_form_infor: ST_MainForm_Infor; protocol_manager_ptr:
      IProtocolManager);
    property ListenerInterface: IProtocolEventListener read
      getListenerInterface;
  end;

implementation

{ TMainWindow }

constructor TMainWindow.Create(main_form_infor: ST_MainForm_Infor; protocol_manager_ptr: IProtocolManager);
var
  L_FontInfo: TLogFont;
begin
  //构建主窗体
  m_main_window := NewForm(nil, 'Netspecters Data Transfer').CenterOnParent;
  with m_main_window^ do
  begin
    if main_form_infor.right = 0 then
      WindowState := wsMaximized
    else
    begin
      SetPosition(main_form_infor.left, main_form_infor.top);
      SetSize(main_form_infor.right - main_form_infor.left,
        main_form_infor.bottom - main_form_infor.top);
    end;
    SystemParametersInfo(SPI_GETICONTITLELOGFONT, SizeOf(L_FontInfo),
      @L_FontInfo, 0);
    Font.FontHeight := L_FontInfo.lfHeight;
    OnClose := onMainFormClose;
    OnQueryEndSession := onMainFormQueryEndSession;
    MinWidth := 350;
  end;
  Applet := m_main_window;

  //建立系统通知图标
  m_sys_tray := NewTrayIcon(m_main_window, 0);
  m_sys_tray.AutoRecreate := True;
  m_sys_tray.OnMouse := onTrayMouse;
  m_sys_tray.Active := True;

  //构建主窗体菜单
  m_main_menu := NewMenu(m_main_window, 0
    , ['文件(&F)', '(', '新建下载任务...', '新建批量任务...', '新建Torrent文件', '-', '退出', ')',
    '查看(&F)',
      '选项(&O)', '(', '配置', ')',
      '帮助(&H)', '(', '关于...', ')',
      ''], OnMainMenuItem);

  //构建主面板
  m_main_tab_ctrl := NewTabControl(m_main_window, ['传输中', '已完成'],
    [tcoFixedWidth], nil, 0);
  with m_main_tab_ctrl^ do
  begin
    SetAlign(caClient);
    Font.Assign(m_main_window.Font);
    SetUnicode(True);
    ExStyle := ExStyle or WS_EX_COMPOSITED;
  end;

  //构建工作区工具栏
  m_work_eare_toobar := NewToolbar(m_main_tab_ctrl.Pages[0], caTop,
    [tboTextBottom], 0,
    ['新建', '开始', '暂停', '删除', '属性', '上移', '下移', '选项', '详细'],
    []).SetAlign(caTop);
  with m_work_eare_toobar^ do
  begin
    setWorkEareToolbarState(-1);
    OnTBClick := onWorkEareToobarButtonClick;
    //Font.Assign(m_main_window.Font);
  end;

  //构建任务列表(job管理器)
  m_job_manager := CJobManager.Create(m_main_window, m_main_tab_ctrl.Pages[0],
    protocol_manager_ptr);
  with m_job_manager do
  begin
    onMenuUp := onTaskViewMenuUp;
    onSelChange := onTaskViewSelChange;
  end;

  //构建任务列表菜单
  m_taskview_menu := NewMenu(m_main_tab_ctrl.Pages[0], 255, ['开始'], nil);

  //构建完成区工具栏
  m_finish_eare_toobar := NewToolbar(m_main_tab_ctrl.Pages[1], caTop,
    [tboTextBottom], 0,
    ['删除', '属性', '上移', '下移', '目录'],
    []).SetAlign(caTop);
  with m_finish_eare_toobar^ do
  begin
    TBButtonEnabled[1] := False;
    TBButtonEnabled[2] := False;
    TBButtonEnabled[3] := False;
    OnTBClick := onFinishEareToobarButtonClick;
    //Font.Assign(m_main_window.Font);
  end;

  //构建完成区控件面板
  m_finish_eare_splite_panel[0] := NewPanel(m_main_tab_ctrl.Pages[1],
    esNone).SetAlign(caClient);

  //构建目录树
  m_catalog_tree := NewTreeView(m_finish_eare_splite_panel[0], [tvoLinesRoot],
    nil, nil).SetAlign(caLeft);
  with m_catalog_tree^ do
  begin
    style := style and (not (WS_BORDER or WS_MAXIMIZEBOX));
    TVInsert(TVI_ROOT, TVI_LAST, '完成任务');
    SetUnicode(True);
    Show();
  end;

  //构建完成区分割线
  m_spliter := NewSplitter(m_finish_eare_splite_panel[0], m_catalog_tree.width,
    m_catalog_tree.width).SetAlign(caLeft);

  m_fake_treeview := NewTreeView(m_finish_eare_splite_panel[0], [tvoLinesRoot],
    nil, nil).SetAlign(caClient);

  //流量窗口
  if (main_form_infor.flux_window.top = 0) and (main_form_infor.flux_window.left
    = 0) then
  begin
    main_form_infor.flux_window.top := 60;
    main_form_infor.flux_window.left := GetSystemMetrics(SM_CXSCREEN) - 80;
  end;
  m_flux_window := NewDataFluxWindow(main_form_infor.flux_window.top,
    main_form_infor.flux_window.left);
  with m_flux_window^ do
  begin
    onGetURL := onFluxWindowGetURL;
    OnMouseEvent := onFluxWindowMouseEvent;
    OnDataLineDrawing := OnFluxWindowDataLineDrawing;
    MaxRange := main_form_infor.flux_window.max_range;
    Show;
  end;

  //浮动菜单
  m_float_menu := NewMenu(m_main_window, 255, ['隐藏/显示主窗口', '-', '新建下载任务',
    '新建批量任务', '-', '退出'], onFloatMenuItem);

  m_main_window.Show();
end;

procedure TMainWindow.onFinishEareToobarButtonClick(Sender: PObj);
begin

end;

procedure TMainWindow.onFloatMenuItem(Sender: PMenu; Item: Integer);
begin
  case Item of
    0: m_main_window.visible := not m_main_window.visible;
    2: onFluxWindowGetURL('http://127.0.0.1/sv_test.mp4');
    3: ;
    5: m_main_window.Close;
  end;
end;

function TMainWindow.OnFluxWindowDataLineDrawing(const dwTimeCount: Cardinal; var dwCurrentJobPercent, dwGlobalSpeed: Cardinal; var
  bJobHealth: Boolean): Boolean;
begin
  dwCurrentJobPercent := Trunc(m_finished_data_size / m_total_data_size * 100);
  dwGlobalSpeed := m_global_down_speed;
end;

procedure TMainWindow.onFluxWindowGetURL(const stURL: string);
begin
  m_job_manager.addJob(stURL);
  setWorkEareToolbarState(m_job_manager.CurIndex);
end;

procedure TMainWindow.onFluxWindowMouseEvent(dwMouseEvent: Cardinal; wParam,
  lParam: Integer);
begin
  case dwMouseEvent of
    WM_LBUTTONDBLCLK:
      begin
        onFloatMenuItem(m_float_menu, 0);
      end;
    WM_RBUTTONUP:
      begin
        m_float_menu.Popup(Word(lParam), HiWord(lParam));
      end;
  end;
end;

procedure TMainWindow.OnMainMenuItem(Sender: PMenu; Item: Integer);
begin
  case Item of
    1: onFloatMenuItem(m_float_menu, 2);
    5: m_main_window.Close;
  end;
end;

procedure TMainWindow.onTaskViewMenuUp(x, y: Word; item_id: Integer);
begin
  m_taskview_menu.Popup(x, y);
  m_taskview_menu.Tag := item_id;
end;

procedure TMainWindow.onTaskViewSelChange(Sender: PObj);
begin
  //设置开始按钮状态
  setWorkEareToolbarState(m_job_manager.CurIndex);
end;

procedure TMainWindow.onTrayMouse(Sender: PObj; Message: Word);
begin
  case Message of
    WM_LBUTTONDOWN:
      begin

      end;
    WM_RBUTTONUP:
      begin

      end;
  end;
end;

procedure TMainWindow.onWorkEareToobarButtonClick(Sender: PObj);
  procedure move_me(offset: Integer);
  begin
    m_job_manager.moveJob(offset);
    setWorkEareToolbarState(m_job_manager.CurIndex);
  end;
begin
  case m_work_eare_toobar.CurIndex of
    0: onFloatMenuItem(m_float_menu, 2);
    1:
      begin
        m_job_manager.resumeJob(m_job_manager.CurIndex);
      end;
    3:
      begin
        if m_job_manager.JobCount > 0 then
        begin
          m_job_manager.delJob(m_job_manager.CurIndex);

          //刷新toobar状态
          setWorkEareToolbarState(m_job_manager.CurIndex);

          if m_job_manager.JobCount = 0 then
            m_flux_window.FinishDrawing;
        end;
      end;
    5: move_me(-1);
    6: move_me(1);
  end;
end;

procedure TMainWindow.setWorkEareToolbarState(job_index: Integer);
var
  job_status: TJobStatus;
  job_status_data_ptr: PJobStatus;
  is_vaild: Boolean;
begin
  is_vaild := (job_index >= 0) and (m_job_manager.JobCount > 0);
  job_status := jsSuspend;
  if is_vaild then
  begin
    job_status_data_ptr := m_job_manager.Jobs[job_index];
    job_status := job_status_data_ptr.status;
  end;

  with m_work_eare_toobar^ do
  begin
    TBButtonEnabled[1] := is_vaild and ((job_status = jsSuspend) or (job_status
      = jsError)); //开始
    TBButtonEnabled[2] := is_vaild and (job_status >= jsConnect) and (job_status
      < jsSuspend); //暂停
    TBButtonEnabled[3] := is_vaild and (job_index >= 0); //删除
    TBButtonEnabled[4] := is_vaild; //属性
    TBButtonEnabled[5] := is_vaild and (job_index > 0); //上移
    TBButtonEnabled[6] := is_vaild and (job_index < m_job_manager.JobCount - 1);
    //下移
  end;
end;

procedure TMainWindow.storeStatus(var main_form_infor: ST_MainForm_Infor);
var
  window_rect: TRect;
begin
  with main_form_infor do
  begin
    if m_main_window.WindowState = wsMaximized then
      right := 0
    else
    begin
      Windows.GetWindowRect(m_main_window.Handle, window_rect);
      right := window_rect.right;
      bottom := window_rect.bottom;
      left := window_rect.left;
      top := window_rect.top;
    end;
    with flux_window do
    begin
      m_flux_window.readWindowPosition(left, top);
      max_range := m_flux_window.MaxRange;
    end;
  end;
end;

procedure TMainWindow.onMainFormClose(Sender: PObj; var Accept: Boolean);
begin
  if Assigned(onStatusMustBeStore) then
    onStatusMustBeStore(sesPosition);
end;

procedure TMainWindow.onMainFormQueryEndSession(Sender: PObj; var Accept:
  Boolean);
begin
  if Assigned(onStatusMustBeStore) then
    onStatusMustBeStore(sesPosition);
end;

function TMainWindow.getListenerInterface: IProtocolEventListener;
begin
  Result := m_job_manager;
end;

end.

