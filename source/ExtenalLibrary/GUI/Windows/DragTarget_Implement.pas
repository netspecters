unit DragTarget_Implement;

interface

uses
  Kol,
  Windows, ActiveX,
  NSErrorCode;

type
  TCustomDragTarget = class(TInterfacedObject, IDropTarget)
  private
    FWindowHandle: THandle;

    (*-> OverWrite TInterfacedObject Begin *)
  protected
    {必须重新实现此函数,原InterfacedObject中函数在计数为0时会将对象释放,如果外部类再调用Free进行对象的释放就会造成释放错误}
    function _Release: Integer; stdcall;
  public
    procedure BeforeDestruction; override;
    (* OverWrite TInterfacedObject End <-*)

  protected
    function DragEnter(const DataObj: IDataObject; grfKeyState: LongInt; pt: TPoint; var dwEffect: LongInt): HResult; virtual; stdcall;
    function DragOver(grfKeyState: LongInt; pt: TPoint; var dwEffect: LongInt): HResult; virtual; stdcall;
    function DragLeave: HResult; virtual; stdcall;
    function Drop(const DataObj: IDataObject; grfKeyState: LongInt; pt: TPoint; var dwEffect: LongInt): HResult; virtual; stdcall;
  public
    function RegisterDestWindow(hWindow: THandle): Integer;
    procedure UnRegisterDestWindow();
    destructor Destroy(); override;
  end;

  TURLDropEvent = procedure(const stURL: string) of object;
  TURLDragTarget = class(TCustomDragTarget)
  private
    FURL: string;
    FOnURLDroped: TURLDropEvent;
    procedure SetOnURLDroped(const Value: TURLDropEvent);
  protected
    function DragEnter(const DataObj: IDataObject; grfKeyState: LongInt; pt: TPoint; var dwEffect: LongInt): HResult; override; stdcall;
    function DragOver(grfKeyState: LongInt; pt: TPoint; var dwEffect: LongInt): HResult; override; stdcall;
    (*不要再Drop中耽误过多时间*)
    function Drop(const DataObj: IDataObject; grfKeyState: LongInt; pt: TPoint; var dwEffect: LongInt): HResult; override; stdcall;
  public
    property OnURLDroped: TURLDropEvent read FOnURLDroped write SetOnURLDroped;
  end;

implementation

var
  CF_URL            : UINT;

  { TDragTarget }

procedure TCustomDragTarget.BeforeDestruction;
begin
  //do nothing
end;

destructor TCustomDragTarget.Destroy;
begin
  if FWindowHandle <> 0 then UnRegisterDestWindow; //如果还没有释放注册的对象，那么现在就释放
  inherited;
end;

function TCustomDragTarget.DragEnter(const DataObj: IDataObject;
  grfKeyState: Integer; pt: TPoint; var dwEffect: Integer): HResult;
{
DROPEFFECT_NONE=0 表示此窗口不能接受拖放。

DROPEFFECT_MOVE=1 表示拖放的结果将使源对象被删除

DROPEFFECT_COPY=2 表示拖放将引起源对象的复制。

DROPEFFECT_LINK =4 表示拖放源对象创建了一个对自己的连接

DROPEFFECT_SCROLL=0x80000000表示拖放目标窗口正在或将要进行卷滚。此标志可以和其他几个合用

对于拖放对象来说，一般只要使用DROPEFFECT_NONE和DROPEFFECT_COPY即可。

}
begin
  {基础类并没有相应的实现代码}
  dwEffect := DROPEFFECT_NONE;
  Result := E_INVALIDARG;
end;

function TCustomDragTarget.DragLeave: HResult;
begin
  Result := E_INVALIDARG;
end;

function TCustomDragTarget.DragOver(grfKeyState: Integer; pt: TPoint;
  var dwEffect: Integer): HResult;
begin
  Result := E_INVALIDARG;
  dwEffect := DROPEFFECT_NONE;
end;

function TCustomDragTarget.Drop(const DataObj: IDataObject; grfKeyState: Integer;
  pt: TPoint; var dwEffect: Integer): HResult;
begin
  Result := E_INVALIDARG;
  dwEffect := DROPEFFECT_NONE;
end;

function TCustomDragTarget.RegisterDestWindow(hWindow: THandle): Integer;
var
  L_RES             : HResult;
begin
  Result := NSEC_NORMALERROR;
  if (FWindowHandle = 0) then
  begin
    L_RES := OleInitialize(nil);
    if L_RES = S_OK then
    begin
      L_RES := RegisterDragDrop(hWindow, Self);
      if L_RES = S_OK then
      begin
        FWindowHandle := hWindow;
        Result := NSEC_SUCCESS;
      end;
    end;
    SetLastError(L_RES);
  end;
end;

{ TURLDragTarget }

function TURLDragTarget.DragEnter(const DataObj: IDataObject;
  grfKeyState: Integer; pt: TPoint; var dwEffect: Integer): HResult;
var
  FURLFormatEtc     : TFORMATETC;
  medium            : TStgMedium;
  L_pBuffer         : PChar;
begin
  with FURLFormatEtc do
  begin
    cfFormat := CF_URL;
    ptd := nil;
    dwAspect := DVASPECT_CONTENT;
    lindex := -1;
    tymed := TYMED_HGLOBAL;
  end;

  {得到URL}
  if DataObj.GetData(FURLFormatEtc, medium) = S_OK then
  try
    L_pBuffer := GlobalLock(medium.HGLOBAL);
    FURL := L_pBuffer;
    //SetString(FURL, L_pBuffer, StrLen(L_pBuffer));
    GlobalUnlock(medium.HGLOBAL);

    Result := S_OK;
    dwEffect := DROPEFFECT_LINK;
  finally
    ReleaseStgMedium(medium);
  end
  else
  begin

    Result := E_INVALIDARG;
    dwEffect := DROPEFFECT_NONE;
  end;
end;

function TURLDragTarget.DragOver(grfKeyState: Integer; pt: TPoint;
  var dwEffect: Integer): HResult;
begin
  Result := S_OK;
  dwEffect := DROPEFFECT_LINK;
end;

function TURLDragTarget.Drop(const DataObj: IDataObject;
  grfKeyState: Integer; pt: TPoint; var dwEffect: Integer): HResult;
begin
  if FURL <> '' then
  begin
    Result := S_OK;
    dwEffect := DROPEFFECT_LINK;
    if Assigned(FOnURLDroped) then
    begin
      FOnURLDroped(FURL);
    end;
    FURL := '';
  end
  else
  begin
    Result := E_INVALIDARG;
    dwEffect := DROPEFFECT_NONE;
  end;
end;

procedure TURLDragTarget.SetOnURLDroped(const Value: TURLDropEvent);
begin
  FOnURLDroped := Value;
end;

procedure TCustomDragTarget.UnRegisterDestWindow;
begin
  if FWindowHandle <> 0 then
  begin
    RevokeDragDrop(FWindowHandle);
    FWindowHandle := 0;
    OleUninitialize;
  end;
end;

function TCustomDragTarget._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount); //当计数为0时不要释放,由创建此类的对象调用Free进行释放工作
end;

initialization

  CF_URL := RegisterClipboardFormat('UniformResourceLocator');

finalization

end.

