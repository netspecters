#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED

#include <wx/app.h>
#include "MainForm.h"
#include "object_proxy.h"

class CApplication : public wxApp
{
	public:
		CApplication() = default;
		virtual bool OnInit();
		virtual int OnExit();

	private:
		CMainForm_Core *main_window_ptr;
};

DECLARE_APP( CApplication )

#endif // CORE_H_INCLUDED
