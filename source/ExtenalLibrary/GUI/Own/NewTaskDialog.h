#ifndef __NewTaskDialog__
#define __NewTaskDialog__

#include "rad_gui.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"

using namespace Netspecters::KernelPlus::ProtocolManager;

class CNewTaskDialog_Core : public NewTaskDialog
{
	public:
		struct ST_NewJobParam : public IProtocol::ST_TaskResource
		{
			wxString comment;
			std::string res_path, login_usr_name, login_passwd;

			void assignToPtr()
			{
				res_path_ptr = res_path.c_str();
				if( !login_usr_name.empty() )
					login_usr_name_ptr = login_usr_name.c_str();
				if( !login_passwd.empty() )
					login_passwd_ptr = login_passwd.c_str();
			}
		} new_task_param;

		CNewTaskDialog_Core( wxWindow *parent );

	private:
		void onListPageChanged( wxListbookEvent &event );
		void onURLInput( wxCommandEvent &event );
		void onOKButtonClick( wxCommandEvent &event );
};

#endif // __NewTaskDialog__
