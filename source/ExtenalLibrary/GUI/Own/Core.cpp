#include <string>
#include <system_error>
#include <wx/msgdlg.h>

#include "Core.h"

IMPLEMENT_APP( CApplication )

bool CApplication::OnInit()
{
	try
	{
		init_global_ipc_client();
	}
	catch( std::system_error &e )
	{
		/* 无法建立与主程序的通信管道时报错退出 */
		wxMessageBox( wxString::Format( _( "无法与Netspecters内核建立通信连接[%s]！\n\n请尝试重新启动Netspecters内核或重新安装Netspecters以解决此问题。" ), wxString( e.code().message().c_str() , wxConvUTF8 ).c_str() ),
					  _( "启动失败" ), wxICON_HAND | wxOK );
		return false;
	}

	main_window_ptr = new CMainForm_Core( NULL );
	main_window_ptr->Show();

	return true;
}

int CApplication::OnExit()
{
	clear_global_ipc_client();
	main_window_ptr->Destroy();
	return 1;
}
