#include "NewTaskDialog.h"
#include <wx/uri.h>

CNewTaskDialog_Core::CNewTaskDialog_Core( wxWindow *parent )
	: NewTaskDialog( parent )
{

}

void CNewTaskDialog_Core::onListPageChanged( wxListbookEvent &event )
{

}

void CNewTaskDialog_Core::onURLInput( wxCommandEvent &event )
{
	wxURI uri( event.GetString() );
	if( uri.HasScheme() && uri.HasPath() )
		m_filename_text_editor_ptr->SetValue( uri.GetPath().AfterLast( wxT( '/' ) ) );
}

void CNewTaskDialog_Core::onOKButtonClick( wxCommandEvent &event )
{
	wxURI uri( m_url_text_editor_ptr->GetValue() );

	/* 检查关键变量是否合法 */
	if( m_url_text_editor_ptr->GetValue().IsEmpty() )
	{
		wxMessageBox( _( "必须填写网络地址" ), _( "错误" ), wxOK | wxICON_ERROR, this );
		m_url_text_editor_ptr->SetFocus();
		return;
	}

	if( !uri.HasScheme() )
	{
		wxMessageBox( _( "网络地址格式错误！" ), _( "错误" ), wxOK | wxICON_ERROR , this );
		m_url_text_editor_ptr->SetFocus();
		return;
	}
//	if(!NSGUI_INSTANCE.getProtocolManagerPtr()->queryProtocol(uri.GetScheme().ToAscii()))
//	{
//		wxMessageBox(_("不可识别的网络地址格式!"), _("错误"), wxOK | wxICON_ERROR , this);
//		m_url_text_editor_ptr->SetFocus();
//		return;
//	}
//	if(m_filename_text_editor_ptr->GetValue().IsEmpty())
//	{
//		wxMessageBox(_("文件名不能为空!"), _("错误"), wxOK | wxICON_ERROR , this);
//		m_filename_text_editor_ptr->SetFocus();
//		return;
//	}

	/* 填写参数 */
//	memset( &new_task_param, 0, sizeof( IProtocol::ST_TaskResource ) );
//
//	if( m_login_server_checkbox->IsChecked() )
//	{
//		new_task_param.login_usr_name.assign( uri.GetUser().ToUTF8() );
//		new_task_param.login_passwd.assign( uri.GetPassword().ToUTF8() );
//	}
//	else if( uri.HasUserInfo() )
//	{
//		new_task_param.login_usr_name.assign( m_textCtrl4->GetValue().ToUTF8() );
//		new_task_param.login_passwd.assign( m_textCtrl5->GetValue().ToUTF8() );
//	}
//	else
//	{
//		new_task_param.login_passwd.clear();
//		new_task_param.login_usr_name.clear();
//	}
//
//	if( uri.HasPort() )
//		new_job_param.host_port = wxAtoi( uri.GetPort() );
//
//	new_task_param.res_path.assign( m_url_text_editor_ptr->GetValue().ToUTF8() ); //url只能是ascii码
//
//
//	if( m_retry_checkbox->IsChecked() )
//	{
//		new_task_param.retry_limit = m_spinCtrl3->GetValue();
//	}

	new_task_param.assignToPtr();

	Close();
}
