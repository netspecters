///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version May 25 2010)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __rad_gui__
#define __rad_gui__

#include <wx/intl.h>

#include <wx/string.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/toolbar.h>
#include <wx/listctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/listbook.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/combobox.h>
#include <wx/button.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class MainForm
///////////////////////////////////////////////////////////////////////////////
class MainForm : public wxFrame 
{
	private:
	
	protected:
		enum
		{
			wxID_START = 1000,
		};
		
		wxMenuBar* m_menubar1;
		wxMenu* m_menu_file;
		wxListbook* m_listbook2;
		wxPanel* m_panel2;
		wxToolBar* m_toolBar1;
		wxListCtrl* m_task_list;
		wxPanel* m_panel3;
		wxToolBar* m_toolBar2;
		wxListCtrl* m_finished_task_list;
		wxStatusBar* m_statusBar1;
		
		// Virtual event handlers, overide them in your derived class
		virtual void onNewTask( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		MainForm( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Netpsecters"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 651,527 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		~MainForm();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class NewTaskDialog
///////////////////////////////////////////////////////////////////////////////
class NewTaskDialog : public wxDialog 
{
	private:
	
	protected:
		wxListbook* m_listbook3;
		wxPanel* m_panel6;
		wxStaticText* m_staticText1;
		wxTextCtrl* m_url_text_editor_ptr;
		wxStaticText* m_taticText3;
		wxTextCtrl* m_filename_text_editor_ptr;
		wxStaticText* m_staticText2;
		wxComboBox* m_comboBox1;
		wxPanel* m_panel7;
		wxStdDialogButtonSizer* m_sdbSizer1;
		wxButton* m_sdbSizer1OK;
		wxButton* m_sdbSizer1Cancel;
		
		// Virtual event handlers, overide them in your derived class
		virtual void onListPageChanged( wxListbookEvent& event ) { event.Skip(); }
		virtual void onURLInput( wxCommandEvent& event ) { event.Skip(); }
		virtual void onOKButtonClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		NewTaskDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("新建任务"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 498,177 ), long style = wxDEFAULT_DIALOG_STYLE );
		~NewTaskDialog();
	
};

#endif //__rad_gui__
