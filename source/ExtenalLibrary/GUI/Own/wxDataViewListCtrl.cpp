#include "wxDataViewListCtrl.h"

BEGIN_EVENT_TABLE( wxDataViewListCtrl, wxDataViewCtrl )
	EVT_SIZE( wxDataViewListCtrl::OnSize )
END_EVENT_TABLE()

wxDataViewListCtrl::wxDataViewListCtrl()
{
}

wxDataViewListCtrl::wxDataViewListCtrl( wxWindow *parent, wxWindowID id,
										const wxPoint &pos, const wxSize &size, long style,
										const wxValidator &validator )
{
	Create( parent, id, pos, size, style, validator );

	wxDataViewListStore *store = new wxDataViewListStore;
	AssociateModel( store );
	store->DecRef();
}

wxDataViewListCtrl::~wxDataViewListCtrl()
{
}


bool wxDataViewListCtrl::Create( wxWindow *parent, wxWindowID id,
								 const wxPoint &pos, const wxSize &size, long style,
								 const wxValidator &validator )
{
	return wxDataViewCtrl::Create( parent, id, pos, size, style, validator );
}

bool wxDataViewListCtrl::AppendColumn( wxDataViewColumn *column, const wxString &varianttype )
{
	GetStore()->AppendColumn( varianttype );
	return wxDataViewCtrl::AppendColumn( column );
}

bool wxDataViewListCtrl::PrependColumn( wxDataViewColumn *column, const wxString &varianttype )
{
	GetStore()->PrependColumn( varianttype );
	return wxDataViewCtrl::PrependColumn( column );
}

bool wxDataViewListCtrl::InsertColumn( unsigned int pos, wxDataViewColumn *column, const wxString &varianttype )
{
	GetStore()->InsertColumn( pos, varianttype );
	return wxDataViewCtrl::InsertColumn( pos, column );
}

bool wxDataViewListCtrl::PrependColumn( wxDataViewColumn *col )
{
	return PrependColumn( col, "string" );
}

bool wxDataViewListCtrl::InsertColumn( unsigned int pos, wxDataViewColumn *col )
{
	return InsertColumn( pos, col, "string" );
}

bool wxDataViewListCtrl::AppendColumn( wxDataViewColumn *col )
{
	return AppendColumn( col, "string" );
}

wxDataViewColumn *wxDataViewListCtrl::AppendTextColumn( const wxString &label,
		wxDataViewCellMode mode, int width, wxAlignment align, int flags )
{
	GetStore()->AppendColumn( wxT( "string" ) );

	wxDataViewColumn *ret = new wxDataViewColumn( label,
			new wxDataViewTextRenderer( wxT( "string" ), mode ),
			GetStore()->GetColumnCount() - 1, width, align, flags );

	wxDataViewCtrl::AppendColumn( ret );

	return ret;
}

wxDataViewColumn *wxDataViewListCtrl::AppendToggleColumn( const wxString &label,
		wxDataViewCellMode mode, int width, wxAlignment align, int flags )
{
	GetStore()->AppendColumn( wxT( "bool" ) );

	wxDataViewColumn *ret = new wxDataViewColumn( label,
			new wxDataViewToggleRenderer( wxT( "bool" ), mode ),
			GetStore()->GetColumnCount() - 1, width, align, flags );

	wxDataViewCtrl::AppendColumn( ret );

	return ret;
}

wxDataViewColumn *wxDataViewListCtrl::AppendProgressColumn( const wxString &label,
		wxDataViewCellMode mode, int width, wxAlignment align, int flags )
{
	GetStore()->AppendColumn( wxT( "long" ) );

	wxDataViewColumn *ret = new wxDataViewColumn( label,
			new wxDataViewProgressRenderer( wxEmptyString, wxT( "long" ), mode ),
			GetStore()->GetColumnCount() - 1, width, align, flags );

	wxDataViewCtrl::AppendColumn( ret );

	return ret;
}

wxDataViewColumn *wxDataViewListCtrl::AppendIconTextColumn( const wxString &label,
		wxDataViewCellMode mode, int width, wxAlignment align, int flags )
{
	GetStore()->AppendColumn( wxT( "wxDataViewIconText" ) );

	wxDataViewColumn *ret = new wxDataViewColumn( label,
			new wxDataViewIconTextRenderer( wxT( "wxDataViewIconText" ), mode ),
			GetStore()->GetColumnCount() - 1, width, align, flags );

	wxDataViewCtrl::AppendColumn( ret );

	return ret;
}

void wxDataViewListCtrl::OnSize( wxSizeEvent &event )
{
	event.Skip( true );
}
