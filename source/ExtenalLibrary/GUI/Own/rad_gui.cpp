///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version May 25 2010)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wx_pch.h"

#include "rad_gui.h"

///////////////////////////////////////////////////////////////////////////

MainForm::MainForm( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	m_menubar1 = new wxMenuBar( 0 );
	m_menu_file = new wxMenu();
	m_menubar1->Append( m_menu_file, _("文件") ); 
	
	this->SetMenuBar( m_menubar1 );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	m_listbook2 = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLB_DEFAULT );
	m_panel2 = new wxPanel( m_listbook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );
	
	m_toolBar1 = new wxToolBar( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL|wxTB_NOICONS|wxTB_TEXT ); 
	m_toolBar1->AddTool( wxID_NEW, _("新建"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolBar1->AddSeparator(); 
	m_toolBar1->AddTool( wxID_START, _("开始"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolBar1->AddTool( wxID_STOP, _("暂停"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolBar1->AddSeparator(); 
	m_toolBar1->AddTool( wxID_CANCEL, _("取消"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolBar1->Realize();
	
	bSizer4->Add( m_toolBar1, 0, wxEXPAND, 5 );
	
	m_task_list = new wxListCtrl( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	bSizer4->Add( m_task_list, 1, wxALL|wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer4 );
	m_panel2->Layout();
	bSizer4->Fit( m_panel2 );
	m_listbook2->AddPage( m_panel2, _("任务"), true );
	m_panel3 = new wxPanel( m_listbook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxVERTICAL );
	
	m_toolBar2 = new wxToolBar( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL|wxTB_NOICONS|wxTB_TEXT ); 
	m_toolBar2->AddTool( wxID_DELETE, _("删除"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolBar2->Realize();
	
	bSizer8->Add( m_toolBar2, 0, wxEXPAND, 5 );
	
	m_finished_task_list = new wxListCtrl( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	bSizer8->Add( m_finished_task_list, 1, wxALL|wxEXPAND, 5 );
	
	m_panel3->SetSizer( bSizer8 );
	m_panel3->Layout();
	bSizer8->Fit( m_panel3 );
	m_listbook2->AddPage( m_panel3, _("已完成"), false );
	#ifndef __WXGTK__ // Small icon style not supported in GTK
	wxListView* m_listbook2ListView = m_listbook2->GetListView();
	long m_listbook2Flags = m_listbook2ListView->GetWindowStyleFlag();
	m_listbook2Flags = ( m_listbook2Flags & ~wxLC_ICON ) | wxLC_SMALL_ICON;
	m_listbook2ListView->SetWindowStyleFlag( m_listbook2Flags );
	#endif
	
	bSizer2->Add( m_listbook2, 1, wxEXPAND | wxALL, 5 );
	
	this->SetSizer( bSizer2 );
	this->Layout();
	m_statusBar1 = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxID_NEW, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainForm::onNewTask ) );
}

MainForm::~MainForm()
{
	// Disconnect Events
	this->Disconnect( wxID_NEW, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainForm::onNewTask ) );
	
}

NewTaskDialog::NewTaskDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );
	
	m_listbook3 = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLB_TOP );
	m_panel6 = new wxPanel( m_listbook3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText1 = new wxStaticText( m_panel6, wxID_ANY, _("URL："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer6->Add( m_staticText1, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_url_text_editor_ptr = new wxTextCtrl( m_panel6, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer6->Add( m_url_text_editor_ptr, 1, wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	bSizer8->Add( bSizer6, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );
	
	m_taticText3 = new wxStaticText( m_panel6, wxID_ANY, _("文件名："), wxDefaultPosition, wxDefaultSize, 0 );
	m_taticText3->Wrap( -1 );
	bSizer9->Add( m_taticText3, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_filename_text_editor_ptr = new wxTextCtrl( m_panel6, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_filename_text_editor_ptr, 1, wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText2 = new wxStaticText( m_panel6, wxID_ANY, _("保存在："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer9->Add( m_staticText2, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_comboBox1 = new wxComboBox( m_panel6, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 ); 
	bSizer9->Add( m_comboBox1, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	bSizer8->Add( bSizer9, 0, wxEXPAND, 5 );
	
	m_panel6->SetSizer( bSizer8 );
	m_panel6->Layout();
	bSizer8->Fit( m_panel6 );
	m_listbook3->AddPage( m_panel6, _("常规"), true );
	m_panel7 = new wxPanel( m_listbook3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_listbook3->AddPage( m_panel7, _("登录"), false );
	#ifndef __WXGTK__ // Small icon style not supported in GTK
	wxListView* m_listbook3ListView = m_listbook3->GetListView();
	long m_listbook3Flags = m_listbook3ListView->GetWindowStyleFlag();
	m_listbook3Flags = ( m_listbook3Flags & ~wxLC_ICON ) | wxLC_SMALL_ICON;
	m_listbook3ListView->SetWindowStyleFlag( m_listbook3Flags );
	#endif
	
	bSizer5->Add( m_listbook3, 1, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_sdbSizer1 = new wxStdDialogButtonSizer();
	m_sdbSizer1OK = new wxButton( this, wxID_OK );
	m_sdbSizer1->AddButton( m_sdbSizer1OK );
	m_sdbSizer1Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer1->AddButton( m_sdbSizer1Cancel );
	m_sdbSizer1->Realize();
	bSizer5->Add( m_sdbSizer1, 0, wxEXPAND|wxALL, 5 );
	
	this->SetSizer( bSizer5 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_listbook3->Connect( wxEVT_COMMAND_LISTBOOK_PAGE_CHANGED, wxListbookEventHandler( NewTaskDialog::onListPageChanged ), NULL, this );
	m_url_text_editor_ptr->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NewTaskDialog::onURLInput ), NULL, this );
	m_sdbSizer1OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewTaskDialog::onOKButtonClick ), NULL, this );
}

NewTaskDialog::~NewTaskDialog()
{
	// Disconnect Events
	m_listbook3->Disconnect( wxEVT_COMMAND_LISTBOOK_PAGE_CHANGED, wxListbookEventHandler( NewTaskDialog::onListPageChanged ), NULL, this );
	m_url_text_editor_ptr->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NewTaskDialog::onURLInput ), NULL, this );
	m_sdbSizer1OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewTaskDialog::onOKButtonClick ), NULL, this );
	
}
