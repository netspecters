#ifndef NS_OBJECT_PROXY_H_INCLUDED
#define NS_OBJECT_PROXY_H_INCLUDED

#include "../../../KernelLibrary/InternalProtocol/http/Interfaces.h"
#include "../../../KernelLibrary/DataContainer/Interfaces.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"

#include <utility>
#include "ipc.h"

using namespace Netspecters::KernelPlus;
using namespace ProtocolManager;
using namespace InternalProtocol;
using namespace DataContainer;

void init_global_ipc_client();
void clear_global_ipc_client();

/**
	@note a. void* 类型特别对待 b.引用转换为指针调用
*/
class CObjectProxyBase
{
		class CAllocor
		{
			public:
				CAllocor(): m_buffer_ptr( NULL ), m_buffer_size( 0 ), m_alloced_size( 0 ), m_retouch_offset( 0 ) {}
				~CAllocor() {
					free( m_buffer_ptr );
				}
				void *get_buffer_base()const {
					return m_buffer_ptr;
				}
				unsigned get_alloced_size() {
					return m_alloced_size;
				}
				/* 分配数据空间 */
				void *alloc( size_t size );
				void reset() {
					m_alloced_size = 0;
				}
				/* 将地址转换为偏移 */
				uintptr_t pointer_to_offset( void *pointer ) const {
					return ( uintptr_t )pointer - ( uintptr_t )m_buffer_ptr;
				}
				intptr_t get_retouch_offset() {
					return m_retouch_offset;
				}
			private:
				void *m_buffer_ptr;
				unsigned m_buffer_size, m_alloced_size;
				intptr_t m_retouch_offset;
		};
		friend void init_global_ipc_client();
		friend void clear_global_ipc_client();

	protected:
		struct ST_WriteTableItem	//<原始地址，复制缓冲区地址，数据长度>
		{
			void *origin_addr;
			void *buf_addr;
			size_t size;
		};
		CObjectProxyBase( const char *object_path );

		void *call_method( const char *method_name );
		template<typename T, typename ... Args>
		void *call_method( const char *method_name, T && t, Args ... args )
		{
			push( std::forward<T>( t ) );
			return call_method( method_name, std::forward<Args>( args )... );
		}

		void *push_ptr( const void *data, size_t size );
		void push_ptr( void *data, size_t size )
		{
			void *buf = push_ptr(( const void * )data, size );
			if( buf )	/* 保存写回位置 */
				m_write_table.push_back( ST_WriteTableItem {data, buf, size} );
		}
		void push_data( const void *data, size_t size );

		template<typename T> void push( const T &t ) {
			push_data( &t, sizeof( t ) );
		}
		template<typename T> void push( T *t ) {
			push_ptr(( void * )t, sizeof( T ) );
		}
		template<typename T> void push( const T *t ) {
			push_ptr(( const void * )t, sizeof( T ) );
		}
		void push( const char *ch_ptr ) {
			unsigned len = strlen( ch_ptr );
			push_ptr(( const void * )ch_ptr, len == 0 ? 1 : len );
		}
		void push( char *ch_ptr ) {
			unsigned len = strlen( ch_ptr );
			push_ptr(( void * )ch_ptr, len == 0 ? 1 : len );
		}
		/* 在gcc编译器下，sizeof(bool) == 1 但是作为函数的实参，bool类型会认为是4字节，bool 必须特别处理 */
		void push( bool value ) {
			unsigned v = 0;
			v = value;
			push_data( &v, sizeof( v ) );
		}

	private:
		string m_object_path;
		CAllocor m_stack, m_data_area;
		CIPCClient::TAddressTranslateTable m_address_translate_table;
		std::vector<ST_WriteTableItem> m_write_table;

		static CIPCClient *static_m_ipc_client_ptr;
};

#ifdef DEBUG
class CTestProxy : public CObjectProxyBase
{
	public:
		struct X
		{
			int a, b, c;
		};
		CTestProxy() : CObjectProxyBase( "/" ) {}

		bool call_test_method( int first_param, bool second_param, X *third_param, char *forth_param, const char *name ) {
			return ( bool )call_method( "test_method", first_param, second_param, third_param, forth_param, name );
		}
};
#endif

/* 边界声明 */
#define _DECLARE_PROXY_BEGIN(name,path) class name : public CObjectProxyBase { public: name():CObjectProxyBase(path) {}
#define _DECLARE_PROXY_END };
/* 声明有返回值函数 */
#define _DECLARE_METHOD_0(r,n) r n(){ return (r) call_method(#n); }
#define _DECLARE_METHOD_1(r,n,p1a,p1b) r n(p1a p1b){ return (r) call_method(#n,p1b); }
#define _DECLARE_METHOD_2(r,n,p1a,p1b,p2a,p2b) r n(p1a p1b, p2a p2b){ return (r) call_method(#n,p1b,p2b); }
#define _DECLARE_METHOD_3(r,n,p1a,p1b,p2a,p2b,p3a,p3b) r n(p1a p1b, p2a p2b, p3a p3b){ return (r) call_method(#n,p1b,p2b,p3b); }
#define _DECLARE_METHOD_4(r,n,p1a,p1b,p2a,p2b,p3a,p3b,p4a,p4b) r n(p1a p1b, p2a p2b, p3a p3b, p4a p4b){ return (r) call_method(#n,p1b,p2b,p3b,p4b); }
#define _DECLARE_METHOD_5(r,n,p1a,p1b,p2a,p2b,p3a,p3b,p4a,p4b,p5a,p5b) r n(p1a p1b, p2a p2b, p3a p3b, p4a p4b, p5a p5b){ return (r) call_method(#n,p1b,p2b,p3b,p4b,p5b); }
/* 声明无返回值函数 */
#define _DECLARE_METHOD_VOID_0(n) void n(){ call_method(#n); }
#define _DECLARE_METHOD_VOID_1(n,p1a,p1b) void n(p1a p1b){ call_method(#n,p1b); }
#define _DECLARE_METHOD_VOID_2(n,p1a,p1b,p2a,p2b) void n(p1a p1b, p2a p2b){ call_method(#n,p1b,p2b); }
#define _DECLARE_METHOD_VOID_3(n,p1a,p1b,p2a,p2b,p3a,p3b) void n(p1a p1b, p2a p2b, p3a p3b){ call_method(#n,p1b,p2b,p3b); }
#define _DECLARE_METHOD_VOID_4(n,p1a,p1b,p2a,p2b,p3a,p3b,p4a,p4b) void n(p1a p1b, p2a p2b, p3a p3b, p4a p4b){ call_method(#n,p1b,p2b,p3b,p4b); }
#define _DECLARE_METHOD_VOID_5(n,p1a,p1b,p2a,p2b,p3a,p3b,p4a,p4b,p5a,p5b) void n(p1a p1b, p2a p2b, p3a p3b, p4a p4b, p5a p5b){ call_method(#n,p1b,p2b,p3b,p4b,p5b); }

_DECLARE_PROXY_BEGIN( CSegmentProxy, "" )
typedef ISegment::ST_SegmentInfor ST_SegmentInfor;
int read( void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	push_ptr( data_ptr, data_size );
	return ( int )call_method( "read", data_size, offset, io_tag );
}
int write( const void *data_ptr, size_t data_size, off_t offset, long io_tag )
{
	push_ptr( data_ptr, data_size );
	return ( int )call_method( "write", data_size, offset, io_tag );
}
_DECLARE_METHOD_VOID_0( flushData )
_DECLARE_METHOD_0( off_t, getSegmentOffset )
_DECLARE_METHOD_0( off_t, getSegmentSize )
_DECLARE_METHOD_0( off_t, getIOPosition )
_DECLARE_METHOD_VOID_1( getSegmentInformation, ST_SegmentInfor &, segment_infor )
_DECLARE_METHOD_1( bool, setDataWindowSize, size_t, data_window_size )
_DECLARE_PROXY_END

_DECLARE_PROXY_BEGIN( CProtocolProxy, "/" )
typedef IProtocol::ST_TaskResource ST_TaskResource;
typedef IProtocol::ST_TaskStateData ST_TaskStateData;
typedef IProtocol::ST_TaskInitParam ST_TaskInitParam;
typedef IProtocol::ST_TaskInformation ST_TaskInformation;
_DECLARE_PROXY_END


_DECLARE_PROXY_BEGIN( CTaskGroupManagerProxy, "/sys/protocol_manager/" )
_DECLARE_METHOD_3( GROUP_HANDLE, addGroup, const char *, group_file_name_str_ptr, bool, visable, uintptr_t, group_tag )
_DECLARE_METHOD_2( GROUP_TASK_HANDLE, addProtocolTaskToGroup, GROUP_HANDLE, group_handle, const CProtocolProxy::ST_TaskInitParam *, task_init_param )
size_t getGroupHandleList( GROUP_HANDLE *group_handle_buf_ptr, size_t buffer_size )
{
	if( group_handle_buf_ptr )
		push_ptr( group_handle_buf_ptr, buffer_size );
	else
	{
		void *p = NULL;
		push_data( &p, sizeof( p ) );
	}
	return ( size_t )call_method( "getGroupHandleList" );
}
_DECLARE_METHOD_1( uint32_t, getGroupID, GROUP_HANDLE, group_handle )
_DECLARE_METHOD_VOID_2( switchGroupRunningState, GROUP_HANDLE, group_handle, bool, pause )
_DECLARE_PROXY_END

#endif // NS_OBJECT_PROXY_H_INCLUDED
