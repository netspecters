#include "Core.h"
#include "MainForm.h"
#include "ipc.h"
#include "object_proxy.h"

#include "../../../KernelLibrary/InternalProtocol/http/Interfaces.h"

CMainForm_Core::CMainForm_Core( wxWindow *parent )
	: MainForm( parent )
{
	/* 设置任务列表表头 */
	m_task_list->InsertColumn( 0, _( "任务" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 1, _( "完成" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 2, _( "下载速度" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 3, _( "上传速度" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 4, _( "剩余时间" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 5, _( "起始时间" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 6, _( "已耗时" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 7, _( "距离最近一次传输" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 8, _( "资源健康度" ), wxLIST_FORMAT_LEFT, -1 );
	m_task_list->InsertColumn( 9, _( "使用协议" ), wxLIST_FORMAT_LEFT, -1 );

	/* 设置已完成任务列表表头 */
	m_finished_task_list->InsertColumn( 0, _( "任务" ), wxLIST_FORMAT_LEFT, -1 );
	m_finished_task_list->InsertColumn( 1, _( "起始时间" ), wxLIST_FORMAT_LEFT, -1 );
	m_finished_task_list->InsertColumn( 2, _( "共耗时" ), wxLIST_FORMAT_LEFT, -1 );
	m_finished_task_list->InsertColumn( 3, _( "平均下载速度" ), wxLIST_FORMAT_LEFT, -1 );
	m_finished_task_list->InsertColumn( 4, _( "平均上传速度" ), wxLIST_FORMAT_LEFT, -1 );

	/* 从内核中读出未完成的任务 */
	CTaskGroupManagerProxy task_group_manager;
	task_group_manager.getGroupHandleList(NULL);
}

void CMainForm_Core::onNewTask( wxCommandEvent &event )
{
	CNewTaskDialog_Core new_task_dialog( this );
	new_task_dialog.ShowModal();

//#ifdef DEBUG
#if 0
	CTestProxy proxy;
	CTestProxy::X x {1, 2, 3};
	char hello[6] = {0, 0, 0, 0, 0, 0};
	if( proxy.call_test_method( 1, false, &x, hello, "this is a test method call" ) && x.a == 2 && x.b == 3 && x.c == 4 )
		wxMessageBox( _( "成功调用内核函数。" ), _( "成功" ), wxOK );
	else
		wxMessageBox( _( "调用内核函数失败！" ), _( "失败" ), wxOK | wxICON_ERROR );
#endif

	/* 设置参数 */
//	if( new_task_dialog.new_task_param.res_path_ptr )
//	{
//		/* 构建任务参数 */
//		CProtocolProxy::ST_TaskInitParam task_init_param = { 0, true, &new_task_dialog.new_task_param, NULL };
//
//		/* 添加任务组 */
//		CTaskGroupManagerProxy task_group_manager;
//		GROUP_HANDLE group_handle = task_group_manager.addGroup( new_task_dialog.new_task_param.active_node.name.mb_str( wxConvUTF8 ), true, 0 );
//
//		/* 添加任务 */
//		task_group_manager_ptr->addProtocolTaskToGroup( group_handle, task_init_param );
//		new_task_dialog.new_task_param.active_node.taskmanager_group_handle = group_handle;
//		new_task_dialog.new_task_param.active_node.taskmanager_group_id = task_group_manager_ptr->getGroupID( group_handle );
//		m_task_view.addItem( new_task_dialog.new_task_param.active_node );
//
//		/* 启动任务 */
//		task_group_manager_ptr->switchGroupRunningState( group_handle, false );
//
//		/* 将任务加入 显示任务列表 */
//	}
}

void CMainForm_Core::onOKButtonClick( wxCommandEvent &event )
{

}

void CMainForm_Core::update_task_list( void )
{
	/* 遍历刷新每个任务 */
}
