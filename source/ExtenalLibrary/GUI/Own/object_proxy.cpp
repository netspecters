#include "object_proxy.h"

CIPCClient *CObjectProxyBase::static_m_ipc_client_ptr = NULL;

void init_global_ipc_client()
{
	if( !CObjectProxyBase::static_m_ipc_client_ptr )
		CObjectProxyBase::static_m_ipc_client_ptr = new CIPCClient();
}

void clear_global_ipc_client()
{
	delete CObjectProxyBase::static_m_ipc_client_ptr;
}

void *CObjectProxyBase::CAllocor::alloc( size_t size )
{
	/* 保证有足够的空间 */
	if(( m_buffer_size - m_alloced_size ) < size )
	{
		unsigned need_size = m_buffer_size + ( m_buffer_size > size ? m_buffer_size : size );
		void *new_buffer_ptr = realloc( m_buffer_ptr, need_size );

		if( new_buffer_ptr != m_buffer_ptr )
		{
			m_retouch_offset = ( uintptr_t )new_buffer_ptr - ( uintptr_t )m_buffer_ptr;
			m_buffer_ptr = new_buffer_ptr;
		}
		else
			m_retouch_offset = 0;

		m_buffer_size = need_size;
	}

	void *alloced_ptr = ( void * )(( uintptr_t )m_buffer_ptr + m_alloced_size );
	m_alloced_size += size;
	return alloced_ptr;
}

CObjectProxyBase::CObjectProxyBase( const char *object_path )
	: m_object_path( object_path )
{}

void *CObjectProxyBase::call_method( const char *method_name )
{
	void *result = static_m_ipc_client_ptr->call_method( m_object_path.c_str(), method_name
				   , m_stack.get_alloced_size(), m_stack.get_buffer_base()
				   , m_address_translate_table
				   , m_data_area.get_alloced_size(), m_data_area.get_buffer_base() );
	for( auto itr = m_write_table.begin(); itr != m_write_table.end(); ++itr )
		memcpy( itr->origin_addr, itr->buf_addr, itr->size );

	m_stack.reset();
	m_data_area.reset();
	m_write_table.clear();
	return result;
}

void *CObjectProxyBase::push_ptr( const void *data, size_t size )
{
	if( data )
	{
		/* 拷贝数据到数据区内存 */
		void *const buf = m_data_area.alloc( size );
		memcpy( buf, data, size );

		/* 回写表可能在重新分配内存后失效，修复回写表 */
		intptr_t offset = m_data_area.get_retouch_offset();
		if( offset != 0 && m_write_table.size() > 0 )
			for( auto itr = m_write_table.begin(); itr != m_write_table.end(); ++itr )
			{
				void *&t = itr->buf_addr;
				t = ( void * )(( uintptr_t )t + offset );
			}

		/* 地址偏移入栈 */
		CIPCClient::TOffsetType *stack_frame_ptr = ( CIPCClient::TOffsetType * )m_stack.alloc( sizeof( CIPCClient::TOffsetType ) );
		*stack_frame_ptr = m_data_area.pointer_to_offset( buf );

		/* 填写地址翻译表 */
		m_address_translate_table.push_back( m_stack.pointer_to_offset( stack_frame_ptr ) );

		return buf;
	}
	else
	{
		push_data( &data, sizeof( void * ) );
		return NULL;
	}
}

void CObjectProxyBase::push_data( const void *data, size_t size )
{
	/* 拷贝数据到堆栈内存 */
	void *buf = m_stack.alloc( size );
	memcpy( buf, data, size );
}
