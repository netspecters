#ifndef __MainForm__
#define __MainForm__

#include "rad_gui.h"
#include "NewTaskDialog.h"

class CMainForm_Core : public MainForm
{
	public:
		CMainForm_Core( wxWindow *parent );

	private:
		/* 用于存储任务相关数据 */
		struct ST_ItemData : public wxClientData
		{
			ST_ItemData(void);
		};

		virtual void onOKButtonClick( wxCommandEvent &event );
		virtual void onNewTask( wxCommandEvent &event );

		void update_task_list( void );
};

#endif // __MainForm__
