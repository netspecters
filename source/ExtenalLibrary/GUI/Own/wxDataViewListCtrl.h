#ifndef WXDATAVIEWLISTCTRL_H_INCLUDED
#define WXDATAVIEWLISTCTRL_H_INCLUDED

#include <wx/dataview.h>
#include <vector>

#define wxVector std::vector

class wxDataViewListCtrl: public wxDataViewCtrlBase
{
	public:
		wxDataViewListCtrl();
		wxDataViewListCtrl( wxWindow *parent, wxWindowID id,
							const wxPoint &pos = wxDefaultPosition,
							const wxSize &size = wxDefaultSize, long style = wxDV_MULTIPLE,
							const wxValidator &validator = wxDefaultValidator );
		~wxDataViewListCtrl();

		bool Create( wxWindow *parent, wxWindowID id,
					 const wxPoint &pos = wxDefaultPosition,
					 const wxSize &size = wxDefaultSize, long style = wxDV_MULTIPLE,
					 const wxValidator &validator = wxDefaultValidator );

		bool AppendColumn( wxDataViewColumn *column, const wxString &varianttype );
		bool PrependColumn( wxDataViewColumn *column, const wxString &varianttype );
		bool InsertColumn( unsigned int pos, wxDataViewColumn *column, const wxString &varianttype );

		// overridden from base class
		virtual bool PrependColumn( wxDataViewColumn *col );
		virtual bool InsertColumn( unsigned int pos, wxDataViewColumn *col );
		virtual bool AppendColumn( wxDataViewColumn *col );

		wxDataViewColumn *AppendTextColumn( const wxString &label,
											wxDataViewCellMode mode = wxDATAVIEW_CELL_INERT,
											int width = -1, wxAlignment align = wxALIGN_LEFT, int flags = wxDATAVIEW_COL_RESIZABLE );
		wxDataViewColumn *AppendToggleColumn( const wxString &label,
											  wxDataViewCellMode mode = wxDATAVIEW_CELL_ACTIVATABLE,
											  int width = -1, wxAlignment align = wxALIGN_LEFT, int flags = wxDATAVIEW_COL_RESIZABLE );
		wxDataViewColumn *AppendProgressColumn( const wxString &label,
												wxDataViewCellMode mode = wxDATAVIEW_CELL_INERT,
												int width = -1, wxAlignment align = wxALIGN_LEFT, int flags = wxDATAVIEW_COL_RESIZABLE );
		wxDataViewColumn *AppendIconTextColumn( const wxString &label,
												wxDataViewCellMode mode = wxDATAVIEW_CELL_INERT,
												int width = -1, wxAlignment align = wxALIGN_LEFT, int flags = wxDATAVIEW_COL_RESIZABLE );

		void AppendItem( const wxVector<wxVariant> &values, wxClientData *data = NULL )
		{
			AppendItem( values, data );
		}
		void PrependItem( const wxVector<wxVariant> &values, wxClientData *data = NULL )
		{
			PrependItem( values, data );
		}
		void InsertItem( unsigned int row, const wxVector<wxVariant> &values, wxClientData *data = NULL )
		{
			InsertItem( row, values, data );
		}
		void DeleteItem( unsigned row )
		{
			DeleteItem( row );
		}
		void DeleteAllItems()
		{
			DeleteAllItems();
		}

		void SetValue( const wxVariant &value, unsigned int row, unsigned int col )
		{	SetValueByRow( value, row, col );
			RowValueChanged( row, col );
		}
		void GetValue( wxVariant &value, unsigned int row, unsigned int col )
		{
			GetValueByRow( value, row, col );
		}

		void SetTextValue( const wxString &value, unsigned int row, unsigned int col )
		{	SetValueByRow( value, row, col );
			RowValueChanged( row, col );
		}
		wxString GetTextValue( unsigned int row, unsigned int col ) const
		{
			wxVariant value;
			GetValueByRow( value, row, col );
			return value.GetString();
		}

		void SetToggleValue( bool value, unsigned int row, unsigned int col )
		{	SetValueByRow( value, row, col );
			RowValueChanged( row, col );
		}
		bool GetToggleValue( unsigned int row, unsigned int col ) const
		{
			wxVariant value;
			GetValueByRow( value, row, col );
			return value.GetBool();
		}

		void OnSize( wxSizeEvent &event );

	private:
		DECLARE_EVENT_TABLE()
		DECLARE_DYNAMIC_CLASS_NO_ASSIGN( wxDataViewListCtrl )
};

#endif // WXDATAVIEWLISTCTRL_H_INCLUDED
