#include "wxTaskView.h"
#include "GUI_RAD.h"

#define TEXT_BAN 8
#define PATH_ARROW_WIDTH 5

using namespace wxTaskViewSpace;

BEGIN_EVENT_TABLE( wxPathView, wxPanel )
	EVT_PAINT( wxPathView::onPaint )
	EVT_LEFT_DOWN( wxPathView::onClick )
END_EVENT_TABLE()

wxPathView::wxPathView( wxWindow *parent, wxSize size )
	: wxPanel( parent, wxID_PATH_VIEW, wxDefaultPosition, size ), m_select_index( -1 )
{
}

void wxPathView::push( ST_Node *node_ptr )
{
	if(( m_path_stack.size() > 0 ) && ( m_path_stack.back()->node_ptr == node_ptr ) ) return;

	ST_PathItem *path_item_ptr = new ST_PathItem;
	path_item_ptr->node_ptr = node_ptr;

	wxClientDC dc( this );
	wxCoord width, height;
	dc.GetTextExtent( node_ptr->name, &width, &height );
	path_item_ptr->text_area_width = width + TEXT_BAN * 2;

	m_path_stack.push_back( shared_ptr<ST_PathItem>( path_item_ptr ) );
	m_select_index++;
	Refresh();
}

void wxPathView::pop( unsigned num )
{
	if( m_path_stack.size() > 0 )
	{
		for( unsigned i = 0; i < num; i++ )
			m_path_stack.pop_back();

		m_select_index -= num;
		Refresh();
	}
}

void wxPathView::onPaint( wxPaintEvent &event )
{
	wxPaintDC dc( this );
	CwxWrapCanvas canvas( dc );
	canvas.clear( 0xff );

	/* draw agg layer */
	CwxWrapCanvas::ST_Point pointes[3];
	pointes[0].set( 0, 0 );
	pointes[1].set( PATH_ARROW_WIDTH, dc.GetSize().GetHeight() / 2 );
	pointes[2].set( 0, dc.GetSize().GetHeight() );
	int index = 0;
	for( auto itr = m_path_stack.begin(); itr != m_path_stack.end(); ++itr, ++index )
	{
		int text_area_width = ( *itr )->text_area_width;
		for( int i = 0; i < 3; i++ )
			pointes[i].x += text_area_width;

		/* draw backgournd */
		if( m_select_index == index )
		{
			CwxWrapCanvas::ST_Point reg_pointes[6];

			reg_pointes[0] = reg_pointes[1] = pointes[0];
			reg_pointes[2] = reg_pointes[5] = pointes[1];
			reg_pointes[3] = reg_pointes[4] = pointes[2];

			reg_pointes[0].x -= text_area_width;
			reg_pointes[4].x -= text_area_width;
			reg_pointes[5].x -= text_area_width;
			if( 0 == index )
				reg_pointes[5].x -= PATH_ARROW_WIDTH;

			canvas.getBrush().setColor( 0xaf, 0xaf, 0xaf );
			canvas.drawPolygon( reg_pointes, 6, true, 0.0, true );
		}

		canvas.getPen().setWidth( 2 ).setColor( 0xff, 0xff, 0xff );
		canvas.drawPolygon( pointes, 3, false, 0.0, false );

		canvas.getPen().setWidth( 1 ).setColor( 0x9e, 0x9e, 0x9e );
		canvas.drawPolygon( pointes, 3, false, 0.0, false );
	}
	canvas.drawToDC();

	/* draw text */
	wxRect text_rect( TEXT_BAN, 0, dc.GetSize().GetWidth(), dc.GetSize().GetHeight() );
	for( auto itr = m_path_stack.begin(); itr != m_path_stack.end(); ++itr )
	{
		dc.DrawLabel(( *itr )->node_ptr->name, text_rect, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL );
		text_rect.x += ( *itr )->text_area_width;
	}
}

void wxPathView::onClick( wxMouseEvent &event )
{
	long click_x_pos = event.GetX(), x_offset = 0;
	int index = 0;
	for( auto itr = m_path_stack.begin(); itr != m_path_stack.end(); ++itr, ++index )
	{
		x_offset += ( *itr )->text_area_width;
		if( click_x_pos <= x_offset )
		{
			if( m_select_index != index )
			{
				if( index <= ( m_path_stack.size() - 2 ) )	/* pop up */
				{
					pop( m_select_index - index );
					m_task_view_ptr->onPathSelect( m_path_stack.back()->node_ptr );
				}
				else
				{
					m_select_index = index;
					Refresh();
				}
			}
			break;
		}
	}
}
