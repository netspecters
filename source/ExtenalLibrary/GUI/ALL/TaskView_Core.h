#ifndef TASKVIEW_CORE_H_INCLUDED
#define TASKVIEW_CORE_H_INCLUDED

#include <list>
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"
#include "../../../KernelLibrary/InternalProtocol/http/Interfaces.h"
#include "wxTaskView.h"

using namespace Netspecters::KernelPlus::ProtocolManager;
using namespace Netspecters::KernelPlus::InternalProtocol::HTTP;

class CTaskView : private wxTimer
{
	DECLARE_EVENT_TABLE()

	public:
		struct ST_TaskInformation
		{
			;
		};

		CTaskView ( wxTaskView *task_view_ptr );
		~CTaskView ( void );
		void repaint ( wxDC &paint_dc );
		void selectTaskOnViewer ( const wxPoint & click_point );
		void autoRefresh ( unsigned interval )
		{
			Start ( interval );
		}
		void addTaskGroup( GROUP_HANDLE handle );

	private:
		struct ST_Item
		{
			ST_Item ( void ) = default;
			ST_Item ( GROUP_HANDLE handle )
				: task_group_handle ( handle )
			{
			}
			GROUP_HANDLE task_group_handle;
		};
		void drawProgressbar( wxDC &paint_dc, int x_offset, int y_offset,
								off_t position, off_t max_value );
		void drawTaskInformation ( wxDC &paint_dc,
								   int x_offset, int y_offset, int width,
								   const ITaskGroupManager::ST_GroupInformation &task_group_information,
								   bool is_select );

		void repaintItem ( wxDC &paint_dc, int item_top_offset, const ST_Item &item, bool is_select );
		bool getItemFromIndex ( int index, ST_Item &item );
		void getVisableItemRange ( int &start, int &end );
		wxString makeSizeString ( unsigned speed );
		void Notify ( void );	//from wxTimer

		wxTaskView *m_task_view_ptr;

		int m_item_height, m_selected_item_height;
		int m_selected_item_index;
		std::list<ST_Item> m_item_list;
};

#endif // TASKVIEW_CORE_H_INCLUDED
