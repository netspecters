#ifndef WXTREEVIEWCOMBOPOPUP_H_INCLUDED
#define WXTREEVIEWCOMBOPOPUP_H_INCLUDED

#include <wx/combo.h>
#include <wx/filepicker.h>
#include "wxTaskView.h"

class wxTreeViewComboPopup : public wxListBox, public wxComboPopup
{
		DECLARE_EVENT_TABLE()

	public:
		struct ST_InitParam
		{
			const wxTaskViewSpace::wxTaskView *task_view_ptr;
			wxDirPickerCtrl *dir_picker_ptr;
			wxStaticText *space_lable_ptr;
		};

		wxTreeViewComboPopup ( const ST_InitParam &init_param );
		bool Create ( wxWindow* parent );
		wxWindow* GetControl ( void );
		wxString GetStringValue ( void ) const;
		void Init ( void );
		void SetStringValue ( const wxString& value );
		wxSize GetAdjustedSize(int minWidth, int prefHeight, int maxHeight);

		void onMouseMove ( wxMouseEvent &event );
		void onMouseClick ( wxMouseEvent &event );

		wxTaskViewSpace::ST_Node *getNodePtrOfSelectedItem ( void );

	private:
		void fillList ( const wxTaskViewSpace::ST_Node *root_node_ptr, int level );
		wxTaskViewSpace::ST_Node *getNodePtrFromIndex(int index) const;

		const ST_InitParam m_param;
		int m_item_under_mouse;
};

#endif // WXTREEVIEWCOMBOPOPUP_H_INCLUDED
