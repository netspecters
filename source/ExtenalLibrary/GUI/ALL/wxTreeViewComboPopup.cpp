#include "wxTreeViewComboPopup.h"
#include "GUI_RAD.h"
#include "GlobalVars.h"
#include <wx/filename.h>

BEGIN_EVENT_TABLE(wxTreeViewComboPopup, wxListBox)
	EVT_MOTION(wxTreeViewComboPopup::onMouseMove)
	EVT_LEFT_DOWN(wxTreeViewComboPopup::onMouseClick)
END_EVENT_TABLE()

wxTreeViewComboPopup::wxTreeViewComboPopup(const ST_InitParam &init_param)
	: m_param(init_param)
{
}

bool wxTreeViewComboPopup::Create(wxWindow *parent)
{
	if(wxListBox::Create(parent, wxID_SELECT_VIEW, wxDefaultPosition, wxDefaultSize, 0, wxLB_SINGLE | wxLB_NEEDED_SB))
	{
		wxTaskViewSpace::ST_Node *root_node_ptr = m_param.task_view_ptr->getRootNodePtr();

		wxListBox::Append(root_node_ptr->name, (void*) root_node_ptr);

		fillList(root_node_ptr, -2);
		return true;
	}
	else
		return false;
}

wxWindow *wxTreeViewComboPopup::GetControl(void)
{
	return this;
}

wxString wxTreeViewComboPopup::GetStringValue(void) const
{
	wxTaskViewSpace::ST_Node *node_ptr = getNodePtrFromIndex(m_item_under_mouse);
	if(node_ptr)
	{
		if(!node_ptr->filesystem_location.IsEmpty())
		{
			m_param.dir_picker_ptr->SetPath(node_ptr->filesystem_location);

			wxLongLong total, free;
			if(wxGetDiskSpace(node_ptr->filesystem_location, &total, &free))
			{
				wxULongLong size(free.GetHi(), free.GetLo());
				m_param.space_lable_ptr->SetLabel(wxFileName::GetHumanReadableSize(size));
			}
		}

		return node_ptr->name;
	}
	return wxEmptyString;
}

wxSize wxTreeViewComboPopup::GetAdjustedSize(int minWidth, int prefHeight, int maxHeight)
{
	return wxSize(minWidth, GetCharHeight() * (GetCount() + 2));
}

wxTaskViewSpace::ST_Node *wxTreeViewComboPopup::getNodePtrFromIndex(int index) const
{
	return index != wxNOT_FOUND ? (wxTaskViewSpace::ST_Node*) GetClientData(index) : NULL;
}

void wxTreeViewComboPopup::fillList(const wxTaskViewSpace::ST_Node *root_node_ptr, int level)
{
	if(!root_node_ptr) return;

	if(!root_node_ptr->filesystem_location.IsEmpty() && root_node_ptr->has_child)
	{
		wxString item_txt(wxT(' '), level);
		item_txt.append(root_node_ptr->name);
		wxListBox::Append(item_txt, (void*) root_node_ptr);
	}

	/* 遍历子对象 */
	wxTaskViewSpace::ST_Node *child_node_ptr = root_node_ptr->first_child_ptr;
	while(child_node_ptr)
	{
		fillList(child_node_ptr, level + 2);
		child_node_ptr = child_node_ptr->right_brother_node_ptr;
	}
}

void wxTreeViewComboPopup::Init(void)
{
	m_item_under_mouse = wxNOT_FOUND;
}

void wxTreeViewComboPopup::SetStringValue(const wxString &value)
{
	unsigned index = FindString(value);
	if(index >= 0 && index < wxListBox::GetCount())
		wxListBox::Select(index);
}


/** @brief Move selection to cursor if it is inside the popup
  *
  */
void wxTreeViewComboPopup::onMouseMove(wxMouseEvent &event)
{
	int itemHere = HitTest(event.GetPosition());
	if(itemHere >= 0)
		wxListBox::SetSelection(itemHere);

	event.Skip();
}

/**
 * @brief On mouse left, set the value and close the popup
 */
void wxTreeViewComboPopup::onMouseClick(wxMouseEvent &event)
{
	int itemHere = HitTest(event.GetPosition());
	wxTaskViewSpace::ST_Node *node_ptr = getNodePtrFromIndex(itemHere);
	if(node_ptr)
	{
		g_params.least_category.assign(node_ptr->name.ToUTF8());
		m_item_under_mouse = itemHere;
	}
	Dismiss();
}

/** @brief 获取当前选择项对应于TaskView中的node指针
  *
  *
  */
wxTaskViewSpace::ST_Node *wxTreeViewComboPopup::getNodePtrOfSelectedItem(void)
{
	return (wxTaskViewSpace::ST_Node*) GetClientData(GetSelection());
}
