#ifndef CANVAS_PCH_H_INCLUDED
#define CANVAS_PCH_H_INCLUDED

#ifndef __MINGW32__
#include <agg_basics.h>
#include <agg_array.h>
#include <agg_pixfmt_rgb.h>
#include <agg_pixfmt_rgba.h>
#include <agg_rendering_buffer.h>
#include <agg_renderer_scanline.h>
#include <agg_rasterizer_scanline_aa.h>
#include <agg_conv_contour.h>
#include <agg_conv_stroke.h>
#include <agg_conv_transform.h>
#include <agg_conv_smooth_poly1.h>
#include <agg_scanline_u.h>
#include <agg_path_storage.h>
#include <agg_gsv_text.h>
#include <agg_ellipse.h>
#include <agg_arc.h>
#include <agg_curves.h>
#include <agg_rounded_rect.h>
#include <agg_trans_single_path.h>
#include <agg_span_solid.h>
#include <agg_span_interpolator_linear.h>
#include <agg_span_gradient.h>
#include <agg_span_allocator.h>
#include <agg_span_image_filter_gray.h>
#include <agg_span_image_filter_rgb.h>
#include <agg_span_image_filter_rgba.h>
#include <agg_span_pattern_gray.h>
#include <agg_span_pattern_rgb.h>
#include <agg_span_pattern_rgba.h>

#if 0
#include <agg_font_cache_manager.h>
#if defined(_WIN32) || defined(_WIN64)
#	include <agg_font_win32_tt.h>
#else
#	include <agg_font_freetype.h>
#endif
#endif

#endif

#endif // CANVAS_PCH_H_INCLUDED
