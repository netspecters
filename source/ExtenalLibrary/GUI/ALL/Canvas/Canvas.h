#ifndef CANVAS_H_INCLUDED
#define CANVAS_H_INCLUDED

#include "pch.h"
#include "stdint.h"
#include <cmath>

#define BYTES_PER_PIX 3
#define BYTE3(x) (double)(((x) & 0xff000000)>>24)
#define BYTE2(x) (double)(((x) & 0x00ff0000)>>16)
#define BYTE1(x) (double)(((x) & 0x0000ff00)>>8)
#define BYTE0(x) (double)((x) & 0x000000ff)

class CCanvas;
class CCanvasWidgetsBase
{
		friend class CCanvas;

	public:
		CCanvasWidgetsBase(void) : m_color(0, 0, 0) {}
		void setColor(unsigned R, unsigned G, unsigned B)
		{
			m_color.r = B;
			m_color.g = G;
			m_color.b = R;
			m_color.a = agg::rgba8::base_mask;
		}

	private:
		agg::rgba8 m_color;
};

class CPen : public CCanvasWidgetsBase
{
		friend class CCanvas;

	public:
		CPen(void) : m_pen_width(1) {}

		void clear(void) {
			m_pen_path.free_all();
		}
		double getWidth(void) {
			return m_pen_width;
		}
		CPen &setWidth(double width) {
			m_pen_width = width;
			return *this;
		}

	private:
		double m_pen_width;
		agg::path_storage m_pen_path;
};

class CBrush : public CCanvasWidgetsBase
{
		friend class CCanvas;
		typedef agg::pod_auto_array<agg::rgba8, 256> TGradientColors;//The gradient color array

	public:
		CBrush(void)
			: m_max_num(0)
		{
		}
		bool useGradient(void)
		{
			return m_max_num > 0;
		}
		void setMaxGradientColorNum(int num)
		{
			m_max_num = num;
		}
		CBrush &setColorAt(unsigned position, unsigned R, unsigned G, unsigned B)
		{
			if(position < m_max_num)
			{
				agg::rgba8 *color_ptr = NULL;
				if(2 == m_max_num)	//2个单独处理
					switch(m_max_num)
					{
						case 0:
							color_ptr = &m_gradient_colors[0];
						case 1:
							color_ptr = &m_gradient_colors[255];
					}
				else
					color_ptr = &m_gradient_colors[getIndex(position)];

				if(color_ptr)
				{
					color_ptr->r = B;
					color_ptr->g = G;
					color_ptr->b = R;
				}
			}
			return *this;
		}
	private:
		void finish_set_color(void)
		{
			if(useGradient())
			{
				if(2 == m_max_num)	//2个单独处理
				{
					agg::rgba8 begin(m_gradient_colors[0]);
					agg::rgba8 end(m_gradient_colors[255]);
					for(unsigned i = 0; i < 256; i++)
						m_gradient_colors[i] = begin.gradient(end, (double)i / 256.0);
				}
				else
					for(unsigned i = 0; i < (m_max_num - 1); i++)
					{
						unsigned int begin = getIndex(i), end = getIndex(i + 1), len = end - begin;

						const agg::rgba8 current(m_gradient_colors[begin]);
						const agg::rgba8 next(m_gradient_colors[end]);
						int k = 0;
						for(unsigned j = begin; j < end; j++, k++)
							m_gradient_colors[j] = current.gradient(next, (double)k / len);
					}
			}
		}
		unsigned int getIndex(unsigned int position)
		{
			return ceil(((double)position / (double)(m_max_num - 1) * 255));
		}

		unsigned int m_max_num;
		TGradientColors m_gradient_colors;
};


class CCanvas
{
	protected:
#if 0
#if defined(_WIN32) || defined(_WIN64)
		typedef agg::font_engine_win32_tt_int16 TFontEngine;
#else
		typedef agg::font_engine_freetype_int16 TFontEngine;
#endif
#endif
		typedef agg::pixfmt_bgr24				TPixFormat;
		typedef agg::rendering_buffer			TRenderBuffer;
		typedef agg::renderer_base<TPixFormat>	TRender;

	public:
		struct ST_Rect
		{
			ST_Rect(void) : top(0.0), left(0.0), right(0.0), bottom(0.0) {}
			ST_Rect(double xleft, double ytop, double xright, double ybottom)
			{
				top = ytop;
				left = xleft;
				right = xright;
				bottom = ybottom;
			}
			void shrink(int s)
			{
				left += s;
				right -= s;
				top += s;
				bottom -= s;
			}
			double getWidth(void)
			{
				return right - left;
			}
			double getHeight(void)
			{
				return bottom - top;
			}
			double top, left, right, bottom;
		};
		struct ST_Point
		{
			ST_Point(void) : x(0.0), y(0.0) {}
			ST_Point(double X, double Y) : x(X), y(Y) {}
			void set(double X, double Y)
			{
				x = X;
				y = Y;
			}
			double x, y;
		};

		CCanvas(size_t width, size_t height, unsigned char *buffer_ptr)
			: m_rendering_buffer(buffer_ptr, width, height, width *BYTES_PER_PIX),
			m_pix_format(m_rendering_buffer), m_render(m_pix_format), m_lock(false)
		{
#if 0
			initFontEngine();
#endif
		}

		~CCanvas(void)
		{
		}

		void beginDraw(void) {
			m_lock = true;
		}
		void endDraw(void) {
			m_lock = false;
			render(m_pen.m_pen_path, false);
			m_pen.clear();
		}

		CPen &getPen(void) {
			return m_pen;
		}
		CBrush &getBrush(void) {
			return m_brush;
		}

		void clear(uint8_t color)
		{
			m_rendering_buffer.clear(color);
		}
		void clear(unsigned char R, unsigned char G, unsigned char B)
		{
			m_render.clear(agg::rgba8(R, G, B));
		}

		/**
		 * @name brush draw functions
		 * @{
		*/
		void drawRoundedRect(ST_Rect rect, double round, bool solid);
		void drawPolygon(ST_Point point_list[], int point_num, bool auto_close, double smooth_value, bool solid);
		void drawEllipse(ST_Point center, double r_x, double r_y, bool solid);
		/**
		 * @brief 画弧线
		 *
		 * @param center 中心点坐标
		 * @param r_x X轴半径
		 * @param r_y Y轴半径
		 * @param a1 起始角(rad 弧度)
		 * @param a2 终止角(rad 弧度)
		 * @param anticlockwise 弧形方向。当为true时则逆时针方向画弧，反之则顺时针
		 * @param solid 是否用画刷颜色做实心填充
		 */
		void drawArc(ST_Point center, double r_x, double r_y, double a1, double a2, bool anticlockwise, bool solid);
		///绘制线性渐变
		void drawLinearGradient();
		///@}

		/**
		 * @name pen draw functions
		 * @{
		*/
		void moveto(double X, double Y)
		{
			m_pen.m_pen_path.move_to(X, Y);
		}
		void lineto(double X, double Y)
		{
			m_pen.m_pen_path.line_to(X, Y);

			if(!m_lock)
				endDraw();
		}
		///@}

#if 0
		/* Font */
		bool loadFont(const char *font_file_name_str_ptr)
		{
#if defined(_WIN32) || defined(_WIN64)
			return m_font_engine.create_font(font_file_name_str_ptr, agg::glyph_ren_agg_gray8);
#else
			return m_font_engine.load_font(font_file_name_str_ptr, 0, agg::glyph_ren_agg_gray8);
#endif
		}
		void setFontHeight(float height)
		{
			m_font_engine.height(height);
		}
		void drawText(char *text_ptr, int xPos, int yPos);    //utf8
#endif


	protected:
#if 0
		void initFontEngine(void)
		{
			//m_font_engine.flip_y ( true );
			m_font_engine.hinting(true);
		}
#endif

		template<class T>
		void render_gradients(T &shape)
		{
			m_brush.finish_set_color();

			// Gradient shape function (linear, radial, custom, etc)
			//-----------------
			typedef agg::gradient_x gradient_func_type;	//使用x轴渐变

			// Span interpolator. This object is used in all span generators
			// that operate with transformations during iterating of the spans,
			// for example, image transformers use the interpolator too.
			//-----------------
			typedef agg::span_interpolator_linear<> interpolator_type;

			// Span allocator is an object that allocates memory for
			// the array of colors that will be used to render the
			// color spans. One object can be shared between different
			// span generators.
			//-----------------
			typedef agg::span_allocator<agg::rgba8> span_allocator_type;

			// Finally, the gradient span generator working with the agg::rgba8 color type.
			// The 4-th argument is the color function that should have
			// the [] operator returning the color in range of [0...255].
			// In our case it will be a simple look-up table of 256 colors.
			//-----------------
			typedef agg::span_gradient<agg::rgba8, interpolator_type, gradient_func_type, CBrush::TGradientColors> span_gradient_type;

			// The gradient scanline renderer type
			//-----------------
			typedef agg::renderer_scanline_aa<TRender, span_allocator_type, span_gradient_type> renderer_gradient_type;

			// The gradient objects declarations
			//----------------
			gradient_func_type  gradient_func;                   // The gradient function
			agg::trans_affine   gradient_mtx;                    // Affine transformer
			interpolator_type   span_interpolator(gradient_mtx); // Span interpolator
			span_allocator_type span_allocator;                  // Span Allocator

			// Declare the gradient span itself.
			// The last two arguments are so called "d1" and "d2"
			// defining two distances in pixels, where the gradient starts
			// and where it ends. The actual meaning of "d1" and "d2" depands
			// on the gradient function.
			//----------------
			span_gradient_type span_gradient(span_interpolator,
											 gradient_func,
											 m_brush.m_gradient_colors,
											 0, 100);

			// The gradient renderer
			//----------------
			renderer_gradient_type ren_gradient(m_render, span_allocator, span_gradient);


			agg::rasterizer_scanline_aa<> ras;
			agg::scanline_u8 scan_line;

			ras.add_path(shape);
			agg::render_scanlines(ras, scan_line, ren_gradient);
		}
		template<class T>
		void render(T &shape, bool solid)
		{
			if(solid && m_brush.useGradient())
				render_gradients(shape);
			else
			{
				agg::rasterizer_scanline_aa<> ras;
				agg::scanline_u8 scan_line;

				if(solid)
				{
					ras.add_path(shape);
					agg::render_scanlines_aa_solid(ras, scan_line, m_render, m_brush.m_color);
				}
				else
				{
					/* Coordinate conversion pipeline */
					agg::conv_stroke<T> conved_path(shape);
					conved_path.width(m_pen.m_pen_width);

					ras.add_path(conved_path);
					agg::render_scanlines_aa_solid(ras, scan_line, m_render, m_pen.m_color);
				}
			}
		}

		TRenderBuffer m_rendering_buffer;
		TPixFormat m_pix_format;
		TRender m_render;
		CPen m_pen;
		CBrush m_brush;

#if 0
		TFontEngine m_font_engine;
#endif
		bool m_lock;
};

#endif // CANVAS_H_INCLUDED
