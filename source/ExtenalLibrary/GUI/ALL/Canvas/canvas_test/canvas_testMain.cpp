/***************************************************************
 * Name:      canvas_testMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    Wang Guan ()
 * Created:   2010-04-20
 * Copyright: Wang Guan ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "canvas_testMain.h"
#define PI 3.14159265

canvas_testFrame::canvas_testFrame(wxFrame *frame): GUIFrame(frame)
{

}

canvas_testFrame::~canvas_testFrame()
{
}

void canvas_testFrame::OnClose(wxCloseEvent &event)
{
	Destroy();
}

void canvas_testFrame::onPanelPaint(wxPaintEvent &event)
{
#define USE_SOLID		true
#define NOT_USE_SOLD	false

	wxPaintDC dc(m_panel_ptr);
	CwxWrapCanvas canvas(dc);
	canvas.clear(0x88);

	canvas.getPen().setColor(0, 0, 50);
	canvas.getPen().setWidth(1.0);
	canvas.getBrush().setColor(0, 0, 50);

	canvas.moveto(0, 0);
	canvas.lineto(100, 100);
	canvas.moveto(100, 100);
	canvas.lineto(80, 200);

	//canvas.drawText("xx",1,1);

	/* 测试矩形 */
	canvas.drawRoundedRect(CCanvas::ST_Rect(150, 10, 350, 210), 0.0, NOT_USE_SOLD);
	canvas.drawRoundedRect(CCanvas::ST_Rect(150, 220, 350, 420), 15.0, USE_SOLID);

	/* 测试多边形 */
	CCanvas::ST_Point triangle_points[3];
	triangle_points[0].set(500, 10);
	triangle_points[1].set(400, 210);
	triangle_points[2].set(600, 210);
	canvas.drawPolygon(triangle_points, 3, true, 0.0, NOT_USE_SOLD);
	for(int i = 0; i < 3; i++)
		triangle_points[i].y += 210;
	canvas.drawPolygon(triangle_points, 3, true, 1.0, USE_SOLID);

	/* 测试圆形 */
	canvas.drawEllipse(CCanvas::ST_Point(80, 300), 50, 50, NOT_USE_SOLD);
	canvas.getBrush().setMaxGradientColorNum(3);
	canvas.getBrush().setColorAt(0, 0, 50, 50).setColorAt(1, 240, 255, 100).setColorAt(2, 80, 0, 0);
	canvas.drawEllipse(CCanvas::ST_Point(80, 440), 50, 80, USE_SOLID);
	canvas.getBrush().setMaxGradientColorNum(0);

	/* 测试弧形 */
	canvas.drawEllipse(CCanvas::ST_Point(220, 470), 3, 3, USE_SOLID);	//显示出弧形的中点
	canvas.drawArc(CCanvas::ST_Point(220, 470), 40, 40, (PI / 180) * 270, (PI / 180) * 60, true, NOT_USE_SOLD);

#if 0
	/* 测试图像 */
	wxImage image(wxT("/home/love_paraplegic/bin/Debug/para.bmp"));
	canvas.drawImage(image, CCanvas::ST_Rect(300, 470, 500, 670), 1.0);
#endif

	canvas.drawToDC();
	dc.DrawText(wxT("test"), 50, 550);

#undef USE_SOLID
#undef NOT_USE_SOLD
}
