/***************************************************************
 * Name:      canvas_testApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Wang Guan ()
 * Created:   2010-04-20
 * Copyright: Wang Guan ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "canvas_testApp.h"
#include "canvas_testMain.h"

IMPLEMENT_APP(canvas_testApp);

bool canvas_testApp::OnInit()
{
    canvas_testFrame* frame = new canvas_testFrame(0L);

    frame->Show();

    return true;
}
