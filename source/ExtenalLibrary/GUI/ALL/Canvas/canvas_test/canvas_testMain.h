/***************************************************************
 * Name:      canvas_testMain.h
 * Purpose:   Defines Application Frame
 * Author:    Wang Guan ()
 * Created:   2010-04-20
 * Copyright: Wang Guan ()
 * License:
 **************************************************************/

#ifndef canvas_testMAIN_H
#define canvas_testMAIN_H

#include "../wxWrap.hpp"

#include "canvas_testApp.h"

#include "GUIFrame.h"

class canvas_testFrame: public GUIFrame
{
	public:
		canvas_testFrame(wxFrame *frame);
		~canvas_testFrame();
	private:
		virtual void OnClose(wxCloseEvent &event);
		virtual void onPanelPaint(wxPaintEvent &event);
};

#endif // canvas_testMAIN_H
