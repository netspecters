/***************************************************************
 * Name:      canvas_testApp.h
 * Purpose:   Defines Application Class
 * Author:    Wang Guan ()
 * Created:   2010-04-20
 * Copyright: Wang Guan ()
 * License:
 **************************************************************/

#ifndef CANVAS_TESTAPP_H
#define CANVAS_TESTAPP_H

#include <wx/app.h>

class canvas_testApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // CANVAS_TESTAPP_H
