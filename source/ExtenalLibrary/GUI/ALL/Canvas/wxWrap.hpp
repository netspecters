#ifndef WXWRAP_HPP_INCLUDED
#define WXWRAP_HPP_INCLUDED

#include "Canvas.h"
#include <wx/dcmemory.h>

class CwxWrapCanvasBase
{
	protected:
		CwxWrapCanvasBase(int width, int height)
			: m_buffer(width, height)
		{
		}
		wxImage m_buffer;
};

class CwxWrapCanvas : public CwxWrapCanvasBase, public CCanvas
{
	public:
		static ST_Rect rect_from_wxrect(const wxRect &wx_rect)
		{
			return ST_Rect(wx_rect.x, wx_rect.y, wx_rect.x + wx_rect.width, wx_rect.y + wx_rect.height);
		}
		CwxWrapCanvas(wxDC &dc)
			: CwxWrapCanvasBase(dc.GetSize().GetWidth(), dc.GetSize().GetHeight()),
			CCanvas(dc.GetSize().GetWidth(), dc.GetSize().GetHeight(), m_buffer.GetData()),m_dc(dc)
		{
		}
		void drawToDC(void)
		{
			m_dc.DrawBitmap(wxBitmap(m_buffer), 0, 0, false);
		}

#if 0
		void drawImage(wxImage &image, ST_Rect dst_rect, double round)
		{
			unsigned src_image_width = image.GetWidth(), src_image_height = image.GetHeight();
			TRender render;
			agg::rasterizer_scanline_aa<> ras;
			agg::scanline_u8 scan_line;

			agg::rendering_buffer rbuf_img(image.GetData(), src_image_width, src_image_height,  src_image_width * BYTES_PER_PIX);
			agg::pixfmt_bgr24 pixf_img(rbuf_img);

			/* 线段分配器 */
			typedef agg::span_allocator<agg::rgba8> TSpanAllocator;//分配器类型
			TSpanAllocator span_allocator;

			/* 插值器 */
			typedef agg::span_interpolator_linear<> TSpanInterpolator; //插值器类型
			agg::trans_affine img_mtx; // 变换矩阵
			TSpanInterpolator span_interpolator(img_mtx); // 插值器

			/* 线段生成器 */
			typedef agg::span_image_filter_rgb_bilinear_clip<agg::pixfmt_bgr24, TSpanInterpolator > TSpanGenerator; //这个就是Span Generator
			TSpanGenerator span_generator(pixf_img, agg::rgba(0, 1, 0), span_interpolator);

			/* 组合成渲染器 */
			agg::renderer_scanline_aa<TRender, TSpanAllocator, TSpanGenerator> img_renderer(render, span_allocator, span_generator);

			/* 调整目标位置 */
			//img_mtx.scale(dst_rect.getWidth() / src_image_width, dst_rect.getHeight() / src_image_height);
			//img_mtx.translate(dst_rect.left, dst_rect.top);
			//img_mtx.invert();//插值器的矩阵运算是从目标位置向源位置计算的（即根据目标位置变换得到对应的填充源位置），所以想对源图像变换的话要最后调用矩阵的invert()方法取反

			/* 渲染 */
			agg::rounded_rect round_rect(dst_rect.left, dst_rect.top, dst_rect.right, dst_rect.bottom, round);
			ras.add_path(round_rect);
			agg::render_scanlines(ras, scan_line, img_renderer);
		}
#endif
	private:
		wxDC &m_dc;
};

#endif // WXWRAP_HPP_INCLUDED
