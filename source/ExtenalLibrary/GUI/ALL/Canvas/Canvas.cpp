#include "Canvas.h"
#include <vector>

#if 0
void CCanvas::drawText(char *text_ptr, int xPos, int yPos)
{
	typedef TFontEngine::path_adaptor_type TPathAdaptorType;

	TPathAdaptorType path_adaptor;
	agg::conv_curve<TPathAdaptorType> ccvs(path_adaptor);
	std::vector<agg::int8u> data;	/* 存放字体数据 */

	agg::scanline_u8 scan_line;
	agg::rasterizer_scanline_aa<> ras;

	for(; *text_ptr; text_ptr++)
	{
		/* 让字体引擎准备好字体数据 */
		if(!m_font_engine.prepare_glyph(*text_ptr))	continue;

		/* 把字体数据放到容器里 */
		data.resize(m_font_engine.data_size());
		m_font_engine.write_glyph_to(&data[0]);

		/* 从字体数据中得到Rasterizer */
		path_adaptor.init(&data[0], data.size(), xPos, yPos);

		xPos += m_font_engine.advance_x();
		yPos += m_font_engine.advance_y();
		ras.add_path(ccvs);
		agg::render_scanlines_aa_solid(ras, scan_line, m_render, m_pen.m_color);
	}
}
#endif

void CCanvas::drawRoundedRect(ST_Rect rect, double round, bool solid)
{
	agg::rasterizer_scanline_aa<> ras;
	agg::scanline_u8 scan_line;

	if(0.0 == round)
	{
		agg::path_storage rect_path;
		rect_path.move_to(rect.left, rect.top);
		rect_path.line_to(rect.right, rect.top);
		rect_path.line_to(rect.right, rect.bottom);
		rect_path.line_to(rect.left, rect.bottom);
		rect_path.line_to(rect.left, rect.top);

		render(rect_path, solid);
	}
	else
	{
		agg::rounded_rect round_rect(rect.left, rect.top, rect.right, rect.bottom, round);
		render(round_rect, solid);
	}
}

void CCanvas::drawPolygon(ST_Point point_list[], int point_num, bool auto_close, double smooth_value, bool solid)
{
	agg::path_storage polygon_path;
	agg::rasterizer_scanline_aa<> ras;
	agg::scanline_u8 scan_line;

	polygon_path.move_to(point_list[0].x, point_list[0].y);

	for(int i = 1; i < point_num; i++)
		polygon_path.line_to(point_list[i].x, point_list[i].y);

	if(auto_close)
		polygon_path.close_polygon();

	if(0.0 == smooth_value)
		render(polygon_path, solid);
	else
	{
		agg::conv_smooth_poly1_curve<agg::path_storage> smooth_curve(polygon_path);
		render(smooth_curve, solid);
	}
}

void CCanvas::drawEllipse(ST_Point center, double r_x, double r_y, bool solid)
{
	agg::ellipse ellipse(center.x, center.y, r_x, r_y);
	render(ellipse, solid);
}

void CCanvas::drawArc(ST_Point center, double r_x, double r_y, double a1, double a2, bool anticlockwise, bool solid)
{
	agg::arc arc(center.x, center.y, r_x, r_y, a1, a2, anticlockwise);
	render(arc, solid);
}
