#include "GUI.h"
#include "../../../NSErrorCode.h"
#include "../../../StdLib/LibraryMaco.hpp"

CNSGUI CNSGUI::static_m_Instance;

IMPLEMENT_APP_NO_MAIN( CNetspectersGUI )
#if (defined(_WIN32) || defined(_WIN64))
#include <windows.h>
BOOL APIENTRY DllMain( HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved )
{
#	ifdef DEBUG
#		error "Not allow debuging under Windows!"
#	endif
}
#else
int main( int argc, char **argv )
{
#	ifdef DEBUG
	NSGUI_INSTANCE.onLoad( NULL, NULL, 0, 0 );
	NSGUI_INSTANCE.onRun();
	NSGUI_INSTANCE.onUnload();
	return 0;
#	endif
}
#endif
IMPLEMENT_WX_THEME_SUPPORT

bool CNetspectersGUI::OnInit( void )
{
	//初始化国际语言系统
	m_locale.Init( wxLANGUAGE_DEFAULT, wxLOCALE_CONV_ENCODING );	//使用操作系统默认的语言和编码

	//主窗体
	m_main_form_ptr = new CMainForm_Core(( wxWindow* ) NULL );

	//拖动窗体
	m_drag_window_ptr = new CDragWindow_Core(( wxWindow* ) NULL );

	/* 设定位置 */
	wxPoint drag_window_new_pos( wxSystemSettings::GetMetric( wxSYS_SCREEN_X ) - m_drag_window_ptr->GetSize().GetWidth() - 64, 64 );
#ifndef DEBUG
	/* open the core database and local the record */
	HRECORD record_handle = NSGUI_INSTANCE.getPlusTreePtr()->openRecord( NSGUI_INSTANCE.getPlusDirHandle(), 0, 1 );

	/* read configure data from core database */
	int record_data_size = NSGUI_INSTANCE.getPlusTreePtr()->readRecord( record_handle, NULL, 0 );
	if( record_data_size > 0 )
	{
		uint8_t record_data_buf[record_data_size];
		NSGUI_INSTANCE.getPlusTreePtr()->readRecord( record_handle, record_data_buf, record_data_size );
		ST_Configure *configure_infor_ptr = reinterpret_cast<ST_Configure*>( record_data_buf );

		/* set windows' property */
		drag_window_new_pos.y = configure_infor_ptr->drag_window_top < 0 ? : configure_infor_ptr->drag_window_top;
		drag_window_new_pos.x = configure_infor_ptr->drag_window_left < 0 ? : configure_infor_ptr->drag_window_left;
	}
#endif
	m_drag_window_ptr->Move( drag_window_new_pos );

	SetTopWindow( m_drag_window_ptr );

	/* load tray */
	m_tray_ptr = new CTray( true );

	//show all forms
	m_drag_window_ptr->Show();
	m_main_form_ptr ->Show();

	return true;
}

int CNSGUI::onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param )
{
#ifdef DEBUG
	m_protocol_manager_ptr = new MockProtocolManager;
#else
	static_cast<IUIHost*>( plus_tree_ptr->getMainPlus() )->registerGUI( this );

	m_plus_tree_ptr = plus_tree_ptr;
	m_protocol_manager_ptr = static_cast<IProtocolManager*>( parent_plus_ptr );
	m_plus_dir_handle = plus_dir_handle;
#endif

	/* start the timer */
	Start( 1000 );
	return NSEC_SUCCESS;
}

void CNSGUI::onUnload( void )
{
#ifdef DEBUG
	delete static_cast<MockProtocolManager*>( m_protocol_manager_ptr );
#endif
}

int CNSGUI::getParam( const char *param_name, void *value_buffer_ptr )
{
	;
}
void *CNSGUI::setParam( const char *param_name, void *value_ptr )
{
	;
}

int CNSGUI::onRun( void )
{
	wxChar **g_cmd_param_ptr = NULL;
	int g_cmd_count = 0;
	return wxEntry( g_cmd_count, g_cmd_param_ptr );
}

void CNSGUI::kill( int exit_code )
{
	wxGetApp().ExitMainLoop();
}

#ifndef DEBUG
/* 要导出的函数 */
#	ifdef __cplusplus
extern "C" {
#	endif

	DLL_PUBLIC NSAPI int CreateInterface( int interface_index, INSPlus *&plus_interface_ptr )
	{
		plus_interface_ptr = &NSGUI_INSTANCE;
		return NSEC_SUCCESS;
	}

	DLL_PUBLIC NSAPI int DestroyInterface( INSPlus *plus_interface_ptr )
	{
		;
	}

	DLL_PUBLIC NSAPI int DescribePlugin( ST_PluginInformation *plugin_infor_ptr )
	{
		plugin_infor_ptr->name = "GUI";
		plugin_infor_ptr->full_path_on_the_tree = "/sys/protocol_manager/";

		return NSEC_SUCCESS;
	}

#	ifdef __cplusplus
}
#	endif
#endif
