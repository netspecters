#ifndef __GUIPerformanceDialog_Core__
#define __GUIPerformanceDialog_Core__

#include "GUI_RAD.h"
#include "../../../Core/PlusInterface.h"

using namespace WXGUI;

class CPerformanceDialog_Core : public PerformanceDialog
{
	public:
		CPerformanceDialog_Core( wxWindow *parent );

	private:
		friend NSAPI bool on_enum_plus( const ST_PluginInformation*, intptr_t );
		virtual void onPanelChange( wxListbookEvent &event );
};

#endif // __GUIPerformanceDialog_Core__
