/*
 * =====================================================================================
 *
 *       Filename:  wxTaskView.cpp
 *
 *    Description:  implement of wxTaskView.h
 *
 *        Version:  1.0
 *        Created:  2010年03月04日 01时46分06秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */
#include "wxTaskView.h"
#include "GUI.h"
#include <assert.h>

using namespace wxTaskViewSpace;

wxTaskView::wxTaskView( const ST_Child &child,
						ITaskViewNotify *task_view_notify_ptr )
	: m_child( child ), m_task_view_notify_ptr( task_view_notify_ptr ),
	m_root_node_ptr( NULL ), m_active_node_list_head_ptr( NULL ),
	m_level( 0 )
{
	child.left_view_ptr->registerClickEventHandler( this, &wxTaskView::OnLeftViewSelect );
	child.right_view_ptr->registerClickEventHandler( this, &wxTaskView::OnRightViewSelect );
	child.path_view_ptr->registerClickEventHandler( this );
}

wxTaskView::~wxTaskView( void )
{
	//removeNode(m_root_node_ptr);
}

void wxTaskView::addActiveNode( ST_ActiveNode *active_node_ptr )
{
	if( m_active_node_list_head_ptr )
		m_active_node_list_head_ptr->addBrother( active_node_ptr );
	m_active_node_list_head_ptr = active_node_ptr;
}

void wxTaskView::removeNode( ST_Node *node_ptr )
{
	/* remove from node list */
	assert( NULL != node_ptr->parent_ptr );	//根节点是不允许删除的
	node_ptr->parent_ptr->remove_child( node_ptr );

	/* remove from active list */
	if( node_ptr->has_active_data )
	{
		ST_ActiveNode *active_node_ptr = static_cast<ST_ActiveNode*>( node_ptr );
		if( active_node_ptr->in_active_list_privious_ptr )
			active_node_ptr->in_active_list_privious_ptr->in_active_list_next_ptr = active_node_ptr->in_active_list_next_ptr;
		else
			m_active_node_list_head_ptr = active_node_ptr->in_active_list_next_ptr;
	}

	/* remove all child */
	if( node_ptr->has_child )
	{
		ST_Node *current_child_ptr = node_ptr->first_child_ptr;
		while( current_child_ptr )
		{
			ST_Node *store_ptr = current_child_ptr->right_brother_node_ptr;
			removeNode( current_child_ptr );
			current_child_ptr = store_ptr;
		}
	}

	delete node_ptr;
}

ST_Node *wxTaskView::loadNodeFromConfigFile( ptree *ptree_ptr, ST_Node *parent_node_ptr, string node_path )
{
	bool is_actived = ptree_ptr->get<bool> ( node_path + ".is_actived", false );
	bool has_child = ptree_ptr->get<bool> ( node_path + ".has_child", false );
	bool is_empty = ptree_ptr->get<bool> ( node_path + ".is_empty", true );
	ST_Node *new_node_ptr;

	/* check which type of node need to be created */
	if( is_actived )
	{
		ST_ActiveNode *new_active_node_ptr = new ST_ActiveNode( parent_node_ptr );
		uint32_t node_key = ptree_ptr->get<uint32_t> ( node_path + ".group_id", 0 );

		/* attach this node to the list of active node */
		addActiveNode( new_active_node_ptr );

		/* add to key table */
		m_active_node_key_table.insert( std::make_pair( node_key, new_active_node_ptr ) );
		new_active_node_ptr->taskmanager_group_id = node_key;

		new_node_ptr = new_active_node_ptr;
	}
	else
		new_node_ptr = new ST_Node( parent_node_ptr );

	new_node_ptr->has_active_data = is_actived;
	new_node_ptr->has_child = has_child;

	/* read some properties */
	new_node_ptr->filesystem_location = wxString( ptree_ptr->get<string> ( node_path + ".fs_location", "" ).c_str(), wxConvUTF8 );
	new_node_ptr->name = wxString( ptree_ptr->get<string> ( node_path + ".name", "" ).c_str(), wxConvUTF8 );

	if( has_child && !is_empty )
	{
		node_path.append( ".items" );
		/* walk dir to find the file node */
		ptree &child_tree = ptree_ptr->get_child( node_path );
		for( auto itr = child_tree.begin(); itr != child_tree.end(); ++itr )
			loadNodeFromConfigFile( ptree_ptr, new_node_ptr, node_path + '.' + itr->first );
	}

	return new_node_ptr;
}

void wxTaskView::saveNodeToConfigFile( ptree *ptree_ptr, ST_Node *node_ptr, string node_path )
{
	ptree_ptr->put<bool> ( node_path + ".is_actived", node_ptr->has_active_data );
	ptree_ptr->put<bool> ( node_path + ".is_empty", node_ptr->first_child_ptr == NULL );
	ptree_ptr->put<bool> ( node_path + ".has_child", node_ptr->has_child );
	ptree_ptr->put<string> ( node_path + ".fs_location", string( node_ptr->filesystem_location.ToUTF8() ) );
	ptree_ptr->put<string> ( node_path + ".name", string( node_ptr->name.ToUTF8() ) );

	if( node_ptr->has_active_data )
		ptree_ptr->put<uint32_t> ( node_path + ".group_id", static_cast<ST_ActiveNode*>( node_ptr )->taskmanager_group_id );

	ST_Node *child_node_ptr = node_ptr->first_child_ptr;
	while( child_node_ptr )
	{
		string child_node_name( child_node_ptr->name.ToUTF8() );
		saveNodeToConfigFile( ptree_ptr, child_node_ptr, node_path + ".items." + child_node_name );

		child_node_ptr = child_node_ptr->right_brother_node_ptr;
	}
}

/* 生成获取选择节点信息代码的宏 */
#define __SELECT_ITEM_INFORMATION(select_node_ptr,selected_task_view_item_ptr)\
	ST_Node *select_node_ptr = NULL;\
	wxTaskViewItem *selected_task_view_item_ptr;\
	if(wxWindow::FindFocus() == m_child.left_view_ptr)\
	{\
		selected_task_view_item_ptr = m_child.left_view_ptr;\
		select_node_ptr = m_child.left_view_ptr->getNodePointerFromIndex(m_child.left_view_ptr->GetSelection());\
	}\
	else if(wxWindow::FindFocus() == m_child.right_view_ptr)\
	{\
		selected_task_view_item_ptr = m_child.right_view_ptr;\
		select_node_ptr = m_child.right_view_ptr->getNodePointerFromIndex(m_child.right_view_ptr->GetSelection());\
	}

void wxTaskView::deleteSelectedItemes( bool real_del )
{
	__SELECT_ITEM_INFORMATION( select_node_ptr, selected_task_view_item_ptr )

	if( select_node_ptr )
	{
		if( select_node_ptr->has_child )
		{
			if( wxYES == wxMessageBox( _( "您正在试图删除一个目录，\n删除目录会同时将目录下文件一并删除。\n是否确定要删除目录？" ), _( "删除目录" ), wxICON_WARNING | wxYES_NO ) )
			{
				removeNode( select_node_ptr );
				size_t new_item_count = selected_task_view_item_ptr->GetItemCount() - 1;
				if( new_item_count > 0 )
				{
					selected_task_view_item_ptr->SetItemCount( new_item_count );
					selected_task_view_item_ptr->Refresh();
				}
				else
					selected_task_view_item_ptr->setRoot( NULL );
			}
		}
		else
		{
			/* 检测节点状态 */
		}
	}
}

void wxTaskView::addItem( const ST_ActiveNode &active_node )
{
	if( active_node.has_child ) /* a dir need to be added */
	{
		/* get the parent */
		int index = -1;
		ST_Node *parent_node_ptr = getNextSelectedNodePtr( index );
		if( !parent_node_ptr )
			parent_node_ptr = m_child.left_view_ptr->getRoot();

		/* create and append to parent node */
		ST_Node *node_ptr = new ST_Node( parent_node_ptr );

		node_ptr->filesystem_location	= active_node.filesystem_location;
		node_ptr->name 				= active_node.name;
		node_ptr->has_child				= true;
		node_ptr->first_child_ptr		= NULL;
		node_ptr->has_active_data		= false;
	}
	else
	{
		/* create and append to parent node */
		ST_ActiveNode *new_active_node_ptr = new ST_ActiveNode( active_node.parent_ptr );

		new_active_node_ptr->filesystem_location		= active_node.filesystem_location;
		new_active_node_ptr->name						= active_node.name;
		new_active_node_ptr->taskmanager_group_id		= active_node.taskmanager_group_id;
		new_active_node_ptr->taskmanager_group_handle	= active_node.taskmanager_group_handle;
		new_active_node_ptr->has_child					= false;
		new_active_node_ptr->first_child_ptr			= NULL;
		new_active_node_ptr->has_active_data			= true;

		/* append to parent node */
		addActiveNode( new_active_node_ptr );
	}

	m_child.left_view_ptr->resetRoot();
	m_child.right_view_ptr->resetRoot();
}

ITEM_HANDLE wxTaskView::findItem( uint32_t item_tag )
{
	auto itr = m_active_node_key_table.find( item_tag );
	return itr != m_active_node_key_table.end() ? reinterpret_cast<ITEM_HANDLE>( itr->second ) : NS_INVALID_HANDLE ;
}

void wxTaskView::pauseSelectedItemes( void )
{
	toggleSelectedItemesRunningState( true );
}

void wxTaskView::startSelectedItemes( void )
{
	toggleSelectedItemesRunningState( false );
}

void wxTaskView::toggleSelectedItemesRunningState( bool pause )
{
	__SELECT_ITEM_INFORMATION( select_node_ptr, selected_task_view_item_ptr )

	if( select_node_ptr )
	{
		assert( select_node_ptr->has_active_data );

		ST_ActiveNode *active_node_ptr = static_cast<ST_ActiveNode*>( select_node_ptr );
		NSGUI_INSTANCE.getTaskGroupManagerPtr()->switchGroupRunningState( active_node_ptr->taskmanager_group_handle, pause );

		/* set toolbar button state */
		m_task_view_notify_ptr->onSetToolbarButtonState( !pause, pause );
	}
}

/** @brief 用默认参数创建一个新的配置文件
  *
  */
void wxTaskView::createNewConfigure( const string &config_filename )
{
#define NODE_NUM 3

	/* first, try to make dir */
	string download_path_std;
#ifdef DEBUG
	download_path_std.assign(( wxGetCwd() + wxT( "/Download" ) ).ToUTF8() );
#else
	{
		size_t download_path_length = NSGUI_INSTANCE.getPlusTreePtr()->getPlusTreeWorkPath( NULL, 0 );
		char buffer[download_path_length + 1];
		NSGUI_INSTANCE.getPlusTreePtr()->getPlusTreeWorkPath( buffer, download_path_length );
		download_path_std.append( buffer );
		download_path_std.append( "/Download" );
	}
#endif
	wxString download_path( download_path_std.c_str(), wxConvUTF8 );
	if( !wxDir::Exists( download_path ) )
		wxMkdir( download_path );

	wxString sub_dirs[NODE_NUM] = {
		wxString( download_path + wxT( "/Down" ) ),
		wxString( download_path + wxT( "/Movie" ) ),
		wxString( download_path + wxT( "/Music" ) ),
	};
	for( int i = 0; i < NODE_NUM; i++ )
		if( !wxDir::Exists( sub_dirs[i] ) )
			wxMkdir( sub_dirs[i] );

	/* second, make node */
	ST_Node root_node;
	root_node.parent_ptr = NULL;
	root_node.has_active_data = false;
	root_node.has_child = true;

	ST_Node dir_nodes[NODE_NUM];
	ST_Node *last_node_ptr = NULL;
	for( int i = 0; i < NODE_NUM; i++ )
	{
		dir_nodes[i].parent_ptr					= &root_node;
		dir_nodes[i].has_child					= true;
		dir_nodes[i].first_child_ptr			= NULL;
		dir_nodes[i].left_brother_node_ptr		= last_node_ptr;
		dir_nodes[i].right_brother_node_ptr	= NULL;
		dir_nodes[i].has_active_data			= false;
		dir_nodes[i].filesystem_location.assign( sub_dirs[i] );
		if( last_node_ptr )
			last_node_ptr->right_brother_node_ptr = & ( dir_nodes[i] );
		last_node_ptr = & ( dir_nodes[i] );
	}
	dir_nodes[0].name.assign( _( "下载" ) );
	dir_nodes[1].name.assign( _( "音乐" ) );
	dir_nodes[2].name.assign( _( "视频" ) );

	root_node.first_child_ptr = & ( dir_nodes[0] );

	ptree pt;
	saveNodeToConfigFile( &pt, &root_node, "ns_gui.taskes.root" );
	write_xml( config_filename, pt );
#undef NODE_NUM
}

bool wxTaskView::loadConfigure( const wxString &config_filename )
{
	/* == load task from file == */
	ptree pt;
	string filename( config_filename.ToUTF8() );

	/* check file whether exist */
	if( !wxFileName::FileExists( config_filename ) )
	{
		if( wxYES == wxMessageBox( _( "目录配置文件不存在，是否自动建立配置文件？" ), _( "配置文件不存在" ), wxYES_NO | wxICON_EXCLAMATION ) )
			createNewConfigure( filename );
		else
			return false;
	}

	try
	{
		read_xml( filename, pt );
	}
	catch( xml_parser_error &e )
	{
		if( wxYES == wxMessageBox( _( "读取配置文件失败，是否重建配置文件？" ), _( "加载异常" ), wxYES_NO | wxICON_EXCLAMATION ) )
		{
			createNewConfigure( filename );
			read_xml( filename, pt );
		}
		else
			return false;
	}
	m_root_node_ptr = loadNodeFromConfigFile( &pt, NULL, string( "ns_gui.taskes.root" ) );
	m_root_node_ptr->filesystem_location.Empty();
	m_root_node_ptr->name.assign( _( "未分类" ) );

	/* == restore task from task-group-manager == */
	size_t buf_size = NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupHandleList( NULL );
	GROUP_HANDLE *handle_buf_ptr = static_cast<GROUP_HANDLE*>( alloca( buf_size ) );
	NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupHandleList( handle_buf_ptr );
	for( size_t i = 0; i < ( buf_size / sizeof( GROUP_HANDLE ) ); ++i, ++handle_buf_ptr )
	{
		uint32_t group_id = NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupID( *handle_buf_ptr );
		ITEM_HANDLE item_handle = findItem( group_id );
		if( NS_INVALID_HANDLE != item_handle )
			reinterpret_cast<ST_ActiveNode*>( item_handle )->taskmanager_group_handle = *handle_buf_ptr;
		else
		{
			//TODO
		}
	}

	/* set view item root node */
	m_child.left_view_ptr->setRoot( m_root_node_ptr );
	return true;
}

void wxTaskView::saveConfigure( const wxString &config_filename )
{
	ptree pt;

	saveNodeToConfigFile( &pt, m_root_node_ptr, "ns_gui.taskes.root" );

	/* == save information to file == */
	string filename( config_filename.ToUTF8() );
	write_xml( filename, pt );
}

/**
 * @brief 根据选择的条目设置工具栏按钮的状态
*/
void wxTaskView::setToolbarButtonState( ST_Node *node_ptr )
{
	enum __ENM_ButtonTages {btStart, btPause};
	bool tages[2] = {!node_ptr->has_child, !node_ptr->has_child};

	if( node_ptr->has_active_data )
	{
		ST_ActiveNode *active_node_ptr = static_cast<ST_ActiveNode*>( node_ptr );

		/* get task group information */
		ITaskGroupManager::ST_GroupInformation task_group_information;
		task_group_information.no_statistic = true;
		NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupStatistics( active_node_ptr->taskmanager_group_handle, &task_group_information );

		tages[btPause] = task_group_information.group_state == ITaskGroupManager::gsRunning;
		tages[btStart] = !tages[btPause];
	}

	/* set toolbar button state */
	m_task_view_notify_ptr->onSetToolbarButtonState( tages[btStart], tages[btPause] );
}

void wxTaskView::OnLeftViewSelect( int select_index )
{
	ST_Node *select_node_ptr = m_child.left_view_ptr->getNodePointerFromIndex( select_index );
	if( select_node_ptr )
	{
		if( select_node_ptr->has_child )
		{
			if( m_level ) m_level--;

			m_child.right_view_ptr->setRoot( select_node_ptr );

			/* scroll right */
			m_child.path_view_ptr->pop();
			m_child.path_view_ptr->push( select_node_ptr );
		}
		else
			m_child.right_view_ptr->setRoot( NULL );

		setToolbarButtonState( select_node_ptr );
	}
	else
		m_child.right_view_ptr->setRoot( NULL );
}


void wxTaskView::OnRightViewSelect( int select_index )
{
	ST_Node *select_node_ptr = m_child.right_view_ptr->getNodePointerFromIndex( select_index );
	if( select_node_ptr )
	{
		if( select_node_ptr->has_child )
		{
			m_level++;
			m_child.left_view_ptr->setRoot( m_child.right_view_ptr->getRoot(), m_child.right_view_ptr );
			m_child.right_view_ptr->setRoot( select_node_ptr );

			/* scroll left */
			/////////////

			m_child.path_view_ptr->push( select_node_ptr );
		}

		setToolbarButtonState( select_node_ptr );
	}
}

void wxTaskView::onPathSelect( ST_Node *need_select_node_ptr )
{
	m_child.left_view_ptr->setRoot( need_select_node_ptr->parent_ptr );
	m_child.right_view_ptr->setRoot( need_select_node_ptr, m_child.left_view_ptr, false );
}

void wxTaskView::onTaskViewFocus( const wxTaskViewItem *item_ptr )
{
	if( !item_ptr->hasFocus() )
	{
		if( !m_child.left_view_ptr->hasFocus() && !m_child.right_view_ptr->hasFocus() )
		{
			m_child.left_view_ptr->SetSelection( -1 );
			m_child.right_view_ptr->SetSelection( -1 );
		}
	}
}

ST_Node *wxTaskView::getNextSelectedNodePtr( int &id )
{
	unsigned long cookie = id;
	ST_Node *node_ptr = NULL;
	long select_index = m_child.left_view_ptr->GetSelectedCount() > 1 ?
						id == -1 ? m_child.left_view_ptr->GetFirstSelected( cookie ) : m_child.left_view_ptr->GetNextSelected( cookie ) :
							m_child.left_view_ptr->GetSelection();

	if( select_index != wxNOT_FOUND )
		node_ptr = m_child.left_view_ptr->getNodePointerFromIndex( select_index );
	else if(( select_index = m_child.right_view_ptr->GetSelectedCount() > 1 ?
							 id == -1 ? m_child.right_view_ptr->GetFirstSelected( cookie ) : m_child.right_view_ptr->GetNextSelected( cookie ) :
								 m_child.right_view_ptr->GetSelection() ) != wxNOT_FOUND )
			node_ptr = m_child.right_view_ptr->getNodePointerFromIndex( select_index );

	id = cookie;
	return node_ptr;
}

bool wxTaskView::selectedItemIsDir( void )
{
	int index = -1;
	ST_Node *node_ptr = getNextSelectedNodePtr( index );
	return node_ptr ? node_ptr->has_child : false;
}

void wxTaskView::update_active_item( void )
{
	ST_ActiveNode *active_node_ptr = m_active_node_list_head_ptr;
	while( active_node_ptr )
	{
		active_node_ptr->task_group_information.no_statistic = false;
		NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupStatistics( active_node_ptr->taskmanager_group_handle, &active_node_ptr->task_group_information );
		active_node_ptr->task_percent = active_node_ptr->task_group_information.recved_data_size * 100.0 / active_node_ptr->task_group_information.group_file_size;

		active_node_ptr = active_node_ptr->in_active_list_next_ptr;
	}
}
