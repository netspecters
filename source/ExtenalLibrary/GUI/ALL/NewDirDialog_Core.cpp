#include "NewDirDialog_Core.h"

CNewDirDialog_Core::CNewDirDialog_Core( wxWindow *parent )
	: NewDirDialog( parent )
{

}

void CNewDirDialog_Core::onOKButtonClick(wxCommandEvent& event)
{
	if(m_dir_name_ptr->GetValue().IsEmpty())
	{
		wxMessageBox(_("请填入类别名称"), _("错误"), wxOK | wxICON_ERROR, this);
		m_dir_name_ptr->SetFocus();
		return;
	}

	Close();
}
