#ifndef __NewJobDialog_Core__
#define __NewJobDialog_Core__

#include "wxTreeViewComboPopup.h"
#include "GUI_RAD.h"
#include "CapDialog_Core.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"
#include <string>

using namespace WXGUI;
using namespace Netspecters::KernelPlus::ProtocolManager;

struct ST_NewJobParam : public IProtocol::ST_TaskResource
{
	wxTaskViewSpace::ST_ActiveNode active_node;
	wxString comment;
	std::string res_path, login_usr_name, login_passwd;

	void assignToPtr(void)
	{
		res_path_ptr = res_path.c_str();
		if(!login_usr_name.empty())
			login_usr_name_ptr = login_usr_name.c_str();
		if(!login_passwd.empty())
			login_passwd_ptr = login_passwd.c_str();
	}
};

class CNewJobDialog_Core : public NewJobDialog
{
	public:
		CNewJobDialog_Core(wxWindow *parent, const wxTaskViewSpace::wxTaskView &task_view);

		/* 参数 */
		ST_NewJobParam new_job_param;

	private:
		void onURLInput(wxCommandEvent &event);
		void onShowCapDialog(wxCommandEvent &event);
		void onDirChange(wxFileDirPickerEvent &event);
		void onUseMultiThreadCheckBox(wxCommandEvent &event);
		void onOkButtonClick(wxCommandEvent &event);
		void onRetryCheckBox(wxCommandEvent &event);
		void onLoginServerCheckBox(wxCommandEvent &event);
		void onCommondTextClick(wxMouseEvent &event);
		void onShowMoreButtonClick(wxCommandEvent &event);

		bool fillParam(void);

		wxSize m_stored_size;
		wxURI m_uri;
};

#endif // __NewJobDialog_Core__
