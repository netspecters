#ifndef GLOBALVARS_H_INCLUDED
#define GLOBALVARS_H_INCLUDED

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using boost::property_tree::ptree;
using std::string;

struct ST_GParams
{
	unsigned task_thread_limit;
	unsigned retry_times_limit;
	unsigned source_server_thread_limit;
	string least_category;

	void load(const string &filename)
	{
		ptree pt;

		read_xml(filename, pt);

#define LOAD_FIELD(field_name,default_value) field_name = pt.get("ns_gui.global."#field_name, default_value)
		LOAD_FIELD(task_thread_limit, 10);
		LOAD_FIELD(retry_times_limit, 0);
		LOAD_FIELD(least_category, "");
#undef LOAD_FIELD
	}
	void save(const string &filename)
	{
		ptree pt;

#define STORE_FIELD(field_name) pt.put("ns_gui.global."#field_name, field_name)
		STORE_FIELD(task_thread_limit);
		STORE_FIELD(retry_times_limit);
		STORE_FIELD(least_category);
#undef STORE_FIELD

		write_xml(filename, pt);
	}
};

static ST_GParams g_params;

#endif // GLOBALVARS_H_INCLUDED
