/***************************************************************
 * Name:      wx_pch.h
 * Purpose:   Header to create Pre-Compiled Header (PCH)
 * Author:    Wang Guan ()
 * Created:   2009-07-18
 * Copyright: Wang Guan ()
 * License:	Private
 **************************************************************/

#ifndef WX_PCH_H_INCLUDED
#define WX_PCH_H_INCLUDED

// basic wxWidgets headers
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#ifdef WX_PRECOMP
    // put here all your rarely-changing header files
    //#include "Canvas/pch.h"
    #include <wx/dcbuffer.h>
    #include <wx/wupdlock.h>
    #include <wx/filename.h>
    #include <wx/dnd.h>
	#include <wx/dataobj.h>
	#include <wx/control.h>
	#include <wx/vlbox.h>
	#include <wx/splitter.h>
	#include <wx/renderer.h>
	#include <wx/mimetype.h>
	#include <wx/msgdlg.h>
	#include <wx/gdicmn.h>
	#include <wx/dir.h>
	#include <wx/uri.h>
	#include <wx/taskbar.h>
	#include <wx/artprov.h>
	//#include <wx/kbdstate.h> //2.9
#endif // WX_PRECOMP

#endif // WX_PCH_H_INCLUDED
