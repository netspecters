#include "DragWindow_Core.h"
#include "GUI.h"

bool CDragWindow_Core::CDataObject::GetDataHere( const wxDataFormat &format, void *buf ) const
{
}

size_t CDragWindow_Core::CDataObject::GetDataSize( const wxDataFormat &format ) const
{

}

void CDragWindow_Core::CDataObject::GetAllFormats( wxDataFormat *formats, Direction dir ) const
{

}

size_t CDragWindow_Core::CDataObject::GetFormatCount( Direction dir ) const
{
	return 0;
}

wxDataFormat CDragWindow_Core::CDataObject::GetPreferredFormat( Direction dir ) const
{

}

bool CDragWindow_Core::CDropTarget::GetData( void )
{

}

wxDragResult CDragWindow_Core::CDropTarget::OnData( wxCoord x, wxCoord y, wxDragResult def )
{
	return def;
}

bool CDragWindow_Core::CDropTarget::OnDrop( wxCoord x, wxCoord y )
{
	return true;
}

CDragWindow_Core::CDragWindow_Core( wxWindow *parent )
	: DragWindow( parent )
{
	SetWindowStyleFlag( wxFRAME_NO_TASKBAR | GetWindowStyleFlag() );
	SetTransparent( 240 );

	/* set this window is drop target */
	SetDropTarget( new CDropTarget( new CDataObject ) );

	/* 加入信号槽 */
	NSGUI_INSTANCE.getSignal().connect( std::bind( &CDragWindow_Core::update, this ) );
}

void CDragWindow_Core::OnMouseMove( wxMouseEvent &event )
{
	if( event.Dragging() && event.LeftIsDown() )
	{
		wxPoint pt = ClientToScreen( event.GetPosition() );
		int x = pt.x - m_delta.x;
		int y = pt.y - m_delta.y;
		Move( x, y );
	}
}

void CDragWindow_Core::OnMouseLeftDown( wxMouseEvent &event )
{
	CaptureMouse();
	wxPoint pt = ClientToScreen( event.GetPosition() );
	wxPoint origin = GetPosition();
	int dx = pt.x - origin.x;
	int dy = pt.y - origin.y;
	m_delta = wxPoint( dx, dy );
}

void CDragWindow_Core::OnMouseLeftUp( wxMouseEvent &event )
{
	if( HasCapture() )
	{
		ReleaseMouse();
	}
}

void CDragWindow_Core::onTaggleMainWindowVisable( wxCommandEvent &event )
{
	wxGetApp().toggleMainWindowVisable( !event.IsChecked() );
}

void CDragWindow_Core::OnPressExit( wxCommandEvent &event )
{
	NSGUI_INSTANCE.internalClose();
}

void CDragWindow_Core::onMouseLeave( wxMouseEvent &event )
{

}

void CDragWindow_Core::onMouseEnter( wxMouseEvent &event )
{

}

void CDragWindow_Core::update( void )
{

}
