///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version May 25 2010)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif //WX_PRECOMP

#include "GUI_RAD.h"

///////////////////////////////////////////////////////////////////////////
using namespace WXGUI;

MainForm::MainForm( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 600,650 ), wxDefaultSize );
	
	m_menubar1 = new wxMenuBar( 0 );
	menu_file = new wxMenu();
	wxMenuItem* newtask;
	newtask = new wxMenuItem( menu_file, ID_NEW_TASK, wxString( _("新建...") ) , wxEmptyString, wxITEM_NORMAL );
	menu_file->Append( newtask );
	
	wxMenuItem* set_performance;
	set_performance = new wxMenuItem( menu_file, ID_SET_PERFORMANCE, wxString( _("参数...") ) , wxEmptyString, wxITEM_NORMAL );
	menu_file->Append( set_performance );
	
	wxMenuItem* m_separator1;
	m_separator1 = menu_file->AppendSeparator();
	
	wxMenuItem* exit;
	exit = new wxMenuItem( menu_file, ID_EXIT, wxString( _("退出") ) , wxEmptyString, wxITEM_NORMAL );
	menu_file->Append( exit );
	
	m_menubar1->Append( menu_file, _("文件") ); 
	
	this->SetMenuBar( m_menubar1 );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	bSizer1->SetMinSize( wxSize( 550,-1 ) ); 
	m_toolbar_ptr = new wxToolBar( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL|wxTB_NOICONS|wxTB_TEXT ); 
	m_toolbar_ptr->AddTool( ID_NEW_TASK, _("新建"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, _("新建任务"), wxEmptyString ); 
	m_toolbar_ptr->AddTool( ID_DEL_TASK, _("删除"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolbar_ptr->AddSeparator(); 
	m_toolbar_ptr->AddTool( ID_TASK_START, _("开始"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolbar_ptr->AddTool( ID_TASK_PAUSE, _("暂停"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolbar_ptr->AddSeparator(); 
	m_toolbar_ptr->AddTool( ID_VIEW_BACK, _("后退"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolbar_ptr->AddTool( ID_VIEW_NEXT, _("前进"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString ); 
	m_toolbar_ptr->AddSeparator(); 
	m_staticText14 = new wxStaticText( m_toolbar_ptr, wxID_ANY, _("显示内容："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	m_toolbar_ptr->AddControl( m_staticText14 );
	wxString m_show_method_select_ptrChoices[] = { _("双面板显示"), _("目录树"), _("传输中") };
	int m_show_method_select_ptrNChoices = sizeof( m_show_method_select_ptrChoices ) / sizeof( wxString );
	m_show_method_select_ptr = new wxChoice( m_toolbar_ptr, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_show_method_select_ptrNChoices, m_show_method_select_ptrChoices, 0 );
	m_show_method_select_ptr->SetSelection( 0 );
	m_toolbar_ptr->AddControl( m_show_method_select_ptr );
	m_toolbar_ptr->Realize();
	
	bSizer1->Add( m_toolbar_ptr, 0, wxALIGN_TOP|wxEXPAND, 5 );
	
	m_splitter2 = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter2->Connect( wxEVT_IDLE, wxIdleEventHandler( MainForm::m_splitter2OnIdle ), NULL, this );
	m_splitter2->SetMinimumPaneSize( 128 );
	
	m_panel11 = new wxPanel( m_splitter2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );
	
	m_tree_ptr = new wxTreeCtrl( m_panel11, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE|wxTR_HIDE_ROOT );
	m_tree_ptr->Hide();
	
	bSizer31->Add( m_tree_ptr, 1, wxEXPAND|wxTOP|wxBOTTOM|wxLEFT, 5 );
	
	m_left_task_view_item_ptr = new wxTaskViewSpace::wxTaskViewItem(m_panel11);
	bSizer31->Add( m_left_task_view_item_ptr, 1, wxEXPAND|wxTOP|wxBOTTOM|wxLEFT, 5 );
	
	m_panel11->SetSizer( bSizer31 );
	m_panel11->Layout();
	bSizer31->Fit( m_panel11 );
	m_right_view_panel_ptr = new wxPanel( m_splitter2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxVERTICAL );
	
	m_right_task_view_item_ptr = new wxTaskViewSpace::wxTaskViewItem(m_right_view_panel_ptr);
	bSizer32->Add( m_right_task_view_item_ptr, 1, wxEXPAND|wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	m_right_view_panel_ptr->SetSizer( bSizer32 );
	m_right_view_panel_ptr->Layout();
	bSizer32->Fit( m_right_view_panel_ptr );
	m_splitter2->SplitVertically( m_panel11, m_right_view_panel_ptr, 290 );
	bSizer1->Add( m_splitter2, 1, wxEXPAND, 5 );
	
	m_path_view_ptr = new wxTaskViewSpace::wxPathView(this,wxSize( -1,40 ));
	bSizer1->Add( m_path_view_ptr, 0, wxEXPAND, 5 );
	
	m_fast_cmd_input = new wxTextCtrl( this, wxID_FASTCMD, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_LEFT );
	m_fast_cmd_input->Hide();
	
	bSizer1->Add( m_fast_cmd_input, 0, wxALL, 5 );
	
	this->SetSizer( bSizer1 );
	this->Layout();
	m_statusBar1 = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	m_task_menu_ptr = new wxMenu();
	wxMenuItem* m_menuItem5;
	m_menuItem5 = new wxMenuItem( m_task_menu_ptr, wxID_NEW_DIR, wxString( _("新建目录") ) , wxEmptyString, wxITEM_NORMAL );
	m_task_menu_ptr->Append( m_menuItem5 );
	
	this->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( MainForm::MainFormOnContextMenu ), NULL, this ); 
	
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainForm::OnClose ) );
	this->Connect( wxEVT_ICONIZE, wxIconizeEventHandler( MainForm::onMinSize ) );
	this->Connect( ID_NEW_TASK, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainForm::onNewTaskButtonClick ) );
	this->Connect( ID_SET_PERFORMANCE, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainForm::onShowPerformanceDlg ) );
	this->Connect( ID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainForm::OnPressExit ) );
	this->Connect( ID_NEW_TASK, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainForm::onNewTaskButtonClick ) );
	this->Connect( ID_DEL_TASK, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainForm::onDelTask ) );
	this->Connect( ID_TASK_START, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainForm::onStartTaskButtonClick ) );
	this->Connect( ID_TASK_PAUSE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainForm::onPauseButtonClick ) );
	m_show_method_select_ptr->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( MainForm::onShowMethodChanged ), NULL, this );
	this->Connect( wxID_NEW_DIR, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainForm::onNewDirClick ) );
}

MainForm::~MainForm()
{
	delete m_task_menu_ptr; 
}

DragWindow::DragWindow( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHT ) );
	
	m_drag_form_popup_menu_ptr = new wxMenu();
	wxMenuItem* m_menuItem6;
	m_menuItem6 = new wxMenuItem( m_drag_form_popup_menu_ptr, wxID_ANY, wxString( _("隐藏管理窗口") ) , wxEmptyString, wxITEM_CHECK );
	m_drag_form_popup_menu_ptr->Append( m_menuItem6 );
	
	wxMenuItem* m_menuItem4;
	m_menuItem4 = new wxMenuItem( m_drag_form_popup_menu_ptr, wxID_EXIT, wxString( _("退出") ) , wxEmptyString, wxITEM_NORMAL );
	m_drag_form_popup_menu_ptr->Append( m_menuItem4 );
	
	this->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( DragWindow::DragWindowOnContextMenu ), NULL, this ); 
	
	
	// Connect Events
	this->Connect( wxEVT_ENTER_WINDOW, wxMouseEventHandler( DragWindow::onMouseEnter ) );
	this->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( DragWindow::onMouseLeave ) );
	this->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( DragWindow::OnMouseLeftDown ) );
	this->Connect( wxEVT_LEFT_UP, wxMouseEventHandler( DragWindow::OnMouseLeftUp ) );
	this->Connect( wxEVT_MOTION, wxMouseEventHandler( DragWindow::OnMouseMove ) );
	this->Connect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( DragWindow::onTaggleMainWindowVisable ) );
	this->Connect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( DragWindow::OnPressExit ) );
}

DragWindow::~DragWindow()
{
	delete m_drag_form_popup_menu_ptr; 
}

PerformanceDialog::PerformanceDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );
	
	m_listbook1 = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLB_TOP );
	m_panel9 = new wxPanel( m_listbook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxVERTICAL );
	
	m_panel9->SetSizer( bSizer28 );
	m_panel9->Layout();
	bSizer28->Fit( m_panel9 );
	m_listbook1->AddPage( m_panel9, _("常规"), false );
	m_panel10 = new wxPanel( m_listbook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer291;
	bSizer291 = new wxBoxSizer( wxVERTICAL );
	
	m_plugin_list_ptr = new wxListCtrl( m_panel10, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	bSizer291->Add( m_plugin_list_ptr, 1, wxALL|wxEXPAND, 5 );
	
	m_panel10->SetSizer( bSizer291 );
	m_panel10->Layout();
	bSizer291->Fit( m_panel10 );
	m_listbook1->AddPage( m_panel10, _("插件"), true );
	#ifndef __WXGTK__ // Small icon style not supported in GTK
	wxListView* m_listbook1ListView = m_listbook1->GetListView();
	long m_listbook1Flags = m_listbook1ListView->GetWindowStyleFlag();
	m_listbook1Flags = ( m_listbook1Flags & ~wxLC_ICON ) | wxLC_SMALL_ICON;
	m_listbook1ListView->SetWindowStyleFlag( m_listbook1Flags );
	#endif
	
	bSizer29->Add( m_listbook1, 1, wxEXPAND | wxALL, 5 );
	
	m_sdbSizer4 = new wxStdDialogButtonSizer();
	m_sdbSizer4OK = new wxButton( this, wxID_OK );
	m_sdbSizer4->AddButton( m_sdbSizer4OK );
	m_sdbSizer4Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer4->AddButton( m_sdbSizer4Cancel );
	m_sdbSizer4->Realize();
	bSizer29->Add( m_sdbSizer4, 0, wxEXPAND|wxALL, 5 );
	
	this->SetSizer( bSizer29 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_listbook1->Connect( wxEVT_COMMAND_LISTBOOK_PAGE_CHANGED, wxListbookEventHandler( PerformanceDialog::onPanelChange ), NULL, this );
}

PerformanceDialog::~PerformanceDialog()
{
}

NewJobDialog::NewJobDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxDefaultSize );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText1 = new wxStaticText( this, wxID_ANY, _("URL："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer6->Add( m_staticText1, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_url_text_editor_ptr = new wxTextCtrl( this, wxID_ANY, _("http://127.0.0.1/0.jpg"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer6->Add( m_url_text_editor_ptr, 1, 0, 5 );
	
	m_showcap_button_ptr = new wxButton( this, wxID_ANY, _("捕获"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer6->Add( m_showcap_button_ptr, 0, wxRIGHT|wxLEFT, 5 );
	
	bSizer4->Add( bSizer6, 0, wxEXPAND, 5 );
	
	m_staticline2 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer4->Add( m_staticline2, 0, wxEXPAND|wxALL, 4 );
	
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText3 = new wxStaticText( this, wxID_ANY, _("分类："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer8->Add( m_staticText3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_select_view_ptr = new wxComboCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxCB_READONLY | wxCC_STD_BUTTON );
	bSizer8->Add( m_select_view_ptr, 1, wxALL|wxEXPAND, 5 );
	
	m_staticText6 = new wxStaticText( this, wxID_ANY, _("位置："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	bSizer8->Add( m_staticText6, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_dir_picker_ptr = new wxDirPickerCtrl( this, wxID_ANY, wxEmptyString, _("选择文件保存路径"), wxDefaultPosition, wxDefaultSize, wxDIRP_DEFAULT_STYLE );
	bSizer8->Add( m_dir_picker_ptr, 1, wxALIGN_CENTER_VERTICAL, 2 );
	
	m_disk_space_label_ptr = new wxStaticText( this, wxID_ANY, _("-- MB"), wxDefaultPosition, wxDefaultSize, 0 );
	m_disk_space_label_ptr->Wrap( -1 );
	bSizer8->Add( m_disk_space_label_ptr, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	
	bSizer8->Add( 5, 0, 0, 0, 5 );
	
	bSizer4->Add( bSizer8, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2 = new wxStaticText( this, wxID_ANY, _("文件名"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer7->Add( m_staticText2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_filename_text_editor_ptr = new wxTextCtrl( this, wxID_ANY, _("0.jpg"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_filename_text_editor_ptr, 1, 0, 5 );
	
	bSizer4->Add( bSizer7, 0, wxEXPAND, 5 );
	
	bSizer3->Add( bSizer4, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );
	
	m_show_more_button = new wxToggleButton( this, wxID_EXTENDDLGBTN, _("详细 >>"), wxDefaultPosition, wxDefaultSize, 0 );
	m_show_more_button->SetValue( true ); 
	bSizer9->Add( m_show_more_button, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	m_sdbSizer3 = new wxStdDialogButtonSizer();
	m_sdbSizer3OK = new wxButton( this, wxID_OK );
	m_sdbSizer3->AddButton( m_sdbSizer3OK );
	m_sdbSizer3Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer3->AddButton( m_sdbSizer3Cancel );
	m_sdbSizer3->Realize();
	bSizer9->Add( m_sdbSizer3, 1, wxALIGN_RIGHT|wxEXPAND|wxRIGHT, 5 );
	
	bSizer3->Add( bSizer9, 0, wxEXPAND|wxTOP, 5 );
	
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxVERTICAL );
	
	m_option_tabs = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panel3 = new wxPanel( m_option_tabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );
	
	m_use_multi_thread_checkbox = new wxCheckBox( m_panel3, wxID_THREADNUMCHKBOX, _("原始地址使用多线程"), wxDefaultPosition, wxDefaultSize, 0 );
	m_use_multi_thread_checkbox->SetValue(true); 
	bSizer16->Add( m_use_multi_thread_checkbox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_source_thread_num_sel_ptr = new wxSpinCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 5 );
	m_source_thread_num_sel_ptr->SetToolTip( _("设定原始地址使用的线程数量，如果希望在最大线程数范围内自动调整，请设为0") );
	
	bSizer16->Add( m_source_thread_num_sel_ptr, 0, wxALL|wxALIGN_CENTER_VERTICAL, 2 );
	
	m_staticline3 = new wxStaticLine( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer16->Add( m_staticline3, 0, wxEXPAND | wxALL, 5 );
	
	m_staticText12 = new wxStaticText( m_panel3, wxID_ANY, _("任务最大线程数："), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_staticText12->Wrap( -1 );
	bSizer16->Add( m_staticText12, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_max_thread_sel_ptr = new wxSpinCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 60, 31 );
	bSizer16->Add( m_max_thread_sel_ptr, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	bSizer15->Add( bSizer16, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxHORIZONTAL );
	
	m_retry_checkbox = new wxCheckBox( m_panel3, wxID_RETRYCHKBOX, _("重试次数"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer18->Add( m_retry_checkbox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl3 = new wxSpinCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	m_spinCtrl3->Enable( false );
	
	bSizer18->Add( m_spinCtrl3, 0, wxALL, 5 );
	
	bSizer15->Add( bSizer18, 0, wxEXPAND, 5 );
	
	m_staticline4 = new wxStaticLine( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer15->Add( m_staticline4, 0, wxEXPAND | wxALL, 2 );
	
	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxHORIZONTAL );
	
	m_login_server_checkbox = new wxCheckBox( m_panel3, wxID_LOGINCHKBOX, _("登录"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer19->Add( m_login_server_checkbox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText13 = new wxStaticText( m_panel3, wxID_ANY, _("用户名："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	bSizer19->Add( m_staticText13, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl4 = new wxTextCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 120,-1 ), 0 );
	m_textCtrl4->Enable( false );
	
	bSizer19->Add( m_textCtrl4, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_staticText14 = new wxStaticText( m_panel3, wxID_ANY, _("密码："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	bSizer19->Add( m_staticText14, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl5 = new wxTextCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 120,-1 ), wxTE_PASSWORD );
	m_textCtrl5->Enable( false );
	
	bSizer19->Add( m_textCtrl5, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	bSizer15->Add( bSizer19, 0, wxEXPAND, 5 );
	
	m_staticline5 = new wxStaticLine( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer15->Add( m_staticline5, 0, wxEXPAND | wxALL, 2 );
	
	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticBoxSizer* sbSizer2;
	sbSizer2 = new wxStaticBoxSizer( new wxStaticBox( m_panel3, wxID_ANY, _("下载计划") ), wxVERTICAL );
	
	m_radioBtn1 = new wxRadioButton( m_panel3, wxID_ANY, _("立即"), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer2->Add( m_radioBtn1, 0, wxLEFT|wxTOP, 5 );
	
	m_radioBtn2 = new wxRadioButton( m_panel3, wxID_ANY, _("手动"), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer2->Add( m_radioBtn2, 0, wxLEFT|wxTOP, 5 );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );
	
	m_radioBtn4 = new wxRadioButton( m_panel3, wxID_ANY, _("计划"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer21->Add( m_radioBtn4, 1, wxALL, 5 );
	
	m_button2 = new wxButton( m_panel3, wxID_ANY, _("设定计划..."), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer21->Add( m_button2, 0, wxALIGN_RIGHT, 5 );
	
	sbSizer2->Add( bSizer21, 0, wxEXPAND, 5 );
	
	bSizer20->Add( sbSizer2, 0, wxBOTTOM|wxEXPAND|wxRIGHT, 5 );
	
	m_commond_text = new wxTextCtrl( m_panel3, wxID_ANY, _("请输入注释..."), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE );
	bSizer20->Add( m_commond_text, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND, 5 );
	
	bSizer15->Add( bSizer20, 1, wxEXPAND, 5 );
	
	m_panel3->SetSizer( bSizer15 );
	m_panel3->Layout();
	bSizer15->Fit( m_panel3 );
	m_option_tabs->AddPage( m_panel3, _("基本"), true );
	m_panel5 = new wxPanel( m_option_tabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxVERTICAL );
	
	m_listBox2 = new wxListBox( m_panel5, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 ); 
	bSizer23->Add( m_listBox2, 1, wxALL|wxEXPAND, 5 );
	
	m_panel5->SetSizer( bSizer23 );
	m_panel5->Layout();
	bSizer23->Fit( m_panel5 );
	m_option_tabs->AddPage( m_panel5, _("文件集合"), false );
	m_panel4 = new wxPanel( m_option_tabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );
	
	m_panel4->SetSizer( bSizer22 );
	m_panel4->Layout();
	bSizer22->Fit( m_panel4 );
	m_option_tabs->AddPage( m_panel4, _("镜像"), false );
	m_panel6 = new wxPanel( m_option_tabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	m_panel6->SetSizer( bSizer24 );
	m_panel6->Layout();
	bSizer24->Fit( m_panel6 );
	m_option_tabs->AddPage( m_panel6, _("上下文"), false );
	m_panel7 = new wxPanel( m_option_tabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxHORIZONTAL );
	
	m_checkBox4 = new wxCheckBox( m_panel7, wxID_ANY, _("使用代理服务器"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer26->Add( m_checkBox4, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxArrayString m_choice1Choices;
	m_choice1 = new wxChoice( m_panel7, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice1Choices, 0 );
	m_choice1->SetSelection( 0 );
	bSizer26->Add( m_choice1, 1, wxALL, 5 );
	
	bSizer25->Add( bSizer26, 0, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer3;
	sbSizer3 = new wxStaticBoxSizer( new wxStaticBox( m_panel7, wxID_ANY, _("带宽") ), wxVERTICAL );
	
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText15 = new wxStaticText( m_panel7, wxID_ANY, _("下载限速："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	bSizer31->Add( m_staticText15, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl4 = new wxSpinCtrl( m_panel7, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -1, 10, 0 );
	bSizer31->Add( m_spinCtrl4, 0, wxALL, 5 );
	
	m_staticText16 = new wxStaticText( m_panel7, wxID_ANY, _("上传限速："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	bSizer31->Add( m_staticText16, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );
	
	m_spinCtrl5 = new wxSpinCtrl( m_panel7, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -1, 10, 0 );
	bSizer31->Add( m_spinCtrl5, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	sbSizer3->Add( bSizer31, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText17 = new wxStaticText( m_panel7, wxID_ANY, _("最大连接数："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	bSizer33->Add( m_staticText17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl6 = new wxSpinCtrl( m_panel7, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	bSizer33->Add( m_spinCtrl6, 0, wxALL, 5 );
	
	m_staticText18 = new wxStaticText( m_panel7, wxID_ANY, _("最大上传通道："), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer33->Add( m_staticText18, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl7 = new wxSpinCtrl( m_panel7, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	bSizer33->Add( m_spinCtrl7, 0, wxALL, 5 );
	
	sbSizer3->Add( bSizer33, 0, wxEXPAND, 5 );
	
	bSizer25->Add( sbSizer3, 0, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer5;
	sbSizer5 = new wxStaticBoxSizer( new wxStaticBox( m_panel7, wxID_ANY, _("对等网络") ), wxVERTICAL );
	
	bSizer25->Add( sbSizer5, 1, wxEXPAND, 5 );
	
	m_panel7->SetSizer( bSizer25 );
	m_panel7->Layout();
	bSizer25->Fit( m_panel7 );
	m_option_tabs->AddPage( m_panel7, _("网络"), false );
	m_panel8 = new wxPanel( m_option_tabs, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer34;
	bSizer34 = new wxBoxSizer( wxVERTICAL );
	
	m_panel8->SetSizer( bSizer34 );
	m_panel8->Layout();
	bSizer34->Fit( m_panel8 );
	m_option_tabs->AddPage( m_panel8, _("附加参数"), false );
	
	bSizer10->Add( m_option_tabs, 1, wxEXPAND | wxALL, 5 );
	
	bSizer3->Add( bSizer10, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer3 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_url_text_editor_ptr->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NewJobDialog::onURLInput ), NULL, this );
	m_showcap_button_ptr->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewJobDialog::onShowCapDialog ), NULL, this );
	m_dir_picker_ptr->Connect( wxEVT_COMMAND_DIRPICKER_CHANGED, wxFileDirPickerEventHandler( NewJobDialog::onDirChange ), NULL, this );
	m_show_more_button->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( NewJobDialog::onShowMoreButtonClick ), NULL, this );
	m_sdbSizer3OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewJobDialog::onOkButtonClick ), NULL, this );
	m_use_multi_thread_checkbox->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( NewJobDialog::onUseMultiThreadCheckBox ), NULL, this );
	m_retry_checkbox->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( NewJobDialog::onRetryCheckBox ), NULL, this );
	m_login_server_checkbox->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( NewJobDialog::onLoginServerCheckBox ), NULL, this );
	m_commond_text->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( NewJobDialog::onCommondTextClick ), NULL, this );
}

NewJobDialog::~NewJobDialog()
{
}

CapDialog::CapDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 1, 3, 0, 0 );
	fgSizer1->AddGrowableCol( 2 );
	fgSizer1->AddGrowableRow( 0 );
	fgSizer1->SetFlexibleDirection( wxHORIZONTAL );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText13 = new wxStaticText( this, wxID_ANY, _("请选择网络接口："), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
	m_staticText13->Wrap( -1 );
	fgSizer1->Add( m_staticText13, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_LEFT|wxBOTTOM|wxEXPAND|wxLEFT|wxTOP, 5 );
	
	m_comboBox31 = new wxComboBox( this, wxID_ANY, _("Combo!"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_READONLY ); 
	fgSizer1->Add( m_comboBox31, 0, wxALIGN_LEFT|wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_toggleBtn2 = new wxToggleButton( this, wxID_ANY, _("捕获"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_toggleBtn2, 0, wxALIGN_RIGHT|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	bSizer29->Add( fgSizer1, 0, wxEXPAND, 5 );
	
	m_staticline6 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer29->Add( m_staticline6, 0, wxEXPAND | wxALL, 5 );
	
	wxGridSizer* gSizer2;
	gSizer2 = new wxGridSizer( 2, 3, 0, 0 );
	
	m_staticText16 = new wxStaticText( this, wxID_ANY, _("协议过滤"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	gSizer2->Add( m_staticText16, 0, wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText17 = new wxStaticText( this, wxID_ANY, _("关键字过滤"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	gSizer2->Add( m_staticText17, 0, wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText18 = new wxStaticText( this, wxID_ANY, _("格式过滤"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	gSizer2->Add( m_staticText18, 0, wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_comboBox1 = new wxComboBox( this, wxID_ANY, _("Combo!"), wxDefaultPosition, wxSize( 150,-1 ), 0, NULL, wxCB_READONLY ); 
	gSizer2->Add( m_comboBox1, 0, wxRIGHT|wxLEFT, 5 );
	
	m_textCtrl7 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 130,-1 ), 0 );
	gSizer2->Add( m_textCtrl7, 0, wxRIGHT|wxLEFT, 5 );
	
	m_comboBox3 = new wxComboBox( this, wxID_ANY, _("Combo!"), wxDefaultPosition, wxSize( 150,-1 ), 0, NULL, 0 ); 
	gSizer2->Add( m_comboBox3, 0, wxRIGHT|wxLEFT, 5 );
	
	bSizer29->Add( gSizer2, 0, wxEXPAND, 5 );
	
	m_listCtrl1 = new wxListCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	bSizer29->Add( m_listCtrl1, 1, wxALL|wxEXPAND, 5 );
	
	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 1, 3, 0, 0 );
	
	m_button4 = new wxButton( this, wxID_ANY, _("解锁"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_button4, 0, wxALIGN_LEFT|wxLEFT|wxRIGHT, 5 );
	
	m_button5 = new wxButton( this, wxID_ANY, _("下载选中项"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_button5, 0, wxALIGN_RIGHT, 5 );
	
	m_sdbSizer2 = new wxStdDialogButtonSizer();
	m_sdbSizer2Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer2->AddButton( m_sdbSizer2Cancel );
	m_sdbSizer2->Realize();
	gSizer1->Add( m_sdbSizer2, 0, wxALIGN_RIGHT|wxRIGHT, 5 );
	
	bSizer29->Add( gSizer1, 0, wxEXPAND|wxTOP|wxBOTTOM, 5 );
	
	this->SetSizer( bSizer29 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_toggleBtn2->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( CapDialog::onCapToggleButtonClick ), NULL, this );
	m_button4->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CapDialog::onUnlockButtonClick ), NULL, this );
	m_button5->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CapDialog::onDownloadButtonClick ), NULL, this );
}

CapDialog::~CapDialog()
{
}

NewDirDialog::NewDirDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 2, 2, 0, 0 );
	fgSizer1->AddGrowableCol( 1 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText22 = new wxStaticText( this, wxID_ANY, _("文件夹名称："), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
	m_staticText22->Wrap( -1 );
	fgSizer1->Add( m_staticText22, 1, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dir_name_ptr = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_dir_name_ptr, 0, wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText23 = new wxStaticText( this, wxID_ANY, _("文件夹放在："), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
	m_staticText23->Wrap( -1 );
	fgSizer1->Add( m_staticText23, 1, wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	m_path_ptr = new wxDirPickerCtrl( this, wxID_ANY, wxEmptyString, _("Select a folder"), wxDefaultPosition, wxDefaultSize, wxDIRP_DEFAULT_STYLE );
	fgSizer1->Add( m_path_ptr, 0, wxEXPAND|wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	bSizer1->Add( fgSizer1, 0, wxEXPAND, 5 );
	
	m_sdbSizer4 = new wxStdDialogButtonSizer();
	m_sdbSizer4OK = new wxButton( this, wxID_OK );
	m_sdbSizer4->AddButton( m_sdbSizer4OK );
	m_sdbSizer4Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer4->AddButton( m_sdbSizer4Cancel );
	m_sdbSizer4->Realize();
	bSizer1->Add( m_sdbSizer4, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer1 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_sdbSizer4OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewDirDialog::onOKButtonClick ), NULL, this );
}

NewDirDialog::~NewDirDialog()
{
}
