#ifndef __NewDirDialog_Core__
#define __NewDirDialog_Core__

#include "GUI_RAD.h"

using namespace WXGUI;

/** Implementing NewDirDialog */
class CNewDirDialog_Core : public NewDirDialog
{
	public:
		wxString getDirName( void )
		{
			return m_dir_name_ptr->GetValue();
		}

		wxString getPath( void ) const
		{
			return m_path_ptr->GetPath();
		}
		CNewDirDialog_Core( wxWindow *parent );

	private:
		void onOKButtonClick( wxCommandEvent& event );
};

#endif // __NewDirDialog_Core__
