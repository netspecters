#ifndef __CapDialog_Core__
#define __CapDialog_Core__

#include "GUI_RAD.h"
#include "../../../KernelLibrary/InternalProtocol/cap/Interfaces.h"

using namespace WXGUI;
using namespace Netspecters::KernelPlus::InternalProtocol::CAP;

class CCapDialog_Core : public CapDialog
{
	public:
		CCapDialog_Core(wxWindow *parent);

	private:
		void onCapToggleButtonClick(wxCommandEvent &event);
		void onUnlockButtonClick(wxCommandEvent &event);
		void onDownloadButtonClick(wxCommandEvent &event);
};

#endif // __CapDialog_Core__
