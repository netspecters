#include <functional>
#include "MainForm_Core.h"
#include "GUI.h"
#include "GlobalVars.h"
#include "wxTaskView.h"
#include "wxTreeViewComboPopup.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"
#include "../../../KernelLibrary/InternalProtocol/http/Interfaces.h"

CMainForm_Core::CMainForm_Core( wxWindow *parent )
	: MainForm( parent ),
	m_task_view(( wxTaskView::ST_Child ) {
	m_left_task_view_item_ptr, m_right_task_view_item_ptr, m_path_view_ptr, m_task_menu_ptr, m_tree_ptr
}, this )
{
	string data_path;
#ifdef DEBUG
	data_path.assign( "gui_conf.xml" );
#else
	{
		size_t data_path_length = NSGUI_INSTANCE.getPlusTreePtr()->getDataDirPath( NULL, 0 );
		char buffer[data_path_length + 1];
		NSGUI_INSTANCE.getPlusTreePtr()->getDataDirPath( buffer, data_path_length );
		data_path.assign( buffer );
		data_path.append( "/gui_conf.xml" );
	}

#endif
	m_task_view.loadConfigure( wxString( data_path.c_str(), wxConvUTF8 ) );
	g_params.load( data_path );

	/* set icons */
	//wxToolBarToolBase *tool_button_ptr = m_toolbar_ptr->FindById(ID_NEWTASK);
	//tool_button_ptr->SetNormalBitmap(wxArtProvider::GetBitmap(wxART_ADD_BOOKMARK));

	//m_toolbar_ptr->SetWindowStyle( m_toolbar_ptr->GetWindowStyleFlag() & (~wxTB_NOICONS) );

	/* 加入信号槽 */
	NSGUI_INSTANCE.getSignal().connect( std::bind( &CMainForm_Core::update, this ) );
}

void CMainForm_Core::OnPressExit( wxCommandEvent &event )
{
	NSGUI_INSTANCE.internalClose();
}

void CMainForm_Core::onMinSize( wxIconizeEvent &event )
{

}

void CMainForm_Core::OnClose( wxCloseEvent &event )
{
	NSGUI_INSTANCE.internalClose( event );
}

void CMainForm_Core::onShowPerformanceDlg( wxCommandEvent &event )
{
	CPerformanceDialog_Core *performance_dlg_ptr = new CPerformanceDialog_Core( this );
	performance_dlg_ptr->ShowModal();
	delete performance_dlg_ptr;
}

void CMainForm_Core::onNewTaskButtonClick( wxCommandEvent &event )
{
	//TODO
	CNewJobDialog_Core new_job_dialog( this, m_task_view );
	new_job_dialog.ShowModal();

	if( new_job_dialog.new_job_param.res_path_ptr )
	{
		/* 构建任务参数 */
		IProtocol::ST_TaskInitParam task_init_param = { 0, true, &new_job_dialog.new_job_param, NULL };

		/* 添加任务组 */
		ITaskGroupManager *task_group_manager_ptr = NSGUI_INSTANCE.getTaskGroupManagerPtr();
		GROUP_HANDLE group_handle = task_group_manager_ptr->addGroup( new_job_dialog.new_job_param.active_node.name.mb_str( wxConvUTF8 ), true, 0 );

		/* 添加任务 */
		task_group_manager_ptr->addProtocolTaskToGroup( group_handle, task_init_param );
		new_job_dialog.new_job_param.active_node.taskmanager_group_handle = group_handle;
		new_job_dialog.new_job_param.active_node.taskmanager_group_id = task_group_manager_ptr->getGroupID( group_handle );
		m_task_view.addItem( new_job_dialog.new_job_param.active_node );

		/* 启动任务 */
		task_group_manager_ptr->switchGroupRunningState( group_handle, false );
	}
}

void CMainForm_Core::onDelTask( wxCommandEvent &event )
{
	wxKeyEvent kb_state;
	m_task_view.deleteSelectedItemes( kb_state.ShiftDown() );
}

void CMainForm_Core::onStartTaskButtonClick( wxCommandEvent &event )
{

}

void CMainForm_Core::onPauseButtonClick( wxCommandEvent &event )
{

}

void CMainForm_Core::onShowMethodChanged( wxCommandEvent &event )
{
	enum ENM_ShowMethod { smDoublePanel = 0, smTree, smTransfer };
	switch( m_show_method_select_ptr->GetCurrentSelection() )
	{
		case smDoublePanel:
			if( !m_left_task_view_item_ptr->IsShown() )
			{
				m_left_task_view_item_ptr->Show( true );
				m_tree_ptr->Show( false );
			}
			break;

		case smTree:
			if( m_left_task_view_item_ptr->IsShown() )
			{
				m_left_task_view_item_ptr->Show( false );
				m_tree_ptr->Show( true );
			}
			break;

		case smTransfer:
			//m_right_view_panel_ptr->Show(false);
			break;
	}
}

void CMainForm_Core::onNewDirClick( wxCommandEvent &event )
{
	/* show the dialog */
	CNewDirDialog_Core new_dir_dialog( this );
	new_dir_dialog.ShowModal();

	if( !new_dir_dialog.getDirName().IsEmpty() )
	{
		/* fill node struct */
		ST_ActiveNode dir_node;
		dir_node.filesystem_location = new_dir_dialog.getPath();
		dir_node.name = new_dir_dialog.getDirName();
		dir_node.has_child = true;

		/* add the dir */
		m_task_view.addItem( dir_node );
	}
}

void CMainForm_Core::onSetToolbarButtonState( bool start, bool pause )
{
	/* set toolbar button state */
	m_toolbar_ptr->EnableTool( ID_TASK_START, start );
	m_toolbar_ptr->EnableTool( ID_TASK_PAUSE, pause );
}

void CMainForm_Core::update( void )
{

}
