#include <cmath>
#include <string>
#ifdef __MINGW32__
#	include <malloc.h>
#else
#	include <alloca.h>
#endif
#include <algorithm>
#include "TaskView_Core.h"
#include "GUI.h"

#define INTERVAL 1
#define BORDER 2
#define ICON_WIDTH 32
#define ICON_HEIGHT ICON_WIDTH
#define ICON_AREA_WIDTH (ICON_WIDTH + BORDER*2)
#define INFORMATION_AREA_LEFT_OFFSET (ICON_AREA_WIDTH + BORDER)
#define PROGRESS_BAR_HEIGHT 15

BEGIN_EVENT_TABLE ( CTaskView, wxTaskView )
	EVT_LIST_ITEM_SELECTED ( wxID_FIRST_TASK_VIEW, CTaskView::OnFirstViewSelect )
	EVT_LIST_ITEM_SELECTED ( wxID_SECOND_TASK_VIEW, CTaskView::OnSecondViewSelect )
END_EVENT_TABLE()

CTaskView::CTaskView ( wxTaskView *task_view_ptr )
	: m_task_view_ptr(task_view_ptr)
{
	int char_height = wx_panel_ptr->GetCharHeight();
	m_item_height = BORDER * 2 + INTERVAL + PROGRESS_BAR_HEIGHT + char_height;
	m_selected_item_height = m_item_height + char_height;


#ifdef DEBUG
	wxTaskView::ST_Item item;
	for ( int i = 0; i < 14; ++i )
	{
		m_task_view_ptr->updateItem( findItem(i), );
	}
#else
	/* init list */
	size_t buf_size = NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupHandleList ( NULL );
	GROUP_HANDLE *handle_buf_ptr = static_cast<GROUP_HANDLE*> ( alloca ( buf_size ) );
	NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupHandleList ( handle_buf_ptr );
	for ( int i = 0; i < ( buf_size / sizeof ( GROUP_HANDLE ) ); ++i, ++handle_buf_ptr )
		addTaskGroup ( *handle_buf_ptr );
#endif
}

CTaskView::~CTaskView ( void )
{
	Stop();
}

void CTaskView::drawTaskInformation ( wxDC &paint_dc,
									  int x_offset, int y_offset, int width,
									  const ITaskGroupManager::ST_GroupInformation &task_group_information,
									  bool is_select )
{
	wxString base_infor_str ( wxString::Format ( _( "%s/S\t剩余%s\t%.1f%%" )
							  , makeSizeString ( task_group_information.recv_data_speed ).c_str()
							  , makeSizeString ( task_group_information.group_file_size - task_group_information.recved_data_size ).c_str()
							  , ( double ) task_group_information.recved_data_size * 100.0 / ( double ) task_group_information.group_file_size ) );

	wxCoord base_infor_str_width, base_infor_str_height;
	paint_dc.GetTextExtent ( base_infor_str, &base_infor_str_width, &base_infor_str_height );

	paint_dc.DrawText ( wxString ( task_group_information.group_file_name_str_ptr, wxConvUTF8 ), x_offset, y_offset );
	paint_dc.DrawText ( base_infor_str, width - ( base_infor_str_width + BORDER ), y_offset );
}

void CTaskView::drawProgressbar ( wxDC &paint_dc,
								  int x_offset, int y_offset,
								  off_t position, off_t max_value )
{
	paint_dc.SetBrush ( wxBrush ( wxColour ( 0x0000ff00 ), wxSOLID ) );
	//paint_dc.SetPen ( wxPen ( wxColour ( 0x0000ff00 ) ) );
	paint_dc.DrawCircle ( x_offset + ICON_WIDTH / 2, y_offset + ICON_HEIGHT / 2, ICON_WIDTH / 2 );
}

void CTaskView::repaintItem ( wxDC &paint_dc, int item_top_offset, const ST_Item &item, bool is_select )
{
	/* draw background and set foreground text color */
	if ( is_select )
	{
		paint_dc.SetBrush ( wxBrush ( wxColour ( 0x00, 0x00, 0x00 ), wxSOLID ) );
		paint_dc.SetTextForeground ( wxColour ( 0x00ffffff ) );
		paint_dc.DrawRectangle ( 0, item_top_offset, m_viewer_ptr->GetClientSize().GetWidth(), is_select ? m_selected_item_height : m_item_height );
	}

	/* draw icon */

	/* ==== draw task information area ==== */
	int infor_area_width = m_viewer_ptr->GetClientSize().GetWidth() - ICON_AREA_WIDTH - BORDER * 2;
	int infor_area_top = item_top_offset + BORDER;

	/* get task group information */
	ITaskGroupManager::ST_GroupInformation task_group_information;
#ifdef DEBUG
	task_group_information.recv_data_speed = 1050;
	task_group_information.recved_data_size = 1024;
	task_group_information.group_file_size = 2560;
	task_group_information.group_file_name_str_ptr = "MyFile.tst";
#else
	NSGUI_INSTANCE.getTaskGroupManagerPtr()->getGroupStatistics ( item.task_group_handle, &task_group_information );
#endif

	drawTaskInformation ( paint_dc, INFORMATION_AREA_LEFT_OFFSET, infor_area_top, infor_area_width, task_group_information, is_select );

	/* caculate progress bar position and draw it */
	//drawProgressbar( paint_dc, BORDER, infor_area_top, task_group_information.recved_data_size, task_group_information.group_file_size );

	/* reset pen */
	if ( is_select )
	{
		paint_dc.SetTextForeground ( wxColour ( 0x00, 0x00, 0x00 ) );
	}
}

void CTaskView::repaint ( wxDC &paint_dc )
{
	int item_top_offset = 0;
	int index = 0, start, end;
	getVisableItemRange ( start, end );

	for ( auto itr = m_item_list.begin(); itr != m_item_list.end(); ++itr, ++index )
	{
		if ( ( index >= start ) && ( index <= end ) )
		{
			bool is_selected_item = index == m_selected_item_index;
			repaintItem ( paint_dc, item_top_offset, *itr, is_selected_item );
			item_top_offset += is_selected_item ? m_selected_item_height : m_item_height;
		}
		if ( index > end )
			break;
	}
}

void CTaskView::selectTaskOnViewer ( const wxPoint & click_point )
{
	wxPoint logic_point;
	m_viewer_ptr->CalcUnscrolledPosition ( click_point.x, click_point.y, &logic_point.x, &logic_point.y );
	int selecting_item_index = m_selected_item_index;

	int item_offset_before_selected = std::max ( selecting_item_index, 0 ) * m_item_height;
	if ( logic_point.y <= item_offset_before_selected )
	{
		/* caculate the item index under the mouse */
		selecting_item_index = logic_point.y / (m_item_height + 1);
	}
	else if ( ( logic_point.y > ( item_offset_before_selected + m_selected_item_height ) )
			  && ( logic_point.y <= ( ( m_item_list.size() - 1 ) *m_item_height + m_selected_item_height ) ) )
	{
		logic_point.y -= item_offset_before_selected + m_selected_item_height;
		/* caculate the item index under the mouse */
		selecting_item_index = logic_point.y / ( m_item_height + 1 ) + m_selected_item_index + 1;
	}

	if ( m_selected_item_index != selecting_item_index )
	{
		m_selected_item_index = selecting_item_index;
		m_viewer_ptr->Refresh();
	}
}

bool CTaskView::getItemFromIndex ( int index, ST_Item &item )
{
	auto itr = m_item_list.begin();
	for ( ; ( itr != m_item_list.end() ) && ( index > 0 ); ++itr, --index );
	if ( index == 0 )
	{
		item = *itr;
		return true;
	}
	return false;
}

void CTaskView::getVisableItemRange ( int &start, int &end )
{
	int x, y;
	m_viewer_ptr->GetViewStart ( &x, &y );
	start = y / ( m_item_height + 1 );
	end = std::min ( ( y + m_viewer_ptr->GetClientSize().GetHeight() ) / ( m_item_height + 1 ) , ( int ) ( m_item_list.size() - 1 ) );
}

wxString CTaskView::makeSizeString ( unsigned size_x )
{
	wxString size_string;
	if ( size_x < 1024 )
		size_string.assign ( wxString::Format ( wxT ( "%d B" ), size_x ) );
	else if ( size_x < 1024 * 1024 )
		size_string.assign ( wxString::Format ( wxT ( "%.2f KB" ), ( double ) size_x / 1024.0 ) );
	else if ( size_x < 1024 * 1024 * 1024 )
		size_string.assign ( wxString::Format ( wxT ( "%.2f MB" ), ( double ) size_x  / ( 1024.0 * 1024.0 ) ) );
	else if ( size_x < 1024 * 1024 * 1024 )
		size_string.assign ( wxString::Format ( wxT ( "%.2f GB" ), ( double ) size_x / ( 1024.0 * 1024.0 * 1024.0 ) ) );

	return std::move ( size_string );
}

void CTaskView::Notify ( void )
{
	m_viewer_ptr->Refresh();
}

void CTaskView::addTaskGroup ( GROUP_HANDLE handle )
{
	m_item_list.push_back ( ST_Item ( handle ) );
	int virtual_size = ( m_item_list.size() - 1 ) * m_item_height + m_selected_item_height;
	if ( virtual_size > m_viewer_ptr->GetClientSize().GetHeight() )
	{
		m_viewer_ptr->SetScrollbars ( 1, 1, m_viewer_ptr->GetClientSize().GetWidth(), virtual_size );
		m_viewer_ptr->Refresh();
	}
}

