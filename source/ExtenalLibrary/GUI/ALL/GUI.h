/*
 * =====================================================================================
 *
 *       Filename:  GUI.h
 *
 *    Description:  Netspecters' gui for all
 *
 *        Version:  1.0
 *        Created:  2009年07月18日 15时43分13秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan
 *        Company:
 *
 * =====================================================================================
 */

#ifndef  _GUI_INC
#define  _GUI_INC

#include "MainForm_Core.h"
#include "DragWindow_Core.h"
#include "Tray.h"
#include "../../../KernelLibrary/UIHost/Interfaces.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"
#include "../../../Core/PlusInterface.h"
#ifdef DEBUG
#	include "mockes.hpp"
#endif

using namespace WXGUI;
using namespace Netspecters::KernelPlus::UIHost;
using namespace Netspecters::KernelPlus::ProtocolManager;

class CNetspectersGUI : public wxApp
{
	public:
		CNetspectersGUI() = default;
		virtual bool OnInit( void );
		wxMenu *getPopupMenu( void )
		{
			return m_drag_window_ptr->getPopupMenu();
		}
		void toggleMainWindowVisable( bool show )
		{
			if( show && !m_main_form_ptr )
			{
				m_main_form_ptr = new CMainForm_Core(( wxWindow* ) NULL );
				m_main_form_ptr ->Show();
			}
			else if( !show && m_main_form_ptr )
			{
				m_main_form_ptr->Destroy();
				m_main_form_ptr = NULL;
			}
		}

	private:
		struct ST_Configure
		{
			int main_window_top, main_window_left, main_window_width, main_window_height;
			int drag_window_top, drag_window_left;

			bool show_main_window;	//是否在初始化的时候显示主窗体
		};

		wxLocale m_locale;
		CTray *m_tray_ptr;
		CDragWindow_Core *m_drag_window_ptr;
		CMainForm_Core *m_main_form_ptr;
};

class CNSGUI : public INSPlus , public IGUIInstance, private wxTimer
{
	public:
		/* 从INSPlus继承 */
		NSAPI int onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param );
		NSAPI void onUnload( void );

		NSAPI int getParam( const char *param_name, void *value_buffer_ptr );
		NSAPI void *setParam( const char *param_name, void *value_ptr );

		/* 从IGUIInstance继承 */
		NSAPI int onRun( void );
		NSAPI void kill( int exit_code );

		static CNSGUI &getInstance()
		{
			return static_m_Instance;
		}
		IPlusTree *getPlusTreePtr( void )
		{
			return m_plus_tree_ptr;
		}
		IUIHost *getUIHostPtr( void )
		{
			return static_cast<IUIHost*>( m_plus_tree_ptr->getMainPlus() );
		}
		ITaskGroupManager *getTaskGroupManagerPtr( void )
		{
			return m_protocol_manager_ptr->getTaskGroupManager();
		}
		IProtocolManager *getProtocolManagerPtr( void )
		{
			return m_protocol_manager_ptr;
		}
		HDIR getPlusDirHandle( void )
		{
			return m_plus_dir_handle;
		}
		void internalClose( void )
		{
			wxCloseEvent event;
			internalClose( event );
		}
		void internalClose( wxCloseEvent &event )
		{
#ifdef DEBUG
			kill( 0 );
#else
			getUIHostPtr()->kill( 0 );
#endif
		}
		boost::signal<void( void )> &getSignal( void ) {
			return m_update_signal;
		}

	private:
		static CNSGUI static_m_Instance;
		ITaskGroupManager *m_task_group_manager_ptr;
		IProtocolManager *m_protocol_manager_ptr;
		IPlusTree	*m_plus_tree_ptr;
		HDIR	m_plus_dir_handle;
		boost::signal<void( void )> m_update_signal;
};
#define NSGUI_INSTANCE CNSGUI::getInstance()

DECLARE_APP( CNetspectersGUI )

#endif   /* ----- #ifndef _GUI_INC  ----- */
