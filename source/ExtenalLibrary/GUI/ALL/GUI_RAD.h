///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version May 25 2010)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __GUI_RAD__
#define __GUI_RAD__

#include <wx/intl.h>

#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/toolbar.h>
#include <wx/treectrl.h>
#include "wxTaskView.h"
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/dialog.h>
#include <wx/listctrl.h>
#include <wx/listbook.h>
#include <wx/button.h>
#include <wx/statline.h>
#include <wx/combo.h>
#include <wx/filepicker.h>
#include <wx/tglbtn.h>
#include <wx/checkbox.h>
#include <wx/spinctrl.h>
#include <wx/radiobut.h>
#include <wx/statbox.h>
#include <wx/listbox.h>
#include <wx/notebook.h>
#include <wx/combobox.h>

///////////////////////////////////////////////////////////////////////////

namespace WXGUI
{
	#define wxID_MAINWIN 10000
	#define ID_NEW_TASK 10001
	#define ID_SET_PERFORMANCE 10002
	#define ID_EXIT 10003
	#define ID_DEL_TASK 10004
	#define ID_TASK_START 10005
	#define ID_TASK_PAUSE 10006
	#define ID_VIEW_BACK 10007
	#define ID_VIEW_NEXT 10008
	#define wxID_PATH_VIEW 10009
	#define wxID_FASTCMD 10010
	#define wxID_NEW_DIR 10011
	#define wxID_DRAGWIN 10012
	#define wxID_PERFORMANCEDLG 10013
	#define wxID_NEWJBDLG 10014
	#define wxID_SELECT_VIEW 10015
	#define wxID_EXTENDDLGBTN 10016
	#define wxID_THREADNUMCHKBOX 10017
	#define wxID_RETRYCHKBOX 10018
	#define wxID_LOGINCHKBOX 10019
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class MainForm
	///////////////////////////////////////////////////////////////////////////////
	class MainForm : public wxFrame 
	{
		private:
		
		protected:
			wxMenuBar* m_menubar1;
			wxMenu* menu_file;
			wxToolBar* m_toolbar_ptr;
			wxStaticText* m_staticText14;
			wxChoice* m_show_method_select_ptr;
			wxSplitterWindow* m_splitter2;
			wxPanel* m_panel11;
			wxTreeCtrl* m_tree_ptr;
			wxTaskViewSpace::wxTaskViewItem *m_left_task_view_item_ptr;
			wxPanel* m_right_view_panel_ptr;
			wxTaskViewSpace::wxTaskViewItem *m_right_task_view_item_ptr;
			wxTaskViewSpace::wxPathView *m_path_view_ptr;
			wxTextCtrl* m_fast_cmd_input;
			wxStatusBar* m_statusBar1;
			wxMenu* m_task_menu_ptr;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnClose( wxCloseEvent& event ) = 0;
			virtual void onMinSize( wxIconizeEvent& event ) = 0;
			virtual void onNewTaskButtonClick( wxCommandEvent& event ) = 0;
			virtual void onShowPerformanceDlg( wxCommandEvent& event ) = 0;
			virtual void OnPressExit( wxCommandEvent& event ) = 0;
			virtual void onDelTask( wxCommandEvent& event ) = 0;
			virtual void onStartTaskButtonClick( wxCommandEvent& event ) = 0;
			virtual void onPauseButtonClick( wxCommandEvent& event ) = 0;
			virtual void onShowMethodChanged( wxCommandEvent& event ) = 0;
			virtual void onNewDirClick( wxCommandEvent& event ) = 0;
			
		
		public:
			
			MainForm( wxWindow* parent, wxWindowID id = wxID_MAINWIN, const wxString& title = _("Netspecters"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 600,650 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
			~MainForm();
			
			void m_splitter2OnIdle( wxIdleEvent& )
			{
				m_splitter2->SetSashPosition( 290 );
				m_splitter2->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainForm::m_splitter2OnIdle ), NULL, this );
			}
			
			void MainFormOnContextMenu( wxMouseEvent &event )
			{
				this->PopupMenu( m_task_menu_ptr, event.GetPosition() );
			}
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class DragWindow
	///////////////////////////////////////////////////////////////////////////////
	class DragWindow : public wxDialog 
	{
		private:
		
		protected:
			wxMenu* m_drag_form_popup_menu_ptr;
			
			// Virtual event handlers, overide them in your derived class
			virtual void onMouseEnter( wxMouseEvent& event ) = 0;
			virtual void onMouseLeave( wxMouseEvent& event ) = 0;
			virtual void OnMouseLeftDown( wxMouseEvent& event ) = 0;
			virtual void OnMouseLeftUp( wxMouseEvent& event ) = 0;
			virtual void OnMouseMove( wxMouseEvent& event ) = 0;
			virtual void onTaggleMainWindowVisable( wxCommandEvent& event ) = 0;
			virtual void OnPressExit( wxCommandEvent& event ) = 0;
			
		
		public:
			
			DragWindow( wxWindow* parent, wxWindowID id = wxID_DRAGWIN, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 73,45 ), long style = wxSTAY_ON_TOP|wxNO_BORDER );
			~DragWindow();
			
			void DragWindowOnContextMenu( wxMouseEvent &event )
			{
				this->PopupMenu( m_drag_form_popup_menu_ptr, event.GetPosition() );
			}
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class PerformanceDialog
	///////////////////////////////////////////////////////////////////////////////
	class PerformanceDialog : public wxDialog 
	{
		private:
		
		protected:
			wxListbook* m_listbook1;
			wxPanel* m_panel9;
			wxPanel* m_panel10;
			wxListCtrl* m_plugin_list_ptr;
			wxStdDialogButtonSizer* m_sdbSizer4;
			wxButton* m_sdbSizer4OK;
			wxButton* m_sdbSizer4Cancel;
			
			// Virtual event handlers, overide them in your derived class
			virtual void onPanelChange( wxListbookEvent& event ) { event.Skip(); }
			
		
		public:
			
			PerformanceDialog( wxWindow* parent, wxWindowID id = wxID_PERFORMANCEDLG, const wxString& title = _("参数设定"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 512,651 ), long style = wxDEFAULT_DIALOG_STYLE );
			~PerformanceDialog();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class NewJobDialog
	///////////////////////////////////////////////////////////////////////////////
	class NewJobDialog : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText1;
			wxTextCtrl* m_url_text_editor_ptr;
			wxButton* m_showcap_button_ptr;
			wxStaticLine* m_staticline2;
			wxStaticText* m_staticText3;
			wxComboCtrl *m_select_view_ptr;
			wxStaticText* m_staticText6;
			wxDirPickerCtrl* m_dir_picker_ptr;
			wxStaticText* m_disk_space_label_ptr;
			
			wxStaticText* m_staticText2;
			wxTextCtrl* m_filename_text_editor_ptr;
			wxToggleButton* m_show_more_button;
			wxStdDialogButtonSizer* m_sdbSizer3;
			wxButton* m_sdbSizer3OK;
			wxButton* m_sdbSizer3Cancel;
			wxNotebook* m_option_tabs;
			wxPanel* m_panel3;
			wxCheckBox* m_use_multi_thread_checkbox;
			wxSpinCtrl* m_source_thread_num_sel_ptr;
			wxStaticLine* m_staticline3;
			wxStaticText* m_staticText12;
			wxSpinCtrl* m_max_thread_sel_ptr;
			wxCheckBox* m_retry_checkbox;
			wxSpinCtrl* m_spinCtrl3;
			wxStaticLine* m_staticline4;
			wxCheckBox* m_login_server_checkbox;
			wxStaticText* m_staticText13;
			wxTextCtrl* m_textCtrl4;
			wxStaticText* m_staticText14;
			wxTextCtrl* m_textCtrl5;
			wxStaticLine* m_staticline5;
			wxRadioButton* m_radioBtn1;
			wxRadioButton* m_radioBtn2;
			wxRadioButton* m_radioBtn4;
			wxButton* m_button2;
			wxTextCtrl* m_commond_text;
			wxPanel* m_panel5;
			wxListBox* m_listBox2;
			wxPanel* m_panel4;
			wxPanel* m_panel6;
			wxPanel* m_panel7;
			wxCheckBox* m_checkBox4;
			wxChoice* m_choice1;
			wxStaticText* m_staticText15;
			wxSpinCtrl* m_spinCtrl4;
			wxStaticText* m_staticText16;
			wxSpinCtrl* m_spinCtrl5;
			wxStaticText* m_staticText17;
			wxSpinCtrl* m_spinCtrl6;
			wxStaticText* m_staticText18;
			wxSpinCtrl* m_spinCtrl7;
			wxPanel* m_panel8;
			
			// Virtual event handlers, overide them in your derived class
			virtual void onURLInput( wxCommandEvent& event ) = 0;
			virtual void onShowCapDialog( wxCommandEvent& event ) = 0;
			virtual void onDirChange( wxFileDirPickerEvent& event ) = 0;
			virtual void onShowMoreButtonClick( wxCommandEvent& event ) = 0;
			virtual void onOkButtonClick( wxCommandEvent& event ) = 0;
			virtual void onUseMultiThreadCheckBox( wxCommandEvent& event ) = 0;
			virtual void onRetryCheckBox( wxCommandEvent& event ) = 0;
			virtual void onLoginServerCheckBox( wxCommandEvent& event ) = 0;
			virtual void onCommondTextClick( wxMouseEvent& event ) = 0;
			
		
		public:
			
			NewJobDialog( wxWindow* parent, wxWindowID id = wxID_NEWJBDLG, const wxString& title = _("添加任务"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 520,470 ), long style = wxCAPTION|wxCLOSE_BOX );
			~NewJobDialog();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class CapDialog
	///////////////////////////////////////////////////////////////////////////////
	class CapDialog : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText13;
			wxComboBox* m_comboBox31;
			wxToggleButton* m_toggleBtn2;
			wxStaticLine* m_staticline6;
			wxStaticText* m_staticText16;
			wxStaticText* m_staticText17;
			wxStaticText* m_staticText18;
			wxComboBox* m_comboBox1;
			wxTextCtrl* m_textCtrl7;
			wxComboBox* m_comboBox3;
			wxListCtrl* m_listCtrl1;
			wxButton* m_button4;
			wxButton* m_button5;
			wxStdDialogButtonSizer* m_sdbSizer2;
			wxButton* m_sdbSizer2Cancel;
			
			// Virtual event handlers, overide them in your derived class
			virtual void onCapToggleButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void onUnlockButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void onDownloadButtonClick( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			CapDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("捕获"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 480,479 ), long style = wxDEFAULT_DIALOG_STYLE );
			~CapDialog();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class NewDirDialog
	///////////////////////////////////////////////////////////////////////////////
	class NewDirDialog : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText22;
			wxTextCtrl* m_dir_name_ptr;
			wxStaticText* m_staticText23;
			wxDirPickerCtrl* m_path_ptr;
			wxStdDialogButtonSizer* m_sdbSizer4;
			wxButton* m_sdbSizer4OK;
			wxButton* m_sdbSizer4Cancel;
			
			// Virtual event handlers, overide them in your derived class
			virtual void onOKButtonClick( wxCommandEvent& event ) = 0;
			
		
		public:
			
			NewDirDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("新建文件夹"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 359,138 ), long style = wxDEFAULT_DIALOG_STYLE );
			~NewDirDialog();
		
	};
	
} // namespace WXGUI

#endif //__GUI_RAD__
