#ifndef REFRESHUPDATER_H_INCLUDED
#define REFRESHUPDATER_H_INCLUDED

#include "wxTaskView.h"

class CRefreshUpdater : private wxTimer
{
	public:
		CRefreshUpdater( const wxTaskView &task_view )
			: m_task_view(task_view)
		{
		}

		void autoRefresh ( unsigned interval )
		{
			Start ( interval );
		}

	private:
		void Notify ( void )	//from wxTimer
		{
			m_view.Refresh();
		}

		const wxTaskView &m_task_view
};

#endif // REFRESHUPDATER_H_INCLUDED
