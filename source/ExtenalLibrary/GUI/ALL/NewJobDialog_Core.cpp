#include "NewJobDialog_Core.h"
#include "GlobalVars.h"
#include "GUI.h"
#include "../../../KernelLibrary/InternalProtocol/cap/Interfaces.h"

using namespace WXGUI;

CNewJobDialog_Core::CNewJobDialog_Core(wxWindow *parent, const wxTaskViewSpace::wxTaskView &task_view)
	: NewJobDialog(parent), m_stored_size(GetWindowBorderSize())
{
	memset(&new_job_param, 0, sizeof(IProtocol::ST_TaskResource));
	const wxTreeViewComboPopup::ST_InitParam init_param = {&task_view, m_dir_picker_ptr, m_disk_space_label_ptr};
	m_select_view_ptr->SetPopupControl(new wxTreeViewComboPopup(init_param));
	m_select_view_ptr->EnablePopupAnimation(true);

	/* 初始化目录选择器 */
	if(!g_params.least_category.empty())
		m_select_view_ptr->SetValue(wxString(g_params.least_category.c_str(), wxConvUTF8));
	else
		m_select_view_ptr->SetValue(_("未分类"));
}

bool CNewJobDialog_Core::fillParam(void)
{
	memset(&new_job_param, 0, sizeof(IProtocol::ST_TaskResource));

	if(m_login_server_checkbox->IsChecked())
	{
		new_job_param.login_usr_name.assign(m_uri.GetUser().ToUTF8());
		new_job_param.login_passwd.assign(m_uri.GetPassword().ToUTF8());
	}
	else if(m_uri.HasUserInfo())
	{
		new_job_param.login_usr_name.assign(m_textCtrl4->GetValue().ToUTF8());
		new_job_param.login_passwd.assign(m_textCtrl5->GetValue().ToUTF8());
	}
	else
	{
		new_job_param.login_usr_name.clear();
		new_job_param.login_passwd.clear();
	}

	if(m_uri.HasPort())
		new_job_param.host_port = wxAtoi(m_uri.GetPort());

	new_job_param.res_path.assign(m_url_text_editor_ptr->GetValue().ToUTF8());//url只能是ascii码

	/* 构建 node 信息 */
	new_job_param.active_node.has_active_data = true;
	new_job_param.active_node.has_child = false;
	new_job_param.active_node.parent_ptr = static_cast<wxTreeViewComboPopup*>(m_select_view_ptr->GetPopupControl())->getNodePtrOfSelectedItem();
	new_job_param.active_node.name = m_filename_text_editor_ptr->GetValue();
	new_job_param.active_node.filesystem_location = m_dir_picker_ptr->GetPath();

	if(m_retry_checkbox->IsChecked())
	{
		new_job_param.retry_limit = m_spinCtrl3->GetValue();
	}

	new_job_param.assignToPtr();
	return true;
}

void CNewJobDialog_Core::onURLInput(wxCommandEvent &event)
{
	wxURI uri(event.GetString());
	if(uri.HasScheme() && uri.HasPath())
		m_filename_text_editor_ptr->SetValue(uri.GetPath().AfterLast(wxT('/')));
}

void CNewJobDialog_Core::onShowCapDialog(wxCommandEvent &event)
{
	IProtocol *cap_protocol_ptr = NSGUI_INSTANCE.getProtocolManagerPtr()->queryProtocol("nscap");
	if(cap_protocol_ptr)
	{
		if(cap_protocol_ptr->getParam("caplibrary_path", NULL) > 0)
		{
			Netspecters::KernelPlus::InternalProtocol::CAP::INSCap *ns_cap_ptr;
			cap_protocol_ptr->getParam("cap_interface", &ns_cap_ptr);

			CCapDialog_Core cap_dialog(this);
			cap_dialog.ShowModal();
			return;
		}
	}

	wxMessageBox(_("加载Cap库失败"), _("失败"), wxICON_ERROR | wxOK, this);
	return;
}

void CNewJobDialog_Core::onDirChange(wxFileDirPickerEvent &event)
{
	/* get the disk space information */
	wxLongLong total, free;
	wxString path = event.GetPath();
	if(wxGetDiskSpace(path, &total, &free))
	{
		wxULongLong size(free.GetHi(), free.GetLo());
		m_disk_space_label_ptr->SetLabel(wxFileName::GetHumanReadableSize(size));
	}
}

void CNewJobDialog_Core::onUseMultiThreadCheckBox(wxCommandEvent &event)
{
	m_source_thread_num_sel_ptr->Enable(m_use_multi_thread_checkbox->IsChecked());
	m_max_thread_sel_ptr->Enable(m_use_multi_thread_checkbox->IsChecked());
}

void CNewJobDialog_Core::onOkButtonClick(wxCommandEvent &event)
{
	/* 检查关键变量是否合法 */
	if(m_url_text_editor_ptr->GetValue().IsEmpty())
	{
		wxMessageBox(_("必须填写网络地址"), _("错误"), wxOK | wxICON_ERROR, this);
		m_url_text_editor_ptr->SetFocus();
		return;
	}

	m_uri.Create(m_url_text_editor_ptr->GetValue());
	if(!m_uri.HasScheme())
	{
		wxMessageBox(_("网络地址格式错误！"), _("错误"), wxOK | wxICON_ERROR , this);
		m_url_text_editor_ptr->SetFocus();
		return;
	}
	if(!NSGUI_INSTANCE.getProtocolManagerPtr()->queryProtocol(m_uri.GetScheme().ToAscii()))
	{
		wxMessageBox(_("不可识别的网络地址格式!"), _("错误"), wxOK | wxICON_ERROR , this);
		m_url_text_editor_ptr->SetFocus();
		return;
	}
	if(m_filename_text_editor_ptr->GetValue().IsEmpty())
	{
		wxMessageBox(_("文件名不能为空!"), _("错误"), wxOK | wxICON_ERROR , this);
		m_filename_text_editor_ptr->SetFocus();
		return;
	}

	/* 填写返回参数 */
	if(!fillParam())
		return;

	Close();
}

void CNewJobDialog_Core::onRetryCheckBox(wxCommandEvent &event)
{
	m_spinCtrl3->Enable(m_retry_checkbox->IsChecked());
}

void CNewJobDialog_Core::onLoginServerCheckBox(wxCommandEvent &event)
{
	m_textCtrl4->Enable(m_login_server_checkbox->IsChecked());
	m_textCtrl5->Enable(m_login_server_checkbox->IsChecked());
}

void CNewJobDialog_Core::onCommondTextClick(wxMouseEvent &event)
{
	m_commond_text->SetSelection(0, m_commond_text->GetLastPosition());
}

void CNewJobDialog_Core::onShowMoreButtonClick(wxCommandEvent &event)
{
	wxSize new_size;

	m_option_tabs->Show(m_show_more_button->GetValue());
	if(m_show_more_button->GetValue())
		new_size.Set(520, 470);
	else
	{
		new_size.Set(520, m_show_more_button->GetRect().GetY() + m_show_more_button->GetSize().GetHeight() + 3);
	}

	SetMinSize(new_size);
	SetSize(new_size);
	//Centre ( wxVERTICAL );
}
