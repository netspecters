#ifndef TRAY_H_INCLUDED
#define TRAY_H_INCLUDED

class CTray : public wxTaskBarIcon
{
		DECLARE_EVENT_TABLE()
	public:
		CTray(bool close_to_icon);

		wxMenu *CreatePopupMenu(void);
		bool forceICON(void) {
			return m_close_to_icon;
		}

	private:
		void onLeftMouseDClick(wxTaskBarIconEvent &event);
		bool m_close_to_icon;
};

#endif // TRAY_H_INCLUDED
