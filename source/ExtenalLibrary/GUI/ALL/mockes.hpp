#ifndef MOCKES_HPP_INCLUDED
#define MOCKES_HPP_INCLUDED

#include <gmock/gmock.h>
#include <vector>
#include <string>
#include "../../../KernelLibrary/UIHost/Interfaces.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"
#include "../../../Core/PlusInterface.h"

using namespace Netspecters::KernelPlus::UIHost;
using namespace Netspecters::KernelPlus::ProtocolManager;
using testing::_;
using testing::Invoke;
using testing::Return;

class MockITaskGroupManager : public ITaskGroupManager
{
	public:
		MockITaskGroupManager( void )
		{
			EXPECT_CALL( *this, getGroupHandleList( _ ) )
			.WillRepeatedly( Invoke( this, &MockITaskGroupManager::DO_getGroupHandleList ) );

			ON_CALL( *this, addGroup( _, _, _ ) )
			.WillByDefault( Invoke( this, &MockITaskGroupManager::DO_addGroup ) );

			EXPECT_CALL( *this, addProtocolTaskToGroup( _, _ ) )
			.WillRepeatedly( Return( 1 ) );

			ON_CALL( *this, switchGroupRunningState( _, _ ) )
			.WillByDefault( Invoke( this, &MockITaskGroupManager::DO_switchGroupRunningState ) );

			ON_CALL( *this, getGroupStatistics( _, _ ) )
			.WillByDefault( Invoke( this, &MockITaskGroupManager::DO_getGroupStatistics ) );
		}

		MOCK_METHOD3( addGroup, NSAPI GROUP_HANDLE( const char *group_file_name_str_ptr, bool visable, uintptr_t group_tag ) );
		MOCK_METHOD2( delGroup, void NSAPI( GROUP_HANDLE group_handle, bool delete_group_file ) );
		MOCK_METHOD1( getGroupHandleList, NSAPI size_t ( GROUP_HANDLE *group_handle_buf_ptr ) );
		MOCK_METHOD1( getGroupID, NSAPI uint32_t ( GROUP_HANDLE group_handle ) );
		MOCK_METHOD2( getGroupStatistics, NSAPI size_t ( GROUP_HANDLE group_handle, ST_GroupInformation *group_infor_buf_ptr ) );
		MOCK_METHOD2( addProtocolTaskToGroup, NSAPI GROUP_TASK_HANDLE( GROUP_HANDLE group_handle, const IProtocol::ST_TaskInitParam &task_init_param ) );
		MOCK_METHOD1( removeProtocolTask, void NSAPI( GROUP_TASK_HANDLE group_task_handle ) );
		MOCK_METHOD2( switchGroupRunningState, void NSAPI( GROUP_HANDLE group_handle, bool pause ) );
		MOCK_METHOD1( getGroupErrorCode, NSAPI ENM_GroupErrorCode( GROUP_HANDLE group_handle ) );
		MOCK_METHOD2( storeTaskStateData, int NSAPI( GROUP_TASK_HANDLE group_task_handle, IProtocol::ST_TaskStateData *task_state_data_ptr ) );

		size_t DO_getGroupHandleList( GROUP_HANDLE *group_handle_buf_ptr )
		{
			size_t handle_num = m_taskes.size();
			if( group_handle_buf_ptr )
			{
				for( size_t i = 1; i < handle_num; i++, group_handle_buf_ptr++ )
					*group_handle_buf_ptr = i;
			}
			return handle_num * sizeof( GROUP_HANDLE );
		}
		GROUP_HANDLE DO_addGroup( const char *group_file_name_str_ptr, bool visable, uintptr_t group_tag )
		{
			if( !group_file_name_str_ptr )
				group_file_name_str_ptr = "MEM_FILE";
			m_taskes.push_back( group_file_name_str_ptr );
			return m_taskes.size() - 1;
		}
		void DO_switchGroupRunningState( GROUP_HANDLE group_handle, bool pause )
		{
			;
		}
		size_t DO_getGroupStatistics( GROUP_HANDLE group_handle, ST_GroupInformation *group_infor_buf_ptr )
		{
			size_t result = sizeof( ST_GroupInformation );
			if( group_infor_buf_ptr && group_handle < m_taskes.size() )
			{
				group_infor_buf_ptr->group_file_name_str_ptr = m_taskes[group_handle].c_str();
				group_infor_buf_ptr->group_file_size = 10280;
				group_infor_buf_ptr->resource_health_level = 100;
				group_infor_buf_ptr->group_id = 0;
				group_infor_buf_ptr->group_state = ITaskGroupManager::gsRunning;
				group_infor_buf_ptr->recv_data_speed = 15 * 1024;
				group_infor_buf_ptr->recved_data_size = 1026;
				group_infor_buf_ptr->send_data_size = 0;
				group_infor_buf_ptr->send_data_speed = 0;
				group_infor_buf_ptr->user_group_tag = 0;
			}
			return result;
		}

	private:
		std::vector<std::string> m_taskes;
};

class MockIProtocol : public IProtocol
{
	public:
		MOCK_METHOD4( onLoad, int NSAPI( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param ) );
		MOCK_METHOD0( onUnload, void NSAPI( void ) );
		MOCK_METHOD2( getParam, int NSAPI( const char *param_name, void *value_buffer_ptr ) );
		MOCK_METHOD2( setParam, void NSAPI*( const char *param_name, void *value_ptr ) );

		MOCK_METHOD5( startTask, NSAPI TASK_HANDLE( const ST_TaskInitParam &task_init_param, GROUP_TASK_HANDLE group_task_handle, ITaskGroupManager *task_group_manager_ptr, IDataContainer *data_container_ptr, ITaskMonitor *task_monitor_ptr ) );
		MOCK_METHOD1( stopTask, void NSAPI( TASK_HANDLE task_handle ) );
		MOCK_METHOD2( queryTaskInformation, int NSAPI( TASK_HANDLE task_handle, ST_TaskInformation &task_information ) );
		MOCK_METHOD1( delTask, void NSAPI( TASK_HANDLE task_handle ) );
};

class MockProtocolManager : public IProtocolManager
{
	public:
		MockProtocolManager( void )
		{
			EXPECT_CALL( *this, queryProtocol( testing::Ne(( const void* ) NULL ) ) )
			.WillRepeatedly( Return( &m_protocol ) );

			EXPECT_CALL( *this, getTaskGroupManager() )
			.WillRepeatedly( Return( &m_task_manager ) );
		}

		MOCK_METHOD4( onLoad, int NSAPI( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param ) );
		MOCK_METHOD0( onUnload, void NSAPI( void ) );
		MOCK_METHOD2( getParam, int NSAPI( const char *param_name, void *value_buffer_ptr ) );
		MOCK_METHOD2( setParam, void NSAPI*( const char *param_name, void *value_ptr ) );

		MOCK_METHOD2( registerProtocolHook, NSAPI int (const void *protocol_tag_ptr, IProtocolHook *protocol_hook_ptr ) );
		MOCK_METHOD2( unregisterProtocol, NSAPI void ( void *protocol_tag_ptr, int protocol_selector_id ) );
		MOCK_METHOD3( registerProtocol, NSAPI int ( const void *protocol_tag_ptr, int protocol_selector_id, IProtocol *protocol_ptr ) );
		MOCK_METHOD1( queryProtocol, NSAPI IProtocol *( const void *protocol_tag_ptr ) );
		MOCK_METHOD0( getTaskGroupManager, NSAPI ITaskGroupManager *( void ) );

	private:
		MockITaskGroupManager m_task_manager;
		MockIProtocol m_protocol;
};

#endif // MOCKES_HPP_INCLUDED
