#ifndef __MainForm_Core__
#define __MainForm_Core__

#include <boost/signal.hpp>
#include "GUI_RAD.h"
#include "NewJobDialog_Core.h"
#include "GUIPerformanceDialog_Core.h"
#include "NewDirDialog_Core.h"
#include "wxTaskView.h"

using namespace WXGUI;
using namespace wxTaskViewSpace;

class CMainForm_Core : public MainForm, public ITaskViewNotify, public boost::signals::trackable //信号槽自动跟踪
{
	public:
		CMainForm_Core( wxWindow *parent );

	private:
		void OnPressExit( wxCommandEvent &event );
		void onMinSize( wxIconizeEvent &event );
		void OnClose( wxCloseEvent &event );
		void onShowPerformanceDlg( wxCommandEvent &event );
		void onNewTaskButtonClick( wxCommandEvent &event );
		void onDelTask( wxCommandEvent &event );
		void onStartTaskButtonClick( wxCommandEvent &event );
		void onPauseButtonClick( wxCommandEvent &event );
		void onShowMethodChanged( wxCommandEvent &event );
		void onNewDirClick( wxCommandEvent &event );

		void onSetToolbarButtonState( bool start, bool pause );

		void update( void );

		wxTaskViewSpace::wxTaskView m_task_view;
};

#endif // __MainForm_Core__
