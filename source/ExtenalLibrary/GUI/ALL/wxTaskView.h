#ifndef  wxtaskview_INC
#define  wxtaskview_INC

#include <string>
#include <map>
#include <vector>
#include <memory>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <wx/treectrl.h>
#include "../../../StdLib/StdLib.h"
#include "../../../KernelLibrary/ProtocolManager/Interfaces.h"
#include "Canvas/wxWrap.hpp"

//#define USE_NATIVE_LIST_STYLE

using namespace Netspecters::KernelPlus::ProtocolManager;
using boost::property_tree::ptree;
using boost::property_tree::xml_parser::xml_parser_error;
using std::string;
using std::shared_ptr;

namespace wxTaskViewSpace {

struct ST_NodeBaseInformation
{
	wxString filesystem_location;
	wxString name;
};

struct ST_Node : public ST_NodeBaseInformation
{
	ST_Node( ST_Node *parent_node_ptr = NULL )
	{
		parent_ptr = parent_node_ptr;
		first_child_ptr = tail_ptr = NULL;
		left_brother_node_ptr = right_brother_node_ptr = NULL;
		has_child = has_active_data = false;

		if( parent_node_ptr )
			parent_node_ptr->addChild( this );
	}
	void addChild( ST_Node *child_node_ptr )
	{
		if( tail_ptr )
		{
			tail_ptr->right_brother_node_ptr = child_node_ptr;
			child_node_ptr->left_brother_node_ptr = tail_ptr;
			tail_ptr = child_node_ptr;
		}
		else
			first_child_ptr = tail_ptr = child_node_ptr;
	}
	void remove_child( ST_Node *child_node_ptr )
	{
		if( child_node_ptr->left_brother_node_ptr )
			child_node_ptr->left_brother_node_ptr->right_brother_node_ptr = child_node_ptr->right_brother_node_ptr;
		if( child_node_ptr->right_brother_node_ptr )
			child_node_ptr->right_brother_node_ptr->left_brother_node_ptr = child_node_ptr->left_brother_node_ptr;
		if( tail_ptr == child_node_ptr )
			tail_ptr = child_node_ptr->left_brother_node_ptr;
		if( first_child_ptr == child_node_ptr )
			first_child_ptr = child_node_ptr->right_brother_node_ptr;
	}

	ST_Node *parent_ptr;
	ST_Node *first_child_ptr, *tail_ptr;	///< 对于dir有效

	ST_Node *left_brother_node_ptr, *right_brother_node_ptr;

	bool has_child;
	bool has_active_data;
};

struct ST_ActiveNode : public ST_Node
{
	ST_ActiveNode( ST_Node *parent_node_ptr = NULL )
		: ST_Node( parent_node_ptr )
	{
		taskmanager_group_handle = NS_INVALID_HANDLE;
		taskmanager_group_id = 0;
		task_percent = 0;
		in_active_list_privious_ptr = in_active_list_next_ptr = NULL;
	}

	void addBrother( ST_ActiveNode *brother_node_ptr )
	{
		in_active_list_next_ptr = brother_node_ptr;
		brother_node_ptr->in_active_list_privious_ptr = this;
	}

	ST_ActiveNode *in_active_list_privious_ptr, *in_active_list_next_ptr;

	GROUP_HANDLE taskmanager_group_handle;
	uint32_t taskmanager_group_id;				///< 保存在 TaskManager 中的 group_id
	ITaskGroupManager::ST_GroupInformation task_group_information;
	double task_percent;						///任务完成百分比
};

class wxTaskView;
class wxTaskViewItem : public wxVListBox
{
		DECLARE_EVENT_TABLE()

	public:
		typedef void ( wxTaskView::*TClickCallback )( int select_index );

		wxTaskViewItem( wxWindow *parent );
		void setRoot( ST_Node *root_node_ptr, wxTaskViewItem *from = NULL, bool from_left = true );
		ST_Node *getRoot( void )
		{
			return m_root_node_ptr;
		}
		ST_Node *getNodePointerFromIndex( size_t n ) const;
		void registerClickEventHandler( wxTaskView *task_view_ptr, TClickCallback callback )
		{
			m_task_view_ptr = task_view_ptr;
			m_click_callback = callback;
		}
		bool hasFocus( void ) const {
			return m_has_focus;
		}
		void resetRoot( void );

	protected:
		typedef ITaskGroupManager::ST_GroupInformation ST_GroupInformation;

		//from wxVListBox
		wxCoord OnMeasureItem( size_t n ) const;
		void OnDrawItem( wxDC &dc, const wxRect &rect, size_t n ) const;
		void OnClick( wxMouseEvent &event );
		void onKillFocus( wxFocusEvent &event );
		void onSetFocus( wxFocusEvent &event );

		void OnDrawBackground( wxDC &dc, const wxRect &rect, size_t n ) const;
		void drawBackground( CwxWrapCanvas &canvas, const wxRect &rect, size_t n );

	private:
		struct ST_Animate : public wxTimer
		{
			ST_Animate( wxTaskViewItem *parent_ptr ) : m_parent_ptr( parent_ptr ) {}
			wxBitmap *source_buffer_ptr, *dest_buffer_ptr;
			wxMemoryDC source_dc, dest_dc;
			unsigned times;
			bool from_left;

			void start( void );
private:
			void Notify( void );	//from wxTimer
			wxTaskViewItem *m_parent_ptr;
		} m_animate;

		ST_Node *m_root_node_ptr;
		unsigned m_item_height, m_selected_item_height;
		TClickCallback m_click_callback;
		wxTaskView *m_task_view_ptr;
		bool m_has_focus;
		bool m_enable_update;
};

class wxPathView : public wxPanel
{
		DECLARE_EVENT_TABLE()
		struct ST_PathItem
		{
			ST_Node *node_ptr;
			int text_area_width;
		};
	public:
		typedef void ( wxTaskView::*TClickCallback )( ST_Node *need_select_node_ptr );

		wxPathView( wxWindow *parent, wxSize size = wxDefaultSize );
		void registerClickEventHandler( wxTaskView *task_view_ptr )
		{
			m_task_view_ptr = task_view_ptr;
		}
		void push( ST_Node *node_ptr );
		void pop( unsigned num = 1 );

	private:
		void onPaint( wxPaintEvent &event );
		void onClick( wxMouseEvent &event );

		wxTaskView *m_task_view_ptr;
		std::vector<shared_ptr<ST_PathItem>> m_path_stack;
		int m_select_index;
};

class ITaskViewNotify
{
	public:
		virtual void onSetToolbarButtonState( bool start, bool pause ) = 0;
};

#define ITEM_HANDLE uintptr_t
class wxTaskView
{
	public:
		struct ST_Child
		{
			wxTaskViewItem *left_view_ptr;
			wxTaskViewItem *right_view_ptr;
			wxPathView *path_view_ptr;
			wxMenu *pop_menu_ptr;
			wxTreeCtrl *tree_ptr;
		};
		wxTaskView( const ST_Child &child,
					ITaskViewNotify *task_view_notify_ptr );
		~wxTaskView( void );

		ST_Node *getRootNodePtr( void ) const {
			return m_root_node_ptr;
		}
		wxMenu *getPopupMenu( void ) {
			return m_child.pop_menu_ptr;
		}
		///更新活动任务数据
		void update_active_item(void);

		/**
		 * @name wx事件回调
		 * @{
		*/
		void onTaskViewFocus( const wxTaskViewItem *item_ptr );
		void onPathSelect( ST_Node *need_select_node_ptr );
		///@}

		/**
		 * @name 配置文件操作函数
		 * @{
		 */
		void createNewConfigure( const string &config_filename );
		bool loadConfigure( const wxString &config_filename );
		void saveConfigure( const wxString &config_filename );
		/**
		 * @}
		 */

		/**
		 * @name Item操作函数
		 * @{
		 */
		void deleteSelectedItemes( bool real_del = false );
		void addItem( const ST_ActiveNode &active_node );
		ITEM_HANDLE findItem( uint32_t item_tag );
		void pauseSelectedItemes( void );
		void startSelectedItemes( void );
		bool selectedItemIsDir( void );
		/**
		 * @}
		 */

	private:
		void OnLeftViewSelect( int select_index );
		void OnRightViewSelect( int select_index );

		ST_Node *getNextSelectedNodePtr( int &id );

		ST_Node *loadNodeFromConfigFile( ptree *ptree_ptr, ST_Node *parent_node_ptr, string node_path );
		void saveNodeToConfigFile( ptree *ptree_ptr, ST_Node *node_ptr, string node_path );
		void addActiveNode( ST_ActiveNode *active_node_ptr );
		void removeNode( ST_Node *node_ptr );
		void setToolbarButtonState( ST_Node *node_ptr );
		void toggleSelectedItemesRunningState( bool pause );

	private:
		ST_Child m_child;
		ITaskViewNotify *m_task_view_notify_ptr;

		ST_Node *m_root_node_ptr;
		ST_ActiveNode *m_active_node_list_head_ptr;
		std::map<uint32_t, ST_ActiveNode*> m_active_node_key_table;
		unsigned m_level;	//
};

}

#endif   /* ----- #ifndef wxtaskview_INC  ----- */
