#ifndef __DragWindow_Core__
#define __DragWindow_Core__

#include "GUI_RAD.h"
#include <boost/signal.hpp>

using namespace WXGUI;

class CDragWindow_Core : public DragWindow
{
		class CDataObject : public wxDataObject
		{
			public:
				wxDataFormat GetPreferredFormat( Direction dir = Get ) const;
				size_t GetFormatCount( Direction dir = Get ) const;
				void GetAllFormats( wxDataFormat *formats, Direction dir = Get ) const;
				size_t GetDataSize( const wxDataFormat &format ) const;
				bool GetDataHere( const wxDataFormat &format, void *buf ) const;
		};

		class CDropTarget : public wxDropTarget
		{
			public:
				CDropTarget( wxDataObject *dataObject )
					: wxDropTarget( dataObject )
				{
				}
				bool OnDrop( wxCoord x, wxCoord y );
				wxDragResult OnData( wxCoord x, wxCoord y, wxDragResult def );
				bool GetData( void );
		};

	public:
		CDragWindow_Core( wxWindow *parent );
		wxMenu *getPopupMenu( void ) {
			return m_drag_form_popup_menu_ptr;
		}

	private:
		void OnMouseMove( wxMouseEvent &event );
		void OnMouseLeftDown( wxMouseEvent &event );
		void OnMouseLeftUp( wxMouseEvent &event );
		void onMouseEnter( wxMouseEvent &event );
		void onMouseLeave( wxMouseEvent &event );
		void onTaggleMainWindowVisable( wxCommandEvent &event );
		void OnPressExit( wxCommandEvent &event );

		void update( void );

		wxPoint m_delta;
};

#endif // __DragWindow_Core__
