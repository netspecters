#include "GUIPerformanceDialog_Core.h"
#include "GUI.h"

CPerformanceDialog_Core::CPerformanceDialog_Core( wxWindow *parent )
	: PerformanceDialog( parent )
{
	m_plugin_list_ptr->InsertColumn( 0, _( "插件" ) );
	m_plugin_list_ptr->InsertColumn( 1, _( "路径" ) );
}

NSAPI bool on_enum_plus( const ST_PluginInformation *plugin_infor_ptr, intptr_t user_data )
{
	CPerformanceDialog_Core *dlg_ptr = ( CPerformanceDialog_Core* )user_data;

	wxString name( wxString::FromUTF8( plugin_infor_ptr->name ) );
	wxString path( wxString::FromUTF8( plugin_infor_ptr->full_path_on_the_tree ) );
	long item_index = dlg_ptr->m_plugin_list_ptr->InsertItem( dlg_ptr->m_plugin_list_ptr->GetItemCount(), name );
	dlg_ptr->m_plugin_list_ptr->SetItem( item_index, 0, path );

	return true;
}

void CPerformanceDialog_Core::onPanelChange( wxListbookEvent &event )
{
	switch( event.GetSelection() )
	{
		case 0:
			break;
		case 1:	/* 显示插件列表 */
			NSGUI_INSTANCE.getPlusTreePtr()->enumPlus( &on_enum_plus, ( intptr_t )this );
			break;
	}
}
