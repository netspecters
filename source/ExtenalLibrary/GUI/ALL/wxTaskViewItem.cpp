#include "wxTaskView.h"
#include "GUI.h"

///定义在每个Item里，每行之间的间隔
#define INTERVAL 1
#define PROGRESS_BAR_HEIGHT 4
#define BORDER 2
#define ICON_SIZE	32
#define ICON_AREA_WIDTH	(ICON_SIZE + BORDER*2)
#define INFORMATION_AREA_TOP_OFFSET  (PROGRESS_BAR_HEIGHT+BORDER)
#define INFORMATION_AREA_LEFT_OFFSET (ICON_AREA_WIDTH + BORDER)
#define TRIANGLE_WIDTH 16
#define TRIANGLE_HEIGH 16

#define ANIMATE_TIME 500	//动画持续时间(毫秒)

using namespace wxTaskViewSpace;

BEGIN_EVENT_TABLE( wxTaskViewItem, wxVListBox )
	EVT_LEFT_DOWN( wxTaskViewItem::OnClick )
	EVT_RIGHT_DOWN( wxTaskViewItem::OnClick )
	EVT_KILL_FOCUS( wxTaskViewItem::onKillFocus )
	EVT_SET_FOCUS( wxTaskViewItem::onSetFocus )
END_EVENT_TABLE()

wxTaskViewItem::wxTaskViewItem( wxWindow *parent )
	: wxVListBox( parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_LIST | wxLC_VIRTUAL )
	, m_animate( this )
	, m_root_node_ptr( NULL )
	, m_enable_update( true )
{
	unsigned char_height = GetCharHeight();
	m_item_height = INFORMATION_AREA_TOP_OFFSET + INTERVAL + char_height * 2 + BORDER;
	m_selected_item_height = m_item_height + char_height + INTERVAL;

	SetMargins( 0, 3 );
}

void wxTaskViewItem::ST_Animate::Notify( void )
{
	if( times-- > 0 )
	{
		wxClientDC dc( m_parent_ptr );

		int width = m_parent_ptr->GetSize().GetWidth(), width_pos = from_left ? times : width - times ;
		dc.Blit( 0, 0, width_pos, m_parent_ptr->GetSize().GetHeight(), &dest_dc, width - width_pos, 0 );
		dc.Blit( width_pos, 0, width - width_pos, m_parent_ptr->GetSize().GetHeight(), &source_dc, 0, 0 );
	}
	else
	{
		Stop();
		m_parent_ptr->m_enable_update = true;
		m_parent_ptr->resetRoot();

		/* 释放分配的资源 */
		source_dc.SelectObject( wxNullBitmap );
		dest_dc.SelectObject( wxNullBitmap );
		delete source_buffer_ptr;
		delete dest_buffer_ptr;
	}
}

void wxTaskViewItem::ST_Animate::start( void )
{
	Start( ANIMATE_TIME / times );
}

void wxTaskViewItem::setRoot( ST_Node *root_node_ptr, wxTaskViewItem *from, bool from_left )
{
	if( !m_enable_update ) return;

	if( root_node_ptr != m_root_node_ptr )
	{
		m_root_node_ptr = root_node_ptr;

		if( from )
		{
			int src_width = from->GetSize().GetWidth(), src_height = from->GetSize().GetHeight();
			int dst_width = GetSize().GetWidth(), dst_height = GetSize().GetHeight();

			/* 创建拷贝缓存 */
			if( from_left )
			{
				m_animate.source_buffer_ptr = new wxBitmap( src_width, src_height );
				m_animate.dest_buffer_ptr = new wxBitmap( dst_width, dst_height );
			}
			else
			{
				m_animate.source_buffer_ptr = new wxBitmap( dst_width, dst_height );
				m_animate.dest_buffer_ptr = new wxBitmap( src_width, src_height );
			}

			/* 选入dc */
			m_animate.source_dc.SelectObject( *m_animate.source_buffer_ptr );
			m_animate.dest_dc.SelectObject( *m_animate.dest_buffer_ptr );

			/* 复制当前图像 */
			wxClientDC src_dc( from ), dst_dc( this );
			if( from_left )
			{
				m_animate.source_dc.Blit( 0, 0, src_width, src_height, &src_dc, 0, 0 );
				m_animate.dest_dc.Blit( 0, 0, dst_width, dst_height, &dst_dc, 0, 0 );
			}
			else
			{
				m_animate.source_dc.Blit( 0, 0, dst_width, dst_height, &dst_dc, 0, 0 );
				m_animate.dest_dc.Blit( 0, 0, src_width, src_height, &src_dc, 0, 0 );
			}

			/* 启动动画 */
			m_enable_update = false;
			m_animate.times = dst_width;
			m_animate.from_left = from_left;
			m_animate.start();
		}
		else
			resetRoot();
	}
}

void wxTaskViewItem::resetRoot( void )
{
	if( m_root_node_ptr )
	{
		/* count the number of node */
		unsigned node_count = 0;
		ST_Node *node_ptr = m_root_node_ptr->first_child_ptr;
		while( node_ptr )
		{
			node_count++;
			node_ptr = node_ptr->right_brother_node_ptr;
		}
		SetItemCount( node_count );
	}
	else
		Clear();

	Refresh();
}

ST_Node *wxTaskViewItem::getNodePointerFromIndex( size_t n ) const
{
	ST_Node *node_ptr = NULL;

	if( m_root_node_ptr )
	{
		node_ptr = m_root_node_ptr->first_child_ptr;
		while(( node_ptr ) && (( int )( --n ) >= 0 ) )
			node_ptr = node_ptr->right_brother_node_ptr;
	}

	return node_ptr;
}

void wxTaskViewItem::OnClick( wxMouseEvent &event )
{
	int select_index = HitTest( event.GetPosition() );

	if( event.LeftDown() )
	{
		if( m_click_callback )
		{
			if( select_index >= 0 )
				( m_task_view_ptr->*m_click_callback )( select_index );
			else
				SetSelection( -1 );
		}
	}
	else if( event.RightDown() )
	{
		SetSelection( select_index );

		/* 调整菜单项的可用性 */
		wxMenu *pop_menu_ptr = m_task_view_ptr->getPopupMenu();
		pop_menu_ptr->Enable( wxID_NEW_DIR, m_task_view_ptr->selectedItemIsDir() );

		PopupMenu( pop_menu_ptr );
	}
	Refresh();
	event.Skip();
}

wxCoord wxTaskViewItem::OnMeasureItem( size_t n ) const
{
	if( getNodePointerFromIndex( n )->has_child )
		return m_item_height;
	else
	{
		if( IsSelected( n ) )
			return m_selected_item_height;
		else
			return m_item_height;
	}
}

void wxTaskViewItem::drawBackground( CwxWrapCanvas &canvas, const wxRect &rect, size_t n )
{
	if( IsSelected( n ) )
	{
		CwxWrapCanvas::ST_Rect xrect( CwxWrapCanvas::rect_from_wxrect( rect ) );
		canvas.getBrush().setColor( 0xaf, 0xaf, 0xaf );
		canvas.drawRoundedRect( xrect, 5.0, true );
	}
	else
	{
		canvas.getBrush().setColor( 0xff, 0xff, 0xff );
		canvas.drawRoundedRect( CwxWrapCanvas::rect_from_wxrect( rect ), 0.0, true );
	}
}

void wxTaskViewItem::OnDrawBackground( wxDC &dc, const wxRect &rect, size_t n ) const
{
	//do nothing
}

void wxTaskViewItem::OnDrawItem( wxDC &dc, const wxRect &rect, size_t n ) const
{
	struct __ST_ActiveTaskInfor
	{
		ST_GroupInformation task_group_information;
		double task_percent;
	};

	if( !m_root_node_ptr ) return;

	CwxWrapCanvas canvas( dc );
	canvas.clear( 0xff );

	(( wxTaskViewItem* )this )->drawBackground( canvas, rect, n );

	/* get node ptr from index */
	ST_Node *node_ptr = getNodePointerFromIndex( n );
	ST_ActiveNode *active_node_ptr = static_cast<ST_ActiveNode*>( node_ptr );	//当 has_active_data 为真的时候才有效

	/* == draw task information area == */
	int infor_area_top = rect.GetTop() + INFORMATION_AREA_TOP_OFFSET;

	/* draw agg canvas layer */
	{
		/* draw triangle when node is dir */
		if( node_ptr->has_child )
		{
			canvas.getBrush().setColor( 0x20, 0x20, 0x20 );
			CwxWrapCanvas::ST_Point triangle_points[3];
			triangle_points[0].set( rect.GetWidth() - BORDER * 2 - 4, rect.GetTop() + m_item_height / 2 );
			triangle_points[1].set( triangle_points[0].x - TRIANGLE_WIDTH, triangle_points[0].y + TRIANGLE_HEIGH / 2 );
			triangle_points[2].set( triangle_points[0].x - TRIANGLE_WIDTH, triangle_points[1].y - TRIANGLE_HEIGH );
			canvas.drawPolygon( triangle_points, 3, true, 0.0, true );
		}

		if( node_ptr->has_active_data )
		{
			/* draw progress bar background */
			canvas.getBrush().setColor( 0x20, 0x20, 0x20 );
			canvas.drawRoundedRect( CwxWrapCanvas::ST_Rect( 7, rect.GetTop(), rect.GetWidth() - 10, rect.GetTop() + PROGRESS_BAR_HEIGHT - 1 ), 2, true );

			canvas.getBrush().setColor( 0x0d, 0x9b, 0x28 );
			double progress_bar_width = active_node_ptr->task_percent / 100 * ( rect.GetWidth() - 10 );
			canvas.drawRoundedRect( CwxWrapCanvas::ST_Rect( 7, rect.GetTop() - 1, progress_bar_width, rect.GetTop() + PROGRESS_BAR_HEIGHT ), 2, true );
		}
	}
	canvas.drawToDC();

	/* draw task information */
	if( node_ptr->has_child )	// node is dir
	{
		wxRect dir_item_rect( rect );
		dir_item_rect.SetWidth( rect.width - TRIANGLE_WIDTH - INFORMATION_AREA_LEFT_OFFSET );
		dir_item_rect.SetLeft( INFORMATION_AREA_LEFT_OFFSET );
		dc.DrawLabel( node_ptr->name, dir_item_rect, wxALIGN_CENTER );
	}
	else
	{
		if( node_ptr->has_active_data )	// node is file and actived
		{
			ST_ActiveNode *active_node_ptr = static_cast<ST_ActiveNode*>( node_ptr );

			/* draw file name */
			dc.DrawText( active_node_ptr->name, INFORMATION_AREA_LEFT_OFFSET, infor_area_top );

			/* draw persent */
			{
				wxFont old_font = dc.GetFont();
				wxFont new_font( old_font );
				new_font.SetWeight( wxFONTWEIGHT_BOLD );
				new_font.SetPointSize( new_font.GetPointSize() + 3 );
				dc.SetFont( new_font );

				wxString persent_str( wxString::Format( wxT( "%.1f%%" ), active_node_ptr->task_percent ) );
				dc.DrawLabel( persent_str, rect, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL );

				dc.SetFont( old_font );
			}

			/* draw extend information */
			wxString ex_infor_str( wxString::Format( _( "%s/S\t预计剩余时间:" )
								   , wxFileName::GetHumanReadableSize( wxULongLong( 0, active_node_ptr->task_group_information.recv_data_speed ) ).c_str()
												   ) );
			dc.DrawText( ex_infor_str, INFORMATION_AREA_LEFT_OFFSET, infor_area_top + GetCharHeight() + INTERVAL );

			/* draw selection information */
			if( IsSelected( n ) )
			{
				wxString sel_infor_str( wxString::Format( _( "共:%s\t剩余:%s\t总耗时:\t" )
										, wxFileName::GetHumanReadableSize( wxULongLong( 0, active_node_ptr->task_group_information.group_file_size ) ).c_str()
										, wxFileName::GetHumanReadableSize( wxULongLong( 0, active_node_ptr->task_group_information.group_file_size - active_node_ptr->task_group_information.recved_data_size ) ).c_str() ) );
				dc.DrawText( sel_infor_str, INFORMATION_AREA_LEFT_OFFSET, infor_area_top + GetCharHeight() * 2 + INTERVAL * 2 );
			}
		}
		else
		{
			/* draw file name */
			dc.DrawText( node_ptr->name, INFORMATION_AREA_LEFT_OFFSET, infor_area_top );
		}
	}
}

void wxTaskViewItem::onSetFocus( wxFocusEvent &event )
{
	m_has_focus = true;
	//m_task_view_ptr->onTaskViewFocus(this);
}

void wxTaskViewItem::onKillFocus( wxFocusEvent &event )
{
	m_has_focus = false;
	//m_task_view_ptr->onTaskViewFocus(this);
}
