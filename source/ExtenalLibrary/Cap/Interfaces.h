#ifndef CAPLIB_INTERFACES_H_INCLUDED
#define CAPLIB_INTERFACES_H_INCLUDED

#include <pcap.h>
#include <nids.h>

struct ST_PeerSocket
{
	int src_port, dst_port;
	uint32_t src_addr, dst_addr;
	void *user_data;
};

struct ST_Packet
{
	size_t count_new, count, offset;
	void *raw_data_ptr;	///<tcp(udp)数据
};

struct ST_TcpCapPackage
{
	struct ST_Packet client_packet;
	struct ST_Packet server_packet;
	struct ST_PeerSocket *peer_socket_infor_ptr;
};

struct ST_UdpCapPackage
{
	struct ST_Packet udp_packet;
	struct ST_PeerSocket peer_socket_infor;
};

typedef int (*TTCPCapCallback)(struct ST_TcpCapPackage *package_ptr, void *client_data, size_t *discard_len);
typedef void (*TUDPCapCallback)(struct ST_UdpCapPackage *package_ptr, void *client_data);
typedef int (*TENMInterfaceCallback)(const char *interface_name, const char *interface_description, void *client_data);

typedef int (*TEnumInterface)(TENMInterfaceCallback callback_func, void *client_data);
typedef int (*TDoCap)(const char *address_ptr, TTCPCapCallback tcp_callback_func, TUDPCapCallback udp_callback_func, void *client_data);
typedef int (*TGetURL)(const void *data_ptr, size_t data_size, char *output_url, size_t buf_size);

#endif // CAPLIB_INTERFACES_H_INCLUDED
