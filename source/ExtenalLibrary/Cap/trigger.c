#include "trigger.h"

struct ST_Trigger_Param trigger_param;

void trigger_set_udp()
{
	nids_register_udp(trigger_udp);
}

void trigger_set_tcp()
{
	nids_register_tcp(trigger_tcp);
}

/* libnids needs a nids_register_udp()... */
void trigger_udp(struct tuple4 *addr, u_char *data, int len, struct ip *pkt)
{
	struct ST_UdpCapPackage udp_cap_package = {
		{len, len, 0, (void*)data},
		{ addr->source, addr->dest, addr->saddr, addr->daddr}
	};

	if(trigger_param.udp_callback_func)
		trigger_param.udp_callback_func(&udp_cap_package, trigger_param.client_data);
}

void trigger_tcp(struct tcp_stream *ts, void **point_to_peer_socket_ptr)
{
	switch(ts->nids_state)
	{
		case NIDS_JUST_EST:
			/* create peer socket struct */
			*point_to_peer_socket_ptr = malloc(sizeof(struct ST_PeerSocket));
			struct ST_PeerSocket *peer_socket_ptr = (struct ST_PeerSocket*) * point_to_peer_socket_ptr;
			peer_socket_ptr->dst_port = ts->addr.dest;
			peer_socket_ptr->src_port = ts->addr.source;
			peer_socket_ptr->src_addr = ts->addr.saddr;
			peer_socket_ptr->dst_addr = ts->addr.daddr;
			peer_socket_ptr->user_data = NULL;

			ts->client.collect++;
			ts->server.collect++;
			break;

		case NIDS_DATA:
			{
				struct ST_TcpCapPackage tcp_cap_package = {
					{ts->server.count_new, ts->server.count, ts->server.offset},
					{ts->client.count_new, ts->client.count, ts->client.offset},
					(struct ST_PeerSocket*)*point_to_peer_socket_ptr
				};

				size_t discard_len = 0;
				if(trigger_param.tcp_callback_func && trigger_param.tcp_callback_func(&tcp_cap_package, trigger_param.client_data, &discard_len))
					nids_discard(ts, discard_len);
			}
			break;

		default:
			free(*point_to_peer_socket_ptr);
			break;
	}
}

void trigger_run(void)
{
	nids_run();
}

