#ifndef TRIGGER_H
#define TRIGGER_H

#include <sys/types.h>
#include <sys/uio.h>
#include <netinet/in.h>

#include <stdlib.h>
#include <libnet.h>
#include <nids.h>

#include "Interfaces.h"

#define TRIGGER_TCP_RAW_TIMEOUT		30

struct ST_Trigger_Param
{
	void *client_data;
	TTCPCapCallback tcp_callback_func;
	TUDPCapCallback udp_callback_func;
};

extern struct ST_Trigger_Param trigger_param;

void trigger_set_udp();
void trigger_set_tcp();

/* 用于nids注册的回调函数 */
void	trigger_udp(struct tuple4 *addr, u_char *data, int len, struct ip *pkt);
void	trigger_tcp(struct tcp_stream *ts, void **conn_save);

void trigger_run(void);

#endif /* TRIGGER_H */

