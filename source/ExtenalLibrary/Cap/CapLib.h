#ifndef CAPLIB_H_INCLUDED
#define CAPLIB_H_INCLUDED

#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include "../../StdLib/StdLib.h"
#include "../../StdLib/NSFC.h"
#include "../../NSErrorCode.h"
#include "../../StdLib/LibraryMaco.hpp"

#include "Interfaces.h"
#include "trigger.h"

/* 要导出的函数 */
#ifdef __cplusplus
extern "C" {
#endif

	DLL_PUBLIC NSAPI int enumInterface(TENMInterfaceCallback callback_func, void *client_data);
	DLL_PUBLIC NSAPI int doCap(const char *address_ptr, TTCPCapCallback tcp_callback_func, TUDPCapCallback udp_callback_func, void *client_data);
	DLL_PUBLIC NSAPI int getURL(const void *data_ptr, size_t data_size, char *output_url, size_t buf_size);

#ifdef __cplusplus
}
#endif

#endif // CAPLIB_H_INCLUDED
