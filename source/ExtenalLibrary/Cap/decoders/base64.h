#ifndef BASE64_H
#define BASE64_H

int	base64_pton(char const *, unsigned char *, size_t);

#endif /* BASE64_H */

