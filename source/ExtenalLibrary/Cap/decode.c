#include <sys/types.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "decode.h"

extern int decode_http(u_char *, int, u_char *, int);

static struct decode decodes[] = {
	{ "http",	decode_http },
	{ NULL }
};

struct decode *getdecodebyname(const char *name)
{
	struct decode *dc;

	for (dc = decodes; dc->dc_name != NULL; dc++) {
		if (strcasecmp(dc->dc_name, name) == 0)
			return (dc);
	}
	return (NULL);
}

/* Strip a string buffer down to a maximum number of lines. */
int strip_lines(char *buf, int max_lines)
{
	char *p;
	int lines, nonascii;

	if (!buf) return (0);

	lines = nonascii = 0;

	for (p = buf; *p && lines < max_lines; p++) {
		if (*p == '\n') lines++;
		if (!isascii(*p)) nonascii++;
	}
	if (*p) *p = '\0';

	/* XXX - lame ciphertext heuristic */
	if (nonascii * 3 > p - buf)
		return (0);

	return (lines);
}

int is_ascii_string(char *buf, int len)
{
	int i;

	for (i = 0; i < len; i++)
		if (!isascii(buf[i])) return (0);

	return (1);
}

u_char *bufbuf(u_char *big, int blen, u_char *little, int llen)
{
	u_char *p;

         for (p = big; p <= big + blen - llen; p++) {
		 if (memcmp(p, little, llen) == 0)
			 return (p);
	 }
	 return (NULL);
}
