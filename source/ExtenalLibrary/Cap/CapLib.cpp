#include "CapLib.h"

#include <string>

using std::string;

struct ST_AddressInfor
{
	string interface_name;
	string filter_rule;
	int protocol_family;
};

/** @brief enumInterface
  *
  * @todo: document this function
  */
int enumInterface(TENMInterfaceCallback callback_func, void *client_data)
{
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *if_list_ptr;
	if(pcap_findalldevs(&if_list_ptr, errbuf) == 0)
	{
		while(if_list_ptr)
		{
			if(!callback_func(if_list_ptr->name, if_list_ptr->description, client_data))
				break;
			if_list_ptr = if_list_ptr->next;
		}
		return NSEC_SUCCESS;
	}
	else
		return NSEC_NSERROR;
}

bool parserAddress(const string &address, ST_AddressInfor &addr_infor)
{
	enum ENM_Stats { sIFname, sProtocolFamily, sProtocolPort, sRuleEx } stats = sIFname;
	string protocol_family, protocol_port, rule_ex;

	addr_infor.interface_name.reserve(64);
	protocol_family.reserve(64);
	protocol_port.reserve(64);
	rule_ex.reserve(128);

	/* 分割字符串 */
	const char *address_ptr = address.c_str() + 8;	//skip scheme
	while(char current_char = *address_ptr++)
	{
		switch(stats)
		{
			case sIFname:
				if(current_char == '/')
					stats = sProtocolFamily;
				else
					addr_infor.interface_name += current_char;
				break;

			case sProtocolFamily:
				if(current_char == ':')
					stats = sProtocolPort;
				else if(current_char == '/')
					stats = sRuleEx;
				else
					protocol_family += current_char;
				break;

			case sProtocolPort:
				if(current_char == '/')
					stats = sRuleEx;
				else
					protocol_port += current_char;
				break;

			case sRuleEx:
				rule_ex += current_char;
				break;
		}
	}

	/* 检测协议类型 */
	if("tcp" == protocol_family)
		addr_infor.protocol_family = IPPROTO_TCP;
	else if("udp" == protocol_family)
		addr_infor.protocol_family = IPPROTO_UDP;
	else return false;

	/* 构建规则 */
	//addr_infor.filter_rule

	return true;
}

/** @brief openCap
  *
  * @param address_ptr 指向嗅探地址的指针，地址格式见下方说明
  * @note 有效的地址格式为: nscap://interface-name/protocol-family[:port]/rule-extend
  */
int doCap(const char *address_ptr, TTCPCapCallback tcp_callback_func, TUDPCapCallback udp_callback_func, void *client_data)
{
	if(!address_ptr)
		RET_NS_ERR(NSEC_PARAMERROR);

	string address(address_ptr);

	/* 校验参数 */
	if(string::npos == address.find("nscap://"))
		RET_NS_ERR(NSEC_PARAMERROR);

	ST_AddressInfor addr_infor;
	if(!parserAddress(address, addr_infor))
		RET_NS_ERR(NSEC_PARAMERROR);

	/* 设置捕获器参数 */
	nids_params.device = const_cast<char*>(addr_infor.interface_name.c_str());
	if(!addr_infor.filter_rule.empty())	/* 应用规则 */
		nids_params.pcap_filter = const_cast<char*>(addr_infor.filter_rule.c_str());

	/* 设置回调 */
	trigger_param.tcp_callback_func = tcp_callback_func;
	trigger_param.udp_callback_func = udp_callback_func;
	trigger_set_tcp();
	trigger_set_udp();

	/* 运行 */
	trigger_run();

	return NSEC_SUCCESS;
}

int getURL(const void *data_ptr, size_t data_size, char *output_url, size_t buf_size)
{

}

