unit CrashReportMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, XPMan;

type
  TCrashReportForm = class(TForm)
    ErrDetails: TMemo;
    CheckBox1: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    XPManifest1: TXPManifest;
    Label1: TLabel;
    CheckBox2: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ErrDetailsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    netspecters_instance_event: THandle;
  end;

  PST_CrashInformation = ^ST_CrashInformation;
  ST_CrashInformation = packed record
    base_file_name_ptr: AnsiChar;
    crash_context: CONTEXT;
    crash_code: Cardinal;
  end;

var
  CrashReportForm   : TCrashReportForm;

implementation

{$R *.dfm}

procedure TCrashReportForm.FormCreate(Sender: TObject);
var
  crash_infor_ptr   : PST_CrashInformation;
  share_mem_handle  : THandle;
  base_name, status_file_name, dump_file_name: string;
begin
  //加载错误信息
  share_mem_handle := CreateFileMapping($FFFFFFFF, nil, PAGE_READWRITE, 0, SizeOf(ST_CrashInformation), 'Netspecters_Crash_Information');
  if (share_mem_handle > 0) and (GetLastError() = ERROR_ALREADY_EXISTS) then
  begin
    crash_infor_ptr := PST_CrashInformation(MapViewOfFile(share_mem_handle, FILE_MAP_ALL_ACCESS, 0, 0, SizeOf(ST_CrashInformation)));
    if crash_infor_ptr <> nil then
    begin
      //初始化错误对话框
      ErrDetails.Lines.Add(Format('EIP=0x%x', [crash_infor_ptr.crash_context.Eip]));

      //隐私信息
      base_name := crash_infor_ptr.base_file_name_ptr;
      status_file_name := base_name + '.status';
      dump_file_name := base_name + '.dump';
      ErrDetails.Lines.Add(Format('状态说明文件保存在:%s\%s', [ExtractFileDir(Application.ExeName), status_file_name]));
      ErrDetails.Lines.Add(Format('dump文件保存在:%s\%s', [ExtractFileDir(Application.ExeName), dump_file_name]));

      //卸载共享内存
      UnmapViewOfFile(crash_infor_ptr);
    end;
  end;
  CloseHandle(share_mem_handle);
end;

procedure TCrashReportForm.Button2Click(Sender: TObject);
begin
  SetEvent(netspecters_instance_event);
  CloseHandle(netspecters_instance_event);
  close();
end;

procedure TCrashReportForm.Button1Click(Sender: TObject);
var
  start_infor       : STARTUPINFO;
  process_infor     : PROCESS_INFORMATION;
begin
  SetEvent(netspecters_instance_event);
  CloseHandle(netspecters_instance_event);

  if CheckBox1.Checked then
  begin
    FillChar(start_infor, SizeOf(start_infor), 0);
    FillChar(process_infor, SizeOf(start_infor), 0);
    if CreateProcess('CrashReport.exe', nil, nil, nil, False, CREATE_DEFAULT_ERROR_MODE, nil, nil, start_infor, process_infor) then
      CloseHandle(process_infor.hProcess);
  end;

  close();
end;

procedure TCrashReportForm.ErrDetailsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Button1.SetFocus;
end;

end.

