program CrashReport;

uses
  Forms, Windows,
  CrashReportMain in 'CrashReportMain.pas' {CrashReportForm};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TCrashReportForm, CrashReportForm);

  CrashReportForm.netspecters_instance_event := OpenEvent(EVENT_ALL_ACCESS, False, 'NetspectersInstance');
  if CrashReportForm.netspecters_instance_event > 0 then
    Application.Run
  else
  begin
    MessageBox(0, 'Crash Report没有发现Netspecters的异常运行实例！', '错误', MB_OK or MB_ICONWARNING);
    CrashReportForm.Free;
  end;
end.

