object CrashReportForm: TCrashReportForm
  Left = 350
  Top = 137
  Width = 456
  Height = 497
  BorderIcons = [biSystemMenu]
  Caption = 'CrashReport'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 150
    Width = 133
    Height = 13
    AutoSize = False
    Caption = #35814#32454#25216#26415#20449#24687#22914#19979#65306
  end
  object ErrDetails: TMemo
    Left = 13
    Top = 173
    Width = 419
    Height = 225
    TabStop = False
    Color = clBtnFace
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
    OnMouseDown = ErrDetailsMouseDown
  end
  object CheckBox1: TCheckBox
    Left = 30
    Top = 414
    Width = 116
    Height = 17
    Caption = #37325#21551'Netspecters'
    TabOrder = 2
  end
  object Button1: TButton
    Left = 275
    Top = 434
    Width = 75
    Height = 25
    Caption = #30830#23450
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 360
    Top = 433
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 1
    OnClick = Button2Click
  end
  object CheckBox2: TCheckBox
    Left = 30
    Top = 441
    Width = 116
    Height = 17
    Caption = #21457#36865#25216#26415#25253#21578
    TabOrder = 4
  end
  object XPManifest1: TXPManifest
    Left = 47
    Top = 317
  end
end
