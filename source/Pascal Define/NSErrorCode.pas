Unit NSErrorCode;

Interface

Uses
	 Windows {OS/Compeleter Units};

Type
	 DWord = Cardinal;

		{
     30~31  00:成功(系统代码) 01:供参考(信息) 10:警告 11:错误

     29      1
     28      0:NS官方使用  1:第三方插件使用    [31,30,29,28: 0010:$2(成功) 0110:$6(信息) 1010:$A(警告) 1110:$E(错误)]

     27~24  保留使用,必须置零  [$0]
     23~0   错误码索引
    }

Const
	 {NSEC:NetSpectersErrorCode 网络精灵错误代码}
	 NSEC_SUCCESS		 = 1;
	 NSEC_NORMALERROR = -1;

	 {成功}

		{参考信息}
    NSEC_WaitForFinish = $6000001;
    (* 定义在AddInInterface单元中
    NSEC_InterfaceNeedRelease = $6000002;
    NSEC_PlusDLLNeedFree = $6000003;
    NSEC_BeforChildPlusesInit=$60000004;
    NSEC_AfterChildPlusesInit=$60000005;
    *)
    NSEC_Initiated  = $60000006;

    {警告}
	 NSEC_SkinControlNotExists = $A0000001;

	 {错误}
	 NSEC_ParamError					 = $E0000001;
	 NSEC_UnInit							 = $E0000002;
	 NSEC_GetInterfaceFail		 = $E0000003;
	 NSEC_ReleaseInterfaceFail = $E0000004;
	 NSEC_SysResFull					 = $E0000005;
	 NSEC_SysResEmpty					= $E000006;
	 NSEC_NoSkinFile					 = $E000007;

	 NSEC_LoadConfigFail			 = $E000008;
	 NSEC_DirNotExists				 = $E000009;
	 NSEC_InvalidCall					= $E00000A;
	 NSEC_AlreadyExist				 = $E00000B;
	 NSEC_NOTExist						 = $E000000C;
	 NSEC_NotSupport					 = $E00000D;
	 NSEC_CanNotOpenKernelPipe = $E00000E;
	 NSEC_AllocMemoryError		 = $E000000F;
	 NSEC_AccessDecline				= $E0000010;
	 NSEC_BusyWait						 = $E0000011;
	 NSEC_BufferEmpty					= $E0000012;
	 NSEC_UnKnown							= $E0000013;

	 NSEC_Timeout		= $E0000014;
	 NSEC_BufferFull = $E0000015;

Implementation

End.

