unit pascal_PlusInterface;

interface

uses
  KOL;

type
  TBlockID = Word;
  HRECORD = Cardinal;
  HDIR = Cardinal;

type
  ENM_Event_ID = (
    eidJobAdded,                        //成功添加	param:0			data_ptr:NULL
    eidJobDeleted,                      //成功删除	param:0
    eidJobSuspend,                      //成功挂起	param:(挂起原因)0:来自外部的命令挂起，1:完成任务引发的挂起
    eidJobResume,
    eidJobError,

    eidPeerAdded,                       //成功添加	param:0			data_ptr:NULL
    eidPeerDelete,                      //成功删除	param:0
    eidPeerActive,                      //Peer激活	param:0			data_ptr:NULL
    eidPeerError                        //错误		param:ENM_PeerErrorCode	data_ptr:error msg string pointer
    );
  ENM_PM_InformationType = (
    imEventListener,                    //ctrl_code:0	wparam:0;	lparam:0;	data_ptr:(out:IProtocolEventListener)pointer to event listener
    imJob,                              //see below for details
    imProtocol,
    imProtocolPreprocessor,
    imProtocolSelector,
    imManagerCtrl                       //see below for details
    );
  ENM_JobCtrlCode = (
    jcAdd,                              //wparam:job_tag;	lparam:0;					data_ptr:job_param_ptr	return job_handle
    jcAppend,                           //wparam:job_tag;	lparam:parent_job_handle	data_ptr:job_param_ptr	return parent_job_handle;
    jcSuspend,                          //wparam:0;			lparam:job_handle;			data_ptr:nil
    jcResume,                           //wparam:0;			lparam:job_handle;			data_ptr:nil
    jcDelete,                           //wparam:0;			lparam:job_handle;			data_ptr:nil
    jcThread,                           //wparam:0;			lparam:job_handle;			data_ptr:(out)
    jcUpdateTag,                        //wparam:job_tag	lparam:job_handle;			data_ptr:nil			return old_job_tag
    jcStatistic                         //wparam:ST_JobInformation array count;	lparam:0;	data_ptr:(in:*ST_JobInformation) pointer to ST_JobInformation array;	return number of item in array
    );
  ENM_ContainerType = (ctLiner, ctSpliter);

  ST_ProtocolJob_Extend_Data = packed record
    size: Integer;
    usr_name: PChar;
    usr_passwd: PChar;
    cookie: PChar;

    proxy_type: Integer;
    proxy_address: PChar;
    proxy_port: Integer;
  end;

  ST_ProtocolParam = packed record
    protocol_address: PAnsiChar;            //ansi编码
    extend_data_ptr: Pointer;

    pm_settings_ptr, data_container_ptr: Pointer; //gui不需要填写
  end;

  PST_JobParam = ^ST_JobParam;
  ST_JobParam = packed record
    local_file_path: PWideChar;
    protocol_selector_id: Integer;      //0：表示默认协议选择器
    enable_preprocessor: LongBool;
    protocol_param: ST_ProtocolParam;
  end;

  ST_PM_CtrlInformation = packed record
    infor_size: Integer;
    pm_information_type: Cardinal;
    ctrl_code: Cardinal;
    wparam, lparam: LongInt;
    data_ptr: Pointer;
  end;

  IProtocolMonitor = Pointer;
  IProtocol = Pointer;
  INetLayer = Pointer;
  IDataContainerManager = Pointer;
  IJobMonitor = Pointer;
  IProtocolPreprocessor = Pointer;

  INSPlus = class;
  IPlusTree = class
  public
    function openDir(abs_dir_path: PChar): HDIR; virtual; stdcall; abstract;
    function openRecord(dir_handle: HDIR; record_name_str_ptr: PChar; open_always: LongBool): HRECORD; virtual; stdcall; abstract;

    procedure delRecord(dir_handle: HDIR; record_handle: HRECORD); virtual; stdcall; abstract;
    function readRecord(record_handle: HRECORD; buffer_ptr: PByte; size: Integer): Integer; virtual; stdcall; abstract;
    function writeRecord(record_handle: HRECORD; buffer_ptr: PByte; size: Integer): Integer; virtual; stdcall; abstract;
    procedure closeRecord(record_handle: HRECORD); virtual; stdcall; abstract;
    procedure closeDir(dir_handle: HDIR); virtual; stdcall; abstract;

    function installPlus(plus_library_string_ptr: PChar; path_string_ptr: PChar; plus_name_string_ptr: PChar; reserved: Integer): Integer; virtual; stdcall; abstract;
    procedure uninstallPlus(path_string_ptr: PChar; plus_name_string_ptr: PChar); virtual; stdcall; abstract;
    function getPlusTreeWorkPath(work_path_buffer: PChar; buffer_size: Integer): Integer; virtual; stdcall; abstract;
  end;

  INSPlus = class
  public
    function onLoad(plus_tree_ptr: IPlusTree; parent_plus_ptr: INSPlus; self_dir_handle: HDIR; default_param: Integer): Integer; virtual; stdcall; abstract;
    procedure onUnload(); virtual; stdcall; abstract;
  end;

  IProtocolEventListener = class
  public
    function onEvent(event_id: ENM_Event_ID; job_handle: LongInt; param: Integer; data: Pointer; job_tag: LongInt): Integer; virtual; stdcall; abstract;
  end;

  IProtocolManager = class(INSPlus)
  public
    function registerListener(listener_ptr: IProtocolEventListener): LongBool; virtual; stdcall; abstract;
    function registerPreprocessor(protocol_tag_ptr: PByte; protocol_preprocessor_ptr: IProtocolPreprocessor): Integer; virtual; stdcall; abstract;
    function registerProtocol(protocol_selector_id: Integer; protocol_ptr: IProtocol; protocol_tag_ptr: Pointer; data_container_type: ENM_ContainerType): Integer; virtual; stdcall; abstract;

    function superCall(var ctrl_infor: ST_PM_CtrlInformation): Integer; virtual; stdcall; abstract;
    function getDefaultJobMonitor(): IJobMonitor; virtual; stdcall; abstract;
  end;

implementation

end.

