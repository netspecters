#ifndef  nserrorcode_INC
#define  nserrorcode_INC

#if defined(_WIN32) || defined(_WIN64)
#	include <windows.h>
#else
#	include <errno.h>
#endif
#include <system_error>

#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)

#define NSEC_SUCCESS		0x01

#define NSEC_NSERROR		0x00

#define NSEC_OSERROR 		(NSEC_NSERROR-1)	/** 函数调用过程中发生操作系统函数调用失败的错误 */
#define NSEC_PARAMERROR	(NSEC_NSERROR-2)
#define NSEC_INVALID_CALL	(NSEC_NSERROR-3)
#define NSEC_BUSY_WAIT	(NSEC_NSERROR-4)
#define NSEC_ON_PROCESS	(NSEC_NSERROR-5)
#define NSEC_CONFLICT		(NSEC_NSERROR-6)
#define NSEC_OUT_RANGE	(NSEC_NSERROR-7)
#define NSEC_NOT_SUPPORT	(NSEC_NSERROR-8)
#define NSEC_NOT_INIT		(NSEC_NSERROR-9)
#define NSEC_FULL			(NSEC_NSERROR-10)
#define NSEC_REFUSED		(NSEC_NSERROR-11)

static class netspecters_category : public std::error_category
{
	public:
		const char *name() const {
			return "netspecters";
		}
		std::string message( int ev ) const
		{
			switch( ev )
			{
				case NSEC_PARAMERROR:
					return "参数错误";
				case NSEC_INVALID_CALL:
					return "无效的调用";
				default:
					return "未知错误码";
			}
		}
} ns_category;

///NS系统中用于定义失败返回值的宏
#if defined(_WIN32) || defined(_WIN64)
#	define RET_ERR(result,error)\
	do{\
		SetLastError(error);\
		return result;\
	}while(0)
#else
#	define RET_ERR(result,error)\
	do{\
		errno = error;\
		return result;\
	}while(0)
#endif
#define RET_NS_ERR(error) RET_ERR(NSEC_NSERROR,error)
#define _RET_NS_ERR(error) return std::error_code(error,ns_category)

#endif   /* ----- #ifndef nserrorcode_INC  ----- */
