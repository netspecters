from pyplusplus import module_builder

#Creating an instance of class that will help you to expose your declarations
mb = module_builder.module_builder_t( [r"../proxy.h"]
                                      , gccxml_path=r""
                                      , working_directory=r"../"
                                      , include_paths=['../../ExtenalLibrary/CommunicateLibrary', '../']
                                      , define_symbols=[] )

#Creating code creator. After this step you should not modify/customize declarations.
mb.build_code_creator( module_name='netspecters' )

#Writing code to file.
mb.write_module( '../python_bindings.cpp' )