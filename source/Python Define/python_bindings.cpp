#include "proxy.h"

#include "boost/python.hpp"
#include "boost/python/suite/indexing/vector_indexing_suite.hpp"

namespace bp = boost::python;

void translate_system_error( const std::system_error& exc ){
    PyErr_SetString( PyExc_RuntimeError, exc.code().message().c_str() );
}

BOOST_PYTHON_MODULE(Netspecters){
    { //::std::vector< unsigned int >
        typedef bp::class_< std::vector< unsigned int > > vector_less__unsigned_int__greater__exposer_t;
        vector_less__unsigned_int__greater__exposer_t vector_less__unsigned_int__greater__exposer = vector_less__unsigned_int__greater__exposer_t( "vector_less__unsigned_int__greater_" );
        bp::scope vector_less__unsigned_int__greater__scope( vector_less__unsigned_int__greater__exposer );
        vector_less__unsigned_int__greater__exposer.def( bp::vector_indexing_suite< ::std::vector< unsigned int >, true >() );
    }

    { //::std::vector< ST_GroupInformation_py >
        typedef bp::class_< std::vector< ST_GroupInformation_py > > vector_less__ST_GroupInformation_py__greater__exposer_t;
        vector_less__ST_GroupInformation_py__greater__exposer_t vector_less__ST_GroupInformation_py__greater__exposer = vector_less__ST_GroupInformation_py__greater__exposer_t( "vector_less__ST_GroupInformation_py__greater_" );
        bp::scope vector_less__ST_GroupInformation_py__greater__scope( vector_less__ST_GroupInformation_py__greater__exposer );
        vector_less__ST_GroupInformation_py__greater__exposer.def( bp::vector_indexing_suite< ::std::vector< ST_GroupInformation_py > >() );
    }

    { //::std::vector< ENM_TaskStopCode >
        typedef bp::class_< std::vector< ENM_TaskStopCode > > vector_less__ENM_TaskStopCode__greater__exposer_t;
        vector_less__ENM_TaskStopCode__greater__exposer_t vector_less__ENM_TaskStopCode__greater__exposer = vector_less__ENM_TaskStopCode__greater__exposer_t( "vector_less__ENM_TaskStopCode__greater_" );
        bp::scope vector_less__ENM_TaskStopCode__greater__scope( vector_less__ENM_TaskStopCode__greater__exposer );
        vector_less__ENM_TaskStopCode__greater__exposer.def( bp::vector_indexing_suite< ::std::vector< ENM_TaskStopCode >, true >() );
    }

    bp::enum_< ENM_TaskStopCode>("ENM_TaskStopCode")
        .value("tscRunning", ENM_TaskStopCode::tscRunning)
        .value("tscError", ENM_TaskStopCode::tscError)
        .value("tscPause", ENM_TaskStopCode::tscPause)
        .export_values()
        ;

    bp::class_< CTaskGroupManagerProxy >( "CTaskGroupManagerProxy", bp::init< >() )
        .def(
            "addGroup"
            , (::GROUP_HANDLE ( ::CTaskGroupManagerProxy::* )( char const *,bool,::uintptr_t ) )( &::CTaskGroupManagerProxy::addGroup )
            , ( bp::arg("group_file_name_str_ptr"), bp::arg("visable"), bp::arg("group_tag") ) )
        .def(
            "addProtocolTaskToGroup"
            , (::GROUP_TASK_HANDLE ( ::CTaskGroupManagerProxy::* )( ::GROUP_HANDLE,::ST_TaskInitParam_py const ) )( &::CTaskGroupManagerProxy::addProtocolTaskToGroup )
            , ( bp::arg("group_handle"), bp::arg("task_init_param") ) )
        .def(
            "delGroup"
            , (void ( ::CTaskGroupManagerProxy::* )( ::GROUP_HANDLE,bool ) )( &::CTaskGroupManagerProxy::delGroup )
            , ( bp::arg("group_handle"), bp::arg("delete_group_file") ) )
        .def(
            "getGroupHandleList"
            , (::std::vector< unsigned int > ( ::CTaskGroupManagerProxy::* )(  ) )( &::CTaskGroupManagerProxy::getGroupHandleList ) )
        .def(
            "getGroupID"
            , (::uint32_t ( ::CTaskGroupManagerProxy::* )( ::GROUP_HANDLE ) )( &::CTaskGroupManagerProxy::getGroupID )
            , ( bp::arg("group_handle") ) )
		.def(
            "getGroupStatistic"
            , (::ST_GroupInformation_py ( ::CTaskGroupManagerProxy::* )( ::GROUP_HANDLE ) )( &::CTaskGroupManagerProxy::getGroupStatistic )
            , ( bp::arg("group_handle") ) )
        .def(
            "getGroupStatistics"
            , (::std::vector< ST_GroupInformation_py > ( ::CTaskGroupManagerProxy::* )(  ) )( &::CTaskGroupManagerProxy::getGroupStatistics ) )
        .def(
            "getTaskStopCodes"
            , (::std::vector< ENM_TaskStopCode > ( ::CTaskGroupManagerProxy::* )( ::GROUP_HANDLE ) )( &::CTaskGroupManagerProxy::getTaskStopCodes )
            , ( bp::arg("group_handle") ) )
        .def(
            "removeProtocolTask"
            , (void ( ::CTaskGroupManagerProxy::* )( ::GROUP_TASK_HANDLE ) )( &::CTaskGroupManagerProxy::removeProtocolTask )
            , ( bp::arg("group_task_handle") ) );

    { //::ST_TaskInitParam_py
        typedef bp::class_< ST_TaskInitParam_py > ST_TaskInitParam_py_exposer_t;
        ST_TaskInitParam_py_exposer_t ST_TaskInitParam_py_exposer = ST_TaskInitParam_py_exposer_t( "ST_TaskInitParam_py" );
        bp::scope ST_TaskInitParam_py_scope( ST_TaskInitParam_py_exposer );
        bp::class_< ST_TaskInitParam_py::ST_TaskResource_py >( "ST_TaskResource_py" )
            .def_readwrite( "cookie_data", &ST_TaskInitParam_py::ST_TaskResource_py::cookie_data )
            .def_readwrite( "extend_data", &ST_TaskInitParam_py::ST_TaskResource_py::extend_data )
            .def_readwrite( "host_port", &ST_TaskInitParam_py::ST_TaskResource_py::host_port )
            .def_readwrite( "login_passwd", &ST_TaskInitParam_py::ST_TaskResource_py::login_passwd )
            .def_readwrite( "login_usr_name", &ST_TaskInitParam_py::ST_TaskResource_py::login_usr_name )
            .def_readwrite( "max_thread_num", &ST_TaskInitParam_py::ST_TaskResource_py::max_thread_num )
            .def_readwrite( "proxy_address", &ST_TaskInitParam_py::ST_TaskResource_py::proxy_address )
            .def_readwrite( "proxy_port", &ST_TaskInitParam_py::ST_TaskResource_py::proxy_port )
            .def_readwrite( "proxy_type", &ST_TaskInitParam_py::ST_TaskResource_py::proxy_type )
            .def_readwrite( "referer_uri", &ST_TaskInitParam_py::ST_TaskResource_py::referer_uri )
            .def_readwrite( "res_path", &ST_TaskInitParam_py::ST_TaskResource_py::res_path )
            .def_readwrite( "retry_limit", &ST_TaskInitParam_py::ST_TaskResource_py::retry_limit );
        ST_TaskInitParam_py_exposer.def_readwrite( "enable_hooker", &ST_TaskInitParam_py::enable_hooker );
        ST_TaskInitParam_py_exposer.def_readwrite( "selector_id", &ST_TaskInitParam_py::selector_id );
        ST_TaskInitParam_py_exposer.def_readwrite( "task_resource", &ST_TaskInitParam_py::task_resource );
    }

    { //::std::system_error
        bp::register_exception_translator< std::system_error >( &translate_system_error );
    }

    { //::clear_global_ipc_client

        typedef void ( *clear_global_ipc_client_function_type )(  );

        bp::def(
            "clear_global_ipc_client"
            , clear_global_ipc_client_function_type( &::clear_global_ipc_client ) );

    }

    { //::init_global_ipc_client

        typedef void ( *init_global_ipc_client_function_type )(  );

        bp::def(
            "init_global_ipc_client"
            , init_global_ipc_client_function_type( &::init_global_ipc_client ) );

    }
}
