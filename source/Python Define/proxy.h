#ifndef __PROXY_H_INCLUDED__
#define __PROXY_H_INCLUDED__

#include <vector>
#include <alloca.h>

#include "../KernelLibrary/InternalProtocol/http/Interfaces.h"
#include "../KernelLibrary/DataContainer/Interfaces.h"
#include "../KernelLibrary/ProtocolManager/Interfaces.h"
#include "../ExtenalLibrary/CommunicateLibrary/object_proxy.h"

using namespace Netspecters::KernelPlus;
using namespace ProtocolManager;
using namespace InternalProtocol;
using namespace DataContainer;

typedef IProtocol::ENM_TaskStopCode ENM_TaskStopCode;
typedef ITaskGroupManager::ST_GroupInformation ST_GroupInformation;

///任务初始化参数
struct ST_TaskInitParam_py
{
	int		selector_id;
	bool	enable_hooker;

	struct ST_TaskResource_py
	{
		int max_thread_num;
		int retry_limit;
		int proxy_type;
		int proxy_port;
		int host_port;
		std::string res_path, referer_uri, login_usr_name, login_passwd, proxy_address, cookie_data, extend_data;
	}	task_resource;
};

struct ST_GroupInformation_py
{
	uint32_t group_id;
	uintptr_t user_tag;
	off_t group_file_size;
	unsigned task_count;
	unsigned send_data_speed, recv_data_speed;
	off_t send_data_size, recved_data_size;
	std::string group_file_name_str;

	bool operator == (const ST_GroupInformation_py& infor)
	{
		bool eq = group_id == infor.group_id && user_tag == infor.user_tag && group_file_size == infor.group_file_size;
		eq &= task_count == infor.task_count && send_data_size == infor.send_data_size && recved_data_size == infor.recved_data_size;
		eq &= send_data_speed == infor.send_data_speed && recv_data_speed == infor.recv_data_speed;
		eq &= group_file_name_str == infor.group_file_name_str;
		return eq;
	}
};

class CTaskGroupManagerProxy: public CObjectProxyBase
{	public:
		CTaskGroupManagerProxy(): CObjectProxyBase( "/sys/protocol_manager/" ) {}
		_DECLARE_METHOD_3( GROUP_HANDLE, addGroup, const char *, group_file_name_str_ptr, bool, visable, uintptr_t, group_tag )
		_DECLARE_METHOD_VOID_2( delGroup, GROUP_HANDLE, group_handle, bool, delete_group_file )
		std::vector<GROUP_HANDLE> getGroupHandleList();
		ST_GroupInformation_py getGroupStatistic( GROUP_HANDLE group_handle );
		std::vector<ST_GroupInformation_py> getGroupStatistics();
		_DECLARE_METHOD_1( uint32_t, getGroupID, GROUP_HANDLE, group_handle )
		GROUP_TASK_HANDLE addProtocolTaskToGroup( GROUP_HANDLE group_handle, const ST_TaskInitParam_py task_init_param );
		_DECLARE_METHOD_VOID_1( removeProtocolTask, GROUP_TASK_HANDLE, group_task_handle )
		std::vector<ENM_TaskStopCode> getTaskStopCodes( GROUP_HANDLE group_handle );
};

#endif // __PROXY_H_INCLUDED__
