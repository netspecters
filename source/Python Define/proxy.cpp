#include "proxy.h"

std::vector<GROUP_HANDLE> CTaskGroupManagerProxy::getGroupHandleList()
{
	push_ptr();
	size_t buffer_size = ( size_t )call_method( "getGroupHandleList" );

	if( buffer_size > 0 )
	{
		GROUP_HANDLE *group_handle_buf_ptr = ( GROUP_HANDLE * )alloca( buffer_size );
		push_ptr( group_handle_buf_ptr, buffer_size );
		call_method( "getGroupHandleList" );

		return std::vector<GROUP_HANDLE>( group_handle_buf_ptr, group_handle_buf_ptr + buffer_size / sizeof( GROUP_HANDLE ) );
	}
	else
		return std::vector<GROUP_HANDLE>();
}

ST_GroupInformation_py CTaskGroupManagerProxy::getGroupStatistic( GROUP_HANDLE group_handle )
{
	push( group_handle );
	push_ptr();
	size_t buffer_size = ( size_t )call_method( "getGroupStatistics" );

	ITaskGroupManager::ST_GroupInformation *_group_infor_buf_ptr = ( ITaskGroupManager::ST_GroupInformation * )alloca( buffer_size );
	memset( _group_infor_buf_ptr, 0, buffer_size );

	push( group_handle );
	push_ptr( _group_infor_buf_ptr, buffer_size );
	call_method( "getGroupStatistics" );

	ST_GroupInformation_py result {
		_group_infor_buf_ptr->group_id,
		_group_infor_buf_ptr->user_tag,
		_group_infor_buf_ptr->group_file_size,
		_group_infor_buf_ptr->task_count,
		_group_infor_buf_ptr->send_data_speed,
		_group_infor_buf_ptr->recv_data_speed,
		_group_infor_buf_ptr->send_data_size,
		_group_infor_buf_ptr->recved_data_size,
		_group_infor_buf_ptr->group_file_name_str ? : ""
	};
	return result;
}

std::vector<ST_GroupInformation_py> CTaskGroupManagerProxy::getGroupStatistics()
{
	typedef ITaskGroupManager::ST_GroupInformation ST_GroupInformation;
	GROUP_HANDLE group_handle = NS_INVALID_HANDLE;
	push( group_handle );
	push_ptr();
	size_t buffer_size = ( size_t )call_method( "getGroupStatistics" );

	std::vector<ST_GroupInformation_py> result;
	if( buffer_size > 0 )
	{
		ITaskGroupManager::ST_GroupInformation *_group_infor_buf_ptr = ( ITaskGroupManager::ST_GroupInformation * )alloca( buffer_size );
		push( group_handle );
		push_ptr( _group_infor_buf_ptr, buffer_size );
		call_method( "getGroupStatistics" );

		while( buffer_size > 0 )
		{
			ST_GroupInformation_py infor_py {
				_group_infor_buf_ptr->group_id,
				_group_infor_buf_ptr->user_tag,
				_group_infor_buf_ptr->group_file_size,
				_group_infor_buf_ptr->task_count,
				_group_infor_buf_ptr->send_data_speed,
				_group_infor_buf_ptr->recv_data_speed,
				_group_infor_buf_ptr->send_data_size,
				_group_infor_buf_ptr->recved_data_size,
				_group_infor_buf_ptr->group_file_name_str ? : ""
			};
			result.push_back( infor_py );
			unsigned infor_size = sizeof( ST_GroupInformation ) + infor_py.group_file_name_str.size() + 1;
			_group_infor_buf_ptr = ( ST_GroupInformation * )(( uintptr_t )_group_infor_buf_ptr + infor_size );
			buffer_size -= infor_size;
		}
	}
	return result;
}

GROUP_TASK_HANDLE CTaskGroupManagerProxy::addProtocolTaskToGroup( GROUP_HANDLE group_handle, const ST_TaskInitParam_py task_init_param )
{
#define SIZE(x) task_init_param.task_resource.x.size()+1
	size_t buffer_size = sizeof( IProtocol::ST_TaskResource ) + SIZE( res_path ) + SIZE( referer_uri )
						 + SIZE( login_usr_name ) + SIZE( login_passwd )
						 + SIZE( proxy_address ) + SIZE( cookie_data ) + SIZE( extend_data );
	IProtocol::ST_TaskResource *_task_resource_buf_ptr = ( IProtocol::ST_TaskResource * )alloca( buffer_size );
	memset( _task_resource_buf_ptr, 0, buffer_size );

	_task_resource_buf_ptr->max_thread_num = task_init_param.task_resource.max_thread_num;
	_task_resource_buf_ptr->retry_limit = task_init_param.task_resource.retry_limit;
	_task_resource_buf_ptr->proxy_type = task_init_param.task_resource.proxy_type;
	_task_resource_buf_ptr->proxy_port = task_init_param.task_resource.proxy_port;
	_task_resource_buf_ptr->host_port = task_init_param.task_resource.host_port;

	unsigned offset = 0;
#define _SET_OFFSET(x,y) \
	_task_resource_buf_ptr->x##y = offset;\
	memcpy(&(_task_resource_buf_ptr->resource_data[offset]),task_init_param.task_resource.x.data(), task_init_param.task_resource.x.size());\
	offset += task_init_param.task_resource.x.size();
#define SET_OFFSET(x) _SET_OFFSET(x,_offset)

	SET_OFFSET( referer_uri );
	SET_OFFSET( login_usr_name );
	SET_OFFSET( login_passwd );
	SET_OFFSET( proxy_address );
	SET_OFFSET( cookie_data );

	IProtocol::ST_TaskInitParam _task_init_param {
		task_init_param.selector_id,
		task_init_param.enable_hooker,
		_task_resource_buf_ptr,
		NULL
	};

	return ( GROUP_TASK_HANDLE )call_method( "addProtocolTaskToGroup", group_handle, &_task_init_param );
#undef SIZE
#undef SET_OFFSET
}

std::vector<ENM_TaskStopCode> CTaskGroupManagerProxy::getTaskStopCodes( GROUP_HANDLE group_handle )
{
	push_data( &group_handle, sizeof( GROUP_HANDLE ) );
	push_ptr();
	size_t buffer_size = ( size_t )call_method( "getTaskStopCodes" );

	if( buffer_size > 0 )
	{
		ENM_TaskStopCode *task_stop_code_buf_ptr = ( ENM_TaskStopCode * )alloca( buffer_size );
		push_data( &group_handle, sizeof( GROUP_HANDLE ) );
		push_ptr( task_stop_code_buf_ptr, buffer_size );
		call_method( "getTaskStopCodes" );

		return std::vector<ENM_TaskStopCode>( task_stop_code_buf_ptr, task_stop_code_buf_ptr + buffer_size / sizeof( ENM_TaskStopCode ) );
	}
	else
		return std::vector<ENM_TaskStopCode>();
}
