#include "Core.h"

#include "../KernelLibrary/NetLayer/NetLayer.h"
#include "../KernelLibrary/WebServer/WebServer.h"
#include "../KernelLibrary/DataContainer/data_container_manager.h"
#include "../KernelLibrary/ProtocolManager/ProtocolManager.h"
#include "../KernelLibrary/InternalProtocol/http/http.h"
#ifdef linux
#	include "../KernelLibrary/InternalProtocol/cap/cap.h"
#endif
#include "../KernelLibrary/UIHost/UIHost.h"

using namespace Netspecters::Core;
using namespace Netspecters::KernelPlus;

using namespace NetLayer;
using namespace WebServer;
using namespace DataContainer;
using namespace ProtocolManager;
using namespace InternalProtocol::HTTP;
#ifdef linux
using namespace InternalProtocol::CAP;
#endif
using namespace UIHost;

#define KERNEL_PLUS_INDEX_TABLE_PTR getBlockPtr<ST_IndexTable>(m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttKernelPlusIndexTable])

void CPlusTree::initDBFile( void )
{
	char *current_write_addr_ptr = static_cast<char *>( m_file_mem_ptr );

	//写入文件头
	struct ST_StaticSuperBlock
	{
		ST_FileHeader file_header;
		ST_SuperBlock super_block;
	};
	ST_StaticSuperBlock static_super_block = {
		{	FileTag:
			{'N', 'S', 'P', 'T'}, DataStructVer:
			DATA_STRUCT_VER
}, { SysTableEntries:
			{1, 2, 3, 4}
		}
	};
	memcpy( current_write_addr_ptr, &static_super_block, sizeof( static_super_block ) );
	current_write_addr_ptr += C_BLOCKSIZE;

	//分别写入：空KernelPlusIndexTable，空PlusNameTable，空SysPlusRootDir，空UserPlusRootDir
	memset( current_write_addr_ptr, 0, C_BLOCKSIZE * 4 );

	/* 初始化空闲块分配表 */
	freeBlock( 5, BASIC_CONFIGURE_FILE_SIZE / C_BLOCKSIZE );

	//写入系统插件
	dirscribeKernelPlus( 0, "/sys/", "netlayer" );
	dirscribeKernelPlus( 1, "/sys/", "webserver" );
	dirscribeKernelPlus( 2, "/sys/", "data_container_manager" );
	dirscribeKernelPlus( 3, "/sys/", "protocol_manager" );
	dirscribeKernelPlus( 4, "/sys/protocol_manager/", "http" );
#ifdef linux
	dirscribeKernelPlus( 5, "/sys/protocol_manager/", "cap" );
#endif
	dirscribeKernelPlus( 6, "/sys/", "uihost", true );

#if defined _WIN32 || defined _WIN64

#else
	msync( m_file_mem_ptr, BASIC_CONFIGURE_FILE_SIZE, MS_ASYNC );
#endif
}

bool CPlusTree::checkDBFile( void )
{
	ST_FileHeader *file_header_ptr = static_cast<ST_FileHeader *>( m_file_mem_ptr );
	bool file_availd = ( strncmp( file_header_ptr->FileTag, "NSPT", 4 ) == 0 ) && ( file_header_ptr->DataStructVer <= DATA_STRUCT_VER );
	if( !file_availd )
		printk( KERROR "Configure file is not availd! You can delete it and netspecters will recreate a new one.\n" );
	return file_availd;
}

bool CPlusTree::loadPlusTree( const string &work_path, bool scan_before_load )
{
	if( m_file_mem_ptr ) return false;	//只允许初始化一次

	/* 启动管道监听 */
	m_ipc_server.start();

	m_work_path.assign( work_path );
	string database_name( work_path + "CoreDB.db" );	//utf8与ASCII兼容，可以直接连一起

	//映射数据库
	int file_state = loadFile( database_name.c_str(), BASIC_CONFIGURE_FILE_SIZE );
	m_super_block_ptr = reinterpret_cast<ST_SuperBlock *>( static_cast<char *>( m_file_mem_ptr ) + sizeof( ST_FileHeader ) );
	switch( file_state )
	{
		case 0:
			printk( KERROR "Open configure file failed! Get last os error: %d\n", OSFunc::get_errno() );
			return false;

		case 1:
			printk( KWARNING "Can not find configure file, netspecters will create a new one.\n" );
			initDBFile();
			break;

		case 2:
			//校验配置文件的有效性
			if( !checkDBFile() )
			{
				printk( KERROR "Configure file is not availd! You can delete it and netspecters will recreate a new one.\n" );
				return false;
			}
			printk( KINFORMATION "Configure file has been loaded.\n" );
			break;
	}

	//构建内核插件实例
	m_main_plus_ptr = NULL;//主插件只能有一个
	createStaticKernelPluses<CNetLayer> ( 0, m_main_plus_ptr );				//netlayer是静态实例，实际是不需要构建的，这里只是为了初始化一下tree的数据结构
	createKernelPluses<CWebServer> ( 1, m_main_plus_ptr );
	createStaticKernelPluses<CDataContainerManager> ( 2, m_main_plus_ptr );	//data container manager是静态实例，实际是不需要构建的，这里只是为了初始化一下tree的数据结构
	createKernelPluses<CProtocolManager> ( 3, m_main_plus_ptr );
	createKernelPluses<CHttpProtocol> ( 4, m_main_plus_ptr );
#ifdef linux
	createKernelPluses<CCap> ( 5, m_main_plus_ptr );
#endif
	createKernelPluses<CUIHost> ( 6, m_main_plus_ptr );
	printk( KINFORMATION"All SYSTEM plugines have been created.\n" );

	//装载用户插件对应的动态库
	if( scan_before_load )
		scanPlusDir();
	loadPluses( string( work_path + PLUS ) );

	//初始化插件树(调用每个插件的初始化函数)
#define initPluseTree(PlusDirBlockID) dir_init( getBlockPtr<ST_Dir>(PlusDirBlockID), NULL, PlusDirBlockID )
	if( !initPluseTree( m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttSysPlusRootDir] ) )
	{
		printk( KERROR"Init system plugin FAILD! Remove the configure file may correct this error.\n" );
		return false;
	}
	initPluseTree( m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttUserPlusRootDir] );
#undef initPluseTree
	printk( KINFORMATION"All USER plugines have been initialized.\n" );
	return true;
}

void CPlusTree::unloadPlusTree( void )
{
	//卸载插件
#define uninitPluseTree(PlusDirBlockID) dir_uninit( getBlockPtr<ST_Dir>(PlusDirBlockID) )
	uninitPluseTree( m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttUserPlusRootDir] );
	uninitPluseTree( m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttSysPlusRootDir] );
#undef uninitPluseTree

	//关闭外部插件库
	unloadPluses();

	//删除系统插件
	//destroyKernelPluses<CNetLayer>(0);				//netlayer是静态实例，不需要手动释放
	destroyKernelPluses<CWebServer> ( 1 );
	//destroyKernelPluses<CDataContainerManager>(2);	//data container manager是静态实例，不需要手动释放
	destroyKernelPluses<CProtocolManager> ( 3 );
	destroyKernelPluses<CHttpProtocol> ( 4 );
#ifdef linux
	destroyKernelPluses<CCap> ( 5 );
#endif

	/* unmap and close file */
	unloadFile();

	printk( KINFORMATION "All pluses have been deinitialized.\n" );
}

int CPlusTree::resizeFile( int inc_size )
{
	off_t old_file_size = getFileSize();
	off_t new_file_size = increaseFileSize( inc_size );
	if( new_file_size > 0 )
	{
		m_super_block_ptr = reinterpret_cast<ST_SuperBlock *>( static_cast<char *>( m_file_mem_ptr ) + sizeof( ST_FileHeader ) );

		//添加有效数据块索引
		freeBlock( old_file_size / C_BLOCKSIZE, new_file_size / C_BLOCKSIZE );
		return new_file_size;
	}
}

int CPlusTree::getPlusTreeWorkPath( char *work_path_buffer, size_t buffer_size )
{
	if( buffer_size < m_work_path.size() )
		return m_work_path.size();
	else
	{
		--buffer_size;	//为'\0'留出空间

		int copy_len = buffer_size <= m_work_path.size() ? buffer_size : m_work_path.size();
		memcpy( work_path_buffer, m_work_path.data(), copy_len );
		work_path_buffer[copy_len] = '\0';

		return copy_len;
	}
}

int CPlusTree::getDataDirPath( char *data_dir_path_buffer, size_t buffer_size )
{
	std::string dir_path( m_work_path + DATA_DIR_PATH );

	if( buffer_size < dir_path.size() )
		return dir_path.size();
	{
		--buffer_size;	//为'\0'留出空间

		int copy_len = buffer_size <= dir_path.size() ? buffer_size : dir_path.size();
		memcpy( data_dir_path_buffer, dir_path.data(), copy_len );
		data_dir_path_buffer[copy_len] = '\0';

		return copy_len;
	}
}

INSMain *CPlusTree::getMainPlus( void )
{
	return m_main_plus_ptr;
}

INSPlus *CPlusTree::getPlusInterface( HDIR plus_dir_handle )
{
	ST_Dir *dir_ptr = getBlockPtr<ST_Dir> ( HANDLE_TO_ID( plus_dir_handle ) );
	if( dir_ptr == NULL ) return ( INSPlus * ) NSEC_PARAMERROR;
	return dir_ptr->DirData.iInterface;
}

HRECORD CPlusTree::openRecord( HDIR dir_handle, int record_index, int open_always )
{
	ST_Dir *dir_ptr = getBlockPtr<ST_Dir> ( HANDLE_TO_ID( dir_handle ) );
	if( dir_ptr == NULL ) return NSEC_PARAMERROR;

	//查找record
	TBlockID record_id = dir_getRecord( dir_ptr, record_index );

	//建立record
	if(( record_id == 0 ) && open_always )
		record_id = dir_addRecord( dir_ptr, -1 );

	//返回record handle
	return record_id > 0 ? OPEN_HANDLE( ST_Handle::htRecord, record_id ) : NS_INVALID_HANDLE;
}

void CPlusTree::queryErrorMsg( long error_id, char *&msg_buffer_ptr, int msg_buffer_size )
{
// TODO (wangguan#1#): 转换字符串格式(utf8->unicode->utf8)，调用querySysErrorMsg
	//OSFunc::querySysErrorMsg( error_id, msg_buffer_ptr, msg_buffer_size );
}

bool CPlusTree::initInterfaceIPC( HDIR interface_dir_handle, void *object_instance, unsigned max_method_number )
{
	ST_Dir *dir_ptr = getBlockPtr<ST_Dir> ( HANDLE_TO_ID( interface_dir_handle ) );

	/* 不允许重复创建方法表 */
	if( dir_ptr->ipc_method_table ) return NSEC_INVALID_CALL;

	/* 创建并初始化方法表 */
	dir_ptr->ipc_object_instance = object_instance;
	dir_ptr->max_ipc_method_number = max_method_number;
	dir_ptr->ipc_method_table = new ST_Dir::ST_MethodTableEntry[max_method_number];
	for( unsigned i = 0; i < max_method_number; i++ )
	{
		ST_Dir::ST_MethodTableEntry &mt_entry = dir_ptr->ipc_method_table[i];
		mt_entry.method_address = NULL;
	}
}

int CPlusTree::registerIPCMethod( HDIR interface_dir_handle, const char *method_name, void *method_address )
{
	ST_Dir *dir_ptr = getBlockPtr<ST_Dir> ( HANDLE_TO_ID( interface_dir_handle ) );

	/* 检查方法表是否有效 */
	if( !dir_ptr->ipc_method_table ) return NSEC_NOT_INIT;

	/* 将方法放入表的最后，保证不越界添加 */
	for( unsigned index = 0; index < dir_ptr->max_ipc_method_number; ++index )
	{
		ST_Dir::ST_MethodTableEntry &mt_entry = dir_ptr->ipc_method_table[index];
		if( NULL == mt_entry.method_address )
		{
			mt_entry.method_name.assign( method_name );
			mt_entry.method_address = method_address;
			return NSEC_SUCCESS;
		}
	}

	return NSEC_FULL;	//方法表已满
}

std::pair<void *, bool> CPlusTree::ipc_call_method( const char *interface_path, const char *method_name, unsigned argument_size, void *argument_list )
{
	printk( KINFORMATION ">> IPC CALL: %s->%s.\n", interface_path, method_name );

#if defined DEBUG
	if( 0 == strcmp( interface_path, "/" ) && strcmp( method_name, "test_method" ) == 0 )
	{
		void *call_result = invoke_method( this, ( void * )&CPlusTree::ipc_test_method, argument_size, argument_list );
		return std::make_pair( call_result, true );
	}
	else
	{
#endif
		ST_Dir *dir_ptr = getBlockPtr<ST_Dir>( get_dir_block( interface_path ) );

		if( dir_ptr->ipc_object_instance )
		{
			/* 查找方法 */
			for( int i = 0; i < dir_ptr->max_ipc_method_number; ++i )
			{
				ST_Dir::ST_MethodTableEntry &mt_entry = dir_ptr->ipc_method_table[i];
				if( mt_entry.method_address && mt_entry.method_name == method_name )
				{
					void *call_result = invoke_method( dir_ptr->ipc_object_instance, mt_entry.method_address, argument_size, argument_list );
					return std::make_pair( call_result, true );
				}
			}
		}
		return std::make_pair(( void * )NULL, false );
#if defined DEBUG
	}
#endif
}

void *CPlusTree::invoke_method( void *obj_instance, void *method_address, unsigned arg_list_size, void *arg_list ) throw()
{
	volatile uintptr_t esp_store;	//使用此变量保存esp位置，可以是本代码同时适用于采用 stdcall 或 cdecl 调用方式的函数call
#ifdef i386
	asm volatile(
		"	movl	%%esp,	%4		\n"
		"	cmpl	$0x0,	%0		\n"
		"	je		no_arg			\n"
		"	sub		%%ecx,	%%esp	\n"
		"	push	%%esi			\n"
		"	push	%%edi			\n"
		"	movl	%%esp,	%%edi	\n"
		"	addl	$0x8,	%%edi	\n"
		"	movl	%1,		%%esi	\n"
		"	cld						\n"
		"	rep		movsb			\n"
		"	pop		%%edi			\n"
		"	pop		%%esi			\n"
		"no_arg:					\n"
		"	pushl	%2				\n"
		"	call	*%3				\n"
		"	movl	%4,		%%esp	\n"
		::"c"( arg_list_size ), "m"( arg_list ), "m"( obj_instance ), "m"( method_address ), "m"( esp_store )
	);
#else
#	error "not implement"
#endif
}

#if defined DEBUG
uintptr_t CPlusTree::ipc_test_method( int first_param, bool second_param, X &third_param, char *forth_param, const char *name )
{
	third_param.a++;
	third_param.b++;
	third_param.c++;
	return 1;
}
#endif

void CPlusTree::closeRecord( HRECORD record_handle )
{
	CLOSE_HANDLE( record_handle );
}

TBlockID CPlusTree::allocBlock( void )
{
	std::lock_guard<std::recursive_mutex> locker( m_unused_block_stack_mutex );

	//扩大物理文件尺寸
	if( 0 == m_super_block_ptr->UnusedBlocks_Stack_BlockID )
		switch( resizeFile( MOC_INC_SIZE ) )		//大范围扩展失败后，尝试只扩展一个块大小
		{
			case -1:
				return static_cast<TBlockID>( -1 );
			case 0:
				if( resizeFile( C_BLOCKSIZE ) <= 0 )
					return 0;
		}

	TBlockStack *block_statck_struct_ptr = getBlockPtr<TBlockStack> ( m_super_block_ptr->UnusedBlocks_Stack_BlockID );
	TBlockID new_block_id = ( *block_statck_struct_ptr )[m_super_block_ptr->UnusedBlockStack_Pos++];
	if( m_super_block_ptr->UnusedBlockStack_Pos == C_BLOCKSIZE / sizeof( TBlockID ) )
	{
		TBlockID tmpID = m_super_block_ptr->UnusedBlocks_Stack_BlockID;
		m_super_block_ptr->UnusedBlocks_Stack_BlockID = new_block_id;
		new_block_id = tmpID;
		m_super_block_ptr->UnusedBlockStack_Pos = 0;
	}

	//初始化分配的数据块
	memset( getBlockPtr<uint8_t> ( new_block_id ), 0, C_BLOCKSIZE );

	return new_block_id;
}

///释放区间 [begin,end)
void CPlusTree::freeBlock( TBlockID idBlock_Begin, TBlockID idBlock_End )
{
	std::lock_guard<std::recursive_mutex> locker( m_unused_block_stack_mutex );

	for( TBlockID block_id = idBlock_Begin; block_id < idBlock_End; block_id++ )
		if( m_super_block_ptr->UnusedBlockStack_Pos > 0 )
		{
			TBlockStack *block_statck_ptr = getBlockPtr<TBlockStack> ( m_super_block_ptr->UnusedBlocks_Stack_BlockID );
			( *block_statck_ptr )[--m_super_block_ptr->UnusedBlockStack_Pos] = block_id;
		}
		else
		{
			TBlockID old_block_id = m_super_block_ptr->UnusedBlocks_Stack_BlockID;

			m_super_block_ptr->UnusedBlocks_Stack_BlockID = block_id;
			m_super_block_ptr->UnusedBlockStack_Pos = C_BLOCKSIZE / sizeof( TBlockID );
			freeBlock( old_block_id );
		}
}

void CPlusTree::freeBlock( TBlockID block_id )
{
	freeBlock( block_id, block_id + 1 );
}

template<typename TKernelPlusClass> void CPlusTree::createKernelPluses( int nKernelPlusID, INSMain *&iMainPlus )
{
	uint16_t param_value = 0;

	TBlockID dir_id = index_table_getItem( KERNEL_PLUS_INDEX_TABLE_PTR, nKernelPlusID, param_value );
	getBlockPtr<ST_Dir> ( dir_id )->DirLoadMethod = ST_Dir::dlmNow;
	ST_Dir::ST_DirData &dir_data = getBlockPtr<ST_Dir> ( dir_id )->DirData;
	dir_data.iInterface = static_cast<INSPlus *>( new TKernelPlusClass() );
	dir_data.LibraryHandle = NULL;

	iMainPlus = ( param_value == 0 ? iMainPlus : reinterpret_cast<INSMain *>( dir_data.iInterface ) );
}

template<typename TKernelPlusClass> void CPlusTree::createStaticKernelPluses( int nKernelPlusID, INSMain *&iMainPlus )
{
	uint16_t param_value = 0;

	TBlockID dir_id = index_table_getItem( KERNEL_PLUS_INDEX_TABLE_PTR, nKernelPlusID, param_value );
	getBlockPtr<ST_Dir> ( dir_id )->DirLoadMethod = ST_Dir::dlmNow;
	ST_Dir::ST_DirData &dir_data = getBlockPtr<ST_Dir> ( dir_id )->DirData;
	dir_data.iInterface = static_cast<INSPlus *>( &TKernelPlusClass::getStaticInstance() );
	dir_data.LibraryHandle = NULL;

	iMainPlus = ( param_value == 0 ? iMainPlus : reinterpret_cast<INSMain *>( dir_data.iInterface ) );
}

template<typename TKernelPlusClass> void CPlusTree::destroyKernelPluses( int nKernelPlusID )
{
	uint16_t param_value = 0;

	TBlockID dir_id = index_table_getItem( KERNEL_PLUS_INDEX_TABLE_PTR, nKernelPlusID, param_value );
	delete static_cast<TKernelPlusClass *>( getBlockPtr<ST_Dir>( dir_id )->DirData.iInterface );
}

int CPlusTree::installPlus( const char *plus_library_string_ptr, const char *path_string_ptr, const char *plus_name_string_ptr, int reserved )
{
	int result = NSEC_NSERROR;

	//查找父目录对应的block
	TBlockID parent_dir_id = get_dir_block( path_string_ptr );
	if( parent_dir_id )
	{
		ST_Dir *parent_dir_ptr = getBlockPtr<ST_Dir> ( parent_dir_id );

		//重复检查
		uint16_t param_value;
		if( dir_findSubDir( parent_dir_ptr, plus_name_string_ptr, param_value ) == 0 )
		{
			//将plus挂到树上
			TBlockID plus_dir_id = dir_addSubDir( parent_dir_ptr, plus_name_string_ptr );

			//填入插件表中
			if( plus_dir_id )
			{
				ST_NameTable *parent_name_table = getBlockPtr<ST_NameTable> ( m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttPlusNameTable] );
				if( name_table_addItem( parent_name_table, plus_library_string_ptr, plus_dir_id, 0 ) )
				{
					printk( KINFORMATION"Plus named \"%s\" installed under %s\n", plus_name_string_ptr, path_string_ptr );
					result = NSEC_SUCCESS;
				}
			}
		}
		else
		{
			printk( KWARNING"Plus named \"%s\" has exist under %s!\n", plus_name_string_ptr, path_string_ptr );
			result = NSEC_CONFLICT;
		}
	}
	return result;
}

bool CPlusTree::enum_plus( ST_NameTable *base_name_table_ptr , const char *base_path_name, TPlusTreeEnumFunc call_back_func, intptr_t user_data )
{
	/* 设定守卫结构，防止意外退出导致的内存泄漏 */
	struct __scoped_guard
	{
		__scoped_guard( void ) {
			plugin_infor_ptr = new ST_PluginInformation;
		}
		~__scoped_guard( void ) {
			delete plugin_infor_ptr;
		}
		ST_PluginInformation *plugin_infor_ptr;
	} scoped_guard;

	/* 在堆內分配需要的数据结构，防止栈溢出攻击 */
	ST_PluginInformation *plugin_infor_ptr = scoped_guard.plugin_infor_ptr;

	if( base_name_table_ptr )
	{
		/* 初始化枚举结构 */
		ST_NameTable::ST_FindContent find_content_struct;
		name_table_initFindContent( base_name_table_ptr, find_content_struct );

		TBlockID dir_id;
		uint16_t param;
		string path_name( base_path_name );
		while( enumNameTableItem( find_content_struct, dir_id, param ) )
		{
			/* 添加本次遍历经过的路径 */
			path_name += '/';
			path_name.append( find_content_struct.ItemNameStringPtr );

			/* 回调 */
			plugin_infor_ptr->full_path_on_the_tree = path_name.c_str();
			plugin_infor_ptr->name = find_content_struct.ItemNameStringPtr;
			if( !call_back_func( plugin_infor_ptr, user_data ) ) return false;

			/* 若存在子目录，继续遍历 */
			ST_Dir *dir_ptr = getBlockPtr<ST_Dir>( dir_id );
			if( dir_ptr->SubDirNameTable )
				if( !enum_plus( getBlockPtr<ST_NameTable>( dir_ptr->SubDirNameTable ), path_name.c_str(), call_back_func, user_data ) ) return false;
		}
	}
	return true;
}

void CPlusTree::enumPlus( TPlusTreeEnumFunc call_back_func, intptr_t user_data )
{
	/* 声明枚举过程中要使用的常量 */
	const ST_SuperBlock::ENM_SysTabelType enum_types[2] = {ST_SuperBlock::sttSysPlusRootDir, ST_SuperBlock::sttUserPlusRootDir};
	const char *base_names[2] = { "/sys", "/usr" };

	for( int i = 0; i < 2; i++ )
		if( !enum_plus( getBlockPtr<ST_NameTable> ( m_super_block_ptr->SysTableEntries[enum_types[i]] ), base_names[i], call_back_func, user_data ) ) return;
}

int CPlusTree::scanPlusDir( void )
{
	int new_plugin_count = 0;
	std::string dir_path( m_work_path + PLUS );
	ST_PluginInformation *plugin_information_ptr = new ST_PluginInformation;

	printk( KINFORMATION"Scan plugin dir...\n" );

#if defined _WIN32 || defined _WIN64
	/* utf-8 to unicode */
	DECL_UTF8_UNICODE( dir_path.c_str(), dir_path_unicode )
	WIN32_FIND_DATA find_data;

	HANDLE find_handle = FindFirstFile( dir_path_unicode, &find_data );
	do
	{
		std::wstring filename( find_data.cFileName );
		auto find_pos = filename.rfind( L".dll" );
		if( std::wstring::npos != find_pos )
		{
			HMODULE plus_library_handle = LoadLibrary( filename.c_str() );
			if( plus_library_handle )
			{
				func_DescribePlugin pfuncDescribePlugin = ( func_DescribePlugin ) GetProcAddress( plus_library_handle, "DescribePlugin" );
				bool load_lib_success = pfuncDescribePlugin && ( pfuncDescribePlugin( plugin_information_ptr ) == NSEC_SUCCESS );
				if( load_lib_success )
				{
					std::wstring filename_base( filename.substr( 0, find_pos ) );
					DECL_UNICODE_UTF8( filename_base.c_str(), filename_utf8 )
					load_lib_success = installPlus( filename_utf8, plugin_information_ptr->full_path_on_the_tree, plugin_information_ptr->name, 0 ) == NSEC_SUCCESS;
				}

				if( !load_lib_success )
					FreeLibrary( plus_library_handle );
				else
					new_plugin_count++;
			}
		}
	} while( FindNextFile( find_handle, &find_data ) );
	FindClose( find_handle );
#elif defined linux
	DIR *dir_ptr = opendir( dir_path.c_str() );
	if( !dir_ptr )
		return NSEC_OSERROR;

	struct dirent *dir_ent_ptr;
	while( NULL != ( dir_ent_ptr = readdir( dir_ptr ) ) )
	{
		std::string filename( dir_ent_ptr->d_name );
		auto find_pos = filename.rfind( DL );
		if( std::string::npos != find_pos )
		{
			/* 必须校验动态库合法性 */
			void *plus_library_handle = dlopen(( dir_path + filename ).c_str(), RTLD_NOW );
			if( plus_library_handle )
			{
				func_DescribePlugin pfuncDescribePlugin = ( func_DescribePlugin ) dlsym( plus_library_handle, "DescribePlugin" );
				bool load_lib_success = pfuncDescribePlugin && ( pfuncDescribePlugin( plugin_information_ptr ) == NSEC_SUCCESS );
				if( load_lib_success )
				{
					std::string filename_base( filename.substr( 0, find_pos ) );
					load_lib_success = installPlus( filename_base.c_str(), plugin_information_ptr->full_path_on_the_tree, plugin_information_ptr->name, 0 ) == NSEC_SUCCESS;
				}

				if( !load_lib_success )
					dlclose( plus_library_handle );
				else
					new_plugin_count++;
			}
		}
	}

	closedir( dir_ptr );
#else
#	error "not implement!!"
#endif
	delete plugin_information_ptr;

	return new_plugin_count;
}

/**
 * @brief 描述内核插件(将内核插件挂到树上)
 * @param kernel_plus_id 内核插件的id，也就是内核插件在 KERNEL_PLUS_INDEX_TABLE 中的位置
 * @param path_string_ptr 在插件树中的路径
 * @param plus_name_string_ptr 内核插件的名称
 * @param bIsRegisterAsMain 是否注册为主插件
 * @note 由于主插件只能注册一次，所以后注册的主插件会替代以前注册的主插件
*/
void CPlusTree::dirscribeKernelPlus( int kernel_plus_id, const char *path_string_ptr, const char *plus_name_string_ptr, bool bIsRegisterAsMain )
{
	//将kernel plus挂到树上
	TBlockID idSubDir = dir_addSubDir( getBlockPtr<ST_Dir> ( get_dir_block( path_string_ptr ) ), plus_name_string_ptr );

	//添加到内核插件描述索引表中
	index_table_addItem( KERNEL_PLUS_INDEX_TABLE_PTR, kernel_plus_id, idSubDir, bIsRegisterAsMain ? 1 : 0 );
}

void CPlusTree::uninstallPlus( const char *path_string_ptr, const char *plus_name_string_ptr )
{
	TBlockID parent_id = get_dir_block( path_string_ptr );
	if( parent_id > 0 )
	{
		uint16_t param_value;
		TBlockID idPlus = dir_findSubDir( getBlockPtr<ST_Dir> ( parent_id ), plus_name_string_ptr, param_value );
		if( idPlus > 0 )
		{
			dir_uninit( getBlockPtr<ST_Dir> ( idPlus ) );
			dir_delSubDir( getBlockPtr<ST_Dir> ( parent_id ), plus_name_string_ptr );
		}
	}
}

int CPlusTree::readRecord( HRECORD record_handle, void *buffer_ptr, size_t buffer_size )
{
	TBlockID record_id = HANDLE_TO_ID( record_handle );
	if( record_id == 0 ) return NSEC_PARAMERROR;

	return data_block_read( getBlockPtr<ST_Data> ( record_id ), buffer_ptr, buffer_size );
}

int CPlusTree::writeRecord( HRECORD record_handle, const void *buffer_ptr, size_t buffer_size )
{
	TBlockID record_id = HANDLE_TO_ID( record_handle );
	if( record_id == 0 ) return NSEC_PARAMERROR;

	return data_block_write( getBlockPtr<ST_Data> ( record_id ), buffer_ptr, buffer_size );
}

void CPlusTree::delRecord( HDIR dir_handle, int record_index )
{
	if(( dir_handle == 0 ) || ( record_index == 0 ) ) return;
	dir_delRecord( getBlockPtr<ST_Dir> ( HANDLE_TO_ID( dir_handle ) ), record_index );
}

void CPlusTree::loadPluses( const string &plus_search_path )
{
	//扫描路径
	ST_NameTable::ST_FindContent find_content_struct;
	TBlockID base_id = m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttPlusNameTable];
	name_table_initFindContent( getBlockPtr<ST_NameTable> ( base_id ), find_content_struct );

	TBlockID value_dir_id = 0;
	uint16_t param_value = 0;
	while( CPlusTree::enumNameTableItem( find_content_struct, value_dir_id, param_value ) )
	{
		ST_Dir *dir_ptr = getBlockPtr<ST_Dir> ( value_dir_id );
		if( ST_Dir::dlmNone == dir_ptr->DirLoadMethod )
		{
			dir_ptr->DirData.iInterface = NULL;
			dir_ptr->DirData.LibraryHandle = NULL;
			continue;
		}

		OSFunc::native_handle_type plus_library_handle = 0;
		string library_name( plus_search_path + find_content_struct.ItemNameStringPtr + DL );
#if defined _WIN32 || defined _WIN64
		/* utf8 to unicode */
		DECL_UTF8_UNICODE( library_name.c_str(), library_name_unicode )
		plus_library_handle = LoadLibrary( library_name_unicode );
#else
		plus_library_handle = ( OSFunc::native_handle_type ) dlopen( library_name.c_str(), RTLD_NOW );
#endif
		if( plus_library_handle )
		{
			int intInterfaceIndex = 0;
			INSPlus *iInterface = NULL;

			//申请创建接口
			func_CreateInterface pfuncCreateInterface = NULL;
#if defined _WIN32 || defined _WIN64
			pfuncCreateInterface = ( func_CreateInterface ) GetProcAddress(( HMODULE ) plus_library_handle, "CreateInterface" );
#else
			pfuncCreateInterface = ( func_CreateInterface ) dlsym(( void * ) plus_library_handle, "CreateInterface" );
#endif
			if( pfuncCreateInterface )
			{
				pfuncCreateInterface( intInterfaceIndex++, iInterface );
			}
			else
			{
				printk( KERROR"Can't find 'CreateInterface' function in plugin library! Load plugin failed\n" );
			}

			//填写目录数据
			dir_ptr->DirData.iInterface = iInterface;
			dir_ptr->DirData.LibraryHandle = plus_library_handle;
		}
		else
		{
			printk( KERROR"Can't load plugin! Get last os error: %d\n", OSFunc::get_errno() );
		}
	}
}

void CPlusTree::unloadPluses( void )
{
	//扫描路径
	ST_NameTable::ST_FindContent find_content_struct;
	TBlockID base_id = m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttPlusNameTable];
	name_table_initFindContent( getBlockPtr<ST_NameTable> ( base_id ), find_content_struct );

	TBlockID value_dir_id = 0;
	uint16_t param_value = 0;
	while( CPlusTree::enumNameTableItem( find_content_struct, value_dir_id, param_value ) )
	{
		ST_Dir *dir_struct_ptr = getBlockPtr<ST_Dir> ( value_dir_id );

		if( dir_struct_ptr->DirData.iInterface )
		{
			//释放创建的接口
			OSFunc::native_handle_type plus_library_handle = dir_struct_ptr->DirData.LibraryHandle;
			func_DestroyInterface pfuncDestroyInterface = NULL;
#if defined _WIN32 || defined _WIN64
			pfuncDestroyInterface = ( func_DestroyInterface ) GetProcAddress(( HMODULE ) plus_library_handle, "DestroyInterface" );
			pfuncDestroyInterface( dir_struct_ptr->DirData.iInterface );
			FreeLibrary(( HMODULE ) plus_library_handle );
#else
			pfuncDestroyInterface = ( func_DestroyInterface ) dlsym(( void * ) plus_library_handle, "DestroyInterface" );
			pfuncDestroyInterface( dir_struct_ptr->DirData.iInterface );
			dlclose(( void * ) plus_library_handle );
#endif
			//还原目录数据
			dir_struct_ptr->DirData.iInterface = NULL;
			dir_struct_ptr->DirData.LibraryHandle = NULL;
		}
	}
}

HDIR CPlusTree::openDir( const char *abs_dir_path )
{
	/* 确保参数的合法性 */
	if(( *abs_dir_path != '/' ) || ( abs_dir_path[strlen( abs_dir_path ) - 1] != '/' ) )
		return NS_INVALID_HANDLE;

	TBlockID dir_id = get_dir_block( abs_dir_path );
	return dir_id != INVALID_BLOCK_ID ? OPEN_HANDLE( ST_Handle::htDir, dir_id ) : NS_INVALID_HANDLE;
}

void CPlusTree::closeDir( HDIR dir_handle )
{
	CLOSE_HANDLE( dir_handle );
}

TBlockID CPlusTree::get_dir_block( const char *dir_path_string_ptr )
{
	/* 必须以'/'起始 */
	if( '/' != *dir_path_string_ptr )	return INVALID_BLOCK_ID;

	TBlockID found_dir_block_id = 0;

	/* 因后面会修改请求匹配的字符串内容，故需拷贝字符串到临时缓存 */
	int dir_path_string_len = strlen( dir_path_string_ptr );
	char dir_string_buffer[dir_path_string_len--];									//复制时少复制一个'/'，留出空间放'\0'
	strncpy( dir_string_buffer, dir_path_string_ptr + 1, dir_path_string_len );	//跳过前导'/'复制字符串
	dir_string_buffer[dir_path_string_len] = '\0';

	//检测起始目录(三个字符长度)
	if( strncmp( dir_string_buffer, "sys", 3 ) == 0 )
		found_dir_block_id = m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttSysPlusRootDir];
	else if( strncmp( dir_string_buffer, "usr", 3 ) == 0 )
		found_dir_block_id = m_super_block_ptr->SysTableEntries[ST_SuperBlock::sttUserPlusRootDir];
	else
		return INVALID_BLOCK_ID;

	/* 为了安全，需要检验起始目录是不已建立 */
	if( 0 == found_dir_block_id ) return INVALID_BLOCK_ID;

	//逐层搜索
	char *sub_dir_char_end_ptr = &dir_string_buffer[3], *finish_char_pos_ptr = dir_string_buffer + dir_path_string_len - 1;
	while( sub_dir_char_end_ptr < finish_char_pos_ptr )
	{
		char *sub_dir_char_start_ptr = sub_dir_char_end_ptr + 1;
		if( *sub_dir_char_start_ptr == '\0' ) break;

		while( *++sub_dir_char_end_ptr != '/' ) ;
		*sub_dir_char_end_ptr = '\0';

		//查找子目录
		uint16_t param_value;
		found_dir_block_id = dir_findSubDir( getBlockPtr<ST_Dir > ( found_dir_block_id ), sub_dir_char_start_ptr, param_value );

		//找不到就退出
		if( !found_dir_block_id ) break;
	}

	return found_dir_block_id;
}
