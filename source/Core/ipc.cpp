#include <alloca.h>

#include "ipc.h"
#include "Core.h"

using namespace Netspecters::Core;
typedef CMessageSocket::CMessage CMessage;

CIPCClient::CIPCClient( CMessageSocket::socket_t client_socket_fd )
	: m_client_socket( client_socket_fd )
	, m_service_thread( std::bind( &CIPCClient::onExecute, this ) )
{}

void CIPCClient::onExecute()
{
	while( true )
	{
		CMessage msg;
		if( !m_client_socket.read_message( msg ) ) break;
		if( CMessage::cmdCallMethod == msg.get_cmd() )
		{
			/* 读取路经和函数名 */
			string obj_path, function_name;
			msg >> obj_path >> function_name;

			/* 读取参数表 */
			char *arg_list = NULL, *data_area = NULL;
			unsigned arg_list_size, data_area_size;
			msg >> arg_list_size;
			if( arg_list_size > 0 )
			{
				arg_list = ( char * )malloc( arg_list_size );
				msg.read_body_raw_data( arg_list, arg_list_size );	//参数表

				/* 读取地址变换表 */
				unsigned address_translate_table_len;
				msg >> address_translate_table_len;
				uintptr_t address_translate_table[address_translate_table_len];
				msg.read_body_raw_data( address_translate_table, address_translate_table_len * sizeof( uintptr_t ) );

				/* 读取数据区 */
				msg >> data_area_size;
				data_area = ( char * )malloc( data_area_size );
				msg.read_body_raw_data( data_area, data_area_size );

				/* 翻译地址 */
				void *base_address = data_area;
				for( unsigned i = 0; i < address_translate_table_len; i++ )
				{
					uintptr_t &offset_address = *reinterpret_cast<uintptr_t *>( &arg_list[address_translate_table[i]] );
					offset_address += ( uintptr_t )base_address;
				}
			}

			auto call_result = CPlusTree::getPlusTreeInstancePtr()->ipc_call_method( obj_path.c_str(), function_name.c_str(), arg_list_size, arg_list );

			/* 返回函数值 */
			CMessage return_msg( call_result.second ? CMessage::cmdMethodReturn : CMessage::cmdMethodNotFound );
			return_msg.write_body_raw_data( data_area, data_area_size );
			return_msg.write_body_raw_data( &call_result.first, sizeof( void * ) );
			m_client_socket.write_message( return_msg );

			free( arg_list );
			free( data_area );
		}
	}

	printk( KINFORMATION "One client disconnected from IPC server.\n" );
	delete this;
}

CIPCServer::CIPCServer()
	: m_service_thread_ptr( NULL )
{
	sockaddr_in addr;
	memset( &addr, 0, sizeof( addr ) );
	addr.sin_family = AF_INET;
	addr.sin_port = htons( 32785 );	//监听端口 32785
	addr.sin_addr.s_addr = INADDR_ANY;
	if( -1 == ::bind( m_listen_socket.get_socket_fd(), ( sockaddr * )&addr, sizeof( addr ) ) )
		throw std::system_error( Netspecters::NSStdLib::OSFunc::get_errno(), std::system_category() );

	::listen( m_listen_socket.get_socket_fd(), 5 );
}

void CIPCServer::start()
{
	m_service_thread_ptr = new std::thread(std::bind( &CIPCServer::onExecute, this ));
}

void CIPCServer::onExecute()
{
	printk( KINFORMATION "IPC Server running...\n" );
	while( true )
	{
		sockaddr client_addr;
		unsigned int addr_len = sizeof( client_addr );
		CMessageSocket::socket_t client_socket_fd = ::accept( m_listen_socket.get_socket_fd(), ( sockaddr * )&client_addr, &addr_len );
		printk( KINFORMATION "IPC server accepted one client.\n" );
		CIPCClient *client_ptr = new CIPCClient( client_socket_fd );
	}
}
