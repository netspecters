#include "Core.h"

using namespace Netspecters::Core;

static int findLastRecordIndex(ST_Dir *dir_ptr)
{
	for(unsigned i = 0; i < ST_Dir::C_DIR_INDEX_TABLE_NUM; i++)
		if(!dir_ptr->RecordIndexTable[i])
			return i;

	//已经没有空间了
	return -1;
}

TBlockID CPlusTree::dir_addSubDir(ST_Dir *dir_ptr, const char *sub_dir_name_str_ptr)
{
	//如果项目还没有分配名字表,则先分配一个名字表
	if((dir_ptr->SubDirNameTable == 0) && ((dir_ptr->SubDirNameTable = allocBlock()) == 0))
		return 0;

	TBlockID new_id = allocBlock();	//在名字表中加入一项
	name_table_addItem(getBlockPtr<ST_NameTable> (dir_ptr->SubDirNameTable), sub_dir_name_str_ptr, new_id, 0);	//获取名字表入口
	return new_id;
}

TBlockID CPlusTree::dir_addRecord(ST_Dir *dir_ptr, int record_index)
{
	/* 尝试查找最后一个记录号 */
	if((record_index  < 0) && ((record_index = findLastRecordIndex(dir_ptr)) == -1))
		return 0;

	return dir_ptr->RecordIndexTable[record_index] = allocBlock();
}

bool CPlusTree::dir_delSubDir(ST_Dir *dir_ptr, const char *sub_dir_name_str_ptr)
{
	//删除指定表中的项
	bool has_deleted = dir_ptr->SubDirNameTable == 0;
	if(!has_deleted)
	{
		TBlockID dir_need_delete = name_table_delItem(getBlockPtr<ST_NameTable> (dir_ptr->SubDirNameTable), sub_dir_name_str_ptr);
		has_deleted = dir_need_delete == 0;
		if(!has_deleted)	//释放dir数据结构
		{
			dir_clear(getBlockPtr<ST_Dir> (dir_need_delete));
			freeBlock(dir_need_delete);
		}
	}
	return has_deleted;
}

bool CPlusTree::dir_delRecord(ST_Dir *dir_ptr, int record_index)
{
	TBlockID data_table_block_id = dir_ptr->RecordIndexTable[record_index];
	bool deleted = data_table_block_id > 0;
	if(deleted)
	{
		data_block_clear(getBlockPtr<ST_Data> (data_table_block_id));
		freeBlock(data_table_block_id);
		dir_ptr->RecordIndexTable[record_index] = 0;
	}
	return deleted;
}

TBlockID CPlusTree::dir_findSubDir(ST_Dir *dir_ptr, const char *sub_dir_name_str_ptr, uint16_t &param_value)
{
	TBlockID found_item = 0;

	//获取名字表入口
	ST_NameTable *name_table_ptr = getBlockPtr<ST_NameTable> (dir_ptr->SubDirNameTable);

	//查询名字表中的项目找到需要的项目伪指针
	name_table_findItemData(name_table_ptr, sub_dir_name_str_ptr, found_item, param_value);
	return found_item;
}

TBlockID CPlusTree::dir_getRecord(ST_Dir *dir_ptr, int record_index)
{
	/* 尝试查找最后一个记录号 */
	if((record_index  < 0) && ((record_index = findLastRecordIndex(dir_ptr)) == -1))
		return 0;

	return dir_ptr->RecordIndexTable[record_index];
}

bool CPlusTree::dir_init(ST_Dir *dir_ptr, INSPlus *parent_plus_ptr, TBlockID plus_dir_id)//使用深度优先遍历
{
	bool init_plus_interface_successed = true;

	if(ST_Dir::dlmNow == dir_ptr->DirLoadMethod)
	{
		//父对象先初始化
		if(dir_ptr->DirData.iInterface)    //sys与usr根目录保持接口为空，不需要初始化
			init_plus_interface_successed = dir_ptr->DirData.iInterface->onLoad(this, parent_plus_ptr, plus_dir_id, 0) == NSEC_SUCCESS;

		/* 输出错误信息 */
		if(!init_plus_interface_successed)
			printk(KERROR"Init dir faild!\n");

		//初始化子对象
		if(init_plus_interface_successed && dir_ptr->SubDirNameTable)
		{
			ST_NameTable::ST_FindContent find_content_struct;
			name_table_initFindContent(getBlockPtr<ST_NameTable> (dir_ptr->SubDirNameTable), find_content_struct);

			TBlockID value_dir_id = 0;
			uint16_t param_value = 0;
			while(CPlusTree::enumNameTableItem(find_content_struct, value_dir_id, param_value))
				init_plus_interface_successed &= dir_init(getBlockPtr<ST_Dir> (value_dir_id), dir_ptr->DirData.iInterface, value_dir_id);
		}
	}

	return init_plus_interface_successed;
}

void CPlusTree::dir_uninit(ST_Dir *dir_ptr)
{
	//使用深度优先遍历
	if(dir_ptr->SubDirNameTable)
	{
		ST_NameTable::ST_FindContent find_content_struct;
		name_table_initFindContent(getBlockPtr<ST_NameTable> (dir_ptr->SubDirNameTable), find_content_struct);

		TBlockID value_dir_id = 0;
		uint16_t param_value = 0;
		while(CPlusTree::enumNameTableItem(find_content_struct, value_dir_id, param_value))
			dir_uninit(getBlockPtr<ST_Dir> (value_dir_id));
	}

	if(dir_ptr->DirData.iInterface)
		dir_ptr->DirData.iInterface->onUnload();
}

void CPlusTree::dir_clear(ST_Dir *dir_ptr)
{
	name_table_clear(getBlockPtr<ST_NameTable> (dir_ptr->SubDirNameTable));

	/* 释放记录表 */
	for(unsigned i = 0; i < ST_Dir::C_DIR_INDEX_TABLE_NUM; i++)
		if(dir_ptr->RecordIndexTable[i])
		{
			TBlockID data_table_block_id = dir_ptr->RecordIndexTable[i];
			data_block_clear(getBlockPtr<ST_Data> (data_table_block_id));
			freeBlock(data_table_block_id);
		}
}
