#include "NS_Exception.h"

#	include "../../ThirdParty/google_breakpad/src/client/minidump_file_writer.cc"
#	include "../../ThirdParty/google_breakpad/src/common/convert_UTF.c"
#	include "../../ThirdParty/google_breakpad/src/common/string_conversion.cc"
#if defined(_WIN32) || defined(_WIN64)
#	include "../../ThirdParty/google_breakpad/src/common/windows/guid_string.cc"
#	include "../../ThirdParty/google_breakpad/src/client/windows/crash_generation/crash_generation_client.cc"
#	include "../../ThirdParty/google_breakpad/src/client/windows/handler/exception_handler.cc"
#else
#	include "../../ThirdParty/google_breakpad/src/common/linux/guid_creator.cc"
#	include "../../ThirdParty/google_breakpad/src/common/linux/file_id.cc"
#	include "../../ThirdParty/google_breakpad/src/client/linux/minidump_writer/minidump_writer.cc"
#	include "../../ThirdParty/google_breakpad/src/client/linux/minidump_writer/linux_dumper.cc"
#	include "../../ThirdParty/google_breakpad/src/client/linux/handler/exception_handler.cc"
#	include "../../ThirdParty/google_breakpad/src/client/linux/crash_generation/crash_generation_client.cc"
#endif

using namespace Netspecters::Core;

CNSException::CNSException ( const std::TSTRING &dump_path )
	: m_exception_handler ( dump_path, CNSException::doFilterCallback, CNSException::doMinidumpCallback, NULL,
#if defined(_WIN32) || defined(_WIN64)
							ExceptionHandler::HANDLER_ALL )
#else
							true )
#endif
{
#if defined(_WIN32) || defined(_WIN64)
	m_exclusive_locker = CreateMutexW ( NULL, false, L"NETSPECTERS" );
	if ( GetLastError() == ERROR_ALREADY_EXISTS )
		m_exclusive_locker = 0;
#else
	m_exclusive_locker = open ( "/tmp/.netspecters_locker", O_WRONLY | O_CREAT );
	if ( m_exclusive_locker != -1 )
		flock ( m_exclusive_locker, LOCK_EX );
#endif
}

bool CNSException::isFirstInstance ( void )
{
#if defined(_WIN32) || defined(_WIN64)
	return m_exclusive_locker != 0;
#else
	return m_exclusive_locker != -1;
#endif
}

bool CNSException::doMinidumpCallback ( const TCHAR *dump_path, const TCHAR *minidump_id, void *context
#if defined(_WIN32) || defined(_WIN64)
										,EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion
#endif
										,bool succeeded )
{
#if defined(_WIN32) || defined(_WIN64)
#else

#endif
	return true;
}

bool CNSException::doFilterCallback ( void *context
#if defined(_WIN32) || defined(_WIN64)
									  , EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion
#endif
									)
{
	return true;
}
