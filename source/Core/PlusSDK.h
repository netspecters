#ifndef PLUSSDK_H_INCLUDED
#define PLUSSDK_H_INCLUDED

template<typename TSubClass>
class ANSMain
{
	public:
		int a_onLoop(void){
			return getUpperClassPtr()->onLoop();
		}
		void a_kill( int exit_code ){
			getUpperClassPtr()->kill(exit_code);
		}
	private:
		TSubClass *getUpperClassPtr(void)
		{
			return static_cast<TSubClass*>(this);
		}
};

#endif // PLUSSDK_H_INCLUDED
