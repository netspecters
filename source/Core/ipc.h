#ifndef NS_CORE_IPC_H_INCLUDED
#define NS_CORE_IPC_H_INCLUDED

#include <string>
#include <stdarg.h>
#include <thread>
#include <boost/interprocess/shared_memory_object.hpp>	//使用boost的share memory
#include <boost/interprocess/mapped_region.hpp>

#include "message_pipe.hpp"
#include "nslib.h"

namespace Netspecters { namespace Core {

using std::string;

class CIPCClient
{
	public:
		CIPCClient( CMessageSocket::socket_t client_socket_fd );

	private:
		void onExecute();
		CMessageSocket m_client_socket;
		std::thread m_service_thread;
};

class CIPCServer
{
	public:
		CIPCServer();
		void start();
	private:
		void onExecute();
		CMessageSocket m_listen_socket;
		std::thread *m_service_thread_ptr;
};

}/* Core **/ }/* Netspecters **/

#endif // NS_CORE_IPC_H_INCLUDED
