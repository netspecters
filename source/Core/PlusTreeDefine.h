#ifndef PLUSTREEDEFINE_H_INCLUDED
#define PLUSTREEDEFINE_H_INCLUDED

#include <ctime>
#include <cstdlib>
#include <string>
#include <memory>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include "PlusInterface.h"
#include "../NSDefine.h"
#include "nslib.h"

namespace Netspecters {
namespace Core {

/* 定义数据结构版本 */
#define DATA_STRUCT_VER 1

/* 定义平台相关字符串常量 */
#if defined(_WIN32) || defined(_WIN64)
#	define nsL	L
#else
#	define nsL
#endif
#if defined(_WIN32) || defined(_WIN64)
#	define PLUS "plugin\\"
#	define DATA_DIR_PATH "data\\"
#	define DL	".dll"
#else
#	define PLUS "plugin/"
#	define DATA_DIR_PATH "data/"
#	define DL	".so"
#endif

static const int C_BLOCKSIZE = 64;
static const int MOC_INC_SIZE = 64 * 16;
static const int BASIC_CONFIGURE_FILE_SIZE = 2 * 1024;
typedef TBlockID TBlockStack[C_BLOCKSIZE/sizeof(TBlockID)];

/* ST_FileHeader 与 ST_SuperBlock 共同占用一个 block (0号block) */

///@brief Netspecters的内核数据库文件头
struct ST_FileHeader
{
	char	FileTag[4];						/**< 文件有效标识 */
	uint8_t	DataStructVer;			/**< 数据结构版本 */
	uint8_t	DataCompressVer;	/**< 压缩方式(0:不压缩， 1:zip压缩方法) */
	uint8_t	ProtectVer;				/**< 数据保护方法 */
} __attribute__((packed));

///@brief Netspecters的内核数据库文件中的超级块
struct ST_SuperBlock
{
	enum ENM_SysTabelType
	{
		sttKernelPlusIndexTable,	/**< 内核插件索引表(为了快速定位内核插件而使用的索引表) */
		sttPlusNameTable,			/**< 用户插件命名表（动态库路径表），用于加载插件的时候加载对应的动态库 */
		sttSysPlusRootDir,				/**< 系统插件根目录(NameTable) */
		sttUserPlusRootDir,			/**< 用户插件根目录(NameTable) */
		sttTypeCount
	};

	TBlockID	SysTableEntries[sttTypeCount];		/** 系统表入口表的块索引 */
	TBlockID	UnusedBlocks_Stack_BlockID;		/**< 空闲数据块链栈的块索引 @note 向大方向增长 */
	int			UnusedBlockStack_Pos;					/**< 空闲数据块链栈指针 @note 向大方向增长 */
} __attribute__((packed));

struct ST_Dir
{
	enum ENM_DirItemType { diSubDir, diRecord };
	enum ENM_DirLoadMethod : unsigned int { dlmNow = 0, dlmDelay, dlmNone };
	struct ST_DirData
	{
		INSPlus	*iInterface;			//目录引用接口指针
		Netspecters::NSStdLib::OSFunc::native_handle_type LibraryHandle;
	};
	struct ST_MethodTableEntry
	{
		std::string method_name;		/**< 接口方法名称 */
		void *method_address;
	};
	static const unsigned C_DIR_INDEX_TABLE_NUM = (C_BLOCKSIZE
		- sizeof(TBlockID) * 2
		- sizeof(ST_DirData)
		- sizeof(ENM_DirLoadMethod)
		- sizeof(void*)
		- sizeof(unsigned)
		- sizeof(ST_MethodTableEntry*))  / sizeof(TBlockID);

	TBlockID	SubDirNameTable;			/**< 子目录命名表伪指针 */
	TBlockID	ReleateDir;					/**< 相关目录（存在于一个动态库中的目录）[此版用不到] */
	ST_DirData	DirData;					/**< 目录数据 */
	ENM_DirLoadMethod DirLoadMethod;		/**< 插件的加载方法（方式） */
	void 		*ipc_object_instance;		/**< 用于ipc调用的目的对象指针 */
	unsigned	max_ipc_method_number;		/**< 在ipc方法表中预设拥有的方法总数 */
	ST_MethodTableEntry *ipc_method_table;	/**< ipc方法表 @note 一次性动态分配 */
	TBlockID	RecordIndexTable[C_DIR_INDEX_TABLE_NUM];	/**< 未命名记录索引表 */
} __attribute__((packed));

struct ST_NameTable
{
	struct ST_FindContent
	{
		ST_NameTable *CurrentTableStructPtr;
		char *NextItemPosPtr;
		char *ItemNameStringPtr;
	};

	static const unsigned C_NAME_TABLE_SPACE_SIZE = (C_BLOCKSIZE - sizeof(TBlockID) - sizeof(uint8_t)) / sizeof(uint8_t);
	TBlockID	NextNameTable;			//名字表链中的下一项
	uint8_t	UsedSize;
	uint8_t	NameItemDatas[C_NAME_TABLE_SPACE_SIZE];	//保存(Name(sz),value(TBlockID),param(uint16_t))数据
} __attribute__((packed));

struct	ST_Data
{
	static const size_t C_DATASIZE = C_BLOCKSIZE - sizeof(uint32_t);

	uint32_t	data_size;
	union
	{
		char	raw_data[C_DATASIZE];	//如果数据长度超过DataSize,数据将被放入磁盘的文件中
		boost::uuids::uuid	file_tag;	//磁盘文件以Tag值命名
	} data;
} __attribute__((packed));

struct	ST_IndexTable
{
	struct ST_Index
	{
		TBlockID	Value;
		uint16_t	Param;
	};
	static const size_t C_INDEX_NUM_PER_BLOCK = (C_BLOCKSIZE - sizeof(TBlockID)) / sizeof(ST_Index);

	TBlockID	NextIndexTable;
	ST_Index	Indexs[C_INDEX_NUM_PER_BLOCK];
} __attribute__((packed));

#ifdef USE_SAFE_HANDLE
static const int HANDLE_BASE_INDEX = 123;
template <class IDType, class HandleType>
class CHandleTable
{
	public:
		typedef unsigned int Handle;

		Handle openHandle(IDType id, HandleType handle_type)
		{
			Netspecters::NSStdLib::CTinyThread::scoped_lock locker(m_mutex);
			m_handle_table.push_back((ST_Handle) {
				id, handle_type
			});
			return m_handle_table.size + HANDLE_BASE_INDEX;
		}
		void closeHandle(Handle handle)
		{
			//TODO
		}
		IDType HandleToID(Handle handle)
		{
			return m_handle_table[handle - HANDLE_BASE_INDEX].id;
		}
		HandleType getHandleType(Handle handle)
		{
			return m_handle_table[handle - HANDLE_BASE_INDEX].handle_type;
		}

	private:
		struct ST_Handle
		{
			IDType id;
			HandleType handle_type;
		};
		Netspecters::NSStdLib::CSimpleArray<ST_Handle> m_handle_table;
		Netspecters::NSStdLib::CTinyThread::mutex m_mutex;
};
#endif

}/* Core **/

}/* Netspecters **/

#endif // PLUSTREEDEFINE_H_INCLUDED
