#include "Core.h"

using namespace Netspecters::Core;

bool CPlusTree::enumNameTableItem(ST_NameTable::ST_FindContent &find_content_struct, TBlockID &value, uint16_t &param_value)
{
	if(find_content_struct.CurrentTableStructPtr == NULL)
		return false;

	find_content_struct.ItemNameStringPtr = find_content_struct.NextItemPosPtr;
	ST_NameTable *current_name_table_ptr = find_content_struct.CurrentTableStructPtr;

	int name_string_len = strlen(find_content_struct.ItemNameStringPtr) + 1;
	char *current_pos_ptr = find_content_struct.ItemNameStringPtr + name_string_len;

	//读取value
	value = *(reinterpret_cast<TBlockID*>(current_pos_ptr));
	current_pos_ptr += sizeof(TBlockID);

	//读取param
	param_value = *(reinterpret_cast<uint16_t *>(current_pos_ptr));
	current_pos_ptr += sizeof(uint16_t);

	//设定下次搜索位置
	find_content_struct.NextItemPosPtr = current_pos_ptr;
	if(current_pos_ptr >= (reinterpret_cast<char*>(current_name_table_ptr->NameItemDatas) + current_name_table_ptr->UsedSize))
	{
		if(current_name_table_ptr->NextNameTable == 0)
			find_content_struct.CurrentTableStructPtr = NULL;
		else
		{
			current_name_table_ptr = getBlockPtr<ST_NameTable>(current_name_table_ptr->NextNameTable);
			find_content_struct.CurrentTableStructPtr = current_name_table_ptr;
			find_content_struct.NextItemPosPtr = reinterpret_cast<char*>(current_name_table_ptr->NameItemDatas);
		}
	}

	return true;
}

void CPlusTree::name_table_initFindContent(ST_NameTable *name_table_ptr, ST_NameTable::ST_FindContent &find_content_struct)
{
	if(name_table_ptr->UsedSize > 0)
	{
		find_content_struct.NextItemPosPtr = find_content_struct.ItemNameStringPtr = reinterpret_cast<char*>(name_table_ptr->NameItemDatas);
		find_content_struct.CurrentTableStructPtr = name_table_ptr;
	}
	else
	{
		find_content_struct.CurrentTableStructPtr = NULL;
	}
}

bool CPlusTree::name_table_addItem(ST_NameTable *name_table_ptr, const char *item_name_str_ptr, TBlockID value, uint16_t param_value)
{
	bool has_enought_space = false;
	ST_NameTable *last_name_table_struct_ptr = name_table_ptr;

	//循环搜索名字表链，找到有足够空间的名字表 (也可以使用递归)
	while(name_table_ptr)
	{
		size_t item_name_string_len = strlen(item_name_str_ptr) + 1;
		size_t total_len = item_name_string_len + sizeof(TBlockID) + sizeof(uint16_t);
		has_enought_space = (ST_NameTable::C_NAME_TABLE_SPACE_SIZE - name_table_ptr->UsedSize) >= total_len;
		if(has_enought_space)
		{
			char *pszSpace = reinterpret_cast<char*>(name_table_ptr->NameItemDatas + name_table_ptr->UsedSize);
			memcpy(pszSpace, item_name_str_ptr, item_name_string_len);

			TBlockID *value_ptr = reinterpret_cast<TBlockID*>(pszSpace + item_name_string_len);
			*value_ptr++ = value;

			uint16_t *param_ptr = static_cast<uint16_t*>(value_ptr);
			*param_ptr = param_value;

			name_table_ptr->UsedSize += total_len;
			break;
		}

		//当前名字表空间没有空闲区域,继续向下找
		last_name_table_struct_ptr = name_table_ptr;
		name_table_ptr = getBlockPtr<ST_NameTable>(name_table_ptr->NextNameTable);
	}

	if(!has_enought_space)	//名字表链中没有足够的空间，需要开辟新的名字表空间
	{
		TBlockID idNew = allocBlock();
		last_name_table_struct_ptr->NextNameTable = idNew;
		has_enought_space = name_table_addItem(getBlockPtr<ST_NameTable>(idNew), item_name_str_ptr, value, param_value);
	}

	return has_enought_space;
}

bool CPlusTree::name_table_findItemData(ST_NameTable *name_table_ptr, const char *item_name_str_ptr, TBlockID &value, uint16_t &param_value)
{
	value = 0;
	bool found = false;

	//循环搜寻名字表链 (也可以使用递归)
	while(name_table_ptr)
	{
		char *on_table_search_ptr = reinterpret_cast<char *>(name_table_ptr->NameItemDatas);	//定位到最起始字符串
		char *pEnd = on_table_search_ptr + name_table_ptr->UsedSize;			//计算结束边界
		while(on_table_search_ptr < pEnd)
		{
			//比对字符串
			const char *on_name_search_ptr = item_name_str_ptr;
			for(; (*on_name_search_ptr != '\0') && (*on_table_search_ptr == *on_name_search_ptr); on_table_search_ptr++, on_name_search_ptr++) ;
			found = (*on_table_search_ptr++ - *on_name_search_ptr) == 0;
			if(found)
			{
				TBlockID *value_ptr = reinterpret_cast<TBlockID *>(on_table_search_ptr);
				value = *value_ptr++;
				param_value = *(static_cast<uint16_t *>(value_ptr));
				break;
			}
			else
			{
				while(*on_table_search_ptr++) ;
				on_table_search_ptr += sizeof(TBlockID) + sizeof(uint16_t);				//定位到下一个项目起始字符串
			}
		}
		if(!found)
			name_table_ptr = getBlockPtr<ST_NameTable>(name_table_ptr->NextNameTable);
		else
			break;
	}

	return found;
}

TBlockID CPlusTree::name_table_delItem(ST_NameTable *name_table_ptr, const char *item_name_str_ptr)
{
	ST_NameTable::ST_FindContent find_content_struct;
	TBlockID value_id = 0;
	uint16_t param_value = 0;

	//遍历查找指定项
	name_table_initFindContent(name_table_ptr, find_content_struct);
	while(enumNameTableItem(find_content_struct, value_id, param_value))
	{
		if(strcmp(item_name_str_ptr, find_content_struct.ItemNameStringPtr) == 0)  //find it!!!
		{
			//删除内部数据结构
			ST_NameTable *current_name_table_ptr = find_content_struct.CurrentTableStructPtr;
			int nItemLength = strlen(item_name_str_ptr) + 1 + sizeof(TBlockID) + sizeof(uint16_t);
			int nMovedSize = current_name_table_ptr->NameItemDatas + current_name_table_ptr->UsedSize - (current_name_table_ptr->NameItemDatas + nItemLength);
			memcpy(find_content_struct.ItemNameStringPtr, find_content_struct.ItemNameStringPtr + nItemLength, nMovedSize);
			current_name_table_ptr->UsedSize -= nItemLength;

			break;
		}
	}

	/* 返回name table中存储的dir block id，由调用者负责删除 */
	return value_id;
}

void CPlusTree::name_table_clear(ST_NameTable *name_table_ptr)
{
	ST_NameTable::ST_FindContent find_content_struct;
	name_table_initFindContent(name_table_ptr, find_content_struct);

	TBlockID value_id = 0;
	uint16_t param_value = 0;
	while(CPlusTree::enumNameTableItem(find_content_struct, value_id, param_value))
	{
		dir_clear(getBlockPtr<ST_Dir>(value_id));
		freeBlock(value_id);
	}
}
