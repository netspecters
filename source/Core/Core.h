#ifndef  PLUS_TREE_INC
#define  PLUS_TREE_INC

#include <string>
#include <utility>
#include <stdint.h>
#include <mutex>

#include "nslib.h"

#include "ipc.h"
#include "NS_Exception.h"
#include "PlusInterface.h"
#include "PlusTreeDefine.h"

namespace Netspecters { namespace Core {

using std::string;
using namespace Netspecters::NSStdLib;

///内核中用于管理插件的类，实现 IPlusTree 接口
class CPlusTree : public IPlusTree , protected CMappedFile
{
	public:
		/**
		 * @name 实现 IPlusTree 接口
		 * @{
		*/
		//查找目录
		//使用目录的绝对路径
		virtual NSAPI HDIR openDir( const char *abs_dir_path );

		//获取插件接口指针
		virtual NSAPI INSPlus *getPlusInterface( HDIR plus_dir_handle );

		//打开指定目录下的一条记录(索引)
		virtual NSAPI HRECORD openRecord( HDIR dir_handle, int record_index, int open_always );

		//在指定的目录下删除指定记录(索引)
		//打开的记录会被自动关闭
		virtual NSAPI void	delRecord( HDIR dir_handle, int record_index );

		//读(写)取指定记录(索引)的数据
		virtual NSAPI int readRecord( HRECORD record_handle, void *buffer_ptr, size_t size );
		virtual NSAPI int writeRecord( HRECORD record_handle, const void *buffer_ptr, size_t size );

		//关闭打开的记录
		virtual NSAPI void closeRecord( HRECORD record_handle );
		virtual NSAPI void closeDir( HDIR dir_handle );

		//安装(卸载)插件
		virtual NSAPI int installPlus( const char *plus_library_string_ptr, const char *path_string_ptr, const char *plus_name_string_ptr, int reserved );
		virtual NSAPI void uninstallPlus( const char *path_string_ptr, const char *plus_name_string_ptr );
		virtual NSAPI void enumPlus( TPlusTreeEnumFunc call_back_func, intptr_t user_data );
		virtual NSAPI int scanPlusDir( void );

		//获取插件树工作路径
		virtual NSAPI int getPlusTreeWorkPath( char *work_path_buffer, size_t buffer_size );
		virtual NSAPI int getDataDirPath( char *data_dir_path_buffer, size_t buffer_size );
		virtual NSAPI INSMain *getMainPlus( void );
		virtual NSAPI void queryErrorMsg( long error_id, char *&msg_buffer_ptr, int msg_buffer_size );

		//ipc相关
		virtual NSAPI bool initInterfaceIPC( HDIR interface_dir_handle, void *object_instance, unsigned max_method_number );
		virtual NSAPI int registerIPCMethod( HDIR interface_dir_handle, const char *method_name, void *method_address );
		///@}

		///装载（初始化）插件树
		bool	loadPlusTree( const string &work_path, bool scan_before_load );
		///卸载插件树
		void	unloadPlusTree( void );
		///生成并初始化一个默认的数据文件
		void 	initDBFile( void );
		///检验打开的DB文件是否合法
		bool	checkDBFile( void );
		///枚举插件
		bool enum_plus( ST_NameTable *base_name_table_ptr , const char *base_path_name, TPlusTreeEnumFunc call_back_func, intptr_t user_data );
		///(向插件树)描述内核插件(向插件树添加内核插件)
		void	dirscribeKernelPlus( int kernel_plus_id, const char *path_string_ptr, const char *plus_name_string_ptr , bool bIsRegisterAsMain = false );
		static CPlusTree *getPlusTreeInstancePtr()	//ns系统会保证初始化的时候对象被创建，内部使用的情况下可直接用
		{
			static CPlusTree static_plus_tree_instance;
			return &static_plus_tree_instance;
		}
		///调用ipc方法
		std::pair<void *, bool> ipc_call_method( const char *interface_path, const char *method_name, unsigned argument_size, void *argument_list );

	private:
		///创建内核插件实例
		template<typename TKernelPlusClass> void createKernelPluses( int nKernelPlusID, INSMain *&iMainPlus );
		///创建静态内核插件实例(如果使用模板编译太麻烦)
		template<typename TKernelPlusClass> void createStaticKernelPluses( int nKernelPlusID, INSMain *&iMainPlus );
		///析构内核插件实例
		template<typename TKernelPlusClass> void destroyKernelPluses( int nKernelPlusID );
		TBlockID	get_dir_block( const char *dir_path_string_ptr );
		///插件所在的库装入内存，并创建插件实例
		void		loadPluses( const string &plus_search_path );
		void		unloadPluses( void );

		TBlockID	allocBlock( void );
		void 		freeBlock( TBlockID block_id );
		void 		freeBlock( TBlockID idBlock_Begin, TBlockID idBlock_End );
		int	 		resizeFile( int inc_size = MOC_INC_SIZE );
		template <typename xReturn>xReturn *getBlockPtr( TBlockID block_id ) {
			return block_id == 0 ? NULL : ( xReturn * )( static_cast<unsigned char *>( m_file_mem_ptr ) + block_id * C_BLOCKSIZE );
		}

		/** @brief 调用指定对象的函数
		 *
		 * @param obj_instance 对象实例指针
		 * @param method_address
		 * @param arg_list_size
		 * @param arg_list
		 * @return 返回无类型的调用结果
		 * @note 函数不会抛出异常
		 *
		 */
		static void *invoke_method( void *obj_instance, void *method_address, unsigned arg_list_size, void *arg_list ) throw();
#if defined DEBUG
		struct X {
			int a, b, c;
		};
		NSAPI uintptr_t ipc_test_method( int first_param, bool second_param, X &third_param, char *forth_param, const char *name );
#endif

#ifdef USE_SAFE_HANDLE
		enum ENM_HandleType { htDir, htRecord };
		typedef CHandleTable<TBlockID, ENM_HandleType> CCoreHandleTable;
#define DEFINE_HANDLE_TABLE CCoreHandleTable m_handle_table;
#define OPEN_HANDLE( type, id ) m_handle_table.openHandle( type, id )
#define HANDLE_TO_ID( handle ) m_handle_table.HandleToID( handle )
#define CLOSE_HANDLE( handle ) m_handle_table.closeHandle( handle )
#else
#define DEFINE_HANDLE_TABLE
#define OPEN_HANDLE( type, handle ) (handle)
#define HANDLE_TO_ID( handle ) (handle)
#define CLOSE_HANDLE( handle )
#endif

		/**
		*	@name 操作dir的函数
		*   @{
		*/
		TBlockID	dir_addSubDir( ST_Dir *dir_ptr, const char *sub_dir_name_str_ptr );
		TBlockID	dir_addRecord( ST_Dir *dir_ptr, int record_index );
		bool		dir_delSubDir( ST_Dir *dir_ptr, const char *sub_dir_name_str_ptr );
		bool		dir_delRecord( ST_Dir *dir_ptr, int record_index );
		TBlockID	dir_findSubDir( ST_Dir *dir_ptr, const char *sub_dir_name_str_ptr, uint16_t &param_value );
		TBlockID	dir_getRecord( ST_Dir *dir_ptr, int record_index );
		bool		dir_init( ST_Dir *dir_ptr, INSPlus *parent_plus_ptr, TBlockID plus_dir_id );
		void		dir_uninit( ST_Dir *dir_ptr );
		void		dir_clear( ST_Dir *dir_ptr );
		/// @}

		/**
		*	@name 操作name table的函数
		*   @{
		*/
		bool 		enumNameTableItem( ST_NameTable::ST_FindContent &find_content_struct, TBlockID &value, uint16_t &param_value );
		void		name_table_initFindContent( ST_NameTable *name_table_ptr, ST_NameTable::ST_FindContent &find_content_struct );
		bool		name_table_addItem( ST_NameTable *name_table_ptr, const char *item_name_str_ptr, TBlockID value, uint16_t param_value );
		bool		name_table_findItemData( ST_NameTable *name_table_ptr, const char *item_name_str_ptr, TBlockID &value, uint16_t &param_value );
		TBlockID	name_table_delItem( ST_NameTable *name_table_ptr, const char *item_name_str_ptr );
		void		name_table_clear( ST_NameTable *name_table_ptr );
		/// @}

		/**
		*	@name 操作data的函数
		*   @{
		*/
		size_t	data_block_write( ST_Data *data_block_ptr, const void *buffer_ptr, size_t buffer_size );
		size_t	data_block_read( ST_Data *data_block_ptr, void *buffer_ptr, size_t buffer_size );
		void	data_block_clear( ST_Data *data_block_ptr );
		/// @}

		/**
		*	@name 操作index table的函数
		*   @{
		*/
		bool		index_table_addItem( ST_IndexTable *index_table_ptr, size_t index, TBlockID value, uint16_t param_value );
		TBlockID	index_table_getItem( ST_IndexTable *index_table_ptr, size_t index, uint16_t &param_value );
		bool		index_table_delItem( ST_IndexTable *index_table_ptr, size_t index );
		void		index_table_clear( ST_IndexTable *index_table_ptr );
		/// @}

	private:
		INSMain *m_main_plus_ptr;
		ST_SuperBlock *m_super_block_ptr;					//超级块指针
		std::recursive_mutex m_unused_block_stack_mutex;	//用于锁定为使用块链栈的可递归互斥对象
		string m_work_path;								//工作路径（Netspecters文件夹所在目录）
		CIPCServer m_ipc_server;

		DEFINE_HANDLE_TABLE
};

}/* Core **/ }/* Netspecters **/

#endif   /* ----- #ifndef PLUS_TREE_INC  ----- */
