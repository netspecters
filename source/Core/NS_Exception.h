#ifndef NS_EXCEPTION_H_INCLUDED
#define NS_EXCEPTION_H_INCLUDED

#include <string>
#if defined(_WIN32) || defined(_WIN64)
#	include <windows.h>
#	include "../../ThirdParty/google_breakpad/src/client/windows/handler/exception_handler.h"
#	define TCHAR	wchar_t
#	define TSTRING	wstring
#else
#	include <sys/file.h>
#	include <sys/types.h>
#	include <sys/stat.h>
#	include <fcntl.h>
#	include "../../ThirdParty/google_breakpad/src/client/linux/handler/exception_handler.h"
#	define TCHAR	char
#	define TSTRING	string
#endif

namespace Netspecters { namespace Core {

using namespace google_breakpad;

/**
 * @brief 管理全局异常对象，处理崩溃、转储
 * @note 此类全局只能有一个实例
*/
class CNSException
{
	public:
		/**
		 * @param dump_path 程序崩溃时用于存储转储文件的目录
		*/
		CNSException ( const std::TSTRING &dump_path );
		/**
		 * @brief 此Netspecters实例(进程)是不是第一个
		 * @note 用于不允许Nespecters重复运行时检测Netspecters是否为单一实例
		*/
		bool isFirstInstance ( void );

	private:
		static bool doMinidumpCallback ( const TCHAR *dump_path, const TCHAR *minidump_id, void *context,
#if defined(_WIN32) || defined(_WIN64)
										 EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion,
#endif
										 bool succeeded );
		static bool doFilterCallback ( void *context
#if defined(_WIN32) || defined(_WIN64)
									   , EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion
#endif
									 );

		ExceptionHandler m_exception_handler;
#if defined(_WIN32) || defined(_WIN64)
		HANDLE	m_exclusive_locker;
#else
		int	m_exclusive_locker;
#endif
};

}/* Core **/ }/* Netspecters **/

#endif // NS_EXCEPTION_H_INCLUDED
