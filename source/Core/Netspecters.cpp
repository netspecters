#include "../NSErrorCode.h"

#include "Netspecters.h"

#include <string>
#include <cstdlib>

#if !(defined(_WIN32) || defined(_WIN64))
#	include <unistd.h>
#	include <sys/stat.h>
#	include <sys/types.h>
#	include <getopt.h>
#endif

using std::string;
using namespace Netspecters::Core;

/* 命令选项 */
const option ns_long_options[] = {
	{"scan-plugin", 0, NULL, 's'},	//扫描插件
	{NULL, 0, NULL, 0}
};
const char ns_short_options[] = {"s"};

#if !(defined(_WIN32) || defined(_WIN64))
/* 处理 *nix 下的kill中断 */
static void handleKillSignal(int sig)
{
	printf("\n");
	CPlusTree::getPlusTreeInstancePtr()->getMainPlus()->kill(0);
}
#endif

#if defined(_WIN32) || defined(_WIN64)
int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow)
#else
int main(int argc, char *argv[])
#endif
{
	/* 命令参数 */
	bool need_scan_plugin = false;

	/* 解析命令行 */
	while(1)
	{
		int option_index = 0;
		int c = getopt_long(argc, argv, ns_short_options, ns_long_options, &option_index);

		if(c == -1)
			break;

		switch(c)
		{
			case 's':
				need_scan_plugin = true;
				break;
		}
	}

	//获取程序执行路径
#if defined(_WIN32) || defined(_WIN64)
	//提取路径
	wchar_t aAppFullPath[MAX_PATH];
	GetModuleFileName(NULL, aAppFullPath, MAX_PATH);
	wstring work_path_tmp(aAppFullPath);	//工作路径
	int nPos = work_path_tmp.rfind('\\');
	work_path_tmp.resize(nPos + 1);

	/* 系统内部使用utf8处理字符串，要将unicode转换成utf8 */
	string work_path;
	{
		DECL_UNICODE_UTF8(work_path_tmp.c_str(), work_path_utf8)
		work_path.assign(work_path_utf8);
	}

	/* create a global exception-instance in stack*/
	CNSException global_exception(work_path_tmp);
#else
#	ifdef DEBUG
	char *abs_cwd_path_ptr = get_current_dir_name();
	string work_path(abs_cwd_path_ptr);
	free(abs_cwd_path_ptr);
#	else
	//linux 下的工作路径设置在home中的.config下
	string work_path(getenv("XDG_CONFIG_HOME"));
	work_path += "/netspecters";
	chdir(work_path.c_str());	//改变当前工作路径
#	endif
	if(access(work_path.c_str(), F_OK) == -1)
	{
		if(mkdir(work_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0)
		{
			printk(KWARNING"\"$XDG_CONFIG_HOME/netspecters\" not exist, netspecters now making it!");
		}
		else
			return -1;
	}
	work_path += '/';

	/* create a global exception-instance in stack*/
	CNSException global_exception(work_path);

	/* connect kill signal */
	signal(SIGINT, handleKillSignal);
	signal(SIGKILL, handleKillSignal);
#endif

	//构建插件树
	int result = 0;
	try
	{
		CPlusTree *plus_tree_ptr = CPlusTree::getPlusTreeInstancePtr();
		if(!plus_tree_ptr->loadPlusTree(work_path, need_scan_plugin))
			return -2;
		INSMain *main_plus_ptr = plus_tree_ptr->getMainPlus();

		printk(KINFORMATION"=====> Netspecters' kernel is running! <=====\n");
		if(main_plus_ptr != NULL)
			result = main_plus_ptr->onLoop();

		printk(KINFORMATION"Received kill signal.\n");
		plus_tree_ptr->unloadPlusTree();
	}
	catch(...)
	{
		printk(KERROR"!!Netspecters Exception!!\n\n");
		return -3;
	}
	printk(KINFORMATION"Netspecters' kernel stop! BYE!\n\n");
	return result;
}
