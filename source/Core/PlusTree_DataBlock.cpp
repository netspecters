#if defined(_WIN32) || defined(_WIN64)
#	include <windows.h>
#else
#	include <unistd.h>
#	include <fcntl.h>
#endif
#include <string>
#include <utility>

#include "Core.h"

using namespace Netspecters::Core;
using std::string;

void getFilePathNameFromTag ( string &file_path_name, decltype ( ST_Data::data.file_tag ) file_tag )
{
	static char s_ArrayOfIndex[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	//磁盘数据文件路径
	file_path_name.append ( DATA_DIR_PATH );

	//生成磁盘文件名(使用小写十六进制字符)
	file_path_name.reserve( file_path_name.size() + 33 );
	for( auto itr = file_tag.begin(); itr != file_tag.end(); ++itr )
	{
		file_path_name += s_ArrayOfIndex[ ( *itr >> 4 ) ];
		file_path_name += s_ArrayOfIndex[ ( *itr & 0x0f ) ];
	}
}

size_t CPlusTree::data_block_write ( ST_Data *data_block_ptr, const void *buffer_ptr, size_t buffer_size )
{
	size_t writed_size = 0;
	if ( ( buffer_ptr == NULL ) && ( data_block_ptr->data_size > 0 ) )
	{
		data_block_clear ( data_block_ptr );
	}
	else if ( buffer_ptr )
	{
		string file_path ( m_work_path );

		if ( buffer_size >= ST_Data::C_DATASIZE )	//数据将写入磁盘 TODO
		{
			/* 磁盘文件以前不存在, 计算文件Tag（tag = rand()） */
			if ( data_block_ptr->data_size < ST_Data::C_DATASIZE )
				data_block_ptr->data.file_tag = boost::uuids::random_generator()();

			//获得磁盘文件名(使用小写十六进制字符)
			string file_path ( m_work_path );
			getFilePathNameFromTag ( file_path, data_block_ptr->data.file_tag );

			//保存数据到磁盘文件
#if defined(_WIN32) || defined(_WIN64)
			DECL_UTF8_UNICODE ( file_path.c_str(), file_path_unicode )

			HANDLE file_handle = CreateFile ( file_path_unicode, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0 );
			WriteFile ( file_handle, buffer_ptr, buffer_size, reinterpret_cast<LPDWORD> ( &writed_size ), NULL );
			CloseHandle ( file_handle );
#else
			int file_handle = open ( file_path.c_str(), O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP );
			writed_size = write ( file_handle, buffer_ptr, buffer_size );
			close ( file_handle );
#endif
		}
		else
		{
			/* 直接将数据拷入data block中即可 */
			memcpy ( data_block_ptr->data.raw_data, buffer_ptr, buffer_size );
			writed_size = buffer_size;
		}

		data_block_ptr->data_size = writed_size;
	}

	return writed_size;
}

size_t	CPlusTree::data_block_read ( ST_Data *data_block_ptr, void *buffer_ptr, size_t buffer_size )
{
	size_t readed_size = 0;

	if ( ( buffer_ptr == NULL ) && ( data_block_ptr->data_size > 0 ) )
	{
		readed_size = data_block_ptr->data_size;	//预取数据大小
	}
	else if ( buffer_ptr )
	{
		buffer_size = buffer_size > data_block_ptr->data_size ? data_block_ptr->data_size : buffer_size;

		if ( data_block_ptr->data_size >= ST_Data::C_DATASIZE )	//数据在磁盘上
		{
			//获得磁盘文件名(使用小写十六进制字符)
			string file_path ( m_work_path );
			getFilePathNameFromTag ( file_path, data_block_ptr->data.file_tag );

#if defined(_WIN32) || defined(_WIN64)
			DECL_UTF8_UNICODE ( file_path.c_str(), file_path_unicode )

			HANDLE file_handle = CreateFile ( file_path_unicode, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0 );
			ReadFile ( file_handle, buffer_ptr, buffer_size, reinterpret_cast<LPDWORD> ( &readed_size ), NULL );
			CloseHandle ( file_handle );
#else
			int file_handle = open ( file_path.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP );
			readed_size = read ( file_handle, buffer_ptr, buffer_size );
			close ( file_handle );
#endif
		}
		else
		{
			/* 直接从data block中读出 */
			memcpy ( buffer_ptr, data_block_ptr->data.raw_data, buffer_size );
			readed_size = buffer_size;
		}
	}

	return readed_size;
}

void CPlusTree::data_block_clear ( ST_Data *data_block_ptr )
{
	if ( data_block_ptr->data_size >= ST_Data::C_DATASIZE )	//数据在磁盘上
	{
		//删除文件
		string file_path ( m_work_path );
		getFilePathNameFromTag ( file_path, data_block_ptr->data.file_tag );

#if defined(_WIN32) || defined(_WIN64)
		DECL_UTF8_UNICODE ( file_path.c_str(), file_path_unicode )

		DeleteFile ( file_path_unicode );
#else
		unlink ( file_path.c_str() );
#endif
	}

	data_block_ptr->data_size = 0;
}
