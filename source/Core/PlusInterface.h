/**
 * @file __FILE__
*/

#ifndef  PLUSINTERFACE_INC
#define  PLUSINTERFACE_INC

#include <cstddef>
#include <stdint.h>
#include "../NSDefine.h"
#include "../StdLib/LibraryMaco.hpp"
#include "KernelConsole.h"

typedef uint32_t HDIR;
typedef uint32_t HRECORD;

class INSPlus;
class INSMain;

struct ST_PluginInformation
{
	const char *name;
	const char *full_path_on_the_tree;
};

/**
	@class IPlusTree
	@brief 内核插件树抽象接口
*/
class IPlusTree
{
	public:
		typedef uint16_t TBlockID;

		/**
		 * @brief 查找指定目录
		 * @param abs_dir_path 指向目录绝对路径的字符串指针，字符串必须以NULL结尾，路径必须以'/'起，以'/'终
		 * @return 返回目录句柄
		 *	- NS_INVALID_HANDLE:操作失败
		 * @note 如果函数成功返回目录句柄，则在句柄使用完毕后必须用 closeDir 关闭打开的句柄
		 * @see closeDir
		*/
		virtual NSAPI HDIR openDir(const char *abs_dir_path) = 0;

		/**
		 * @brief 获取指定目录的插件接口指针
		 * @param plus_dir_handle 目录句柄
		 * @return 返回插件接口指针
		 *	- NULL:操作失败
		 * @see ::INSPlus openDir
		*/
		virtual NSAPI INSPlus* getPlusInterface( HDIR plus_dir_handle ) = 0;

		/**
		 * @brief 打开指定目录下的一条记录
		 * @param dir_handle 目录句柄
		 * @param record_index 记录索引值(从0开始)
		 * @param open_always 在 record_index 指定的记录不存在的时候是否自动创建并打开一个空白记录
		 * @return 返回记录句柄，句柄值恒大于零
		 *	- NS_INVALID_HANDLE:操作失败
		 * @note 如果函数成功返回记录句柄，则在句柄使用完毕后必须用 closeRecord 关闭打开的句柄
		 * @see closeRecord
		*/
		virtual NSAPI HRECORD openRecord(HDIR dir_handle, int record_index, int open_always) = 0;

		/**
		 * @brief 在指定的目录下删除指定记录
		 * @param dir_handle 目录句柄
		 * @param record_index 记录索引值(从0开始)
		 * @note 在执行删除操作之前，record_handle指定的记录会先被自动关闭
		 * @see openDir
		*/
		virtual NSAPI void	delRecord(HDIR dir_handle, int record_index) = 0;

		/**
		 * @brief 读取指定记录中的数据
		 * @param record_handle 记录句柄
		 * @param buffer_ptr 数据区指针
		 * @param size 数据区大小
		 * @see openRecord
		*/
		virtual NSAPI int readRecord(HRECORD record_handle, void *buffer_ptr, size_t size) = 0;
		/**
		 * @brief 将数据写入指定记录中
		 * @param record_handle 记录句柄
		 * @param buffer_ptr 数据区指针
		 * @param size 数据区大小
		 * @see openRecord
		*/
		virtual NSAPI int writeRecord(HRECORD record_handle, const void *buffer_ptr, size_t size) = 0;

		/**
		 * @brief 关闭记录
		 * @param record_handle 记录句柄
		 * @see openRecord
		*/
		virtual NSAPI void closeRecord( HRECORD record_handle ) = 0;
		/**
		 * @brief 关闭目录
		 * @param dir_handle 目录句柄
		 * @see openDir
		*/
		virtual NSAPI void closeDir( HDIR dir_handle ) = 0;

		/**
		 * @name 插件操作函数
		 * @{
		 */
		/**
		 * @brief 安装插件
		 * @param plus_library_string_ptr 指向插件动态库在文件系统中的名字，名字必须使用UTF8编码
		 * @param path_string_ptr 指明插件在NS系统插件树中的位置，字符串必须使用ASCII编码
		 * @param plus_name_string_ptr 指明插件在NS系统插件系统中的名称，字符串必须使用ASCII编码
		 * @return 如果成功，返回NSEC_SUCESS，否则返回NSEC_NSERROR，调用 Netspecters::NSStdLib::OSFunc::get_errno 得到具体错误码：
		 *	- <b>NSEC_PARAMERROR</b> 参数错误
		 * @note 插件成功安装后要到下次启动的时候才能生效
		 * @note 插件在注册成功后一直有效，直到调用 installPlus 从NS系统中移除
		 * @see uninstallPlus
		*/
		virtual NSAPI int installPlus( const char *plus_library_string_ptr, const char *path_string_ptr, const char *plus_name_string_ptr, int reserved ) = 0;

		/**
		 * @brief 卸载插件
		 * @param path_string_ptr 指明插件在NS系统插件树中的位置，字符串必须使用ASCII编码
		 * @param plus_name_string_ptr 指明插件在NS系统插件系统中的名称，字符串必须使用ASCII编码
		 * @note 卸载插件只表示从NS系统中移除关于插件的信息，并不代表从文件系统中物理删除
		 * @see installPlus
		*/
		virtual NSAPI void uninstallPlus(const char *path_string_ptr, const char *plus_name_string_ptr) = 0;

		typedef NSAPI bool (*TPlusTreeEnumFunc)(  const ST_PluginInformation *plugin_infor_ptr, intptr_t user_data );
		/**
		 * @brief 插件遍历
		 * @param call_back_func 回调函数，每当枚举一个插件的时候回调一次，回调函数返回false结束枚举过程
		 * @param user_data 用户定义的数据，每次枚举回调时将附带此参数
		*/
		virtual NSAPI void enumPlus( TPlusTreeEnumFunc call_back_func, intptr_t user_data ) = 0;

		/**
		 * @brief 扫描插件目录
		 * @return 新增插件个数
		*/
		virtual NSAPI int scanPlusDir( void ) = 0;
		///@}

		/**
		 * @brief 获取插件树配置文件工作路径(也就是Kerneldb.db所在目录)
		 * @param work_path_buffer 接收工作路径字符串的缓冲区指针，如果：
		 *	- NULL:或略 buffer_size 参数，返回工作路径字符串长度
		 * @param buffer_size 缓冲区大小
		 * @return 返回工作路径字符串的实际长度，如果失败返回 0
		 * @note 工作路径字符串以UTF8格式返回
		*/
		virtual NSAPI int getPlusTreeWorkPath( char *work_path_buffer, size_t buffer_size ) = 0;

		/**
		 * @brief 获取数据目录路径
		 * @param data_dir_path_buffer 接收数据目录路径字符串的缓冲区指针，如果：
		 *	- NULL:或略 buffer_size 参数，返回数据目录路径字符串长度
		 * @param buffer_size 缓冲区大小
		 * @return 返回数据目录路径字符串的实际长度，如果失败返回 0
		 * @note 数据目录路径字符串以UTF8格式返回
		*/
		virtual NSAPI int getDataDirPath( char *data_dir_path_buffer, size_t buffer_size ) = 0;

		/**
		 * @brief 获取主插件指针
		 * @return 返回在NS系统中注册的主插件指针
		 * @note NS系统如果正常运行必须包含一个主插件，因此返回的指针恒有效
		*/
		virtual NSAPI INSMain *getMainPlus( void ) = 0;

		/**
		 * @brief 将NS系统和OS的错误码转换成字符串
		*/
		virtual NSAPI void queryErrorMsg( long error_id, char *&msg_buffer_ptr, int msg_buffer_size ) = 0;

        /** @brief
         *
         * @param interface_dir_handle HDIR
         * @param max_method_number unsigned
         * @return virtual NSAPI bool
         *
         */
		virtual NSAPI bool initInterfaceIPC( HDIR interface_dir_handle, void *object_instance, unsigned max_method_number ) = 0;

        /** @brief 注册进程间可调用（接口）方法
         *
         * @param interface_dir_handle 接口目录句柄;由 openDir 获得
         * @param method_name 方法名
         * @param arg_num 方法参数个数
         * @param args 方法参数表(每个参数的字节长度组成的数组)
         * @param method_address 方法的实际内存地址
         * @return 如果方法表没有初始化，返回 NSEC_NOT_INIT
         * @see OpenDir
         */
		virtual NSAPI int registerIPCMethod( HDIR interface_dir_handle, const char *method_name, void *method_address ) = 0;
};


/**
	@class INSPlus
	@brief 插件基础抽象接口
	@note NS系统中的所有插件必须实现此接口
	@note 插件对象建立过程中如果发生异常，不应该抛出，而应该在onLoad时返回失败
*/
class INSPlus
{
	public:
		/**
		 * @brief 当NS系统初始化插件的时候(回)调用的函数
		 * @param plus_tree_ptr 插件树接口指针
		 * @param parent_plus_ptr 在插件树中父插件接口指针
		 * @param plus_dir_handle 插件所在目录的句柄;是由 openDir 获得的，但不要尝试关闭此句柄，此句柄由内核管理
		 * @param default_param 在使用 installPlus 注册的时候提供的插件参数
		 * @return 返回插件初始化状态:
		 *	- NSEC_SUCCESS:插件成功完成初始化
		 *	- NSEC_NSERROR:插件初始化的时候发生错误，初始化中断
		 * @note 当插件返回非 NSEC_SUCCESS 时：
		 *	- 当插件为用户插件时，NS的插件系统会认为插件无法正常初始化，因此不会装载此插件及其子插件
		 *	- 当插件为系统插件时，NS系统会中断退出
		*/
		virtual NSAPI int onLoad( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param ) = 0;

		/**
		 * @brief 当NS系统释放插件的时候(回)调用的函数
		*/
		virtual NSAPI void onUnload(void) = 0;

		virtual NSAPI int getParam(const char *param_name, void *value_buffer_ptr)= 0;
		virtual NSAPI void *setParam(const char *param_name, void *value_ptr) = 0;
};

/**
	@class INSMain
	@brief 主插件抽象接口
	@note NS系统中的主插件由内核提供，用户不需要实现此接口
*/
class INSMain : public INSPlus
{
	public:
		virtual NSAPI int onLoop(void) = 0;
		virtual NSAPI void kill( int exit_code ) = 0;
};

typedef NSAPI int (*func_CreateInterface)(int, INSPlus*&);
typedef NSAPI int (*func_DestroyInterface)(INSPlus*);
typedef NSAPI int (*func_DescribePlugin)(ST_PluginInformation*);
typedef IPlusTree::TBlockID TBlockID;

///无效的block id
#define	INVALID_BLOCK_ID	NS_INVALID_HANDLE

#endif   /* ----- #ifndef PLUSINTERFACE_INC  ----- */
