/*
 * =====================================================================================
 *
 *       Filename:  KernelConsole.cpp
 *
 *    Description:  内核信息终端实现
 *
 *        Version:  1.0
 *        Created:  2009年09月19日 20时31分06秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Wang Guan (), netspecters@gmail.com
 *        Company:
 *
 * =====================================================================================
 */

#include "KernelConsole.h"

#ifndef USE_FILE_LOG
#	undef LOG_FILE_APPEND					//开启USE_FILE_LOG时LOG_FILE_APPEND才有效
#	if defined(_WIN32) || defined(_WIN64)	//在不使用文件作为日志记录终端的时候，windows系统下要开辟一个console
#		define NEED_WIN_CONSOLE
#	endif
#endif

#ifdef LOG_FILE_APPEND
#	define FILE_OPEN_MODLE "a"
#else
#	define FILE_OPEN_MODLE "w"
#endif

#ifdef NEED_WIN_CONSOLE
	#include <windows.h>
#endif

#ifdef USE_FILE_LOG
	FILE *g_printk_log_file_handle;
#endif
#ifdef ERR_LOG_TO_FILE
	FILE *g_dbg_log_file_handle;
#endif

static void __attribute__((constructor)) initConsole(void)
{

#ifdef USE_FILE_LOG
	g_printk_log_file_handle = fopen("netspecters_sys_log.txt", FILE_OPEN_MODLE);
#endif

#ifdef ERR_LOG_TO_FILE
	g_dbg_log_file_handle = fopen("netspecters_dbg_log.txt", FILE_OPEN_MODLE);
#endif

#ifdef NEED_WIN_CONSOLE
	AllocConsole();
	freopen("CONOUT$","w+t",stdout);
	freopen("CONOUT$","w+t",stderr);
	freopen("CONIN$","r+t",stdin);
#endif
}

static void __attribute__((destructor)) finitConsole(void)
{
#ifdef USE_FILE_LOG
	fclose( g_printk_log_file_handle );
#endif
#ifdef ERR_LOG_TO_FILE
	fclose( g_dbg_log_file_handle );
#endif

#ifdef NEED_WIN_CONSOLE
	FreeConsole();
#endif
}
