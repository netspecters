#include "Core.h"

using namespace Netspecters::Core;

bool CPlusTree::index_table_addItem( ST_IndexTable *index_table_ptr, size_t index, TBlockID value, uint16_t param_value)
{
	bool has_added = false;
	while (index_table_ptr)
	{
		has_added = index < ST_IndexTable::C_INDEX_NUM_PER_BLOCK;
		if (has_added)
		{
			index_table_ptr->Indexs[index].Value = value;
			index_table_ptr->Indexs[index].Param = param_value;
			break;
		}
		else
		{
			index -= ST_IndexTable::C_INDEX_NUM_PER_BLOCK;
			if (!index_table_ptr->NextIndexTable)
				index_table_ptr->NextIndexTable = allocBlock();
			index_table_ptr = getBlockPtr<ST_IndexTable>( index_table_ptr->NextIndexTable );
		}
	}
	return has_added;
}

TBlockID CPlusTree::index_table_getItem( ST_IndexTable *index_table_ptr, size_t index, uint16_t &param_value )
{
	TBlockID item_block_id = 0;
	while (index_table_ptr)
	{
		if (index < ST_IndexTable::C_INDEX_NUM_PER_BLOCK)
		{
			param_value = index_table_ptr->Indexs[index].Param;
			item_block_id = index_table_ptr->Indexs[index].Value;
			break;
		}
		else
		{
			index_table_ptr = getBlockPtr<ST_IndexTable>( index_table_ptr->NextIndexTable );
			index -= ST_IndexTable::C_INDEX_NUM_PER_BLOCK;
		}
	}
	return item_block_id;
}

bool CPlusTree::index_table_delItem( ST_IndexTable *index_table_ptr, size_t index )
{
	bool deleted = false;
	while (index_table_ptr)
	{
		deleted = index < ST_IndexTable::C_INDEX_NUM_PER_BLOCK;
		if (deleted)
		{
			TBlockID block_id = index_table_ptr->Indexs[index].Value;
			data_block_clear(getBlockPtr<ST_Data>(block_id));
			freeBlock(block_id);

			memcpy(index_table_ptr->Indexs+index*sizeof(ST_IndexTable::ST_Index),
				index_table_ptr->Indexs+(index+1)*sizeof(ST_IndexTable::ST_Index),
				(ST_IndexTable::C_INDEX_NUM_PER_BLOCK-index-1)*sizeof(ST_IndexTable::ST_Index));
		}
		else
		{
			index -= ST_IndexTable::C_INDEX_NUM_PER_BLOCK;
			index_table_ptr = getBlockPtr<ST_IndexTable>( index_table_ptr->NextIndexTable );
		}
	}
	return deleted;
}

void CPlusTree::index_table_clear( ST_IndexTable *index_table_ptr )
{
	while (index_table_ptr)
	{
		for (size_t i=0; i<ST_IndexTable::C_INDEX_NUM_PER_BLOCK; i++)
		{
			TBlockID block_id = index_table_ptr->Indexs[i].Value;
			data_block_clear(getBlockPtr<ST_Data>(block_id));
			freeBlock(block_id);
		}
	}
}
