#ifndef NS_MESSAGE_PIPE_HPP_INCLUDED
#define NS_MESSAGE_PIPE_HPP_INCLUDED

#if defined(_WIN32) || defined(_WIN64)
#	include <windows.h>
#else
#	include <sys/types.h>
#	include <sys/socket.h>
#	include <sys/stat.h>
#	include <fcntl.h>
#	include <unistd.h>
#	include <netinet/in.h>
#endif
#include <string>

#include "nslib.h"

using std::string;

class CMessageSocket
{
	public:
#if defined(_WIN32) || defined(_WIN64)
		typedef SOCKET socket_t;
#else
		typedef int socket_t;
#endif
		class CMessage
		{
			public:
				enum ENM_CMD {
					cmdCallMethod,
					cmdMethodReturn,
					cmdMethodNotFound
				};

				CMessage() : m_body_buf_size( 0 ), m_body_op_pos( 0 ), m_body_ptr( NULL ) {}
				CMessage( ENM_CMD cmd )
					: m_body_buf_size( 0 ), m_body_op_pos( 0 ), m_body_ptr( NULL )
				{
					m_header.cmd = cmd;
				}
				CMessage( const CMessage & ) = delete;
				~CMessage() {
					free( m_body_ptr );
				}
				CMessage &operator >>( string &value )
				{
					unsigned str_len;
					read_body_raw_data( &str_len, sizeof( str_len ) );
					char buf[str_len];
					read_body_raw_data( buf, str_len );
					value.assign( buf, str_len );
					return *this;
				}
				CMessage &operator >>( unsigned int &value )
				{
					read_body_raw_data( &value, sizeof( value ) );
					return *this;
				}
				CMessage &operator <<( const string &value )
				{
					unsigned str_len = value.size();
					write_body_raw_data( &str_len, sizeof( str_len ) );
					write_body_raw_data( value.data(), str_len );
					return *this;
				}
				CMessage &operator <<( unsigned int value )
				{
					write_body_raw_data( &value, sizeof( value ) );
					return *this;
				}
				void read_body_raw_data( void *buf, size_t size )
				{
					memcpy( buf, m_body_ptr + m_body_op_pos, size );
					m_body_op_pos += size;
				}
				void write_body_raw_data( const void *buf, size_t size )
				{
					/* 确保缓冲区有足够空间 */
					if(( m_body_buf_size - m_body_op_pos ) < size )
					{
						unsigned need_size = m_body_buf_size + ( m_body_buf_size > size ? m_body_buf_size : size );
						m_body_ptr = ( char * )realloc( m_body_ptr, need_size );
						m_body_buf_size = need_size;
					}

					memcpy( m_body_ptr + m_body_op_pos, buf, size );
					m_body_op_pos += size;
				}
				char *get_body() {
					return m_body_ptr;
				}
				ENM_CMD get_cmd() {
					return m_header.cmd;
				}

			private:
				friend class CMessageSocket;
				struct ST_Header
				{
					unsigned protocol_version;
					ENM_CMD cmd;
					unsigned body_size;
				} m_header;
				unsigned m_body_buf_size;
				unsigned m_body_op_pos;	//缓冲区读写位置
				char *m_body_ptr;
		};

		CMessageSocket( socket_t socket_fd ) : m_socket_fd( socket_fd ) {}
		CMessageSocket()
		{
			m_socket_fd = ::socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );
			if( -1 == m_socket_fd )
				throw std::system_error( Netspecters::NSStdLib::OSFunc::get_errno(), std::system_category() );
		}
		~CMessageSocket() {
			close_socket();
		}

		socket_t get_socket_fd() {
			return m_socket_fd;
		}

		bool read_message( CMessage &msg )
		{
			/* 检测同步字符串 */
			unsigned int XID = 0;
			do
			{
				unsigned char xid;
				if( ::recv( m_socket_fd, &xid, sizeof( char ), 0 ) <= 0 ) return false;
				XID = ( XID << 8 ) | xid;
			} while( 0x5c5c5c5c != XID );

			/* 清空原有数据 */
			free( msg.m_body_ptr );
			msg.m_body_op_pos = 0;

			/* 读取消息头 */
			if( ::recv( m_socket_fd, &msg.m_header, sizeof( CMessage::ST_Header ), 0 ) <= 0 ) return false;
			msg.m_body_buf_size = msg.m_header.body_size;
			msg.m_body_ptr = ( char * )malloc( msg.m_header.body_size );

			/* 读取消息体 */
			if( ::recv( m_socket_fd, msg.m_body_ptr, msg.m_header.body_size, 0 ) <= 0 ) return false;

			return true;
		}
		bool write_message( CMessage &msg )
		{
			msg.m_header.protocol_version = 1;
			msg.m_header.body_size = msg.m_body_op_pos;

			unsigned int XID = 0x5c5c5c5c;
			if( ::send( m_socket_fd, &XID, sizeof( XID ), 0 ) <= 0 ) return false;
			if( ::send( m_socket_fd, &msg.m_header, sizeof( CMessage::ST_Header ), 0 ) <= 0 ) return false;
			if( ::send( m_socket_fd, msg.m_body_ptr, msg.m_header.body_size, 0 ) <= 0 ) return false;

			return true;
		}

	private:
		void close_socket()
		{
#if defined(_WIN32) || defined(_WIN64)
			closesocket( m_socket_fd );
#else
			::close( m_socket_fd );
#endif
		}
		socket_t m_socket_fd;
};


#endif // NS_MESSAGE_PIPE_HPP_INCLUDED
