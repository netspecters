#ifndef KERNELCONSOLE_HPP_INCLUDED
#define KERNELCONSOLE_HPP_INCLUDED

#include <stdio.h>
#include <time.h>
#include "../NSDefine.h"

#ifdef ENABLE_PRINTK
	#define DATE_TIME_DEC\
		time_t rawtime;\
		struct tm * timeinfo;\
		char time_string[25];\
		time ( &rawtime );\
		timeinfo = localtime ( &rawtime );\
		strftime(time_string, 24, "%Y-%m-%d %T", timeinfo);\
		time_string[24] = '\0';

	#ifdef ERR_LOG_TO_FILE
		extern FILE *g_dbg_log_file_handle;
		#define STD_ERR g_dbg_log_file_handle
	#else
		#define STD_ERR stderr
	#endif

	#ifdef USE_FILE_LOG
		extern FILE *g_printk_log_file_handle;
		#define STD_OUT g_printk_log_file_handle
	#else
		#define STD_OUT stdout
	#endif

	#ifdef PRINT_DATE_TIME
		#define printk(fmt,arg...) \
			do{\
				DATE_TIME_DEC\
				fprintf(STD_OUT, "%s | ", time_string);\
				fprintf(STD_OUT, fmt, ##arg);\
			}while(0)
		#define dbg_log(level,fmt,arg...) \
			do{\
				DATE_TIME_DEC\
				fprintf(STD_ERR, "%s |>>[DL:%d Func:%s] ", time_string, level, __FUNCTION__);\
				fprintf(STD_ERR, fmt, ##arg);\
			}while(0)
	#else
		#define printk(fmt,arg...) fprintf(STD_OUT, fmt, ##arg)
		#define dbg_log(level, fmt, arg...)\
		do{\
			fprintf(STD_ERR, ">>[DL:%d Func:%s] ", level, __FUNCTION__);\
			fprintf(STD_ERR, fmt, ##arg);\
		}while(0)
	#endif

	#if DBG_LEVEL_SOL <=  CORE_DBG_LEVEL
		#define dbg_CORE_DBG_LEVEL( fmt,arg... )
		#define dbg_SYS_PLUGIN_DBG_LEVEL( fmt,arg... )
		#define dbg_EXT_PLUGIN_DBG_LEVEL( fmt,arg... )
		#define dbg_TEST_DBG_LEVEL( fmt,arg... )

	#elif DBG_LEVEL_SOL <= SYS_PLUGIN_DBG_LEVEL
		#define dbg_CORE_DBG_LEVEL(fmt,arg...) dbg_log(CORE_DBG_LEVEL, fmt, ##arg)
		#define dbg_SYS_PLUGIN_DBG_LEVEL( fmt,arg... )
		#define dbg_EXT_PLUGIN_DBG_LEVEL( fmt,arg... )
		#define dbg_TEST_DBG_LEVEL( fmt,arg... )

	#elif DBG_LEVEL_SOL <= EXT_PLUGIN_DBG_LEVEL
		#define dbg_CORE_DBG_LEVEL( fmt,arg...) dbg_log(CORE_DBG_LEVEL, fmt, ##arg)
		#define dbg_SYS_PLUGIN_DBG_LEVEL( fmt,arg... ) dbg_log(SYS_PLUGIN_DBG_LEVEL, fmt, ##arg)
		#define dbg_EXT_PLUGIN_DBG_LEVEL( fmt,arg... )
		#define dbg_TEST_DBG_LEVEL( fmt,arg... )

	#elif DBG_LEVEL_SOL <= dbg_TEST_DBG_LEVEL
		#define dbg_CORE_DBG_LEVEL(fmt,arg...) dbg_log(CORE_DBG_LEVEL, fmt, ##arg)
		#define dbg_SYS_PLUGIN_DBG_LEVEL( fmt,arg... ) dbg_log(SYS_PLUGIN_DBG_LEVEL, fmt, ##arg)
		#define dbg_EXT_PLUGIN_DBG_LEVEL( fmt,arg... ) dbg_log(EXT_PLUGIN_DBG_LEVEL, fmt, ##arg)
		#define dbg_TEST_DBG_LEVEL( fmt,arg... )

	#else
		#define dbg_CORE_DBG_LEVEL( fmt,arg... ) dbg_log(CORE_DBG_LEVEL, fmt, ##arg)
		#define dbg_SYS_PLUGIN_DBG_LEVEL( fmt,arg... ) dbg_log(SYS_PLUGIN_DBG_LEVEL, fmt, ##arg)
		#define dbg_EXT_PLUGIN_DBG_LEVEL( fmt,arg... ) dbg_log(EXT_PLUGIN_DBG_LEVEL, fmt, ##arg)
		#define dbg_TEST_DBG_LEVEL( fmt,arg... ) dbg_log(TEST_DBG_LEVEL, fmt, ##arg)
	#endif

	#define dbg(level, fmt, arg...) dbg_##level(fmt, ##arg)
#else
	#define printk(fmt,...)
	#define dbg(level,fmt,...)
	#endif

#define KINFORMATION	"[II] "
#define KERROR			"[EE] "
#define KWARNING		"[WW] "

#define print_func_infor\
	printk("%s: func_addr = %p, called by = %p", __FUNCTION__, __builtin_return_address)

#endif // KERNELCONSOLE_HPP_INCLUDED
