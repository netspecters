#ifndef CONVERT_UTF_UNICODE_HPP_INCLUDED
#define CONVERT_UTF_UNICODE_HPP_INCLUDED

#if defined(_WIN32) || defined(_WIN64)

#include <windows.h>

static int unicode_to_utf8( const wchar_t *from_unicode_str_ptr, char *to_utf8_str_ptr )
{
	int need_buffer_length = WideCharToMultiByte( CP_UTF8, NULL, from_unicode_str_ptr, -1, NULL, 0, NULL, FALSE );

	if( to_utf8_str_ptr )
		WideCharToMultiByte( CP_UTF8, NULL, from_unicode_str_ptr, -1, to_utf8_str_ptr, need_buffer_length, NULL, FALSE );

	return need_buffer_length;
}

static int utf8_to_unicode( const char *from_utf8_str_ptr, wchar_t *to_unicode_str_ptr )
{
	int need_buffer_length = MultiByteToWideChar( CP_UTF8, 0, from_utf8_str_ptr, -1, NULL, 0 );

	if( to_unicode_str_ptr )
		MultiByteToWideChar( CP_UTF8, 0, from_utf8_str_ptr, -1, to_unicode_str_ptr, need_buffer_length );

	return need_buffer_length;
}

#define DECL_UTF8_UNICODE( utf8_str, unicode_str_name )\
		wchar_t unicode_str_name[ utf8_to_unicode( utf8_str, NULL ) ];\
		utf8_to_unicode ( utf8_str, unicode_str_name )

#define DECL_UNICODE_UTF8( unicode_str, utf8_str_name )\
	char utf8_str_name[ unicode_to_utf8( unicode_str, NULL ) ];\
	unicode_to_utf8( unicode_str, utf8_str_name )

#endif

#endif // CONVERT_UTF_UNICODE_HPP_INCLUDED
