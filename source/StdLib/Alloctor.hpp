#ifndef NS_ALLOCTOR_HPP_INCLUDED
#define NS_ALLOCTOR_HPP_INCLUDED

#include <string.h>
#ifdef EX_TEST
#	include <gtest/gtest_prod.h>
#endif
#include "LibObject/MappedFile.hpp"

namespace Netspecters { namespace NSStdLib {

class CAlloctor : public CMappedFile
{
#define DEFAULT_FILE_SIZE	1024
#define SUPER_BLOCK_PTR (( ST_SuperBlock * )m_file_mem_ptr)
#define GET_TYPE_PTR(type,relative_addr) ((type*)(( uintptr_t )m_file_mem_ptr + relative_addr ))
#define BLOCK_HEADER_PTR(base) GET_TYPE_PTR( ST_BlockHeader, base )
#define GET_BLOCK_DATA_AREA(base) GET_TYPE_PTR( void, base + sizeof(ST_BlockHeader) )
#define BLOCK_HEADER_FROM_DATA_AREA(ptr) GET_TYPE_PTR(ST_BlockHeader, get_relative_address(ptr) - sizeof(ST_BlockHeader))

	public:
		typedef unsigned TRelativeAddress;
	protected:
		struct ST_ListItem
		{
			TRelativeAddress next, previous;
		};
		void add_to_list( TRelativeAddress &header, TRelativeAddress item )	//加入列表头
		{
			ST_ListItem *item_ptr = GET_TYPE_PTR( ST_ListItem, item );
			item_ptr->next = header;
			item_ptr->previous = 0;
			if( header )
				GET_TYPE_PTR( ST_ListItem, header )->previous = item;
			header = item;
		}
		void remove_from_list( TRelativeAddress &header, TRelativeAddress item )
		{
			ST_ListItem *item_ptr = GET_TYPE_PTR( ST_ListItem, item );
			if( item_ptr->next )
				GET_TYPE_PTR( ST_ListItem, item_ptr->next )->previous = item_ptr->previous;
			else if( item == header )	//在没有下一个空闲块并且此空闲块是空闲链上的最后一个块
				header = 0;
			if( item_ptr->previous )
				GET_TYPE_PTR( ST_ListItem, item_ptr->previous )->next = item_ptr->next;
			item_ptr->previous = item_ptr->next = 0;
		}

		struct ST_BlockHeader : public ST_ListItem
		{
			size_t block_size; //指明块的整体大小，包含块头和块未大小
		};
		inline TRelativeAddress get_space_base() {
			return sizeof( ST_SuperBlock );
		}

		inline TRelativeAddress get_relative_address( void *ptr ) {
			return ( TRelativeAddress )(( uintptr_t )ptr - ( uintptr_t )m_file_mem_ptr );
		}
		inline void *get_ptr( TRelativeAddress ra ) {
			return ra >= get_space_base() ? GET_TYPE_PTR( void, ra ) : NULL;
		}
	private:
		struct ST_SuperBlock
		{
			TRelativeAddress free_block_list;
			size_t total_block_size;				//包含ST_SuperBlock大小
		};
		struct ST_BlockTail
		{
			TRelativeAddress header;
		};

	public:
		typedef CMappedFile::ENM_FileState ENM_FileState;
		ENM_FileState load_file( const string &file_name, size_t default_size = DEFAULT_FILE_SIZE )
		{
			ENM_FileState load_state = CMappedFile::loadFile( file_name, default_size );
			if( fsOpenNew == load_state )
			{
				SUPER_BLOCK_PTR->free_block_list = sizeof( ST_SuperBlock );
				SUPER_BLOCK_PTR->total_block_size = getFileSize();
				init_block( SUPER_BLOCK_PTR->free_block_list, SUPER_BLOCK_PTR->total_block_size - sizeof( ST_SuperBlock ) );
			}
			return load_state;
		}

		void *malloc( size_t size )
		{
			if( 0 == SUPER_BLOCK_PTR->free_block_list )	/* 没有空间了，调整文件大小 */
				increase_file_size();

			size_t min_size = size + sizeof( ST_BlockHeader ) + sizeof( ST_BlockTail );	//最小块长
			TRelativeAddress alloced = SUPER_BLOCK_PTR->free_block_list;
			ST_BlockHeader *header_ptr = BLOCK_HEADER_PTR( alloced );
			/* 使用首次适应算法查找空闲块 */
			while( alloced )
			{
				if( header_ptr->block_size >= min_size )	//找到了一个满足最小块长度的空闲块
				{
					remove_block_from_free_list( alloced );	//先从空闲链表中移除然后再尝试分裂
					if(( header_ptr->block_size - min_size ) > sizeof( ST_BlockHeader ) + sizeof( ST_BlockTail ) )	//要分裂
					{
						size_t total_size = header_ptr->block_size;
						TRelativeAddress new_base = alloced + min_size;
						init_block( alloced, min_size );
						init_block( new_base, total_size - min_size );
						add_block_to_free_list( new_base );	//把分裂剩余块挂入空闲链表
					}
					break;
				}
				else
				{
					alloced = header_ptr->next;
					header_ptr = BLOCK_HEADER_PTR( alloced );
				}
			}

			/* 没有可用的空闲块了 */
			if( !alloced )
			{
				increase_file_size();
				return malloc( size );
			}
			else
				return GET_TYPE_PTR( void, alloced + sizeof( ST_BlockHeader ) );
		}
		void *realloc( void *space_ptr, size_t new_size, bool need_copy_old_data )
		{
			/*
				需要重新分配空间的数据块可能内部还有空间，但在此函数中不需要特别处理
				释放后会排在空闲链的第一位，malloc函数会首先检测此块，并极有可能重新使用此数据块，并不会浪费空间和过多时间
			*/
			size_t old_data_size = BLOCK_HEADER_FROM_DATA_AREA( space_ptr )->block_size - sizeof( ST_BlockHeader ) - sizeof( ST_BlockTail );
			free( space_ptr );
			void *new_space_ptr = malloc( new_size );
			/* 位置可能发生变化，根据需要拷贝原有数据 */
			if( need_copy_old_data && new_space_ptr != space_ptr )
				memmove( new_space_ptr, space_ptr, old_data_size );
			return new_space_ptr;
		}
		void free( void *space_ptr )
		{
			if( !space_ptr ) return;

			ST_BlockHeader *current_header_ptr = BLOCK_HEADER_FROM_DATA_AREA( space_ptr );
			TRelativeAddress current_header_offset = get_relative_address( current_header_ptr );
			bool already_combine = false;
			/* 尝试向后合并(必须先向后合并，而后再向前合并才能保证以最少的代码实现合并功能) */
			TRelativeAddress next = current_header_offset + current_header_ptr->block_size;
			if( next != SUPER_BLOCK_PTR->total_block_size && !is_block_in_use( next ) )	//没有越界并且下一块空闲
			{
				combine_block( current_header_offset, next );
				already_combine = true;
			}
			/* 尝试向前合并 */
			TRelativeAddress previous = GET_TYPE_PTR( ST_BlockTail, current_header_offset - sizeof( ST_BlockTail ) )->header;
			if( previous != sizeof( ST_SuperBlock ) && !is_block_in_use( previous ) )
			{
				combine_block( previous, current_header_offset );
				already_combine = true;
			}
			/* 没有能完成合并，直接将空闲块挂入空闲块列表中 */
			if( !already_combine )
				add_block_to_free_list( current_header_offset );
		}

	private:
		void init_block( TRelativeAddress base, size_t size )
		{
			ST_BlockHeader *header_ptr = BLOCK_HEADER_PTR( base );
			header_ptr->block_size = size;
			header_ptr->next = header_ptr->previous = 0;

			ST_BlockTail *tail_ptr = GET_TYPE_PTR( ST_BlockTail, ( base + size - sizeof( ST_BlockTail ) ) );
			tail_ptr->header = base;
		}
		inline bool is_block_in_use( TRelativeAddress base ) {
			ST_BlockHeader *header_ptr = BLOCK_HEADER_PTR( base );
			return ( header_ptr->next == header_ptr->previous && base != SUPER_BLOCK_PTR->free_block_list );
		}
		void combine_block( TRelativeAddress front, TRelativeAddress back )
		{
			ST_BlockHeader *front_header_ptr = BLOCK_HEADER_PTR( front );
			ST_BlockHeader *back_header_ptr = BLOCK_HEADER_PTR( back );
			/* 为了安全，先从空闲列表中移除被合并块 */
			if( front_header_ptr->next != front_header_ptr->previous || SUPER_BLOCK_PTR->free_block_list == front )
				remove_block_from_free_list( front );
			remove_block_from_free_list( back );
			/* 合并 */
			init_block( front, front_header_ptr->block_size + back_header_ptr->block_size );
			/* 将合并后的块添加到空闲列表中 */
			add_block_to_free_list( front );
		}
		void add_block_to_free_list( TRelativeAddress base ) {
			add_to_list( SUPER_BLOCK_PTR->free_block_list, base );
		}
		void remove_block_from_free_list( TRelativeAddress base ) {
			remove_from_list( SUPER_BLOCK_PTR->free_block_list, base );
		}
		void increase_file_size()
		{
			increaseFileSize( DEFAULT_FILE_SIZE );
			init_block( SUPER_BLOCK_PTR->total_block_size, DEFAULT_FILE_SIZE );
			SUPER_BLOCK_PTR->total_block_size += DEFAULT_FILE_SIZE;
		}
#undef DEFAULT_FILE_SIZE
#undef SUPER_BLOCK_PTR
#undef GET_TYPE_PTR
#undef BLOCK_HEADER_PTR
#undef GET_BLOCK_DATA_AREA
#undef BLOCK_HEADER_FROM_DATA_AREA

#ifdef EX_TEST
	private:/* only for test */
		FRIEND_TEST( NSStanderLibrary, Alloctor_LoadFile );
		FRIEND_TEST( NSStanderLibrary, Alloctor_Malloc );
		FRIEND_TEST( NSStanderLibrary, Alloctor_Free );
#endif
};

} /* NetLayer **/ } /* NSStdLib **/

#endif // NS_ALLOCTOR_HPP_INCLUDED
