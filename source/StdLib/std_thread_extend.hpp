#ifndef STD_THREAD_EXTEND_HPP_INCLUDED
#define STD_THREAD_EXTEND_HPP_INCLUDED

#include <thread>
#include <sys/select.h>

namespace std { namespace this_thread{
#ifndef _GLIBCXX_USE_NANOSLEEP
	/// sleep_until
    template<typename _Clock, typename _Duration>
	inline void sleep_until(const chrono::time_point<_Clock, _Duration>& __atime)
    { sleep_for(__atime - _Clock::now()); }

    /// sleep_for
    template<typename _Rep, typename _Period>
	inline void sleep_for(const chrono::duration<_Rep, _Period>& __rtime)
    {
		chrono::seconds __s = chrono::duration_cast<chrono::seconds>(__rtime);

		chrono::nanoseconds __ns = chrono::duration_cast<chrono::nanoseconds>(__rtime - __s);

		timespec __timeout =
		{
			static_cast<std::time_t>(__s.count()),
			static_cast<long>(__ns.count())
		};

		::pselect(0, NULL, NULL, NULL, &__timeout, NULL);
	}
#endif
}}

#endif // STD_THREAD_EXTEND_HPP_INCLUDED
