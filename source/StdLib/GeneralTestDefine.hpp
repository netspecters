#ifndef GENERALTESTDEFINE_HPP_INCLUDED
#define GENERALTESTDEFINE_HPP_INCLUDED#include "../KernelLibrary/DataContainer/Interfaces.h"#include "../KernelLibrary/ProtocolManager/Interfaces.h"
#include <gmock/gmock.h>struct __segment_mock : public Netspecters::KernelPlus::DataContainer::ISegment
{
	MOCK_METHOD4( read, NSAPI int( void *, size_t, off_t, long ) );
	MOCK_METHOD4( write, NSAPI int ( void *, size_t, off_t, long ) );

	MOCK_METHOD0( getSegmentOffset, NSAPI off_t() );
	MOCK_METHOD0( getIOPosition, NSAPI off_t() );
	MOCK_METHOD0( getSegmentSize, NSAPI off_t() );

	MOCK_METHOD0( flushData, NSAPI void () );
	MOCK_METHOD1( getSegmentInformation, NSAPI void ( ST_SegmentInfor & ) );
	MOCK_METHOD1( setDataWindowSize, NSAPI bool ( size_t ) );
};struct __data_container_mock : public Netspecters::KernelPlus::DataContainer::IDataContainer
{
	MOCK_METHOD0( getDataContainerName, NSAPI const char * () );

	MOCK_METHOD2( addSegment, NSAPI ISegment * ( ENM_OffsetType offset_type, ISegment::ST_SegmentInfor & ) );	MOCK_METHOD0( getSegmentNum, NSAPI unsigned () );
	MOCK_METHOD2( tryMergeSegment, NSAPI bool ( ISegment *, ENM_MergeStyle ) );
	MOCK_METHOD2( moveSegment, NSAPI off_t( ISegment *op_segment_ptr, off_t new_base_position ) );
	MOCK_METHOD1( deleteSegment, NSAPI void ( ISegment * ) );

	MOCK_METHOD0( getDataSize, NSAPI off_t() );
	MOCK_METHOD1( resetDataSize, NSAPI off_t( off_t ) );
	MOCK_METHOD0( writeBack, NSAPI void () );

	MOCK_METHOD2( close, NSAPI void ( bool, bool ) );
};struct __task_group_manager_mock : public Netspecters::KernelPlus::ProtocolManager::ITaskGroupManager
{
	MOCK_METHOD3( addGroup, NSAPI GROUP_HANDLE( const char *, bool, uintptr_t ) );
	MOCK_METHOD2( delGroup, NSAPI void ( GROUP_HANDLE, bool ) );
	MOCK_METHOD1( getGroupHandleList, NSAPI size_t ( GROUP_HANDLE * ) );
	MOCK_METHOD1( getGroupID, NSAPI uint32_t ( GROUP_HANDLE ) );
	MOCK_METHOD2( getGroupStatistics, NSAPI size_t ( GROUP_HANDLE, ST_GroupInformation * ) );
	MOCK_METHOD2( getGroupDetails, NSAPI size_t ( GROUP_HANDLE , void * ) );
	MOCK_METHOD2( addProtocolTaskToGroup, NSAPI GROUP_TASK_HANDLE( GROUP_HANDLE, const IProtocol::ST_TaskInitParam & ) );
	MOCK_METHOD1( removeProtocolTask, NSAPI void ( GROUP_TASK_HANDLE ) );
	MOCK_METHOD2( switchGroupRunningState, NSAPI void ( GROUP_HANDLE, bool ) );
	MOCK_METHOD2( modifyGroupState, NSAPI ENM_GroupState( GROUP_HANDLE, ENM_GroupState ) );
	MOCK_METHOD2( getTaskStopCodes, NSAPI size_t ( GROUP_HANDLE , IProtocol::ENM_TaskStopCode * ) );
	MOCK_METHOD2( storeTaskStateData, NSAPI int ( GROUP_TASK_HANDLE, IProtocol::ST_TaskStateData * ) );
	MOCK_METHOD3( openSegment, NSAPI int ( GROUP_TASK_HANDLE, ISegment::ST_SegmentInfor::ENM_SegmentType, ISegment * & ) );
	MOCK_METHOD2( closeSegment, NSAPI void ( GROUP_TASK_HANDLE, ISegment * ) );
};struct __protocol_mock : public Netspecters::KernelPlus::ProtocolManager::IProtocol
{
	MOCK_METHOD4( onLoad, NSAPI int ( IPlusTree *plus_tree_ptr, INSPlus *parent_plus_ptr, HDIR plus_dir_handle, int default_param ) );
	MOCK_METHOD0( onUnload, NSAPI void () );
	MOCK_METHOD2( getParam, NSAPI int ( const char *param_name, void *value_buffer_ptr ) );
	MOCK_METHOD2( setParam, NSAPI void * ( const char *param_name, void *value_ptr ) );

	MOCK_METHOD4( startTask, NSAPI TASK_HANDLE( const ST_TaskInitParam &, GROUP_TASK_HANDLE, IDataContainer *, ITaskMonitor * ) );
	MOCK_METHOD1( stopTask, NSAPI void ( TASK_HANDLE ) );
	MOCK_METHOD1_WITH_CALLTYPE( NSAPI, getStopCode, ENM_TaskStopCode( TASK_HANDLE task_handle ) );
	MOCK_METHOD2( queryTaskInformation, NSAPI int ( TASK_HANDLE task_handle, ST_TaskInformation *task_infor_ptr ) );
	MOCK_METHOD1( delTask, NSAPI void ( TASK_HANDLE ) );
};

#endif // GENERALTESTDEFINE_HPP_INCLUDED
