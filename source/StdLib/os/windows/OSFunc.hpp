#include <windows.h>

#include <cstring>
#include <stdint.h>
#include <sys/types.h>

#define ROUND_SIZE( addr, size ) \
	(((UINT)(size) + ((UINT_PTR)(addr) & page_mask) + page_mask) & ~page_mask)

namespace Netspecters{ namespace NSStdLib{ namespace OSFunc{

	typedef HANDLE native_handle_type;
	static const size_t OS_MEM_PAGE_SIZE = 4096;

	inline static int get_errno(void)
	{
		return GetLastError();
	}

	inline static __attribute__ ((malloc)) void *getMemPageBySize(void *base_address_ptr = NULL, size_t size = OS_MEM_PAGE_SIZE)
	{
		return VirtualAlloc( base_address_ptr, size, MEM_COMMIT, PAGE_READWRITE );
	}

	inline static void freeMemPageBySize( void *base_address_ptr, size_t size )
	{
		VirtualFree( base_address_ptr, size, MEM_DECOMMIT);
	}

	inline static void *getMemPage(void *base_address_ptr = NULL, unsigned int page_num = 1)
	{
		return getMemPageBySize( base_address_ptr, page_num * OS_MEM_PAGE_SIZE );
	}

	inline static void freeMemPage(void *base_address_ptr, unsigned int page_num = 1)
	{
		freeMemPageBySize( base_address_ptr,  page_num * OS_MEM_PAGE_SIZE);
	}

	inline static HANDLE openFile( const wchar_t *file_name_str_ptr, bool create_always, int extend_param = 0 )
	{
		extend_param |= FILE_ATTRIBUTE_NORMAL;
		return CreateFileW ( file_name_str_ptr, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, create_always ? CREATE_ALWAYS : OPEN_ALWAYS, extend_param, 0 );
	}

	static void *mapFile( HANDLE file_handle, off_t range_low, off_t range_high )
	{
		range_low = range_low > 0 ?: 0;
		range_high = range_high > 0 ?: 0;
		void *mem_pointer = NULL;

		HANDLE map_file_handle = CreateFileMapping ( file_handle, NULL, PAGE_READWRITE, 0, range_high, NULL );
		if ( map_file_handle > 0 )
		{
			mem_pointer = MapViewOfFile ( map_file_handle, FILE_MAP_ALL_ACCESS, 0, range_low, range_high );
			CloseHandle( map_file_handle );
		}
		return mem_pointer;
	}

	inline static void unmapFile( void *file_mem_pointer, off_t unmap_size )
	{
		(void) unmap_size;	//windows下用不到此参数
		UnmapViewOfFile ( file_mem_pointer );
	}

	inline static void closeFile( HANDLE file_handle )
	{
		CloseHandle( file_handle );
	}

	static void querySysErrorMsg( long error_id, wchar_t *&msg_buffer_ptr, int msg_buffer_size )
	{
		DWORD flage = FORMAT_MESSAGE_FROM_SYSTEM;
		if( msg_buffer_ptr == NULL )
		{
			flage |= FORMAT_MESSAGE_ALLOCATE_BUFFER;
			msg_buffer_size = 0;
		}
		else if(msg_buffer_size == 0)
		{
			LocalFree(reinterpret_cast<HLOCAL>(msg_buffer_ptr));
			return;
		}
		FormatMessageW(flage, NULL, error_id, 0, reinterpret_cast<wchar_t*>(&msg_buffer_ptr), msg_buffer_size, NULL);
	}

	static unsigned getCPUNum(void)
	{
		SYSTEM_INFO sys_infor;
		GetSystemInfo ( &sys_infor );
		return sys_infor.dwNumberOfProcessors;
	}

	inline static void *getPageBaseAddress( const void *current_address )
	{
#ifdef __i386__
		///@@ 32位cpu转换方法
		//4k页面的os，高20位为页号（与运算后就是页首址）
		return reinterpret_cast<void*>(reinterpret_cast<uint32_t>(current_address) & 0xFFFFF000);
#else
#error __x86_64__ method has no implemented
#endif
	}

} /** OSFunc **/ } /** NSStdLib **/ } /** Netspecters **/
