#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <dirent.h>

#include <cstring>
#include <stdint.h>

#define ROUND_SIZE( addr, size ) \
	(((UINT)(size) + ((UINT_PTR)(addr) & page_mask) + page_mask) & ~page_mask)

namespace Netspecters{ namespace NSStdLib{ namespace OSFunc{

	typedef int native_handle_type;
	static const size_t OS_MEM_PAGE_SIZE = 4096;

	inline static int get_errno(void)
	{
		return errno;
	}

	inline static __attribute__((alloc_size(2))) void *getMemPageBySize(void *base_address_ptr = NULL, size_t size = OS_MEM_PAGE_SIZE)
	{
		return mmap(base_address_ptr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	}

	inline static void freeMemPageBySize( void *base_address_ptr, size_t size )
	{
		munmap(base_address_ptr, size);
	}

	/* 按页分配 */
	inline static void *getMemPage(void *base_address_ptr = NULL, unsigned int page_num = 1)
	{
		return getMemPageBySize( base_address_ptr, page_num * OS_MEM_PAGE_SIZE );
	}

	/* 按页释放 */
	inline static void freeMemPage(void *base_address_ptr, unsigned int page_num = 1)
	{
		freeMemPageBySize( base_address_ptr,  page_num * OS_MEM_PAGE_SIZE);
	}

	/* 以读写方式打开文件 */
	inline static int openFile( const char *file_name_str_ptr, bool create_always, int extend_param = 0 )
	{
		extend_param |= O_RDWR | O_CREAT;
		if( create_always )
			extend_param |= O_TRUNC;
		return open ( file_name_str_ptr, extend_param, S_IRUSR | S_IWUSR | S_IRGRP );
	}

	inline static void *mapFile( int file_handle, off_t range_low, off_t range_high )
	{
		range_low = range_low > 0 ?: 0;
		range_high = range_high > 0 ?: 0;
		return mmap ( NULL, range_high - range_low, PROT_READ | PROT_WRITE, MAP_SHARED, file_handle, range_low );
	}

	inline static void unmapFile( void *file_mem_pointer, off_t unmap_size )
	{
		munmap ( file_mem_pointer, unmap_size );
	}

	inline static void closeFile( int file_handle )
	{
		close ( file_handle );
	}

	static __attribute__((unused)) void querySysErrorMsg ( long error_id, char *&msg_buffer_ptr, int msg_buffer_size )
	{
		if( msg_buffer_ptr == NULL )
			msg_buffer_ptr = strerror(error_id);
		else if(msg_buffer_size>0)
			strerror_r(error_id, msg_buffer_ptr, msg_buffer_size);
	}

	static __attribute__((unused)) unsigned getCPUNum(void)
	{
		return sysconf ( _SC_NPROCESSORS_ONLN );
	}

	inline static void *getPageBaseAddress( const void *current_address )
	{
#ifdef __i386__
		///@@ 32位cpu转换方法
		//4k页面的os，高20位为页号（与运算后就是页首址）
		return reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(current_address) & 0xFFFFF000);
#else
#	error __x86_64__ method has not implemented
#endif
	}

} /** OSFunc **/ } /** NSStdLib **/ } /** Netspecters **/
