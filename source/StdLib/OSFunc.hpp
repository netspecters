#ifndef  osfunc_INC
#define  osfunc_INC

#if defined(_WIN32) || defined(_WIN64)
	#include "os/windows/OSFunc.hpp"
#elif defined(__linux)
	#include "os/linux/OSFunc.hpp"
#else
	#error "NOT IMPLEMENT"
#endif

#endif   /* ----- #ifndef osfunc_INC  ----- */
