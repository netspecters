#ifndef CONTAINERLOCK_HPP_INCLUDED
#define CONTAINERLOCK_HPP_INCLUDED#include <mutex>

namespace Netspecters { namespace NSStdLib {

///为标准容器加锁
template<class TContainer>
class CContainerLock
{
	public:
		TContainer &lock()
		{
			m_mutex.lock();
			return m_container;
		}
		void unlock()
		{
			m_mutex.unlock();
		}

	private:		std::mutex m_mutex;
		TContainer m_container;

};

}/* NSStdLib **/ }/* Netspecters **/

#endif // CONTAINERLOCK_HPP_INCLUDED
