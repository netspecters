#ifndef  userlevelthread_INC
#define  userlevelthread_INC

#if defined(_WIN32) || defined(_WIN64)
#	include "os/windows/ucontext.h"
#else
#	include <ucontext.h>
#endif
#include <list>
#include "TinyThread.hpp"

#define DEFAULT_UL_THREAD_STACK_SIZE 4096

class CUserLevelThread;
/**
 * @brief 用户级线程调度器
 * @note 本调度器为协作式。由于windows下实现中断困难，故无法实现跨平台的抢占式调度器
 */
class CUserLevelThreadScheduler : private CTinyThread
{
	friend class CUserLevelThread;

	private:
		void addThread(CUserLevelThread *thread_ptr)
		{
			m_active_list.push_back(thread_ptr);
			if(isSuspend())
				resume();
		}
		void removeThread(CUserLevelThread *thread_ptr)
		{
			m_active_list.remove(thread_ptr);
		}

		void onExecute(void)
		{
			while(!isTerminate())
			{
				//首先执行active列表中的thread
				for(auto itr = m_active_list.begin(); itr != m_active_list.end())
					if( (*itr)->m_status == CUserLevelThread::sActive )
					{
						m_current_running_thread_ptr = itr;
						swapcontext(&m_ucontext, &m_current_running_thread_ptr->m_ucontext);	//切换到线程
					}
			}
		}
		void switch_to_scheduler(void)
		{
			swapcontext(&m_current_running_thread_ptr->m_ucontext, &m_ucontext);	//切换到的调度器
		}

		std::list<CUserLevelThread*> m_active_list;
		CUserLevelThread *m_current_running_thread_ptr;
		ucontext_t m_ucontext;
};
CUserLevelThreadScheduler g_scheduler;

class CUserLevelThread
{
	friend class CUserLevelThreadScheduler;

	public:
		enum ENM_Status
		{
			sActive, sSleep, sDead, sSuspend
		};

		CUserLevelThread(size_t stack_size = DEFAULT_UL_THREAD_STACK_SIZE, CUserLevelThreadScheduler *scheduler_ptr = &g_scheduler)
			: m_scheduler_ptr(scheduler_ptr)
		{
			m_stack_ptr = new char[stack_size];

			getcontext(&m_ucontext);
			m_ucontext.uc_stack.ss_sp = stack;
			m_ucontext.uc_stack.ss_size = ssize;
			makecontext(&m_ucontext, CUserLevelThread::routine, 1, this);

			scheduler_ptr->addThread(this);
		}
		~CUserLevelThread(void)
		{
			delete[] m_stack_ptr;
		}
		bool isTerminate(void)
		{
			return m_status == sDead;
		}
		bool isSuspend(void)
		{
			return m_status == sSuspend;
		}
		void terminateThread( void )
		{
			;
		}

	protected:
		/** 
		 * @brief 实现协作式调度
		 * @note 只能线程自己调用本函数
		 * @note 所有使用本用户线程的子类都需要在适当的时候调用本函数，以便调度器能够运行
		 */
		void yield(void)
		{
			m_scheduler_ptr->switch_to_scheduler();
		}

		virtual void onExecute( void ) = 0;

	private:
		ucontext_t m_ucontext;
		char *m_stack_ptr;
		ENM_Status m_status;
		CUserLevelThreadScheduler *m_scheduler_ptr;
		
		static void routine( CUserLevelThread *thread_ptr )
		{
			thread_ptr->onExecute();
		}
};

#endif   /* ----- #ifndef userlevelthread_INC  ----- */
