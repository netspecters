#include <gtest/gtest.h>
#include "nslib.h"

using namespace Netspecters::NSStdLib;
using namespace OSFunc;

static const uint32_t DEMO_TAG = 0x55AA55AA;

class CTestObject
{
	public:
		char fake_data[8];
		bool availd;
};

TEST( NSStanderLibrary, StdLib_ObjectPool )
{
	CObjectPool<CTestObject> objects;

#define Obj_Per_Page ((OS_MEM_PAGE_SIZE-sizeof(void*)) / sizeof(CTestObject))
	CTestObject *object_ptr[Obj_Per_Page*5];	//

	int i = 0;
	//先分配
	for( ; i < Obj_Per_Page * 3; i++ )
	{
		object_ptr[i] = objects.allocObject();
		object_ptr[i]->availd = true;
		EXPECT_NE( 0, ( uintptr_t )( object_ptr[i] ) );
	}

	//释放一部分
	for( int j = 0; j < Obj_Per_Page * 2; j++ )
	{
		objects.freeObject( object_ptr[j] );
	}

	//再分配一部分
	for( ; i < Obj_Per_Page * 5; i++ )
	{
		object_ptr[i] = objects.allocObject();
		object_ptr[i]->availd = true;
		EXPECT_NE( 0, ( uintptr_t )( object_ptr[i] ) );
	}

	//遍历所有分配的节点
	int count = 0;
	for( auto itr = objects.begin(); itr != objects.end(); ++itr )
	{
		EXPECT_TRUE( itr->availd );
		count++;
	}
	EXPECT_EQ( count, Obj_Per_Page * 3 );
#undef Obj_Per_Page
}

TEST( NSStanderLibrary, StdLib_DynArray )
{
	CDynArray<int> dyn_array( 25 );
	for( int i = 0; i < 25; i++ )
		dyn_array[i] = i;

	EXPECT_FALSE( dyn_array.empty() );

	int j = 0;
	for( auto itr = dyn_array.begin(); itr != dyn_array.end(); ++itr )
		EXPECT_EQ( j++, *itr );
}

class CThreadListTestObj
{
	public:
		char fake_data[8];
		int index;
		uint32_t tag;
		CThreadListTestObj( void )
		{
			tag = DEMO_TAG;
		}
};

TEST( NSStanderLibrary, StdLib_ThreadedList )
{
	//添加
	CThreadedList<CThreadListTestObj> threaded_list;
	for( int i = 0; i < 50; i++ )
	{
		auto item_itr = threaded_list.allocItem();
		EXPECT_EQ( DEMO_TAG, item_itr->tag );
		item_itr->index = i;
		threaded_list.push_back( item_itr );
	}
	EXPECT_EQ( 50, threaded_list.size() );

	//测试
	int j = 0;
	for( auto itr = threaded_list.begin(); itr != threaded_list.end(); ++itr )
		EXPECT_EQ( j++, itr->index );

	//删除
	for( auto itr = threaded_list.begin(); itr != threaded_list.end(); ++itr )
		threaded_list.erase( itr );
	EXPECT_EQ( 0, threaded_list.size() );
}

TEST( NSStanderLibrary, StdLib_SimpleBitSet )
{
	CSimpleBitset<60> bit_set;

	for( int i = 0; i < 60; i++ )
		bit_set.set( i );
	for( int i = 0; i < 60; i++ )
		EXPECT_TRUE( bit_set.test( i ) );

	bit_set.reset( 43 );
	EXPECT_EQ( 43, bit_set.first_false() );

	bit_set.clear();
	bit_set.set( 33 );
	EXPECT_EQ( 1, bit_set.count() );
	EXPECT_EQ( 33, bit_set.last_true() );
}

TEST( NSStanderLibrary, StdLib_SimpleArray )
{
	CSimpleArray<int> simple_array;

	//test init state
	EXPECT_TRUE( simple_array.empty() );

	//test push function
	for( int i = 0; i < 67; i++ )
		simple_array.push_back( i );
	EXPECT_EQ( 67, simple_array.size() );

	//test iterator
	int j = 0;
	for( auto itr = simple_array.begin(); itr != simple_array.end(); ++itr )
		EXPECT_EQ( j++, *itr );

	//test clear
	simple_array.clear();
	EXPECT_TRUE( simple_array.empty() );
}
