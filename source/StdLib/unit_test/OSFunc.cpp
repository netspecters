#include <gtest/gtest.h>
#include "../OSFunc.hpp"

using namespace Netspecters::NSStdLib::OSFunc;

TEST( NSStanderLibrary, OSFunc_getMemPageBySize )
{
	/* mass test */
	for( int i = 1; i < 10; i++ )
	{
		void *alloced_ptr = getMemPageBySize( NULL, OS_MEM_PAGE_SIZE * i );
		EXPECT_NE( 0, ( uintptr_t ) alloced_ptr );
		freeMemPageBySize( alloced_ptr, OS_MEM_PAGE_SIZE * i );
	}
}

TEST( NSStanderLibrary, OSFunc_getMemPage )
{
	/* mass test */
	for( int i = 0; i < 10; i++ )
	{
		void *alloced_ptr = getMemPage( NULL, i );
		EXPECT_NE( 0, ( uintptr_t ) alloced_ptr );
		freeMemPage( alloced_ptr, i );
	}
}

TEST( NSStanderLibrary, OSFunc_querySysErrorMsg )
{
	char *msg_buffer_ptr = NULL;
	querySysErrorMsg( 0, msg_buffer_ptr, 0 );
}

TEST( NSStanderLibrary, OSFunc_getPageBaseAddress )
{
	EXPECT_EQ( 0x11223344 & 0xFFFFF000, ( uintptr_t ) getPageBaseAddress(( void * ) 0x11223344 ) );
}
