#include <gtest/gtest.h>
#include <stdio.h>
#include "../Alloctor.hpp"

namespace Netspecters {
namespace NSStdLib {

#define DB_FILENAME_BASE "nslib_alloctor_test.tmp"
#ifdef linux
#	define DB_FILENAME "/tmp/"DB_FILENAME_BASE
#else
#	define DB_FILENAME DB_FILENAME_BASE
#endif

class __alloctor_helper
{
	public:
		__alloctor_helper()
		{
			while( 1 )
			{
				/* 加载数据库文件 */
				CAlloctor::ENM_FileState file_state = alloctor.load_file( DB_FILENAME );
				EXPECT_EQ( CAlloctor::fsOpenNew, file_state );	/* 此前，数据库文件不应该存在 */
				if( CAlloctor::fsOpenNew != file_state )
					remove( DB_FILENAME );
				else
					break;
			}
		}
		~__alloctor_helper()
		{
			/* 删除数据块文件 */
			remove( DB_FILENAME );
		}

		CAlloctor alloctor;
};

static bool enable_continue = true;

#define TEST_BEGIN()\
	ASSERT_TRUE( enable_continue );\
	__alloctor_helper alloctor_helper;\
	CAlloctor &alloctor = alloctor_helper.alloctor;\
	enable_continue = false
#define TEST_END()\
	enable_continue = true
#define INDEX_INFORMATION " with i = (" << i << ")"
#define SUPER_BLOCK_PTR ((CAlloctor::ST_SuperBlock*)(alloctor.m_file_mem_ptr))
#define TEST_DATA_SIZE	64

TEST( NSStanderLibrary, Alloctor_LoadFile )
{
	TEST_BEGIN();

	CAlloctor::ST_SuperBlock *super_block_ptr = ( CAlloctor::ST_SuperBlock * )( alloctor.m_file_mem_ptr );
	CAlloctor::ST_BlockHeader *first_block_ptr = ( CAlloctor::ST_BlockHeader * )( alloctor.get_ptr(super_block_ptr->free_block_list ) );

	/* 测试文件头的字段是否设置正确 */
	ASSERT_EQ( sizeof( CAlloctor::ST_SuperBlock ), super_block_ptr->free_block_list );
	ASSERT_EQ( alloctor.getFileSize(), super_block_ptr->total_block_size );
	ASSERT_EQ( 0, first_block_ptr->next );
	ASSERT_EQ( 0, first_block_ptr->previous );
	ASSERT_EQ( alloctor.getFileSize() - sizeof( CAlloctor::ST_SuperBlock ), first_block_ptr->block_size );

	TEST_END();
}

TEST( NSStanderLibrary, Alloctor_Malloc )
{
#define MIN_SIZE sizeof(CAlloctor::ST_BlockHeader)+sizeof(CAlloctor::ST_BlockTail)
	TEST_BEGIN();

	/* 测试分配，分配三次 */
	void *alloc_array[3];
	for( int i = 0; i < 3; i++ )
	{
		/* 保存应该分配地址 */
		void *need_address = ( void * )(( uintptr_t )alloctor.m_file_mem_ptr + SUPER_BLOCK_PTR->free_block_list + sizeof( CAlloctor::ST_BlockHeader ) );

		alloc_array[i] = alloctor.malloc( TEST_DATA_SIZE );
		CAlloctor::TRelativeAddress alloc_base = ( uintptr_t )alloc_array[i] - sizeof( CAlloctor::ST_BlockHeader );

		/* 检验分配的结构是否正确 */
		ASSERT_EQ( need_address, alloc_array[i] );
		CAlloctor::ST_BlockHeader *header_ptr = ( CAlloctor::ST_BlockHeader * )alloc_base;
		ASSERT_EQ( MIN_SIZE + TEST_DATA_SIZE, header_ptr->block_size );
		ASSERT_EQ( 0, header_ptr->next );
		ASSERT_EQ( 0, header_ptr->previous );

		/* 分配数据块后，检查 CAlloctor 实例的成员 */
		ASSERT_EQ( sizeof( CAlloctor::ST_SuperBlock ) + ( i + 1 ) * ( MIN_SIZE + TEST_DATA_SIZE ), SUPER_BLOCK_PTR->free_block_list ) << INDEX_INFORMATION;
	}

	TEST_END();
}

TEST( NSStanderLibrary, Alloctor_Free )
{
	TEST_BEGIN();

	/* 测试分配，分配三次 */
	void *alloc_array[3];
	long alloc_base[4];
	for( int i = 0; i < 3; i++ )
	{
		alloc_array[i] = alloctor.malloc( TEST_DATA_SIZE ) - ( uintptr_t )alloctor.m_file_mem_ptr;
		alloc_base[i] = ( uintptr_t )alloc_array[i] - sizeof( CAlloctor::ST_BlockHeader );
	}
	alloc_base[3] = SUPER_BLOCK_PTR->free_block_list;

#define GET_HANDER_PTR(x) (( CAlloctor::ST_BlockHeader * )alloctor.get_ptr(x))
	/* 释放分配的数据块(不按分配顺序) */
	/* 首先，释放1号块。由于缺少2号块，用于测试缺项释放是否正确 */
	alloctor.free( ( uintptr_t )alloctor.m_file_mem_ptr + alloc_array[1] );
	ASSERT_EQ( alloc_base[1] , SUPER_BLOCK_PTR->free_block_list );
	CAlloctor::ST_BlockHeader *header_ptr = GET_HANDER_PTR(alloc_base[1]);
	ASSERT_EQ( alloc_base[3], header_ptr->next );	/* 第一块是否指向第三块 */
	ASSERT_EQ( alloc_base[1], GET_HANDER_PTR(alloc_base[3])->previous ); /* 第三块是否指向第一块 */

	/* 其次，释放2号块。此时，1、2、3号块依次排列，用于测试相邻项合并功能是否正确 */
	alloctor.free( ( uintptr_t )alloctor.m_file_mem_ptr + alloc_array[2]  );
	ASSERT_EQ( alloc_base[1] , SUPER_BLOCK_PTR->free_block_list );
	header_ptr = GET_HANDER_PTR(alloc_base[1]);
	ASSERT_EQ( 0, header_ptr->next );
	ASSERT_EQ( 0, header_ptr->previous );

	/* 最后，释放0号块。此后，应该回到初始状态 */
	alloctor.free( (uintptr_t )alloctor.m_file_mem_ptr + alloc_array[0] );
	ASSERT_EQ( sizeof( CAlloctor::ST_SuperBlock ), SUPER_BLOCK_PTR->free_block_list );
	ASSERT_EQ(GET_HANDER_PTR( SUPER_BLOCK_PTR->free_block_list )->block_size, SUPER_BLOCK_PTR->total_block_size - sizeof( CAlloctor::ST_SuperBlock ) );

	TEST_END();
}

} /* NetLayer **/
} /* NSStdLib **/
