#include <gtest/gtest.h>
#include "../NSFC.h"

TEST(NSStanderLibrary, NSFC_double2int)
{
	EXPECT_EQ(0,double2int(0.0));
	EXPECT_EQ(0,double2int(0.1));
	EXPECT_EQ(0,double2int(0.5));
	EXPECT_EQ(1,double2int(0.6));
	EXPECT_EQ(1,double2int(0.9));

	EXPECT_EQ(100,double2int(100.0));
	EXPECT_EQ(100,double2int(100.1));
	EXPECT_EQ(100,double2int(100.5));
	EXPECT_EQ(101,double2int(100.6));
	EXPECT_EQ(101,double2int(100.9));
}

TEST(NSStanderLibrary, NSFC_getRand)
{
	int last = getRand();
	for(int i=0; i<100; i++)
	{
		int now = getRand();
		EXPECT_NE(last, now);
		last = now;
	}
}
