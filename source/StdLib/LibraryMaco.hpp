#ifndef __stdcall
#	define __stdcall __attribute__((stdcall))
#endif

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
#	ifdef BUILDING_DLL
#		ifdef __GNUC__
#			define DLL_PUBLIC __attribute__((dllexport))
#		else
#			define DLL_PUBLIC __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#		endif
#	else
#		if defined __GNUC__
#			define DLL_PUBLIC __attribute__((dllimport))
#		else
#			define DLL_PUBLIC __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#		endif
#		define DLL_LOCAL
#	endif
#	define ns_stdcall __stdcall
#else
#	define __fastcall	__attribute__((fastcall))	//__attribute__((regparm(3)))
#	if __GNUC__ >= 4
#		define DLL_PUBLIC __attribute__ ((visibility("default")))
#		define DLL_LOCAL  __attribute__ ((visibility("hidden")))
#	else
#		define DLL_PUBLIC
#		define DLL_LOCAL
#	endif
#	define ns_stdcall
#endif

#define NSAPI ns_stdcall
