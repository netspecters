#ifndef NSLIB_PIPE_HPP_INCLUDED
#define NSLIB_PIPE_HPP_INCLUDED

#if defined(_WIN32) || defined(_WIN64)
#	include <windows.h>
#	include "convert_utf_unicode.hpp"
#else
#	include <sys/types.h>
#	include <sys/stat.h>
#	include <fcntl.h>
#	include <unistd.h>
#endif
#include <system_error>
#include <iostream>
#include <string>
//#include <boost/iostreams/stream.hpp>  //boost::iostreams::stream
//#include <boost/iostreams/categories.hpp>  // 各种标记

#define PIPE_REJECT_REMOTE_CLIENTS 0x00000008

namespace Netspecters { namespace NSStdLib {
class CPipeStream
{
	public:
		CPipeStream( const char *pipe_name, bool own_the_pipe = true )
			: m_pipe_fd( 0 )
#if defined linux
			, m_own_the_pipe( own_the_pipe )
			, m_pipe_name( pipe_name )
#endif
		{
#if defined _WIN32 || defined _WIN64
			m_pipe_name.assign( "\\\\.\\pipe\\" );
			m_pipe_name.append( pipe_name );
			pipe_name = m_pipe_name.c_str();
			DECL_UTF8_UNICODE(pipe_name, pipe_name_w);

			if( own_the_pipe )	/* 拥有管道一端为服务器 */
				m_pipe_fd = CreateNamedPipe( pipe_name_w, PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS, PIPE_UNLIMITED_INSTANCES, 512, 512, 0, NULL );
			else
				m_pipe_fd = CreateFile( pipe_name_w, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL );
			if( INVALID_HANDLE_VALUE == m_pipe_fd )
				throw std::system_error( GetLastError(), std::system_category() );
#else
			/* 尝试建立本地管道 */
			if( own_the_pipe && ( mkfifo( pipe_name, 0777 ) < 0 ) && ( errno != EEXIST ) )
				throw std::system_error( errno, std::system_category() );

			m_pipe_fd = open( pipe_name, O_RDWR );
#endif
		}
		~CPipeStream()
		{
#if defined _WIN32 || defined _WIN64
			CloseHandle( m_pipe_fd );
#elif defined linux
			if( m_own_the_pipe )	/* 删除pipe */
				unlink( m_pipe_name.c_str() );
			close( m_pipe_fd );
#endif
		}

		static unsigned get_pid()
		{
#if defined _WIN32 || defined _WIN64
			return GetCurrentProcessId();
#elif defined linux
			return getpid();
#endif
		}

		unsigned read( char *s, unsigned n )
		{
#if defined _WIN32 || defined _WIN64
			/* 确认在服务器端已经有客户连接上了 */
			affirm_instance_ok();

			DWORD  number_of_bytes_op = 0;
			ReadFile( m_pipe_fd, s, n, &number_of_bytes_op, NULL );
			return number_of_bytes_op;
#elif defined linux
			return ::read( m_pipe_fd, s, n );
#endif
		}

		unsigned write( const char *s, unsigned n )
		{
#if defined _WIN32 || defined _WIN64
			/* 确认在服务器端已经有客户连接上了 */
			affirm_instance_ok();

			DWORD  number_of_bytes_op = 0;
			WriteFile( m_pipe_fd, s, n, &number_of_bytes_op, NULL );
			return number_of_bytes_op;
#elif defined linux
			return ::write( m_pipe_fd, s, n );
#endif
		}

		const std::string &get_pipe_name() {
			return m_pipe_name;
		}

	private:
#if defined(_WIN32) || defined(_WIN64)
		void affirm_instance_ok()
		{
			DWORD flages, instance_num;
			GetNamedPipeInfo( m_pipe_fd, &flages, NULL, NULL, NULL );
			GetNamedPipeHandleState( m_pipe_fd, NULL, &instance_num, NULL, NULL, NULL, 0 );
			if(( PIPE_SERVER_END == flages ) && ( 1 == instance_num ) )
				ConnectNamedPipe( m_pipe_fd, NULL );
		}

		HANDLE m_pipe_fd;
#else
		int m_pipe_fd;
		bool m_own_the_pipe;
#endif
		std::string m_pipe_name;
};
} /* NSStdLib */ } /* Netspecters */

#endif // NSLIB_PIPE_HPP_INCLUDED
