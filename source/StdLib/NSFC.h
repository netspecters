///@file __FILE__

#ifndef  nsfc_INC
#define  nsfc_INC

#include <cstdlib>
#include <ctime>
#include <string>

#define UNSAFE_CODE_BEGIN\
	try

#define UNSAFE_CODE_END(return_exp)\
	catch(...)\
	{\
		return_exp;\
	}

/* 小数部位<=5被舍去 */
static int  __attribute__((unused)) double2int ( double d )
{
	union {
		double l_d;
		int l_l;
	} magic_cast;
	magic_cast.l_d = d + 6755399441055744.0;
	return magic_cast.l_l;
}

static int __attribute__((unused)) getRand ( void )
{
	return rand();
}

static inline void __attribute__((unused)) charToHexStr ( std::string &hex_str, uint8_t xchar )
{
	static char s_ArrayOfIndex[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	hex_str += s_ArrayOfIndex[ ( xchar >> 4 ) ];
	hex_str += s_ArrayOfIndex[ ( xchar & 0x0f ) ];
}

static void __attribute__ ( ( constructor ) )
init_rand ( void )
{
	srand ( ( unsigned ) time ( 0 ) );
}

#endif   /* ----- #ifndef nsfc_INC  ----- */
