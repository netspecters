#ifndef  nslib_INC
#define  nslib_INC

#include <utility>
#include "../NSErrorCode.h"
#include "../NSDefine.h"

#include "LibraryMaco.hpp"	/* 使用到的宏定义 */
#include "OSFunc.hpp"
#include "convert_utf_unicode.hpp"
#include "NSFC.h"
#include "pipe.hpp"
#include "Alloctor.hpp"
#include "std_thread_extend.hpp"

#include "ContainerLock.hpp"
#include "LibObject/ObjectPool.hpp"
#include "LibObject/ThreadedList.hpp"
#include "LibObject/SimpleBitset.hpp"
#include "LibObject/SimpleArray.hpp"
#include "LibObject/DynArray"
#include "LibObject/ReferenceInterface.hpp"
#include "LibObject/MappedFile.hpp"

///无效句柄定义
#define	NS_INVALID_HANDLE	0

#endif   /* ----- #ifndef nslib_INC  ----- */
