#ifndef  wrrm_INC
#define  wrrm_INC

#include <unordered_set>
#include <StdLib.h>

namespace Netspecters{ namespace NSStdLib{

#define CAS(val_ptr, compare_val, new_val) __sync_bool_compare_and_swap(val_ptr, compare_val, new_val)

//Hazard pointer record basic implement
template <class TCSData>
class CHazardPointerPool_BASE
{
	public:
		class CHazardPointer
		{
			private:
				CHazardPointer(bool is_active, const TCSData *&cs_data_ptr)
					: m_is_active(is_active)
				{
					sync_set_data_ptr(cs_data_ptr);
				}
				void sync_set_data_ptr(const TCSData *&cs_data_ptr)
				{
					do
					{
						m_data_ptr = old_ptr = cs_data_ptr;
					}while(cs_data_ptr != old_ptr);
				}
				friend class CHazardPointerPool_BASE;

				CHazardPointer *m_next_hazard_pointer_ptr;
				TCSData *m_data_ptr;
				bool m_is_active;
		}

		~CHazardPointerPool(void)
		{
			/* delete all hazard-pointer instance */
			CHazardPointer *current_hp_ptr = m_pool_header_ptr;
			while(current_hp_ptr)
			{
				CHazardPointer *next_hp_ptr = current_hp_ptr->m_next_hazard_pointer_ptr;
				delete current_hp_ptr;
				current_hp_ptr = next_hp_ptr;
			}
		}

		CHazardPointer *acquire(const TCSData *&cs_data_ptr)
		{
			for(CHazardPointer *found_one_ptr = m_pool_header_ptr; found_one_ptr; found_one_ptr = found_one_ptr->m_next_hazard_pointer_ptr)
			{
				if(found_one_ptr->m_is_active || !CAS(&found_one_ptr->m_is_active, false, true) )
					continue;

				/* we got it */
				found_one_ptr->sync_set_data_ptr(cs_data_ptr);
				return found_one_ptr;
			}

			//alloc new one
			CHazardPointer *new_hp_ptr = new CHazardPointer(true, cs_data_ptr);

			/* push it onto the front of pool */
			CHazardPointer *old_header_ptr;
			do
			{
				new_hp_ptr->m_next_hazard_pointer_ptr = old_header_ptr = m_pool_header_ptr;
			}while(!CAS( &m_pool_header_ptr, old_header_ptr, new_hp_ptr ));

			return new_hp_ptr;
		}

		void release(CHazardPointer *hazard_pointer_ptr)
		{
			hazard_pointer_ptr->m_is_active = false;
			hazard_pointer_ptr->m_data_ptr = NULL;
		}

	protected:
		CHazardPointer	*m_pool_header_ptr;
};

template <class TCSData, bool EnableExternalScan = true, unsigned int ArrayItemNumber = 0>
class CHazardPointerPool : public CHazardPointerPool_BASE<TCSData>
{
	public:
		class scope_read
		{
			public:
				scope_read(const CHazardPointerPool &hp_pool, TCSData *&cs_data_ptr)
					: m_hp_pool(hp_pool)
				{
					m_hazard_pointer_ptr = hp_pool.acquire(cs_data_ptr);
				}
				~scope_read(void)
				{
					m_hp_pool.release(m_hazard_pointer_ptr);
				}
			private:
				CHazardPointer *m_hazard_pointer_ptr;
				const CHazardPointerPool &m_hp_pool;
		}

		void hasBeenReplaced(const TCSData *cs_data_ptr)
		{
			m_retired_vector.push_back(cs_data_ptr);
			if(m_retired_vector.size() > x )
				scan();
		}

		size_t scanImmediately(void)
		{
			if(EnableExternalScan)
				scan();
			return m_retired_vector.size();
		}

	protected:
		void scan(void)
		{
			/* Now let us scan the hazard-pointer pool and collect all the non-null m_data_ptr */
			std::unordered_multiset<TCSData*> m_collection_set;
			CHazardPointer *current_hp_ptr = m_pool_header_ptr;
			while(current_hp_ptr)
			{
				if(current_hp_ptr->m_is_active)
					m_collection_set.insert(current_hp_ptr->m_data_ptr);
				current_hp_ptr = current_hp_ptr->m_next_hazard_pointer_ptr;
			}

			/* search all the unused object */
			for(auto itr = m_retired_vector.begin(); itr != m_retired_vector.end(); ++itr)
			{
				if( m_collection_set.find(itr) == m_collection_set.end() )
					delete *itr;

				/* move itr's value to the end of vector, then popup the last item */
				if(&*itr != &m_retired_vector.back())
					*itr = m_retired_vector.back();
				m_retired_vector.pop_back();
			}
		}
		__builtin_choose_expr(ArrayItemNumber==0, std::vector<TCSData*>,SimpleArray<TCSData*>) m_retired_vector;
}

/* This class is not safe enought!! Designed only for netspecters' netlayer library!! */
template <class TCSData>
class CWRRMObjectPool : public CHazardPointerPool_BASE<TCSData>
{
	public:
		CSingleWRRMObject(const TCSData *single_object_ptr)
			: m_single_object_ptr(single_object_ptr)
		{
			;
		}
		bool collectGarbage(void)
		{
			bool enable_free;
			CHazardPointer *current_hp_ptr = m_pool_header_ptr;
			while(current_hp_ptr)
			{
				if( !(enable_free = !current_hp_ptr->m_is_active) )
					break;
				current_hp_ptr = current_hp_ptr->m_next_hazard_pointer_ptr;
			}

			if(enable_free)
			{
				delete m_single_object_ptr;
			}

			return enable_free;
		}
	private:
		const TCSData *m_single_object_ptr;
};

} /** NSStdLib **/ } /** Netspecters **/

#endif   /* ----- #ifndef wrrm_INC  ----- */
