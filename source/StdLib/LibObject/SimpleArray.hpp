/*
	不要直接引用本单元
*/

#include <utility>
#include "../../NSErrorCode.h"
#include "../../NSDefine.h"

namespace Netspecters{ namespace NSStdLib{

template<typename TItem>
class CSimpleArray
{
	public:
		typedef TItem *TItemPointer;
		typedef CSimpleArray<TItem> self_type;
		class iterator
		{
			public:
				iterator(self_type *simple_array_ptr, TItemPointer current_item_ptr) : m_simple_array_ptr(simple_array_ptr), m_current_item_ptr(current_item_ptr) {}
				iterator(void) = default;
				inline iterator &operator++(void)
				{
					if ((m_current_item_ptr) &&
						(++m_current_item_ptr >= (m_simple_array_ptr->m_item_array_ptr + m_simple_array_ptr->m_size)))
						m_current_item_ptr = NULL;
					return *this;
				}
				inline const iterator operator++(int)
				{
					const iterator old_itr = *this;
					operator++();
					return std::move(old_itr);
				}
				inline bool operator != (const iterator &iter) const
				{
					return (m_current_item_ptr != iter.m_current_item_ptr) || (m_simple_array_ptr != iter.m_simple_array_ptr);
				}
				inline TItem &operator *(void) { return *m_current_item_ptr; }
				inline TItem *operator ->(void) { return m_current_item_ptr; }
				inline operator TItem*(void) { return m_current_item_ptr; }

			private:
				self_type *m_simple_array_ptr;
				TItemPointer m_current_item_ptr;
		};

		CSimpleArray( void )
			: m_size(0), m_capacity( 0 ), m_item_array_ptr(NULL)
		{}
		CSimpleArray( const CSimpleArray & simple_array )
		{
			m_size = simple_array.m_size;
			m_capacity = simple_array.m_capacity;
			m_item_array_ptr = (TItemPointer)malloc(m_capacity * sizeof(TItem));
			for(decltype(m_size) i=0; i<m_size; i++)
				m_item_array_ptr[i] = simple_array.m_item_array_ptr[i];
		}
		CSimpleArray( CSimpleArray && simple_array )
			: m_item_array_ptr(NULL)
		{
			m_size = simple_array.m_size;
			m_capacity = simple_array.m_capacity;
			std::swap(m_item_array_ptr, simple_array.m_item_array_ptr);
		}
		~CSimpleArray(void)
		{
			if( m_item_array_ptr )
				free(m_item_array_ptr);
		}
		TItem &operator[]( size_t index )
		{
			return m_item_array_ptr[ index ];
		}
		void push_back( const TItem &item )
		{
			if( m_size >= m_capacity)
			{
				if( m_item_array_ptr==NULL )
				{
					m_capacity = 8;
					m_item_array_ptr = (TItemPointer)malloc(m_capacity * sizeof(TItem));
				}
				else
				{
					m_capacity += m_capacity;
					m_item_array_ptr = (TItemPointer)realloc(m_item_array_ptr, m_capacity * sizeof(TItem));
				}
			}
			m_item_array_ptr[m_size++] = item;
		}
		size_t size(void) { return m_size; }
		bool empty(void) { return m_size == 0; }
		size_t capacity(void) { return m_capacity; }
		void clear(void) { m_size = 0; }
		iterator begin(void) { return iterator(this, m_item_array_ptr); }
		iterator end(void) { return iterator(this, NULL); }

	private:
		size_t m_size;
		size_t m_capacity;
		TItemPointer m_item_array_ptr;
		friend class iterator;
};

} /* NSStdLib **/ } /* Netspecters **/
