/*
	不要直接引用本单元
*/

#include <stdint.h>
#include <stdarg.h>
#include <mutex>

namespace Netspecters{ namespace NSStdLib{

/**
 * @brief 线程安全列表类
 * @note TItem 必须有零参数的兼容构造函数
*/

template < typename TItem >
class CThreadedList
{
	private:
		struct ST_List_Item
		{
			ST_List_Item *previous_item_ptr, *next_item_ptr;
			TItem item_value;
		};
		typedef CThreadedList<TItem> self_type;

	public:
		typedef TItem *TItemPointer;
		typedef TItem item_type;

		class iterator
		{
			public:
				iterator() : m_list_ptr(NULL), m_item_ptr(NULL) {}
				iterator( self_type *list_ptr, ST_List_Item *item_ptr ):m_list_ptr(list_ptr), m_item_ptr(item_ptr) {}
				inline iterator &operator++()
				{
					m_item_ptr = m_item_ptr->next_item_ptr;
					return *this;
				}
				inline const iterator operator++(int)
				{
					const iterator old_itr = *this;
					operator++();
					return std::move(old_itr);
				}
				inline iterator &operator--()
				{
					m_item_ptr = m_item_ptr->previous_item_ptr ;
					return *this;
				}
				inline const iterator operator--(int)
				{
					const iterator old_itr = *this;
					operator--();
					return std::move(old_itr);
				}
				inline bool operator != (const iterator &iter) const
				{
					return (m_list_ptr != iter.m_list_ptr) || (m_item_ptr != iter.m_item_ptr);
				}
				inline TItem &operator *() { return m_item_ptr->item_value; }
				inline TItem *operator ->() { return &m_item_ptr->item_value; }
				inline operator TItem*() { return &m_item_ptr->item_value; }

			private:
				friend class CThreadedList;
				self_type *m_list_ptr;
				ST_List_Item *m_item_ptr;
		};

	public:
		CThreadedList()
			:m_item_count(0)
		{
			m_item_list_header.previous_item_ptr = NULL;
			m_item_list_header.next_item_ptr = &m_item_list_tail;
			m_item_list_tail.previous_item_ptr = &m_item_list_header;
			m_item_list_tail.next_item_ptr = NULL;
		}
		~CThreadedList()
		{
			if(empty()) return;

			ST_List_Item *current_item_ptr = m_item_list_header.next_item_ptr;
			while(current_item_ptr != &m_item_list_tail)
			{
				ST_List_Item *tmp_ptr = current_item_ptr->next_item_ptr;
				delete current_item_ptr;
				current_item_ptr = tmp_ptr;
			}
		}

		/**
		 * @brief 分配一个列表项目单元
		 * @param auto_create 是否自动调用TItem的构造函数
		 * @return 新列表项目的迭代器
		 * @note 调用的TItem构造函数是无参构造函数
		*/
		iterator allocItem(bool auto_create = true)
		{
			ST_List_Item *new_item_ptr;
			if(auto_create)
				new_item_ptr = new ST_List_Item();
			else
			{
				char *item_space_ptr = new char[sizeof(ST_List_Item)];
				new_item_ptr = reinterpret_cast<ST_List_Item*>(item_space_ptr);
			}
			new_item_ptr->previous_item_ptr = new_item_ptr->next_item_ptr = NULL;
			return iterator(this, new_item_ptr);
		}

		std::mutex &getListLocker()
		{
			return m_mutex;
		}

		/**
		 * @name 迭代器操作函数
		 * @{
		*/
		//插入操作应该使用本函数，保证线程安全
		/**
		 * @brief 将迭代器指示的列表项目插入到列表中的指定位置
		 * @param previous_itr 迭代器插入位置（新的迭代器将插入到此迭代器后面）
		 * @param need_insert_item_itr 需要插入的迭代器
		 * @return 释放插入成功
		 * @see allocItem push_back
		*/
		bool insertIntoList( iterator &previous_itr, iterator &need_insert_item_itr )
		{
			std::lock_guard<std::mutex> locker(m_mutex);

			need_insert_item_itr.m_item_ptr->next_item_ptr = previous_itr.m_item_ptr->next_item_ptr;
			need_insert_item_itr.m_item_ptr->previous_item_ptr = previous_itr.m_item_ptr;

			previous_itr.m_item_ptr->next_item_ptr->previous_item_ptr = need_insert_item_itr.m_item_ptr;
			previous_itr.m_item_ptr->next_item_ptr = need_insert_item_itr.m_item_ptr;

			m_item_count++;
			return true;
		}
		/**
		 * @brief 将迭代器指示的列表项目放入列表最后
		 * @param item_itr 需要添加的迭代器
		 * @return 是否添加成功
		 * @see allocItem insertIntoList
		*/
		bool push_back( iterator item_itr )
		{
			auto itr = iterator(this, m_item_list_tail.previous_item_ptr);
			return insertIntoList( itr, item_itr );
		}
		bool push_back( const TItem &item )
		{
			iterator new_itr = allocItem();
			*new_itr = item;
			return push_back( new_itr );
		}
		/**
		 * @brief 包裹一个列表项目
		 * @return 包裹后的列表项目对于的迭代器
		 */
		iterator wrap( TItem &item )
		{
			ST_List_Item *item_ptr = (ST_List_Item*)(((uintptr_t)&item) - sizeof(ST_List_Item*)*2);
			return iterator( this, item_ptr );
		}
		/**
		 * @brief 删除一个迭代器
		*/
		void erase( iterator item_itr )
		{
			std::lock_guard<std::mutex> locker(m_mutex);

			if(item_itr.m_item_ptr->previous_item_ptr && item_itr.m_item_ptr->next_item_ptr)
			{
				item_itr.m_item_ptr->previous_item_ptr->next_item_ptr = item_itr.m_item_ptr->next_item_ptr;
				item_itr.m_item_ptr->next_item_ptr->previous_item_ptr = item_itr.m_item_ptr->previous_item_ptr;
				m_item_count--;
			}
			delete item_itr.m_item_ptr;
		}
		///@}

		/**
		 * @name 位置函数
		 * @{
		*/
		TItemPointer front() { return m_item_list_header.next_item_ptr == &m_item_list_tail ? NULL : &m_item_list_header.next_item_ptr->item_value; }
		TItemPointer back() { return m_item_list_header.next_item_ptr == &m_item_list_tail ? NULL : &m_item_list_tail.previous_item_ptr->item_value; }
		iterator begin() { return iterator(this, m_item_list_header.next_item_ptr); }
		iterator end() { return iterator(this, &m_item_list_tail); }
		///@}

		size_t size() { return m_item_count; }
		bool empty() { return m_item_count == 0; }

	private:
		std::mutex m_mutex;
		size_t m_item_count;
		ST_List_Item m_item_list_header, m_item_list_tail;
};

} /* NSStdLib **/ } /* Netspecters **/
