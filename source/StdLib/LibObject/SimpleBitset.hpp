/*
	不要直接引用本单元
*/

#include <utility>
#include "../OSFunc.hpp"

namespace Netspecters{ namespace NSStdLib{

template<size_t N>
class CSimpleBitset
{
	private:
		static const uint8_t C_BITS_PER_ITEM = 32;
		static const uint8_t C_LESS_BITS_PER_ITEM = 31;
		static const size_t C_ITEM_NUM = (N + C_LESS_BITS_PER_ITEM ) / C_BITS_PER_ITEM;
		static const size_t C_SPACE_SIZE = C_ITEM_NUM * sizeof(uint32_t);

	public:
		CSimpleBitset(void)
		{
			clear();
		}
		void assign( uint8_t *src_ptr )
		{
			memcpy( m_bits_space, src_ptr, C_SPACE_SIZE );
		}
		void dump( uint8_t *dst_ptr )
		{
			memcpy( dst_ptr, m_bits_space, C_SPACE_SIZE );
		}

		static const size_t size = N;
		static const size_t byte_num = C_SPACE_SIZE;

		#define	GET_BIT_BYTE(x) (x/C_BITS_PER_ITEM)
		void __fastcall  set(size_t index)
		{
			m_bits_space[ GET_BIT_BYTE(index) ] |= C_BIT_MASK >> ( index & C_LESS_BITS_PER_ITEM );
		}
		void __fastcall reset(size_t index)
		{
			m_bits_space[ GET_BIT_BYTE(index) ] &= C_BIT_FULL - ( C_BIT_MASK >> ( index & C_LESS_BITS_PER_ITEM ) );
		}
		void __fastcall clear(void)
		{
			memset( m_bits_space, 0, C_SPACE_SIZE );
		}
		bool __fastcall test(size_t index)
		{
			return m_bits_space[ GET_BIT_BYTE(index) ] & (C_BIT_MASK>>(index & C_LESS_BITS_PER_ITEM));
		}
		//置1的位计数
		size_t count(void)
		{
			size_t true_count = 0;
			for( size_t i=0; i<C_ITEM_NUM; i++)
				if(m_bits_space[i])	//使用分治法统计1的个数
				{
					uint32_t item = m_bits_space[i];
					item -= ((item >> 1) & 0x55555555);
					item = (((item >> 2) & 0x33333333) + (item & 0x33333333));
					item = (((item >> 4) + item) & 0x0f0f0f0f);
					item += (item >> 8);
					item += (item >> 16);
					true_count += (item & 0x0000003f);	//只有低6位存储了正确的计数
				}
			return true_count;
		}
		size_t first_false(void)
		{
			size_t position = 0;
			for( size_t i=0; i<C_ITEM_NUM; i++)
				if(m_bits_space[i] != C_BIT_FULL)
				{
					uint32_t value = m_bits_space[i];
					uint32_t mask = C_BIT_MASK;
					for( size_t j=0; j<C_BITS_PER_ITEM; j++)
					{
						if( (value & mask) == 0 )
							break;
						position++;
						mask >>= 1;
					}
					break;
				}
				else
					position += C_BITS_PER_ITEM;
			return position;
		}
		size_t last_true(void)
		{
			size_t position = C_SPACE_SIZE * 8 - 1;
			for( int i=C_ITEM_NUM-1; i>=0; i-- )
				if(m_bits_space[i] != C_BIT_EMPTY)
				{
					uint32_t value = m_bits_space[i];
					uint32_t mask = 0x00000001;
					for( int j=C_BITS_PER_ITEM-1; j>=0; j--)
					{
						if( (value & mask) != 0 )
							break;
						position--;
						mask <<= 1;
					}
					break;
				}
				else
					position -= C_BITS_PER_ITEM;
			return position;
		}
		#undef GET_BIT_BYTE

	private:
		static const uint32_t C_BIT_MASK = 0x80000000;
		static const uint32_t C_BIT_FULL = 0xffffffff;
		static const uint32_t C_BIT_EMPTY = 0x00000000;
		uint32_t m_bits_space[C_ITEM_NUM];
};

} /* NSStdLib **/ } /* Netspecters **/
