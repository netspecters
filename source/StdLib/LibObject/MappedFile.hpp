#ifndef MAPPEDFILE_HPP_INCLUDED
#define MAPPEDFILE_HPP_INCLUDED

#include <string>
#include "../convert_utf_unicode.hpp"
#include "../OSFunc.hpp"

namespace Netspecters{ namespace NSStdLib{

using std::string;

#ifndef ALWAYS_CREATE_FILE
#	define ALWAYS_CREATE_FILE false	//[调试用]总是创建配置文件
#endif

class CMappedFile
{
	public:
		enum ENM_FileState
		{
			fsFail, fsOpenNew, fsOpenExist
		};

		CMappedFile(): m_file_mem_ptr(NULL) {}
		CMappedFile(const string &file_name, size_t default_file_size = 0)
			: m_file_mem_ptr(NULL)
		{
			loadFile(file_name, default_file_size);
		}
		~CMappedFile()
		{
			if(m_file_mem_ptr)
				unloadFile();
		}

		/**
		 * @brief 装载文件
		 * @return 返回fsFail表示失败，返回fsOpenNew表示新建文件，返回fsOpenExist表示打开已有文件
		*/
		ENM_FileState loadFile(const string &file_name, size_t default_file_size)
		{
			bool file_need_init;

#if defined(_WIN32) || defined(_WIN64)
			/* utf-8 to unicode */
			DECL_UTF8_UNICODE(file_name.c_str(), file_name_unicode);

			m_file_handle = CreateFileW(file_name_unicode,
										   GENERIC_READ | GENERIC_WRITE,
										   FILE_SHARE_READ,
										   NULL,
										   ALWAYS_CREATE_FILE ? CREATE_ALWAYS : OPEN_ALWAYS,
										   FILE_ATTRIBUTE_NORMAL, 0);
			if(m_file_handle == INVALID_HANDLE_VALUE)
			{
				return fsFail;
			}

			file_need_init = ALWAYS_CREATE_FILE ? true : GetLastError() != ERROR_ALREADY_EXISTS;
			default_file_size = file_need_init ? : GetFileSize(m_file_handle, NULL);

			m_file_mem_ptr = OSFunc::mapFile(m_file_handle, 0, default_file_size);
			if(!m_file_mem_ptr)
			{
				OSFunc::closeFile(m_file_handle);
				goto load_faild;
			}
#else
			m_file_handle = OSFunc::openFile(file_name.c_str(), ALWAYS_CREATE_FILE);
			if(m_file_handle < 0)
				goto load_faild;

			struct stat file_status_struct;
			fstat(m_file_handle, &file_status_struct);
			file_need_init = ALWAYS_CREATE_FILE ? true : 0 == file_status_struct.st_size;
			if(file_need_init)
			{
				if(lseek(m_file_handle, default_file_size, SEEK_SET) == -1)
					goto load_faild;
				lseek(m_file_handle, -1, SEEK_END);
				if(write(m_file_handle, &default_file_size, 1) == -1)
					goto load_faild;
			}
			else
				default_file_size = file_status_struct.st_size;

			m_file_mem_ptr = OSFunc::mapFile(m_file_handle, 0, default_file_size);
			if(MAP_FAILED == m_file_mem_ptr)
			{
				OSFunc::closeFile(m_file_handle);
				m_file_mem_ptr = NULL;
				return fsFail;
			}

			return file_need_init ? fsOpenNew : fsOpenExist;
#endif
load_faild:
			m_file_mem_ptr = NULL;
			return fsFail;
		}

		void unloadFile(void)
		{
			int unmap_size = 0;
#ifdef __linux__
			struct stat file_status_struct;
			fstat(m_file_handle, &file_status_struct);
			unmap_size = file_status_struct.st_size;
#endif
			OSFunc::unmapFile(m_file_mem_ptr, unmap_size);
			OSFunc::closeFile(m_file_handle);

			m_file_mem_ptr = NULL;
		}

		operator bool()
		{
			return m_file_mem_ptr != NULL;
		}

		/**
		 * @return 返回-1表示增加文件大小成功，但是重新映射失败，此时m_file_mem_ptr将不可用(一般不发生);返回0表示尝试增加文件大小失败
		*/
		off_t increaseFileSize(int inc_size)
		{
#if defined(_WIN32) || defined(_WIN64)
			if(m_file_mem_ptr)
				UnmapViewOfFile(m_file_mem_ptr);

			size_t old_file_size =  GetFileSize(m_file_handle, NULL), new_file_size = old_file_size + inc_size;
			HANDLE map_file_handle = CreateFileMapping(m_file_handle, NULL, PAGE_READWRITE, 0, new_file_size, NULL);
			m_file_mem_ptr = MapViewOfFile(map_file_handle, FILE_MAP_ALL_ACCESS, 0, 0, 0);
			CloseHandle(map_file_handle);
#else
			struct stat file_status_struct;
			fstat(m_file_handle, &file_status_struct);
			size_t old_file_size = file_status_struct.st_size, new_file_size = old_file_size + inc_size;

			/* 调整物理文件大小 */
			msync(m_file_mem_ptr, old_file_size, MS_SYNC);
			if(lseek(m_file_handle, new_file_size, SEEK_SET) < 0)
				return 0;	//尝试增加文件大小失败
			lseek(m_file_handle, -1, SEEK_END);
			if(write(m_file_handle, &new_file_size, 1) == -1)
				return 0;
			write(m_file_handle, &new_file_size, 1);

#if defined(__linux)
			void *new_mem_ptr = mremap(m_file_mem_ptr, old_file_size + 1, new_file_size + 1, MREMAP_MAYMOVE);
			if(new_mem_ptr == MAP_FAILED)
				return 0;
#	else
			munmap(m_file_mem_ptr, old_file_size);
			if((m_file_mem_ptr = mmap(NULL, new_file_size, PROT_READ | PORT_WRITE, MAP_SHARED, m_file_handle, 0)) == MAP_FAILED)
				return -1;	//增加文件大小成功，但是重新映射失败，m_file_mem_ptr将不可用(一般不肯能发生)
#	endif
#endif
			return new_file_size;
		}

		off_t getFileSize(void)
		{
#if defined(_WIN32) || defined(_WIN64)
			return GetFileSize(m_file_handle, NULL);
#else
			struct stat file_status_struct;
			fstat(m_file_handle, &file_status_struct);
			return file_status_struct.st_size;
#endif
		}

	protected:
		void *m_file_mem_ptr;	//基本文件指针
		OSFunc::native_handle_type m_file_handle;
};

#undef ALWAYS_CREATE_FILE

} /* NetLayer **/} /* NSStdLib **/

#endif // MAPPEDFILE_HPP_INCLUDED
