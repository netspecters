#ifndef REFERENCEINTERFACE_HPP_INCLUDED
#define REFERENCEINTERFACE_HPP_INCLUDED

#include <atomic>

namespace Netspecters{ namespace NSStdLib{

/**
	基于引用计数的基类
	@note Netspecters中的所有基于引用计数设计的类都要从本类继承
	@note 引用计数类不允许拷贝
*/
class CReferenceInterface
{
	public:
		/**
			增加引用计数
			@param add_inc 本次调用需要增加的计数值，默认为1
		*/
		void addReference( int add_inc = 1 )
		{
			m_reference_count += add_inc;
		}

		///减少引用计数
		void releaseReference()
		{
			if( --m_reference_count == 0 )
				delete this;
		}
		///说明引用计数是否有效
		operator bool()
		{
			return m_reference_count > 0;
		}

		///类构建函数
		CReferenceInterface() : m_reference_count(1) {}
		CReferenceInterface( const CReferenceInterface & ) = delete;
		CReferenceInterface &operator = ( const CReferenceInterface & ) = delete;

		///类析构函数
		virtual ~CReferenceInterface() {}

	protected:
		std::atomic_int	m_reference_count;
};

} /* NetLayer **/} /* NSStdLib **/

#endif // REFERENCEINTERFACE_HPP_INCLUDED
