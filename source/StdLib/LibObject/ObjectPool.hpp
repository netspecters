/*
	不要直接引用本单元
*/

#include <utility>
#include <stdio.h>#include <mutex>
#include "../OSFunc.hpp"

namespace Netspecters { namespace NSStdLib {

//没有使用基类结构设计，使用的时候要小心!!
template < typename TObject >
class CObjectPool
{
	private:
		typedef CObjectPool<TObject> self_type;
		struct __attribute__ ( ( packed ) ) CFakeObject
		{
			static const uint32_t UNUSED_TAG = 0x55AA55AA;

			CFakeObject *m_next_fake_object_ptr;
			uint32_t m_unused_tag;
			uint8_t m_pendding[ sizeof ( TObject ) - sizeof ( CFakeObject* ) - sizeof ( m_unused_tag ) ];
		};
		struct ST_ObjectPage
		{
			static const int C_ITEM_PER_PAGE = ( OSFunc::OS_MEM_PAGE_SIZE - sizeof ( ST_ObjectPage* ) ) / sizeof ( TObject );

			ST_ObjectPage *m_next_object_page_ptr;		//每页头部都是一个指向下一页的指针
			CFakeObject m_objects[0];
		};
		static_assert ( sizeof ( TObject ) >= ( sizeof ( CFakeObject* ) + sizeof ( CFakeObject::m_unused_tag ) ), "The TObject's size must more than (sizeof(CFakeObject*) + 32)!" );

	public:
		class iterator
		{
			public:
				iterator () : m_object_pool_ptr ( NULL ), m_current_fake_object_index ( 0 ), m_current_object_page_ptr ( NULL ) {}
				iterator ( ST_ObjectPage *current_object_page_ptr, self_type *object_pool_ptr )
					: m_object_pool_ptr ( object_pool_ptr ), m_current_fake_object_index ( -1 ), m_current_object_page_ptr ( current_object_page_ptr )
				{
					if(current_object_page_ptr)
						operator++();
					else
						m_current_fake_object_index = -2;
				}
				inline iterator &operator++ ()
				{
					while ( m_current_fake_object_index >= -1 )
					{
						m_current_fake_object_index++;
						for ( ; m_current_fake_object_index < ST_ObjectPage::C_ITEM_PER_PAGE; m_current_fake_object_index++ )
							if ( m_object_pool_ptr->isObjectInUse ( reinterpret_cast<TObject*> ( &m_current_object_page_ptr->m_objects[m_current_fake_object_index] ) ) )
								break;

						/* 当前页面已经没有正在使用的项目了 */
						if ( m_current_fake_object_index == ST_ObjectPage::C_ITEM_PER_PAGE )
						{
							m_current_object_page_ptr = m_current_object_page_ptr->m_next_object_page_ptr;
							m_current_fake_object_index = m_current_object_page_ptr ? -1 : -2;
						}
						else
							break;
					}
					return *this;
				}
				inline bool operator != ( const iterator &iter ) const
				{
					return m_current_fake_object_index != iter.m_current_fake_object_index;
				}
				inline TObject &operator * ()
				{
					return *reinterpret_cast<TObject*> ( &m_current_object_page_ptr->m_objects[m_current_fake_object_index] );
				}
				inline TObject *operator -> ()
				{
					return reinterpret_cast<TObject*> ( &m_current_object_page_ptr->m_objects[m_current_fake_object_index] );
				}
				inline operator TObject * ()
				{
					return reinterpret_cast<TObject*> ( &m_current_object_page_ptr->m_objects[m_current_fake_object_index] );
				}

			private:
				self_type *m_object_pool_ptr;
				int m_current_fake_object_index;
				ST_ObjectPage *m_current_object_page_ptr;
		};

		CObjectPool () : m_first_object_page_ptr ( NULL ), m_alloc_ptr ( NULL )
		{
			newObjectPage();
		}
		~CObjectPool ()
		{
			while ( m_first_object_page_ptr )
			{
				ST_ObjectPage *next_object_page_ptr = m_first_object_page_ptr->m_next_object_page_ptr;
				OSFunc::freeMemPage ( m_first_object_page_ptr );
				m_first_object_page_ptr = next_object_page_ptr;
			}
		}
		bool isValidObjectPtr ( const TObject *object_ptr )
		{
			if ( object_ptr )
			{				std::lock_guard<std::recursive_mutex> locker( m_locker );

				const unsigned long object_page_address = reinterpret_cast<const unsigned long> ( OSFunc::getPageBaseAddress ( reinterpret_cast<const void*> ( object_ptr ) ) );
				ST_ObjectPage *current_object_page_ptr = m_first_object_page_ptr;
				while ( current_object_page_ptr )
				{
					if ( object_page_address ==  reinterpret_cast<unsigned long> ( current_object_page_ptr ) )
						return true;
					current_object_page_ptr = current_object_page_ptr->m_next_object_page_ptr;
				}
			}
			return false;
		}
		/* alloc不会自动调用构造函数 */
		TObject __fastcall *allocObject ()
		{
			std::lock_guard<std::recursive_mutex> locker( m_locker );

			if ( m_alloc_ptr == NULL ) //已经是最后一个元素，开辟新的空间
			{
				newObjectPage();
				if ( m_alloc_ptr == NULL )
					return NULL;
			}

			TObject *new_object_ptr = reinterpret_cast<TObject*> ( m_alloc_ptr );
			m_alloc_ptr = m_alloc_ptr->m_next_fake_object_ptr;

			/* 为了安全要设定fake object数据成员 */
			CFakeObject* fake_obj_ptr = reinterpret_cast<CFakeObject*> ( new_object_ptr );
			fake_obj_ptr->m_next_fake_object_ptr = NULL;
			fake_obj_ptr->m_unused_tag = 0;

			return new_object_ptr;
		}
		void __fastcall freeObject ( TObject *object_ptr )
		{
			std::lock_guard<std::recursive_mutex> locker( m_locker );

			CFakeObject *fake_obj_ptr = reinterpret_cast<CFakeObject*> ( object_ptr );
			fake_obj_ptr->m_unused_tag = CFakeObject::UNUSED_TAG;
			fake_obj_ptr->m_next_fake_object_ptr = m_alloc_ptr;
			m_alloc_ptr = reinterpret_cast<CFakeObject*> ( object_ptr );
		}
		std::recursive_mutex&getPoolLocker ()
		{
			return m_locker;
		}

		/* 返回第一个已经分配的object */
		iterator begin ()
		{
			return iterator ( m_first_object_page_ptr, this );
		}
		iterator end ()
		{
			return iterator ( NULL, this );
		}

	private:
		bool isObjectInUse ( const TObject *object_ptr )	//内部使用的函数，主要保证object_ptr有效
		{
			return reinterpret_cast<const CFakeObject*> ( object_ptr )->m_unused_tag != CFakeObject::UNUSED_TAG;
		}
		void newObjectPage ()				//newPage调用完毕后会准备好m_alloc_ptr指针供分配使用
		{
			//分配一页空间
			ST_ObjectPage *new_object_page_ptr = static_cast<ST_ObjectPage*> ( OSFunc::getMemPage() );

			//设置当前分配指针
			m_alloc_ptr =  new_object_page_ptr->m_objects;

			//初始化数据空间
			CFakeObject *current_fake_object_ptr = new_object_page_ptr->m_objects;
			for ( size_t count = 0; count < ST_ObjectPage::C_ITEM_PER_PAGE - 1; count++ )
			{
				CFakeObject *next_fake_object_ptr = current_fake_object_ptr + 1;
				current_fake_object_ptr->m_next_fake_object_ptr = next_fake_object_ptr;
				current_fake_object_ptr->m_unused_tag = CFakeObject::UNUSED_TAG;
				current_fake_object_ptr = next_fake_object_ptr;
			}
			current_fake_object_ptr->m_next_fake_object_ptr = NULL;

			//将新分配的页面挂到链表中
			new_object_page_ptr->m_next_object_page_ptr = m_first_object_page_ptr;
			m_first_object_page_ptr = new_object_page_ptr;
		}

		ST_ObjectPage *m_first_object_page_ptr;
		CFakeObject *m_alloc_ptr;
		std::recursive_mutex m_locker;
};

} /* NSStdLib **/ } /* Netspecters **/
