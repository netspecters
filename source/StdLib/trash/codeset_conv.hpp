#ifndef ICONV_HPP_INCLUDED
#define ICONV_HPP_INCLUDED

#include <string.h>

#define UTF8_MAX_BYTES_PER_CHAR 6
#define UNICODE_MAX_BYTES_PER_CHAR 4

static int unicode_utf8_getlength ( const wchar_t *from_unicode_str_ptr )
{
	int utf8_str_len = 0;
	while ( *from_unicode_str_ptr )
	{
		wchar_t w_char = *from_unicode_str_ptr;
		if ( w_char < 0x80 )
		{
			// utf char size is 1
			utf8_str_len++;
		}
		else if ( w_char < 0x800 )
		{
			// utf char size is 2
			utf8_str_len += 2;
		}
		else if ( w_char < 0x10000 )
		{
			// utf char size is 3
			utf8_str_len += 3;
		}
		else if ( w_char < 0x200000 )
		{
			// utf char size is 4
			from_unicode_str_ptr++;
			utf8_str_len += 4;
		}
		else if ( w_char < 0x4000000 )
		{
			// utf char size is 5
			from_unicode_str_ptr++;
			utf8_str_len += 5;
		}
		else
		{
			// utf char size is 6
			from_unicode_str_ptr++;
			utf8_str_len += 6;
		}
		++from_unicode_str_ptr;
	}

	return ++utf8_str_len;
}

static __attribute__((unused)) int unicode_utf8 ( const wchar_t *from_unicode_str_ptr, char *to_utf8_str_ptr )
{
	if ( !from_unicode_str_ptr )
		return 0;
	if ( !to_utf8_str_ptr )
		return unicode_utf8_getlength ( from_unicode_str_ptr );

	int utf8_str_len = 0;
	while ( *from_unicode_str_ptr )
	{
		wchar_t w_char = *from_unicode_str_ptr;
		if ( w_char < 0x80 )
		{
			// utf char size is 1
			*to_utf8_str_ptr++ = ( char ) ( w_char );
			utf8_str_len++;
		}
		else if ( w_char < 0x800 )
		{
			// utf char size is 2
			*to_utf8_str_ptr++ = 0xc0 | ( w_char >> 6 );
			*to_utf8_str_ptr++ = 0x80 | ( w_char & 0x3f );
			utf8_str_len += 2;
		}
		else if ( w_char < 0x10000 )
		{
			// utf char size is 3
			*to_utf8_str_ptr++ = 0xe0 | ( w_char >> 12 );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 6 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( w_char & 0x3f );
			utf8_str_len += 3;
		}
		else if ( w_char < 0x200000 )
		{
			// utf char size is 4
			*to_utf8_str_ptr++ = 0xf0 | ( static_cast<int> ( w_char ) >> 18 );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 12 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 6 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( w_char & 0x3f );
			from_unicode_str_ptr++;
			utf8_str_len += 4;
		}
		else if ( w_char < 0x4000000 )
		{
			// utf char size is 5
			*to_utf8_str_ptr++ = 0xf8 | ( static_cast<int> ( w_char ) >> 24 );
			*to_utf8_str_ptr++ = 0x80 | ( ( static_cast<int> ( w_char ) >> 18 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 12 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 6 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( w_char & 0x3f );
			from_unicode_str_ptr++;
			utf8_str_len += 5;
		}
		else
		{
			// utf char size is 6
			*to_utf8_str_ptr++ = 0xfc | ( static_cast<int> ( w_char ) >> 30 );
			*to_utf8_str_ptr++ = 0x80 | ( ( static_cast<int> ( w_char ) >> 24 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( ( static_cast<int> ( w_char ) >> 18 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 12 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( ( w_char >> 6 ) & 0x3f );
			*to_utf8_str_ptr++ = 0x80 | ( w_char & 0x3f );
			from_unicode_str_ptr++;
			utf8_str_len += 6;
		}
		from_unicode_str_ptr++;
	}

	if ( utf8_str_len > 0 )
	{
		*to_utf8_str_ptr = '\0';
		utf8_str_len++;
	}


	return utf8_str_len;
}

static const unsigned char trailingBytesForUTF8[256] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5
};

static __attribute__((unused)) int utf8_unicode_getlength ( const char *from_utf8_str_ptr )
{
	int unicode_str_len = 0;

	while ( *from_utf8_str_ptr )
	{
		unsigned char first_char = *from_utf8_str_ptr++;
		from_utf8_str_ptr += trailingBytesForUTF8[first_char];
		unicode_str_len++;
	}
	return ++unicode_str_len;
}

static const uint32_t offsetsFromUTF8[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL,
										0x03C82080UL, 0xFA082080UL, 0x82082080UL
										};

static __attribute__((unused)) int utf8_unicode ( const char *from_utf8_str_ptr, wchar_t *to_unicode_str_ptr )
{
	int unicode_str_len = 0;

	while ( *from_utf8_str_ptr )
	{
		wchar_t unicode = 0;
		unsigned char first_char = *from_utf8_str_ptr++;
		unsigned short extra_bytes_to_read = trailingBytesForUTF8[first_char];
		switch ( extra_bytes_to_read )
		{
			case 3:
				unicode |= *from_utf8_str_ptr++;
				unicode <<= 6;
			case 2:
				unicode |= *from_utf8_str_ptr++;
				unicode <<= 6;
			case 1:
				unicode |= *from_utf8_str_ptr++;
				unicode <<= 6;
			case 0:
				unicode |= *from_utf8_str_ptr++;
		}
		unicode -= offsetsFromUTF8[extra_bytes_to_read];
		*to_unicode_str_ptr++ = unicode;
		unicode_str_len++;
	}

	return ++unicode_str_len;
}

#define DECL_UTF8_UNICODE( utf8_str, unicode_str_name )\
	wchar_t unicode_str_name[ utf8_unicode_getlength( utf8_str ) ];\
	utf8_unicode ( utf8_str, unicode_str_name );\

#define DECL_UNICODE_UTF8( unicode_str, utf8_str_name )\
	char utf8_str_name[ unicode_utf8_getlength( unicode_str ) ];\
	unicode_utf8( unicode_str, utf8_str_name );\

#endif // ICONV_HPP_INCLUDED

