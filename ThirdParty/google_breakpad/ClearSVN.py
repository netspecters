#!/usr/bin/python
import os
import sys
import shutil

def removeSVN(dir_base):
	print(dir_base)
	for root,dirs,files in os.walk(dir_base):
		for dir_name in dirs:
			if dir_name == '.svn' or dir_name == 'testdata' or dir_name == 'third_party'\
					or dir_name == 'testing' or dir_name == 'binaries':
				svn_path =  os.path.join(root, dir_name)
				print("rm unused dir: "+ svn_path)
				shutil.rmtree(svn_path,True)
		for file_name in files:
			if file_name == 'Makefile':
				file_path = os.path.join(root , file_name)
				print("rm Makefile: "+ file_path)
				os.remove(file_path)
			else:
				ext_name = os.path.splitext(file_name)[1]
				if ext_name == '.sh' or ext_name == '.in' or ext_name == '.am' or ext_name == '.ac'\
					or ext_name == '.vcproj' or ext_name == '.pbxproj'\
						or ext_name == '':
					file_path = os.path.join(root , file_name)
					print("rm unused file: "+ file_path)
					os.remove(file_path)

removeSVN( os.path.split(sys.argv[0])[0]+'/src')